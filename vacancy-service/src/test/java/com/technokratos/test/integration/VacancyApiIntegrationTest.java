package com.technokratos.test.integration;


import authorization.service.DeveloperServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.CreateVacancyRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateVacancyRequest;
import com.technokratos.dto.request.VacancyFilter;
import com.technokratos.dto.response.*;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.VacancyRepository;
import hr.service.HrServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.checkerframework.checker.units.qual.C;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/vacancy-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class VacancyApiIntegrationTest extends AbstractIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";

    private static final String VACANCY_PREFIX = "/vacancy";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String HR_TOKEN_FIRST = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTY5MywianRpIjoiMTY4MzM1NTY5Mzg0NyJ9.s3eUwcY0MHsgDFr9zEwXZ4n7ZcGtn2WIwg8Ma0EDRjF_fV66OcLJbQ_cFEqVFeBMX8SsWsQVVqjLtQ6V6Mi-T4gpKRdRSIv3s95X4HUOwih_0zyxmjOsxUdT1nQ1NOjf5jUd4M7G_QMaqD4aakXsVrQLTW8XZPTS8K1RGN6pthfmOUp_hGTW8XbP6_5d01RKnyUsFaxhIORrs-h1ttPQsYL1vAsy87NguUrEn6D94y_SUM9kYwN-rtIpBYP2wJoUV8oSLK-uKHdCzEQPst5YLRYK7eM2EgFOIT0EjzTtGDI4cBRXI7H4mf4YCHTt2_UoR4GbN3xeZP2vRn4ret42mw";

    private static final String HR_TOKEN_SECOND = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTc0MiwianRpIjoiMTY4MzM1NTc0MjA4MSJ9.A-umgLeC6wiajvTFc9dPKlRJ1piyKypnfF_PLd_SShoMl7bZVtELbKiRS8rajzuwnioWMAg7d-7TKn5NAPI42feAlyl7iGM0fwyqXawtGvRI2lrYjYmqRru9s6cfHGshpsRULmV-5fb2j9IAM_2jjq1bimZxi57SOZzsD-49lfy86urlODeQjjZkfyAl_LtlXgs4ZgUD3FIvp4JrEoUbM7PsLCNTHpmFVetbiAdjmBEN0AWmEE_6kniPtxQ6dAalxqpR1wNs0T-8tACA7wFo2AXIlnr6mzMGNYFvELM2bIGtUgTWu149NahWOxRSwVZqs_3vNN3Dz6g964Llp50Evg";

    private static final String ADMIN_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9BRE1JTiIsImV4cCI6MTk5ODkzMTc4OSwianRpIjoiMTY4MzM1NTc4OTA1NyJ9.SrwnQ3nyazSAtG94WT3c40Ijc_ItZwv58Ap2nlhaAdbezjoE-_HB4B-wPWJ0IctG3FKU9d1_RCe6xay9FrIqSersIaNOPf5MktKvxVshka0iJsrbqVoLCpSusR07l0YsyVwXi32w69aOaXMnMvqKQL5Qke5Pmu7Z4dfQnM94klt6QZ0NnvKR9CLwQ0wkDFpQRYQRkqrCUGfqkfDyFG3k3Vb3La287H_VkAVu55Kb1rgM4B1L18Dte78-eVcYhVyod8DUI_645T4UjRakO1jGWvAoy8JIk2z4Ges3EtcwCQ5_OB0U30xsohPjXWgeLzdBUfxJgtm5kIdtrDdIEwwlOg";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Test
    void addVacancySuccessfully() throws Exception {

        CreateVacancyRequest createVacancyRequest = CreateVacancyRequest.builder()
                .name("Java junior developer")
                .description("You work every day")
                .city("Kazan")
                .remote(false)
                .salary(60000L)
                .status(VacancyEntity.Status.NOT_ACTIVE.toString())
                .build();

        VacancyResponse vacancyResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createVacancyRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyEntity savedVacancy = vacancyRepository.getReferenceById(vacancyResponse.getId());
        CompanyEntity companyEntity = companyRepository.getReferenceById(vacancyResponse.getCompanyResponse().getId());

        Assertions.assertNotEquals(null, companyEntity.getVacancies().stream().filter(vacancyEntity -> vacancyEntity.getId().equals(vacancyResponse.getId())).findAny().get());
        Assertions.assertNotNull(savedVacancy.getCompany().getName());
        Assertions.assertNotNull(savedVacancy.getCompany());
        Assertions.assertEquals(createVacancyRequest.getName(), vacancyResponse.getName());
        Assertions.assertEquals(createVacancyRequest.getDescription(), vacancyResponse.getDescription());
        Assertions.assertEquals(createVacancyRequest.getCity(), vacancyResponse.getCity());
        Assertions.assertEquals(createVacancyRequest.getStatus(), vacancyResponse.getStatus());
        Assertions.assertFalse(vacancyResponse.getRemote());
        Assertions.assertNotNull(vacancyResponse.getId());
        Assertions.assertNotNull(vacancyResponse.getCompanyResponse());
        Assertions.assertEquals(createVacancyRequest.getSalary(), vacancyResponse.getSalary());
    }

    @Test
    void addVacancySuccessfullyWithDefaultStatus() throws Exception {

        CreateVacancyRequest createVacancyRequest = CreateVacancyRequest.builder()
                .name("Java junior developer")
                .description("You work every day")
                .city("Kazan")
                .remote(false)
                .salary(60000L)
                .build();

        VacancyResponse vacancyResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createVacancyRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyEntity savedVacancy = vacancyRepository.getReferenceById(vacancyResponse.getId());
        CompanyEntity companyEntity = companyRepository.getReferenceById(vacancyResponse.getCompanyResponse().getId());

        Assertions.assertNotEquals(null, companyEntity.getVacancies().stream().filter(vacancyEntity -> vacancyEntity.getId().equals(vacancyResponse.getId())).findAny().get());
        Assertions.assertNotNull(savedVacancy.getCompany().getName());
        Assertions.assertNotNull(savedVacancy.getCompany());
        Assertions.assertEquals(createVacancyRequest.getName(), vacancyResponse.getName());
        Assertions.assertEquals(createVacancyRequest.getDescription(), vacancyResponse.getDescription());
        Assertions.assertEquals(createVacancyRequest.getCity(), vacancyResponse.getCity());
        Assertions.assertEquals(VacancyEntity.Status.ACTIVE.toString(), vacancyResponse.getStatus());
        Assertions.assertFalse(vacancyResponse.getRemote());
        Assertions.assertNotNull(vacancyResponse.getId());
        Assertions.assertNotNull(vacancyResponse.getCompanyResponse());
        Assertions.assertEquals(createVacancyRequest.getSalary(), vacancyResponse.getSalary());
    }

    @Test
    void updateVacancySuccessfully() throws Exception {

        VacancyEntity vacancyEntity = vacancyRepository.getReferenceById(UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        UpdateVacancyRequest updateVacancyRequest = UpdateVacancyRequest.builder()
                .id(vacancyEntity.getId())
                .name("Senior developer")
                .build();


        VacancyResponse vacancyResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + VACANCY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateVacancyRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyEntity updatedEntity = vacancyRepository.getReferenceById(vacancyEntity.getId());
        Assertions.assertEquals(updateVacancyRequest.getName(), updatedEntity.getName());
        Assertions.assertEquals(vacancyEntity.getDescription(), updatedEntity.getDescription());
    }

    @Test
    void updateVacancyFailedHrCompanyNotMatched() throws Exception {

        VacancyEntity vacancyEntity = vacancyRepository.getReferenceById(UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        UpdateVacancyRequest updateVacancyRequest = UpdateVacancyRequest.builder()
                .id(vacancyEntity.getId())
                .name("Senior developer")
                .build();
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + VACANCY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateVacancyRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_SECOND))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteVacancySuccessfully() throws Exception {

        String vacancyId = "1769cbf8-32c1-4796-bf43-2f36a8a80cc8";
        RequestId requestId = new RequestId(UUID.fromString(vacancyId));
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void deleteVacancyFailedHrCompanyNotMatched() throws Exception {

        String vacancyId = "1769cbf8-32c1-4796-bf43-2f36a8a80cc8";
        RequestId requestId = new RequestId(UUID.fromString(vacancyId));
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_SECOND))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByIdSuccessfully() throws Exception {
        UUID vacancyId = UUID.fromString("3069cbf8-32c1-4796-bf43-2f36a8a80cc8");

        VacancyResponse vacancyResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_PREFIX + "/" + vacancyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(vacancyResponse.getId());
        Assertions.assertNotNull(vacancyResponse.getName());
        Assertions.assertNotNull(vacancyResponse.getDescription());
        Assertions.assertNotNull(vacancyResponse.getCity());
        Assertions.assertNotNull(vacancyResponse.getSalary());
        Assertions.assertNotNull(vacancyResponse.getStatus());
        Assertions.assertNotNull(vacancyResponse.getCompanyResponse());
        Assertions.assertNotNull(vacancyResponse.getCompanyResponse().getId());
        Assertions.assertNotNull(vacancyResponse.getCompanyResponse().getName());
        Assertions.assertNotNull(vacancyResponse.getCompanyResponse().getWebsiteLink());
        Assertions.assertNotNull(vacancyResponse.getRemote());
        Assertions.assertNotNull(vacancyResponse.getTags());
        Assertions.assertNotNull(vacancyResponse.getTags().get(0));
        Assertions.assertNotNull(vacancyResponse.getTags().get(0).getTag());
        Assertions.assertNotNull(vacancyResponse.getTags().get(0).getId());
        Assertions.assertNotNull(vacancyResponse.getSpecializations().get(0).getSpecialization());
    }

    @Test
    void getAllSuccessfully() throws Exception {

        ResponsePage<VacancyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_PREFIX)
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isOk())

                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(14, page.getTotalElements());
    }

    @Test
    void getAllActiveSuccessfully() throws Exception {

        ResponsePage<VacancyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_PREFIX + "/active"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(11, page.getTotalElements());
    }

    @Test
    void getByCompanySuccessfully() throws Exception {

        UUID companyId = UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ResponsePage<VacancyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_PREFIX + "/by/company/" + companyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(9, page.getTotalElements());
    }

    @Test
    void getByCompanyFailedCompanyNotFound() throws Exception {

        UUID companyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_PREFIX + "/by/company/" + companyId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByHrSuccessFully() throws Exception {

        ResponsePage<VacancyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_PREFIX + "/by/hr")
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(10, page.getTotalElements());
    }

    @Test
    void getByFilterByTagsSuccessfully() throws Exception {

        ArrayList<String> tags = new ArrayList<>();
        tags.add("java");
        tags.add("postgresql");
        VacancyFilter vacancyFilter = VacancyFilter.builder()
                .tags(tags)
                .city("Kazan")
                .sortBy("createdDate")
                .sortType("DESC")
                .minSalary(10000)
                .maxSalary(900000)
                .build();

        ResponsePage<VacancyResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_PREFIX + "/by/filter")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(vacancyFilter)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(5, page.getContent().size());
        Assertions.assertEquals("2269cbf8-32c1-4796-bf43-2f36a8a80cc8", page.getContent().get(0).getId().toString());
    }

    @Test
    void getByFilterByTagsAndSpecSuccessfully() throws Exception {

        ArrayList<String> tags = new ArrayList<>();
        tags.add("java");
        tags.add("postgresql");
        ArrayList<String> spec = new ArrayList<>();
        spec.add("backend");
        VacancyFilter vacancyFilter = VacancyFilter.builder()
//                .name("Junior")
                .tags(tags)
                .specializations(spec)
                .city("Kazan")
                .sortBy("createdDate")
                .sortType("DESC")
                .minSalary(10000)
                .maxSalary(900000)
                .build();

        ResponsePage<VacancyResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_PREFIX + "/by/filter")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(vacancyFilter)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getContent().size());
    }
}
