package com.technokratos.test.integration;


import authorization.service.DeveloperServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.ReviewRequest;
import com.technokratos.dto.request.UpdateReviewRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.dto.response.ReviewResponse;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.ReviewEntity;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.ReviewRepository;
import com.technokratos.repository.VacancyRepository;
import hr.service.HrServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/review-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class ReviewApiIntegrationTest extends AbstractIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";

    private static final String VACANCY_REVIEW_PREFIX = "/review";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String ADMIN_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9BRE1JTiIsImV4cCI6MTk5ODkzMTc4OSwianRpIjoiMTY4MzM1NTc4OTA1NyJ9.SrwnQ3nyazSAtG94WT3c40Ijc_ItZwv58Ap2nlhaAdbezjoE-_HB4B-wPWJ0IctG3FKU9d1_RCe6xay9FrIqSersIaNOPf5MktKvxVshka0iJsrbqVoLCpSusR07l0YsyVwXi32w69aOaXMnMvqKQL5Qke5Pmu7Z4dfQnM94klt6QZ0NnvKR9CLwQ0wkDFpQRYQRkqrCUGfqkfDyFG3k3Vb3La287H_VkAVu55Kb1rgM4B1L18Dte78-eVcYhVyod8DUI_645T4UjRakO1jGWvAoy8JIk2z4Ges3EtcwCQ5_OB0U30xsohPjXWgeLzdBUfxJgtm5kIdtrDdIEwwlOg";

    private static final String DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Test
    void addReviewSuccessfully() throws Exception {

        UUID companyId = UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ReviewRequest reviewRequest = ReviewRequest.builder()
                .companyId(companyId)
                .text("This company is good")
                .evaluation(4)
                .build();

        ReviewResponse response = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_REVIEW_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(reviewRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(reviewRequest.getCompanyId(), response.getCompany().getId());
        Assertions.assertEquals(reviewRequest.getText(), response.getText());
        Assertions.assertEquals(reviewRequest.getEvaluation(), response.getEvaluation());
        Assertions.assertNull(response.getDeveloperResponse());

        ReviewEntity review = reviewRepository.getReferenceById(response.getId());
        Assertions.assertNotNull(review.getCompany());
        Assertions.assertNotNull(review.getEvaluation());
        Assertions.assertNotNull(review.getAuthorId());

        CompanyEntity companyEntity = companyRepository.getReferenceById(companyId);
        Assertions.assertEquals(1, companyEntity.getReviews().size());
    }

    @Test
    void addReviewSuccessfullyAlreadySentReviewToAnotherCompany() throws Exception {

        UUID companyId = UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ReviewRequest reviewRequest = ReviewRequest.builder()
                .companyId(companyId)
                .text("This company is good")
                .evaluation(4)
                .build();

        ReviewResponse response = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_REVIEW_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(reviewRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(reviewRequest.getCompanyId(), response.getCompany().getId());
        Assertions.assertEquals(reviewRequest.getText(), response.getText());
        Assertions.assertEquals(reviewRequest.getEvaluation(), response.getEvaluation());
        Assertions.assertNull(response.getDeveloperResponse());
        ReviewEntity review = reviewRepository.getReferenceById(response.getId());
        Assertions.assertNotNull(review.getCompany());
        Assertions.assertNotNull(review.getEvaluation());
        Assertions.assertNotNull(review.getAuthorId());

        CompanyEntity companyEntity = companyRepository.getReferenceById(companyId);
        Assertions.assertEquals(1, companyEntity.getReviews().size());
    }

    @Test
    void addReviewFailedCompanyNotFound() throws Exception {

        UUID companyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ReviewRequest reviewRequest = ReviewRequest.builder()
                .companyId(companyId)
                .text("This company is good")
                .evaluation(4)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_REVIEW_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(reviewRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void addReviewFailedAccountAlreadySentReview() throws Exception {

        UUID companyId = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ReviewRequest reviewRequest = ReviewRequest.builder()
                .companyId(companyId)
                .text("This company is good")
                .evaluation(4)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_REVIEW_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(reviewRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void updateReviewSuccessfully() throws Exception {

        UUID reviewId = UUID.fromString("1969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        UpdateReviewRequest reviewRequest = UpdateReviewRequest.builder()
                .id(reviewId)
                .text("Company is bad")
                .evaluation(1)
                .build();

        ReviewResponse response = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + VACANCY_REVIEW_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(reviewRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(response.getCompany());
        Assertions.assertNotNull(response.getCompany().getId());

        Assertions.assertEquals(reviewRequest.getText(), response.getText());
        Assertions.assertEquals(reviewRequest.getEvaluation(), response.getEvaluation());
        Assertions.assertNull(response.getDeveloperResponse());

        ReviewEntity review = reviewRepository.getReferenceById(response.getId());
        Assertions.assertNotNull(review.getCompany());
        Assertions.assertNotNull(review.getEvaluation());
        Assertions.assertNotNull(review.getAuthorId());

        CompanyEntity companyEntity = companyRepository.getReferenceById(response.getCompany().getId());
        Assertions.assertEquals(reviewRequest.getText(), companyEntity.getReviews().get(0).getText());
    }

    @Test
    void updateReviewFailedReviewNotFound() throws Exception {

        UUID reviewId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        UpdateReviewRequest reviewRequest = UpdateReviewRequest.builder()
                .id(reviewId)
                .text("Company is bad")
                .evaluation(1)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + VACANCY_REVIEW_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(reviewRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteReviewSuccessfully() throws Exception {

        UUID reviewId = UUID.fromString("1969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId requestId = new RequestId(reviewId);

        CompanyEntity company = companyRepository.getReferenceById(UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        Assertions.assertEquals(1, company.getReviews().size());

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_REVIEW_PREFIX + "/my")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());

        Assertions.assertEquals(Optional.empty(), reviewRepository.findById(reviewId));
        CompanyEntity updatedCompany = companyRepository.getReferenceById(UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        Assertions.assertEquals(0, updatedCompany.getReviews().size());
    }

    @Test
    void deleteReviewFailedReviewNotFound() throws Exception {

        UUID reviewId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId requestId = new RequestId(reviewId);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_REVIEW_PREFIX + "/my")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteReviewForAdminSuccessfully() throws Exception {

        UUID reviewId = UUID.fromString("1969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId requestId = new RequestId(reviewId);

        CompanyEntity company = companyRepository.getReferenceById(UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        Assertions.assertEquals(1, company.getReviews().size());

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_REVIEW_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());

        Assertions.assertEquals(Optional.empty(), reviewRepository.findById(reviewId));
        CompanyEntity updatedCompany = companyRepository.getReferenceById(UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        Assertions.assertEquals(0, updatedCompany.getReviews().size());
    }

    @Test
    void findByIdSuccessfully() throws Exception {

        UUID reviewId = UUID.fromString("1969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ReviewResponse response = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REVIEW_PREFIX + "/" + reviewId))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(response.getDeveloperResponse());
        Assertions.assertNotNull(response.getDeveloperResponse().getId());

        Assertions.assertNotNull(response.getDeveloperResponse().getFirstName());
        Assertions.assertNotNull(response.getDeveloperResponse().getLastName());
        Assertions.assertNotNull(response.getDeveloperResponse().getPhoto());
        Assertions.assertNotNull(response.getDeveloperResponse().getUsername());
        Assertions.assertNotNull(response.getDeveloperResponse().getEmail());
        Assertions.assertNotNull(response.getId());

        ReviewEntity review = reviewRepository.getReferenceById(response.getId());
        Assertions.assertNotNull(review.getCompany());
        Assertions.assertNotNull(review.getCompany().getName());
        Assertions.assertNotNull(review.getEvaluation());
        Assertions.assertNotNull(review.getAuthorId());
    }

    @Test
    void findByIdFailedReviewNotFound() throws Exception {

        UUID reviewId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REVIEW_PREFIX + "/" + reviewId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void findByCompanySuccessfully() throws Exception {

        UUID companyId = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ResponsePage<ReviewResponse> responses = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REVIEW_PREFIX + "/company/" + companyId))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, responses.getTotalElements());

        ReviewResponse response = responses.getContent().get(0);
        Assertions.assertEquals(UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8"), response.getId());
        Assertions.assertNotNull(response.getCompany());
        Assertions.assertNotNull(response.getCompany().getId());
        Assertions.assertNotNull(response.getDeveloperResponse());
        Assertions.assertNotNull(response.getDeveloperResponse().getId());
        Assertions.assertNotNull(response.getEvaluation());
        Assertions.assertNotNull(response.getText());
    }

    @Test
    void findByCompanyFailedCompanyNotFound() throws Exception {

        UUID companyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REVIEW_PREFIX + "/company/" + companyId))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void findAllSuccessfully() throws Exception {

        ResponsePage<ReviewResponse> responses = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REVIEW_PREFIX)
                        .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(3, responses.getTotalElements());

        ReviewResponse response = responses.getContent().get(0);
        Assertions.assertEquals(UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8"), response.getId());
        Assertions.assertNotNull(response.getCompany());
        Assertions.assertNotNull(response.getCompany().getId());
        Assertions.assertNotNull(response.getDeveloperResponse());
        Assertions.assertNotNull(response.getDeveloperResponse().getId());
        Assertions.assertNotNull(response.getEvaluation());
        Assertions.assertNotNull(response.getText());
    }

    @Test
    void findByCompanyAndAuthorSuccessfully() throws Exception {

        UUID companyId = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ReviewResponse response = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REVIEW_PREFIX+"/company/"+companyId+"/my_review")
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                        .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8"), response.getId());
        Assertions.assertNotNull(response.getCompany());
        Assertions.assertNotNull(response.getCompany().getId());
        Assertions.assertNotNull(response.getDeveloperResponse());
        Assertions.assertNotNull(response.getDeveloperResponse().getId());
        Assertions.assertNotNull(response.getEvaluation());
        Assertions.assertNotNull(response.getText());
    }

    @Test
    void findByCompanyAndAuthorFailedCompanyNotFound() throws Exception {

        UUID companyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REVIEW_PREFIX+"/company/"+companyId+"/my_review")
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void findByCompanyAndAuthorFailedReviewIsNotExist() throws Exception {

        UUID companyId = UUID.fromString("2069cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REVIEW_PREFIX+"/company/"+companyId+"/my_review")
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                        .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
