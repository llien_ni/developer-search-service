package com.technokratos.test.integration;


import authorization.service.DeveloperServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.dto.response.VacancyReplyResponse;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.entity.VacancyReplyEntity;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.VacancyReplyRepository;
import com.technokratos.repository.VacancyRepository;
import hr.service.HrServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/vacancy-reply-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class VacancyReplyApiIntegrationTest extends AbstractIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";

    private static final String VACANCY_REPLY_PREFIX = "/vacancy/reply";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String HR_TOKEN_FIRST = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTY5MywianRpIjoiMTY4MzM1NTY5Mzg0NyJ9.s3eUwcY0MHsgDFr9zEwXZ4n7ZcGtn2WIwg8Ma0EDRjF_fV66OcLJbQ_cFEqVFeBMX8SsWsQVVqjLtQ6V6Mi-T4gpKRdRSIv3s95X4HUOwih_0zyxmjOsxUdT1nQ1NOjf5jUd4M7G_QMaqD4aakXsVrQLTW8XZPTS8K1RGN6pthfmOUp_hGTW8XbP6_5d01RKnyUsFaxhIORrs-h1ttPQsYL1vAsy87NguUrEn6D94y_SUM9kYwN-rtIpBYP2wJoUV8oSLK-uKHdCzEQPst5YLRYK7eM2EgFOIT0EjzTtGDI4cBRXI7H4mf4YCHTt2_UoR4GbN3xeZP2vRn4ret42mw";

    private static final String HR_TOKEN_SECOND = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTc0MiwianRpIjoiMTY4MzM1NTc0MjA4MSJ9.A-umgLeC6wiajvTFc9dPKlRJ1piyKypnfF_PLd_SShoMl7bZVtELbKiRS8rajzuwnioWMAg7d-7TKn5NAPI42feAlyl7iGM0fwyqXawtGvRI2lrYjYmqRru9s6cfHGshpsRULmV-5fb2j9IAM_2jjq1bimZxi57SOZzsD-49lfy86urlODeQjjZkfyAl_LtlXgs4ZgUD3FIvp4JrEoUbM7PsLCNTHpmFVetbiAdjmBEN0AWmEE_6kniPtxQ6dAalxqpR1wNs0T-8tACA7wFo2AXIlnr6mzMGNYFvELM2bIGtUgTWu149NahWOxRSwVZqs_3vNN3Dz6g964Llp50Evg";

    private static final String ADMIN_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9BRE1JTiIsImV4cCI6MTk5ODkzMTc4OSwianRpIjoiMTY4MzM1NTc4OTA1NyJ9.SrwnQ3nyazSAtG94WT3c40Ijc_ItZwv58Ap2nlhaAdbezjoE-_HB4B-wPWJ0IctG3FKU9d1_RCe6xay9FrIqSersIaNOPf5MktKvxVshka0iJsrbqVoLCpSusR07l0YsyVwXi32w69aOaXMnMvqKQL5Qke5Pmu7Z4dfQnM94klt6QZ0NnvKR9CLwQ0wkDFpQRYQRkqrCUGfqkfDyFG3k3Vb3La287H_VkAVu55Kb1rgM4B1L18Dte78-eVcYhVyod8DUI_645T4UjRakO1jGWvAoy8JIk2z4Ges3EtcwCQ5_OB0U30xsohPjXWgeLzdBUfxJgtm5kIdtrDdIEwwlOg";

    private static final String DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private VacancyReplyRepository vacancyReplyRepository;

    @Test
    void getAllRepliesByDeveloper() throws Exception {

        ResponsePage<VacancyReplyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/my")
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyReplyResponse vacancyReplyResponseFirst = page.getContent().get(0);
        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertEquals("1769cbf8-32c1-4796-bf43-2f36a8a80cc8", vacancyReplyResponseFirst.getId().toString());
        Assertions.assertNotNull(vacancyReplyResponseFirst.getId());
        Assertions.assertNotNull(vacancyReplyResponseFirst.getVacancyResponse());
        Assertions.assertNotNull(vacancyReplyResponseFirst.getDeveloperResponse());
    }

    @Test
    void replyToVacancySuccessfully() throws Exception {

        String vacancyId = "1969cbf8-32c1-4796-bf43-2f36a8a80cc8";

        VacancyEntity vacancy = vacancyRepository.getReferenceById(UUID.fromString(vacancyId));
        Assertions.assertEquals(0, vacancy.getReplies().size());

        VacancyReplyResponse vacancyReplyResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/" + vacancyId)
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyEntity updatedVacancy = vacancyRepository.getReferenceById(UUID.fromString(vacancyId));
        VacancyReplyEntity reply = updatedVacancy.getReplies().get(0);

        Assertions.assertNotNull(reply);
        Assertions.assertNotNull(reply.getDeveloperId());
        Assertions.assertNotNull(reply.getVacancy());
        Assertions.assertEquals(VacancyReplyEntity.State.WAIT, reply.getState());
        Assertions.assertNotNull(reply.getId());

        VacancyReplyEntity vacancyReplyEntity = vacancyReplyRepository.getReferenceById(reply.getId());
        Assertions.assertNotNull(vacancyReplyEntity.getId());
        Assertions.assertNotNull(vacancyReplyEntity.getVacancy());

        Assertions.assertNotNull(vacancyReplyResponse.getVacancyResponse());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse());
        Assertions.assertNotNull(vacancyReplyResponse.getId());
        Assertions.assertNotNull(vacancyReplyResponse.getState());
    }

    //1769cbf8-32c1-4796-bf43-2f36a8a80cc8

    @Test
    void deleteReply() throws Exception{

        String vacancyId = "1769cbf8-32c1-4796-bf43-2f36a8a80cc8";
        VacancyEntity vacancy = vacancyRepository.getReferenceById(UUID.fromString(vacancyId));
        Assertions.assertEquals(1, vacancy.getReplies().size());

        String replyId = "1769cbf8-32c1-4796-bf43-2f36a8a80cc8";
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/" + replyId)
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                        .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());
        VacancyEntity updatedVacancy = vacancyRepository.getReferenceById(UUID.fromString(vacancyId));
        Assertions.assertEquals(0, updatedVacancy.getReplies().size());
    }

    @Test
    void getAllReplies() throws Exception {

        ResponsePage<VacancyReplyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REPLY_PREFIX)
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertEquals("1769cbf8-32c1-4796-bf43-2f36a8a80cc8", page.getContent().get(0).getId().toString());
    }

    @Test
    void getAllRepliesByState() throws Exception {

        ResponsePage<VacancyReplyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/by/state")
                                .param("state", VacancyReplyEntity.State.REJECTED.toString())
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(0, page.getTotalElements());
    }

    @Test
    void getByIdSuccessfully() throws Exception {

        UUID id = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");

        VacancyReplyResponse vacancyReplyResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/" + id)
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(vacancyReplyResponse.getVacancyResponse());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse());
        Assertions.assertNotNull(vacancyReplyResponse.getId());
        Assertions.assertNotNull(vacancyReplyResponse.getState());
    }

    @Test
    void getAllReplyByIdFailedVacancyReplyNotFound() throws Exception {

        UUID id = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/" + id)
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllReplyByVacancySuccessfully() throws Exception {

        UUID vacancyId = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ResponsePage<VacancyReplyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/by/vacancy/" +vacancyId)
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(1, page.getTotalElements());
    }

    @Test
    void getAllReplyByVacancyFailedVacancyNotFound() throws Exception {

        UUID vacancyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/by/vacancy/" +vacancyId)
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllReplyByVacancyFailedVacancyHrNotMatched() throws Exception {

        UUID vacancyId = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/by/vacancy/" +vacancyId)
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_SECOND))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void rejectVacancyReplySuccessfully() throws Exception {
        UUID vacancyReplyId = UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8");

        VacancyReplyEntity vacancyReplyEntity = vacancyReplyRepository.getReferenceById(vacancyReplyId);
        Assertions.assertEquals(VacancyReplyEntity.State.WAIT, vacancyReplyEntity.getState());
        RequestId requestId = new RequestId(vacancyReplyId);

        VacancyReplyResponse vacancyReplyResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/reject")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyReplyEntity updatedVacancyReply = vacancyReplyRepository.getReferenceById(vacancyReplyId);
        Assertions.assertEquals(VacancyReplyEntity.State.REJECTED, updatedVacancyReply.getState());

        Assertions.assertNotNull(vacancyReplyResponse.getVacancyResponse());
        Assertions.assertNotNull(vacancyReplyResponse.getVacancyResponse().getId());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getLastName());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getId());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getLastName());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getUsername());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getPhoto());
        Assertions.assertNotNull(vacancyReplyResponse.getId());
        Assertions.assertEquals(VacancyReplyEntity.State.REJECTED.toString(), vacancyReplyResponse.getState());
    }

    @Test
    void rejectVacancyReplyFailedVacancyNotFound() throws Exception {

        UUID vacancyReplyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId requestId = new RequestId(vacancyReplyId);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/reject")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void viewedVacancyReplySuccessfully() throws Exception {

        UUID vacancyReplyId = UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8");

        VacancyReplyEntity vacancyReplyEntity = vacancyReplyRepository.getReferenceById(vacancyReplyId);
        Assertions.assertEquals(VacancyReplyEntity.State.WAIT, vacancyReplyEntity.getState());
        RequestId requestId = new RequestId(vacancyReplyId);

        VacancyReplyResponse vacancyReplyResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + VACANCY_REPLY_PREFIX + "/viewed")

                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyReplyEntity updatedVacancyReply = vacancyReplyRepository.getReferenceById(vacancyReplyId);
        Assertions.assertEquals(VacancyReplyEntity.State.VIEWED, updatedVacancyReply.getState());

        Assertions.assertNotNull(vacancyReplyResponse.getVacancyResponse());
        Assertions.assertNotNull(vacancyReplyResponse.getVacancyResponse().getId());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getLastName());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getId());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getLastName());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getUsername());
        Assertions.assertNotNull(vacancyReplyResponse.getDeveloperResponse().getPhoto());
        Assertions.assertNotNull(vacancyReplyResponse.getId());
        Assertions.assertEquals(VacancyReplyEntity.State.VIEWED.toString(), vacancyReplyResponse.getState());
    }
}
