package com.technokratos.test.integration;

import authorization.service.DeveloperServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.LocationRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.*;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.LocationEntity;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.LocationRepository;
import hr.service.HrServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.retry.MessageBatchRecoverer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/location-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class LocationApiIntegrationTest extends AbstractIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";

    private static final String COMPANY_LOCATION_PREFIX = "/company/location";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String HR_TOKEN_FIRST = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTY5MywianRpIjoiMTY4MzM1NTY5Mzg0NyJ9.s3eUwcY0MHsgDFr9zEwXZ4n7ZcGtn2WIwg8Ma0EDRjF_fV66OcLJbQ_cFEqVFeBMX8SsWsQVVqjLtQ6V6Mi-T4gpKRdRSIv3s95X4HUOwih_0zyxmjOsxUdT1nQ1NOjf5jUd4M7G_QMaqD4aakXsVrQLTW8XZPTS8K1RGN6pthfmOUp_hGTW8XbP6_5d01RKnyUsFaxhIORrs-h1ttPQsYL1vAsy87NguUrEn6D94y_SUM9kYwN-rtIpBYP2wJoUV8oSLK-uKHdCzEQPst5YLRYK7eM2EgFOIT0EjzTtGDI4cBRXI7H4mf4YCHTt2_UoR4GbN3xeZP2vRn4ret42mw";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Test
    void addLocationToCompanySuccessfully() throws Exception{

        LocationRequest locationRequest = LocationRequest.builder()
                .address("Ivanova 18")
                .city("Kazan")
                .build();

        LocationResponse response  = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + COMPANY_LOCATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(locationRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        CompanyEntity company = companyRepository.getReferenceById(response.getCompanyId());
        Assertions.assertNotEquals(Optional.empty(), company.getLocations()
                .stream()
                .filter(locationEntity -> locationEntity.getCity().equals(locationRequest.getCity()))
                .findAny());
        LocationEntity location = locationRepository.getReferenceById(response.getId());
        Assertions.assertEquals(locationRequest.getCity(), location.getCity());
        Assertions.assertEquals(locationRequest.getAddress(), location.getAddress());
    }

    @Test
    void deleteLocationToCompanySuccessfully() throws Exception{

        UUID locationId = UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId requestId = new RequestId(locationId);

        CompanyEntity company = companyRepository.getReferenceById(UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        Assertions.assertEquals(2, company.getLocations().size());
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + COMPANY_LOCATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());
        CompanyEntity updatedCompany = companyRepository.getReferenceById(UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        Assertions.assertEquals(1, updatedCompany.getLocations().size());
    }

    @Test
    void deleteLocationToCompanyFailedLocationNotFound() throws Exception{

        UUID locationId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId requestId = new RequestId(locationId);
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + COMPANY_LOCATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllByCompanySuccessfully() throws Exception{
        UUID companyId = UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ResponsePage<LocationResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + COMPANY_LOCATION_PREFIX+"/"+companyId))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
    }

    @Test
    void getAllByCompanyFailed() throws Exception{
        UUID companyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + COMPANY_LOCATION_PREFIX+"/"+companyId))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
