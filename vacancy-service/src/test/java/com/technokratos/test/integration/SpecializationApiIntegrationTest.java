package com.technokratos.test.integration;


import authorization.service.DeveloperServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListSpecializationsRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.ListResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.entity.SpecializationEntity;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.VacancyRepository;
import hr.service.HrServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/specialization-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class SpecializationApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String VACANCY_SPEC_PREFIX = "/vacancy/specialization";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String HR_TOKEN_FIRST = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTY5MywianRpIjoiMTY4MzM1NTY5Mzg0NyJ9.s3eUwcY0MHsgDFr9zEwXZ4n7ZcGtn2WIwg8Ma0EDRjF_fV66OcLJbQ_cFEqVFeBMX8SsWsQVVqjLtQ6V6Mi-T4gpKRdRSIv3s95X4HUOwih_0zyxmjOsxUdT1nQ1NOjf5jUd4M7G_QMaqD4aakXsVrQLTW8XZPTS8K1RGN6pthfmOUp_hGTW8XbP6_5d01RKnyUsFaxhIORrs-h1ttPQsYL1vAsy87NguUrEn6D94y_SUM9kYwN-rtIpBYP2wJoUV8oSLK-uKHdCzEQPst5YLRYK7eM2EgFOIT0EjzTtGDI4cBRXI7H4mf4YCHTt2_UoR4GbN3xeZP2vRn4ret42mw";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Test
    void addSpecializationListToVacancySuccessfully() throws Exception{

        UUID vacancyId = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");

        List<String> list = new ArrayList<>();
        list.add("data science");
        list.add("backend");
        ListSpecializationsRequest request = ListSpecializationsRequest.builder()
                .vacancyId(vacancyId)
                .specializations(list)
                .build();
        ListResponse<SpecializationResponse> listResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_SPEC_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyEntity vacancy = vacancyRepository.getReferenceById(vacancyId);
        SpecializationEntity specialization = vacancy.getSpecializations().get(0);
        Assertions.assertNotNull(specialization.getSpecialization());
        Assertions.assertNotNull(specialization.getVacancy());
        Assertions.assertEquals(2, vacancy.getSpecializations().size());
        Assertions.assertNotNull(listResponse.getData().get(0));
        Assertions.assertNotNull(listResponse.getData().get(1));
        Assertions.assertNotNull(listResponse.getData().get(0).getId());
        Assertions.assertNotNull(listResponse.getData().get(0).getSpecialization());
        Assertions.assertNotNull(listResponse.getData().get(0).getVacancyId());

    }

    @Test
    void addSpecializationListToVacancyFailedVacancyNotFound() throws Exception{

        UUID vacancyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        List<String> list = new ArrayList<>();
        list.add("data science");
        list.add("backend");
        ListSpecializationsRequest request = ListSpecializationsRequest.builder()
                .vacancyId(vacancyId)
                .specializations(list)
                .build();
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_SPEC_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteSpecializationListFromVacancySuccessfully() throws Exception{

        UUID vacancyId = UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8");
        List<UUID> list = new ArrayList<>();
        list.add(UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        ListOfIdsRequest request = ListOfIdsRequest.builder()
                .vacancyId(vacancyId)
                .ids(list)
                .build();

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_SPEC_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyEntity updatedVacancy = vacancyRepository.getReferenceById(vacancyId);
        Assertions.assertEquals(1, updatedVacancy.getSpecializations().size());
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void deleteSpecializationListFromVacancyFailedSpecIsNotExist() throws Exception{

        UUID vacancyId = UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8");
        List<UUID> list = new ArrayList<>();
        list.add(UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        ListOfIdsRequest request = ListOfIdsRequest.builder()
                .vacancyId(vacancyId)
                .ids(list)
                .build();

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_SPEC_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                        .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());
    }
}
