package com.technokratos.test.integration;


import authorization.service.DeveloperServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.CreateCompanyRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateCompanyRequest;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.repository.CompanyRepository;
import hr.service.HrServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/company-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class CompanyApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String COMPANY_PREFIX = "/company";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String HR_TOKEN_FIRST = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTY5MywianRpIjoiMTY4MzM1NTY5Mzg0NyJ9.s3eUwcY0MHsgDFr9zEwXZ4n7ZcGtn2WIwg8Ma0EDRjF_fV66OcLJbQ_cFEqVFeBMX8SsWsQVVqjLtQ6V6Mi-T4gpKRdRSIv3s95X4HUOwih_0zyxmjOsxUdT1nQ1NOjf5jUd4M7G_QMaqD4aakXsVrQLTW8XZPTS8K1RGN6pthfmOUp_hGTW8XbP6_5d01RKnyUsFaxhIORrs-h1ttPQsYL1vAsy87NguUrEn6D94y_SUM9kYwN-rtIpBYP2wJoUV8oSLK-uKHdCzEQPst5YLRYK7eM2EgFOIT0EjzTtGDI4cBRXI7H4mf4YCHTt2_UoR4GbN3xeZP2vRn4ret42mw";

    private static final String HR_TOKEN_SECOND = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTc0MiwianRpIjoiMTY4MzM1NTc0MjA4MSJ9.A-umgLeC6wiajvTFc9dPKlRJ1piyKypnfF_PLd_SShoMl7bZVtELbKiRS8rajzuwnioWMAg7d-7TKn5NAPI42feAlyl7iGM0fwyqXawtGvRI2lrYjYmqRru9s6cfHGshpsRULmV-5fb2j9IAM_2jjq1bimZxi57SOZzsD-49lfy86urlODeQjjZkfyAl_LtlXgs4ZgUD3FIvp4JrEoUbM7PsLCNTHpmFVetbiAdjmBEN0AWmEE_6kniPtxQ6dAalxqpR1wNs0T-8tACA7wFo2AXIlnr6mzMGNYFvELM2bIGtUgTWu149NahWOxRSwVZqs_3vNN3Dz6g964Llp50Evg";

    private static  final String ADMIN_TOKEN ="eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9BRE1JTiIsImV4cCI6MTk5ODkzMTc4OSwianRpIjoiMTY4MzM1NTc4OTA1NyJ9.SrwnQ3nyazSAtG94WT3c40Ijc_ItZwv58Ap2nlhaAdbezjoE-_HB4B-wPWJ0IctG3FKU9d1_RCe6xay9FrIqSersIaNOPf5MktKvxVshka0iJsrbqVoLCpSusR07l0YsyVwXi32w69aOaXMnMvqKQL5Qke5Pmu7Z4dfQnM94klt6QZ0NnvKR9CLwQ0wkDFpQRYQRkqrCUGfqkfDyFG3k3Vb3La287H_VkAVu55Kb1rgM4B1L18Dte78-eVcYhVyod8DUI_645T4UjRakO1jGWvAoy8JIk2z4Ges3EtcwCQ5_OB0U30xsohPjXWgeLzdBUfxJgtm5kIdtrDdIEwwlOg";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private CompanyRepository companyRepository;

    @Test
    void saveCompanySuccessfully() throws Exception{
        CreateCompanyRequest companyRequest = CreateCompanyRequest.builder()
                .name("Company")
                .description("We work")
                .websiteLink("http://company.com")
                .email("Company@gmail.com")
                .type("Digital Company")
                .build();

        CompanyResponse companyResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + COMPANY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(companyRequest)))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(companyRequest.getName(), companyResponse.getName());
        Assertions.assertNotNull(companyResponse.getId());
        Assertions.assertEquals(CompanyEntity.Status.NOT_CONFIRMED.toString(), companyResponse.getStatus());
        CompanyEntity companyEntity = companyRepository.getReferenceById(companyResponse.getId());
        Assertions.assertNotNull(companyEntity.getConfirmationToken());
    }

    @Test
    void saveCompanyFailedCompanyAlreadyExist() throws Exception{

        CreateCompanyRequest companyRequest = CreateCompanyRequest.builder()
                .name("Pokemons")
                .description("We work")
                .websiteLink("http://company.com")
                .email("Company@gmail.com")
                .type("Digital Company")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + COMPANY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(companyRequest)))
                        .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void updateCompanySuccessfully() throws  Exception{

        CompanyEntity company = companyRepository.getReferenceById(UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        UpdateCompanyRequest updateCompanyRequest = UpdateCompanyRequest.builder()
                .id(company.getId())
                .name("New name")
                .type("new type")
                .build();

        CompanyResponse companyResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + COMPANY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateCompanyRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        CompanyEntity updatedCompany = companyRepository.getReferenceById(company.getId());

        Assertions.assertEquals(company.getDescription(), updatedCompany.getDescription());
        Assertions.assertEquals(updateCompanyRequest.getName(), updatedCompany.getName());
        Assertions.assertEquals(updateCompanyRequest.getName(), companyResponse.getName());
        Assertions.assertEquals(updateCompanyRequest.getType(), updatedCompany.getType());
        Assertions.assertEquals(CompanyEntity.Status.MODERATED.toString(), companyResponse.getStatus());
        Assertions.assertNotNull(updatedCompany.getUpdatedBy());
    }

    @Test
    void updateCompanyFailedHrCompanyNotMatched() throws  Exception{

        CompanyEntity company = companyRepository.getReferenceById(UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        UpdateCompanyRequest updateCompanyRequest = UpdateCompanyRequest.builder()
                .id(company.getId())
                .name("New name")
                .type("new type")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + COMPANY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateCompanyRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_SECOND))
                        .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteCompanySuccessfully() throws  Exception{

        CompanyEntity company = companyRepository.getReferenceById(UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        RequestId requestId = new RequestId(company.getId());
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + COMPANY_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(messageResponse.getMessage());
        CompanyEntity deletedCompany = companyRepository.getReferenceById(company.getId());
        Assertions.assertEquals(CompanyEntity.Status.DELETED, deletedCompany.getStatus());
    }

    @Test
    void getByIdSuccessfully() throws Exception{

        CompanyResponse companyResponse = objectMapper.readValue(mockMvc.perform(
                       get(LOCALHOST + port + COMPANY_PREFIX+"/1669cbf8-32c1-4796-bf43-2f36a8a80cc8")
                                .contentType("application/json"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(companyResponse.getId());
        Assertions.assertNotNull(companyResponse.getName());
        Assertions.assertNotNull(companyResponse.getDescription());
        Assertions.assertNotNull(companyResponse.getWebsiteLink());
        Assertions.assertNotNull(companyResponse.getEmail());
        Assertions.assertNotNull(companyResponse.getType());
        Assertions.assertNotNull(companyResponse.getStatus());
        Assertions.assertNotNull(companyResponse.getCreatedDate());
        Assertions.assertNotNull(companyResponse.getCreatedDate());
        Assertions.assertEquals(0, companyResponse.getRating());
    }

    @Test
    void getConfirmedCompaniesSuccessfully() throws Exception{

        ResponsePage<CompanyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + COMPANY_PREFIX)
                                .contentType("application/json"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
    }

    @Test
    void getAllSuccessfully() throws Exception{

        ResponsePage<CompanyResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + COMPANY_PREFIX+"/all")
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(4, page.getTotalElements());
    }
}
