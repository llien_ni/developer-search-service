package com.technokratos.test.integration;


import authorization.service.DeveloperServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListTagsRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.ListResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.TagResponse;
import com.technokratos.entity.TagEntity;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.TagRepository;
import com.technokratos.repository.VacancyRepository;
import hr.service.HrServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/tag-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class TagApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String VACANCY_TAG_PREFIX = "/vacancy/tag";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String HR_TOKEN_FIRST = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9IUiIsImV4cCI6MTk5ODkzMTY5MywianRpIjoiMTY4MzM1NTY5Mzg0NyJ9.s3eUwcY0MHsgDFr9zEwXZ4n7ZcGtn2WIwg8Ma0EDRjF_fV66OcLJbQ_cFEqVFeBMX8SsWsQVVqjLtQ6V6Mi-T4gpKRdRSIv3s95X4HUOwih_0zyxmjOsxUdT1nQ1NOjf5jUd4M7G_QMaqD4aakXsVrQLTW8XZPTS8K1RGN6pthfmOUp_hGTW8XbP6_5d01RKnyUsFaxhIORrs-h1ttPQsYL1vAsy87NguUrEn6D94y_SUM9kYwN-rtIpBYP2wJoUV8oSLK-uKHdCzEQPst5YLRYK7eM2EgFOIT0EjzTtGDI4cBRXI7H4mf4YCHTt2_UoR4GbN3xeZP2vRn4ret42mw";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private TagRepository tagRepository;

    @Test
    void addTagListToVacancySuccessfully()throws Exception{

        UUID vacancyId = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");
        VacancyEntity vacancy = vacancyRepository.getReferenceById(vacancyId);
        Assertions.assertEquals(0, vacancy.getTags().size());

        List<String> tags = new ArrayList<>();
        tags.add("java");
        tags.add("postgresql");
        ListTagsRequest listTagsRequest = ListTagsRequest.builder()
                .vacancyId(vacancyId)
                .tags(tags)
                .build();

        ListResponse<TagResponse> listResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listTagsRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        VacancyEntity updatedVacancy = vacancyRepository.getReferenceById(vacancyId);
        Assertions.assertEquals(2, updatedVacancy.getTags().size());
        TagEntity tag = tagRepository.getReferenceById(updatedVacancy.getTags().get(0).getId());
        Assertions.assertNotNull(tag.getId());
        Assertions.assertNotNull(tag.getVacancy());
        Assertions.assertNotNull(tag.getTag());
        Assertions.assertNotNull(listResponse.getData());
        Assertions.assertEquals(2, listResponse.getData().size());
    }

    @Test
    void addTagListToVacancyFailedVacancyNotFound()throws Exception{

        UUID vacancyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        List<String> tags = new ArrayList<>();
        tags.add("java");
        tags.add("postgresql");
        ListTagsRequest listTagsRequest = ListTagsRequest.builder()
                .vacancyId(vacancyId)
                .tags(tags)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + VACANCY_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listTagsRequest))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteTagsFromVacancySuccessfully() throws Exception{

        UUID vacancyId = UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8");
        List<UUID> listOfIds = new ArrayList<>();
        listOfIds.add(UUID.fromString("1969cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        ListOfIdsRequest list = ListOfIdsRequest.builder().vacancyId(vacancyId).ids(listOfIds).build();

        VacancyEntity vacancy = vacancyRepository.getReferenceById(vacancyId);
        Assertions.assertEquals(2, vacancy.getTags().size());
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(list))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());
        VacancyEntity updatedVacancy = vacancyRepository.getReferenceById(vacancyId);
        Assertions.assertEquals(1, updatedVacancy.getTags().size());

    }

    @Test
    void deleteTagsFromVacancyFailedVacancyNotFound() throws Exception{

        UUID vacancyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        List<UUID> listOfIds = new ArrayList<>();
        listOfIds.add(UUID.fromString("1969cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        ListOfIdsRequest list = ListOfIdsRequest.builder().vacancyId(vacancyId).ids(listOfIds).build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(list))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteTagsFromVacancyFailedTagIsNotFound() throws Exception{

        UUID vacancyId = UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8");
        List<UUID> listOfIds = new ArrayList<>();
        listOfIds.add(UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8"));

        ListOfIdsRequest list = ListOfIdsRequest.builder().vacancyId(vacancyId).ids(listOfIds).build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + VACANCY_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(list))
                                .header("Authorization", BEARER_PREFIX + HR_TOKEN_FIRST))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
