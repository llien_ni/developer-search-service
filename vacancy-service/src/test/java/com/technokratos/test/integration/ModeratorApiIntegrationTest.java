package com.technokratos.test.integration;


import authorization.service.DeveloperServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.repository.CompanyRepository;
import hr.service.HrServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/moderator-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class ModeratorApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String MODERATOR_COMPANY_PREFIX = "/moderator/company";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String MODERATOR_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9NT0RFUkFUT1IiLCJleHAiOjE5OTkxMTAxNTQsImp0aSI6IjE2ODM1MzQxNTQ4MjYifQ.1k4ErYfvMQycLARmzFlUKSVXiHSd7sO3MdWOlwa5NBh5NGbr9URcXgHTgg0VvYvhBNghUHiz8ah7FJbqK5sK9OMAyWXXJCwIKMKjKaq5OS__zDBXczFkOQeb20hukDcFrrco0A39xUkcF_aKJvY81Y_IlF7iv7Na9S5YXVv9ZZ0aM83TGXGpRBltydjx1rkqlVz4vcZA81kiOWWdeL8qgzHL49p8vyqaS7PTc7au0eo9zu-azhJFwwu1nzIWeiJ4wRMsQAp5E-zfp74cNt0_jbEKJ2wz1nnAH3xjcC4Rh570KHGN8Nk8AcDxpxwyM83CHsmhagUpw9Q2Iuc38yyS2w";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private CompanyRepository companyRepository;

    @Test
    void getModeratedCompaniesSuccessfully() throws Exception{

        ResponsePage<CompanyResponse> page  = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + MODERATOR_COMPANY_PREFIX+"/moderated")
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + MODERATOR_TOKEN))
                        .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(4, page.getTotalElements());
        CompanyResponse companyResponse = page.getContent().get(0);
        Assertions.assertEquals("1969cbf8-32c1-4796-bf43-2f36a8a80cc8", companyResponse.getId().toString());
        Assertions.assertNotNull(companyResponse.getRating());
        Assertions.assertNotNull(companyResponse.getStatus());
        Assertions.assertNotNull(companyResponse.getName());
        Assertions.assertNotNull(companyResponse.getEmail());
        Assertions.assertNotNull(companyResponse.getDescription());
        Assertions.assertNotNull(companyResponse.getWebsiteLink());
    }

    @Test
    void confirmCompanySuccessfully() throws Exception{

        UUID companyId = UUID.fromString("1769cbf8-32c1-4796-bf43-2f36a8a80cc8");
        CompanyEntity company = companyRepository.getReferenceById(companyId);
        Assertions.assertEquals(CompanyEntity.Status.MODERATED, company.getStatus());

        RequestId requestId = new RequestId(companyId);
        CompanyResponse response  = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + MODERATOR_COMPANY_PREFIX+"/confirm")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + MODERATOR_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(CompanyEntity.Status.CONFIRMED.toString(), response.getStatus());
        CompanyEntity updatedCompany = companyRepository.getReferenceById(companyId);
        Assertions.assertEquals(CompanyEntity.Status.CONFIRMED, updatedCompany.getStatus());
    }

    @Test
    void confirmCompanyFailedCompanyIsNotFound() throws Exception{

        UUID companyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        RequestId requestId = new RequestId(companyId);
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + MODERATOR_COMPANY_PREFIX+"/confirm")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + MODERATOR_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void blockCompanySuccessfully() throws Exception{

        UUID companyId = UUID.fromString("1869cbf8-32c1-4796-bf43-2f36a8a80cc8");
        CompanyEntity company = companyRepository.getReferenceById(companyId);
        Assertions.assertEquals(CompanyEntity.Status.MODERATED, company.getStatus());

        RequestId requestId = new RequestId(companyId);
        CompanyResponse response  = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + MODERATOR_COMPANY_PREFIX+"/block")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + MODERATOR_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(CompanyEntity.Status.BLOCKED.toString(), response.getStatus());
        CompanyEntity updatedCompany = companyRepository.getReferenceById(companyId);
        Assertions.assertEquals(CompanyEntity.Status.BLOCKED, updatedCompany.getStatus());
    }

    @Test
    void blockCompanyFailedCompanyNotFound() throws Exception{

        UUID companyId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        RequestId requestId = new RequestId(companyId);
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + MODERATOR_COMPANY_PREFIX+"/confirm")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + MODERATOR_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
