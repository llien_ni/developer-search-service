package com.technokratos.config;


import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;
import notification.service.NotificationServiceGrpc;
import notification.service.NotificationServiceOuterClass;


@GrpcService
public class NotificationServiceImpl extends NotificationServiceGrpc.NotificationServiceImplBase {

    @Override
    public void sendMail(NotificationServiceOuterClass.MailMessageRequest request, StreamObserver<NotificationServiceOuterClass.Empty> responseObserver) {

        responseObserver.onNext(NotificationServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }
}