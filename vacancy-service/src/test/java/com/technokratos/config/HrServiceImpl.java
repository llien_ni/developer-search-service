package com.technokratos.config;

import hr.service.HrServiceGrpc;
import hr.service.HrServiceOuterClass;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import notification.service.NotificationServiceOuterClass;

@GrpcService
public class HrServiceImpl extends HrServiceGrpc.HrServiceImplBase {

    @Override
    public void getHrById(HrServiceOuterClass.GetHrByIdRequest request, StreamObserver<HrServiceOuterClass.HrResponse> responseObserver) {

        if (request.getId().equals("2669cbf8-32c1-4796-bf43-2f36a8a80cc8")) {

            responseObserver.onNext(HrServiceOuterClass.HrResponse.newBuilder()
                    .setId("2669cbf8-32c1-4796-bf43-2f36a8a80cc8")
                    .setCompanyId("2669cbf8-32c1-4796-bf43-2f36a8a80cc8")
                    .build());
            responseObserver.onCompleted();

        }else{
            responseObserver.onNext(HrServiceOuterClass.HrResponse.newBuilder()
                    .setId("1669cbf8-32c1-4796-bf43-2f36a8a80cc8")
                    .setCompanyId("1669cbf8-32c1-4796-bf43-2f36a8a80cc8")
                    .build());
            responseObserver.onCompleted();
        }
    }
}
