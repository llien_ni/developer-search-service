INSERT INTO public.company (id, name, description, website_link, email, type, status, created_date, updated_date, updated_by, confirmation_token, rating) VALUES ('1669cbf8-32c1-4796-bf43-2f36a8a80cc8', 'Pokemons', 'we work every day', 'http://pokemon', 'pokemon@gmail.com', 'OOO', 'CONFIRMED', '2023-05-06 09:11:32.281000 +00:00', '2023-05-06 09:11:34.097000 +00:00', null, '1669cbf8-32c1-4796-bf43-2f36a8a80cc8', 0.0);



INSERT INTO public.vacancy (id, company_id, description, hr_id, city, remote, salary, address, created_date, updated_date, status, name) VALUES ('1769cbf8-32c1-4796-bf43-2f36a8a80cc8', '1669cbf8-32c1-4796-bf43-2f36a8a80cc8', 'Work', '1669cbf8-32c1-4796-bf43-2f36a8a80cc8', 'Kazan', false, 70000, null, '2023-05-06 10:36:56.815000 +00:00', '2023-05-06 10:36:58.601000 +00:00', 'ACTIVE', 'Junior developer');
INSERT INTO public.vacancy (id, company_id, description, hr_id, city, remote, salary, address, created_date, updated_date, status, name) VALUES ('1869cbf8-32c1-4796-bf43-2f36a8a80cc8', '1669cbf8-32c1-4796-bf43-2f36a8a80cc8', 'Work', '1669cbf8-32c1-4796-bf43-2f36a8a80cc8', 'Ulyanovsk', false, 70000, null, '2023-05-06 10:36:56.815000 +00:00', '2023-05-06 10:36:58.601000 +00:00', 'ACTIVE', 'Junior developer');

INSERT INTO public.specialization (id, vacancy_id, specialization) VALUES ('1869cbf8-32c1-4796-bf43-2f36a8a80cc8', '1869cbf8-32c1-4796-bf43-2f36a8a80cc8', 'frontend');
INSERT INTO public.specialization (id, vacancy_id, specialization) VALUES ('1969cbf8-32c1-4796-bf43-2f36a8a80cc8', '1869cbf8-32c1-4796-bf43-2f36a8a80cc8', 'backend');

