package com.technokratos.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UpdateReviewRequest {

    @NotNull
    private UUID id;

    private String text;

    @Min(value = 1)
    @Max(value = 5)
    private Integer evaluation;
}
