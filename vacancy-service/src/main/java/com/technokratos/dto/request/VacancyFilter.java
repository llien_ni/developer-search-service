package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class VacancyFilter {

    private List<String> tags;

    private List<String> specializations;

    private String city;

    private String sortBy;

    private String sortType;

    private Boolean remote;

    private Integer minSalary;

    private Integer maxSalary;

    private LocalDate createdDate;
}
