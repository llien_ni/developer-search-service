package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ReviewRequest {

    @NotNull
    private UUID companyId;

    private String text;

    @Min(value = 1)
    @Max(value = 5)
    @NotNull
    private Integer evaluation;
}
