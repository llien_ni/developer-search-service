package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CreateVacancyRequest {

    @NotBlank
    @Size(max = 50)
    private String name;

    private String description;

    private String city;

    private String address;

    private Boolean remote;

    @NotNull
    @PositiveOrZero
    private Long salary;

    private String status;
}
