package com.technokratos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VacancyReplyResponse {

    private UUID id;

    private DeveloperResponse developerResponse;

    private VacancyResponse vacancyResponse;

    private String state;

    private OffsetDateTime createdDate;
}
