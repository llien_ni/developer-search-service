package com.technokratos.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReviewResponse {

    private UUID id;

    private CompanyResponse company;

    private String text;

    private Integer evaluation;

    private OffsetDateTime updateDate;

    private OffsetDateTime createdDate;

    private DeveloperResponse developerResponse;
}
