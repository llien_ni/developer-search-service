package com.technokratos.controller;

import com.technokratos.api.ModeratorApi;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.service.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ModeratorController implements ModeratorApi {

    private final CompanyService companyService;

    @Override
    public Page<CompanyResponse> getModeratedCompanies(Integer size, Integer number) {

        return companyService.getCompanySortByAndStatus("createdDate",CompanyEntity.Status.MODERATED, size, number);
    }

    @Override
    public CompanyResponse confirmCompany(RequestId request) {

        return companyService.updateCompanyStatusToConfirmed(request.getId());
    }

    @Override
    public CompanyResponse blockCompany(RequestId request) {

        return companyService.updateCompanyStatusToBlocked(request.getId());
    }
}
