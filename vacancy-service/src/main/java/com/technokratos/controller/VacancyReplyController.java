package com.technokratos.controller;

import com.technokratos.api.VacancyReplyApi;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.VacancyReplyResponse;
import com.technokratos.service.VacancyReplyService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;


@RestController
@RequiredArgsConstructor
public class VacancyReplyController implements VacancyReplyApi {

    private final VacancyReplyService vacancyReplyService;

    @Override
    public VacancyReplyResponse replyToVacancy(UUID vacancyId, String developerId) {

        return vacancyReplyService.replyToVacancy(vacancyId, developerId);
    }

    @Override
    public MessageResponse deleteVacancyReply(UUID replyId, String developerId) {

        UUID id = vacancyReplyService.deleteReply(replyId, developerId);
        return MessageResponse.builder().message(String.format("The reply to the vacancy was successfully deleted. Reply id %s", id)).build();
    }

    @Override
    public Page<VacancyReplyResponse> getAllRepliesByDeveloper(String developerId, Integer size, Integer number) {

        return vacancyReplyService.getAllRepliesByDeveloper(developerId, size, number);
    }

    @Override
    public Page<VacancyReplyResponse> getAllReplies(String hrId, Integer size, Integer number) {

        return vacancyReplyService.getAllRepliesForHr(hrId, size, number);
    }

    @Override
    public Page<VacancyReplyResponse> getAllRepliesByState(String state, String hrId, Integer size, Integer number) {

        return vacancyReplyService.getAllRepliesByStateForHr(hrId, state, size, number);
    }

    @Override
    public VacancyReplyResponse rejectVacancyReply(RequestId requestId, String hrId) {

        return vacancyReplyService.rejectVacancyReply(requestId.getId(), hrId);
    }

    @Override
    public VacancyReplyResponse getById(UUID id, String hrId) {

        return vacancyReplyService.getById(id, hrId);
    }

    @Override
    public Page<VacancyReplyResponse> getAllRepliesByVacancy(UUID vacancyId, String hrId, Integer size, Integer number) {

        return vacancyReplyService.getAllRepliesByVacancy(vacancyId, hrId, size, number);
    }

    @Override
    public VacancyReplyResponse viewedVacancyReply(RequestId requestId, String hrId) {

        return vacancyReplyService.viewedVacancyReply(requestId.getId(), hrId);
    }
}
