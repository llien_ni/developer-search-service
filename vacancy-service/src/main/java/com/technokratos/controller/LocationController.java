package com.technokratos.controller;

import com.technokratos.api.LocationApi;
import com.technokratos.dto.request.LocationRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.LocationResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class LocationController implements LocationApi {

    private final LocationService locationService;

    @Override
    public LocationResponse addLocationToCompany(LocationRequest request, String hrId) {

        return locationService.add(request, hrId);
    }

    @Override
    public MessageResponse deleteLocationFromCompany(RequestId requestId, String hrId) {

        locationService.delete(requestId, hrId);

        return MessageResponse.builder()
                .message("Location successfully deleted")
                .build();
    }

    @Override
    public Page<LocationResponse> getByCompany(UUID id, Integer size, Integer number) {

        return locationService.getAllByCompany(id, size, number);
    }
}
