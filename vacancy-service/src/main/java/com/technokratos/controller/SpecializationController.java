package com.technokratos.controller;

import com.technokratos.api.SpecializationApi;
import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListSpecializationsRequest;
import com.technokratos.dto.response.ListResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.service.SpecializationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class SpecializationController implements SpecializationApi {

    private final SpecializationService specializationService;

    @Override
    public ListResponse<SpecializationResponse> addSpecializationListToVacancy(ListSpecializationsRequest list, String hrId) {

        List<SpecializationResponse> data = specializationService.addList(list, hrId);

        return ListResponse.<SpecializationResponse>builder()
                .data(data)
                .build();
    }

    @Override
    public MessageResponse deleteSpecializationFromVacancy(ListOfIdsRequest list, String hrId) {

        specializationService.deleteList(list, hrId);

        return MessageResponse.builder()
                .message("Specializations successfully deleted from vacancy")
                .build();
    }
}
