package com.technokratos.controller;

import com.technokratos.api.VacancyApi;
import com.technokratos.dto.request.*;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.VacancyResponse;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.service.VacancyService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;


@RestController
@RequiredArgsConstructor
public class VacancyController implements VacancyApi {

    private final VacancyService vacancyService;

    @Override
    public VacancyResponse add(CreateVacancyRequest request, String hrId) {

        return vacancyService.save(request, hrId);
    }

    @Override
    public VacancyResponse update(UpdateVacancyRequest request, String hrId) {

        return vacancyService.update(request, hrId);
    }

    @Override
    public MessageResponse delete(RequestId request, String hrId) {

        UUID id = vacancyService.delete(request.getId(), hrId);

        return MessageResponse.builder()
                .message(String.format("Vacancy with id %s successfully deleted", id))
                .build();
    }

    @Override
    public VacancyResponse getById(UUID id) {

        return vacancyService.getById(id);
    }

    @Override
    public Page<VacancyResponse> getAll(Integer size, Integer number) {

        return vacancyService.getAll(size, number);
    }

    @Override
    public Page<VacancyResponse> getAllActive(Integer size, Integer number) {

        return vacancyService.getAllByStatus(VacancyEntity.Status.ACTIVE, size, number);
    }

    @Override
    public Page<VacancyResponse> getByCompany(UUID companyId, Integer size, Integer number) {

        return vacancyService.getByCompany(companyId, size, number);
    }

    @Override
    public Page<VacancyResponse> getByHr(String hrId, Integer size, Integer number) {

        return vacancyService.getByHr(UUID.fromString(hrId), size, number);
    }

    @Override
    public Page<VacancyResponse> getByFilter(VacancyFilter filter, Integer size, Integer number) {

        return vacancyService.searchByFilter(filter, size, number);
    }
}
