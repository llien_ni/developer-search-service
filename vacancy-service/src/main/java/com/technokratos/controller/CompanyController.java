package com.technokratos.controller;

import com.technokratos.api.CompanyApi;
import com.technokratos.dto.request.CreateCompanyRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateCompanyRequest;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.service.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CompanyController implements CompanyApi {

    private final CompanyService companyService;

    @Override
    public CompanyResponse add(CreateCompanyRequest companyRequest) {

        return companyService.save(companyRequest);
    }

    @Override
    public CompanyResponse update(UpdateCompanyRequest updateCompanyRequest, String hrId) {

        return companyService.update(updateCompanyRequest, hrId);
    }

    @Override
    public MessageResponse delete(RequestId request) {

        UUID id = companyService.delete(request.getId());
        return MessageResponse.builder()
                .message(String.format("Company with id %s successfully deleted", id))
                .build();
    }

    @Override
    public CompanyResponse getById(UUID id) {

        return companyService.getById(id);
    }

    @Override
    public Page<CompanyResponse> getConfirmedCompanies(Integer size, Integer number) {

        return companyService.getCompanySortByAndStatus("rating", CompanyEntity.Status.CONFIRMED, size, number);
    }

    @Override
    public Page<CompanyResponse> getAll(Integer size, Integer number) {

        return companyService.getAll(size, number);
    }

    @Override
    public CompanyResponse confirm(String token) {

        return companyService.confirmCompany(token);
    }
}
