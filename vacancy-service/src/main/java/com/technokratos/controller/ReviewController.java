package com.technokratos.controller;

import com.technokratos.api.ReviewApi;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.ReviewRequest;
import com.technokratos.dto.request.UpdateReviewRequest;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ReviewResponse;
import com.technokratos.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ReviewController implements ReviewApi {

    private final ReviewService reviewService;

    @Override
    public ReviewResponse add(ReviewRequest request, String authorId) {

        return reviewService.add(request, authorId);
    }

    @Override
    public ReviewResponse update(UpdateReviewRequest request, String authorId) {

        return reviewService.update(request, authorId);
    }

    @Override
    public MessageResponse delete(RequestId request, String authorId) {

        UUID id = reviewService.delete(request.getId(), authorId);

        return MessageResponse.builder()
                .message(String.format("Review with id %s successfully deleted", id))
                .build();
    }

    @Override
    public MessageResponse delete(RequestId request) {

        UUID id = reviewService.delete(request.getId());

        return MessageResponse.builder()
                .message(String.format("Review with id %s successfully deleted", id))
                .build();
    }

    @Override
    public ReviewResponse findById(UUID reviewId) {

        return reviewService.findById(reviewId);
    }

    @Override
    public Page<ReviewResponse> findByCompany(UUID companyId, Integer size, Integer number) {

        return reviewService.findByCompany(companyId, size, number);
    }

    @Override
    public Page<ReviewResponse> findAll(Integer size, Integer number) {

        return reviewService.findAll(size, number);
    }

    @Override
    public ReviewResponse findByCompanyAndAuthor(UUID companyId, String authorId, Integer size, Integer number) {

        return reviewService.findByCompanyAndAuthor(companyId, authorId);
    }
}
