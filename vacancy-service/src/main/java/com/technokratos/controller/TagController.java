package com.technokratos.controller;

import com.technokratos.api.TagApi;
import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListTagsRequest;
import com.technokratos.dto.response.ListResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.TagResponse;
import com.technokratos.service.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;


@RestController
@RequiredArgsConstructor
public class TagController implements TagApi {

    private final TagService tagService;

    @Override
    public ListResponse<TagResponse> addTagListToVacancy(ListTagsRequest list, String hrId) {

        List<TagResponse> data = tagService.addList(list, hrId);

        return ListResponse.<TagResponse>builder()
                .data(data)
                .build();
    }

    @Override
    public MessageResponse deleteTagsFromVacancy(ListOfIdsRequest list, String hrId) {

        tagService.deleteList(list, hrId);

        return MessageResponse.builder()
                .message(String.format("Tags successfully deleted from vacancy %s", list.getVacancyId()))
                .build();
    }
}
