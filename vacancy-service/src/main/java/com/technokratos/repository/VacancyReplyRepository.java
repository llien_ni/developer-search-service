package com.technokratos.repository;

import com.technokratos.entity.VacancyEntity;
import com.technokratos.entity.VacancyReplyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface VacancyReplyRepository extends JpaRepository<VacancyReplyEntity, UUID> {

    Page<VacancyReplyEntity> findAllByDeveloperId(UUID developerId, Pageable pageable);

    Page<VacancyReplyEntity> findAllByVacancy_HrId(UUID hrId, Pageable pageable);

    Page<VacancyReplyEntity> findAllByStateAndVacancy_HrId(VacancyReplyEntity.State state, UUID hrId, Pageable pageable);

    Page<VacancyReplyEntity> findAllByVacancy(VacancyEntity vacancy, Pageable pageable);

    Optional<VacancyReplyEntity> findByVacancyAndDeveloperId(VacancyEntity vacancy, UUID developerId);

    Optional<VacancyReplyEntity> findByIdAndDeveloperId(UUID replyId, UUID developerId);
}
