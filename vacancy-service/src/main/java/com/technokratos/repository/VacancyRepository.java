package com.technokratos.repository;


import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.VacancyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.UUID;

@Repository
public interface VacancyRepository extends JpaRepository<VacancyEntity, UUID>, JpaSpecificationExecutor<VacancyEntity> {

    Page<VacancyEntity> findVacancyEntitiesByCompanyAndStatus(CompanyEntity company, VacancyEntity.Status status, Pageable pageable);

    Page<VacancyEntity> findVacancyEntitiesByHrId(UUID hrId, Pageable pageable);

    Page<VacancyEntity> findAllByStatus(VacancyEntity.Status status, Pageable pageable);

    List<VacancyEntity> findAllByStatus(VacancyEntity.Status status, Specification<VacancyEntity> specification, Sort sort);

}
