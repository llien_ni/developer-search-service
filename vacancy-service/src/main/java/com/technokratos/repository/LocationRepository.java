package com.technokratos.repository;

import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.LocationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LocationRepository extends JpaRepository<LocationEntity, UUID> {

    Page<LocationEntity> findAllByCompany(CompanyEntity company, Pageable pageable);
}
