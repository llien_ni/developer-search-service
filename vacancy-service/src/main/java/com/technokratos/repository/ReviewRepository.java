package com.technokratos.repository;

import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.ReviewEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ReviewRepository extends JpaRepository<ReviewEntity, UUID> {

    Optional<ReviewEntity> findByCompanyAndAuthorId(CompanyEntity company, UUID authorId);

    Page<ReviewEntity> findByCompany(CompanyEntity company, Pageable pageable);

    Optional<ReviewEntity> findByIdAndAuthorId(UUID reviewId, UUID authorId);
}
