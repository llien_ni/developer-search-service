package com.technokratos.repository;

import com.technokratos.entity.CompanyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CompanyRepository extends JpaRepository<CompanyEntity, UUID> {

    Page<CompanyEntity> findAllByStatus(CompanyEntity.Status status, Pageable pageable);

    Optional<CompanyEntity> findByConfirmationToken(UUID confirmationToken);

    Optional<CompanyEntity> findByName(String name);

    Optional<CompanyEntity> findByNameAndStatus(String name, CompanyEntity.Status status);

    List<CompanyEntity> findAllByStatus(CompanyEntity.Status status);
}
