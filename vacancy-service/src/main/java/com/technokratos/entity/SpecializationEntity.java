package com.technokratos.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.persistence.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "specialization")
public class SpecializationEntity extends AbstractBaseEntity {

    @Column(name = "specialization")
    private String specialization;

    @ManyToOne
    @JoinColumn(name = "vacancy_id", referencedColumnName = "id")
    private VacancyEntity vacancy;
}
