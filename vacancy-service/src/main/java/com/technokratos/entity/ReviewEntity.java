package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "review")
public class ReviewEntity extends AbstractBaseEntity{

    private String text;

    private Integer evaluation;

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;

    @Column(name = "updated_date")
    @UpdateTimestamp
    private OffsetDateTime updateDate;

    @Column(name = "author_id")
    private UUID authorId;

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    private CompanyEntity company;
}
