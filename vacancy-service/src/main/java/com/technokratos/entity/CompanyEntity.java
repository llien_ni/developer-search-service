package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "company")
public class CompanyEntity extends AbstractBaseEntity {

    public enum Status {
        NOT_CONFIRMED, CONFIRMED, MODERATED, DELETED, BLOCKED
    }

    private String name;

    private String description;

    @Column(name = "website_link")
    private String websiteLink;

    private String email;

    private String type;

    @Enumerated(value = EnumType.STRING)
    private Status status;

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;

    @Column(name = "updated_date")
    @UpdateTimestamp
    private OffsetDateTime updateDate;

    @Column(name = "updated_by")
    private UUID updatedBy;

    @Column(name = "confirmation_token")
    private UUID confirmationToken;

    @Column(name = "rating")
    private BigDecimal rating;

    @OneToMany(mappedBy = "company", cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<VacancyEntity> vacancies;

    @OneToMany(mappedBy = "company", cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<LocationEntity> locations;

    @OneToMany(mappedBy = "company", cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<ReviewEntity> reviews;
}
