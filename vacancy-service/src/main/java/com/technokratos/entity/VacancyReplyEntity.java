package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "vacancy_reply")
public class VacancyReplyEntity extends AbstractBaseEntity {

    public enum State {
        WAIT, REJECTED, VIEWED
    }

    @Column(name = "account_id")
    private UUID developerId;

    @ManyToOne
    @JoinColumn(name = "vacancy_id", referencedColumnName = "id")
    private VacancyEntity vacancy;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;
}
