package com.technokratos.exception;

public class ReviewNotExistException  extends BadRequestException{

    private static final String MESSAGE = "Review does not exist";

    public ReviewNotExistException() {
        super(MESSAGE);
    }
}
