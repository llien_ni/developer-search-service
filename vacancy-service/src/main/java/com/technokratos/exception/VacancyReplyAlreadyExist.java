package com.technokratos.exception;

import java.util.UUID;

public class VacancyReplyAlreadyExist extends BadRequestException {

    private static final String MESSAGE = "Developer already responded to the vacancy %s";

    public VacancyReplyAlreadyExist(UUID id) {

        super(String.format(MESSAGE, id));
    }
}
