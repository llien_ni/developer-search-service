package com.technokratos.exception;

public class LocationNotFoundException extends NotFoundException{

    private static final String MESSAGE = "Location not found";

    public LocationNotFoundException() {
        super(MESSAGE);
    }
}
