package com.technokratos.exception;

import java.util.UUID;

public class ReviewNotFoundException extends NotFoundException{

    private static final String MESSAGE = "Review not found";

    private static final String MESSAGE_NOT_FOUND_IN_SUBMITTED_REVIEWS = "User %s did not send a review %s";

    public ReviewNotFoundException() {
        super(MESSAGE);
    }

    public ReviewNotFoundException(UUID reviewId, String authorId) {

        super(String.format(MESSAGE_NOT_FOUND_IN_SUBMITTED_REVIEWS, authorId, reviewId));
    }
}
