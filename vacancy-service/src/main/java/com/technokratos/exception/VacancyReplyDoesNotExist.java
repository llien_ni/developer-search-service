package com.technokratos.exception;

public class VacancyReplyDoesNotExist extends BadRequestException{

    private static final String MESSAGE = "Reply does not exist";

    public VacancyReplyDoesNotExist() {

        super(MESSAGE);
    }
}
