package com.technokratos.exception;

public class VacancyReplyNotFoundException extends NotFoundException {

    private static final String MESSAGE = "Vacancy reply not found.";

    public VacancyReplyNotFoundException() {
        super(MESSAGE);
    }
}
