package com.technokratos.exception;

import java.util.UUID;

public class AccountAlreadySentReviewToCompany extends BadRequestException{

    private static final String MESSAGE = "Account %s already sent review to a company %s";

    public AccountAlreadySentReviewToCompany(UUID companyId, String authorId) {

        super(String.format(MESSAGE, companyId, authorId));
    }
}
