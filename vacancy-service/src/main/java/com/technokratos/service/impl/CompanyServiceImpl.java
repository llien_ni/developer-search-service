package com.technokratos.service.impl;

import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.client.NotificationServiceGrpcClient;
import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.dto.request.CreateCompanyRequest;
import com.technokratos.dto.request.UpdateCompanyRequest;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.exception.CompaniesNotMatchException;
import com.technokratos.exception.CompanyAlreadyExistException;
import com.technokratos.exception.CompanyNotFoundException;
import com.technokratos.mapper.CompanyMapper;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.VacancyRepository;
import com.technokratos.service.CompanyService;
import com.technokratos.util.RequestParamUtil;
import hr.service.HrServiceOuterClass;
import lombok.RequiredArgsConstructor;
import notification.service.NotificationServiceOuterClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import static com.technokratos.util.Constants.VACANCY_SERVICE_URL;

@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    private final RequestParamUtil requestParamUtil;

    private final VacancyRepository vacancyRepository;

    private final CompanyMapper companyMapper;

    private final NotificationServiceGrpcClient notificationServiceGrpcClient;

    private final AuthorizationServiceGrpcClient hrServiceGrpcClient;

    private final NotificationServiceRabbit notificationServiceRabbit;

    @Transactional(readOnly = true)
    @Override
    public Page<CompanyResponse> getCompanySortByAndStatus(String sortBy, CompanyEntity.Status notConfirmedStatus, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, sortBy);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<CompanyEntity> companies = companyRepository.findAllByStatus(notConfirmedStatus, pageRequest.withSort(sort));
        return companies.map(companyMapper::fromEntityToResponse);
    }

    @Transactional
    @Override
    public CompanyResponse updateStatus(UUID id, CompanyEntity.Status status) {

        CompanyEntity companyEntity = companyRepository.findById(id)
                .orElseThrow(CompanyNotFoundException::new);
        companyEntity.setStatus(status);

        return companyMapper.fromEntityToResponse(companyRepository.save(companyEntity));
    }

    @Transactional
    @Override
    public CompanyResponse save(CreateCompanyRequest companyRequest) {

        checkCompanyWithThisNameIsNotExist(companyRequest);
        CompanyEntity companyEntity = companyMapper.fromRequestToEntity(companyRequest);
        companyEntity.setStatus(CompanyEntity.Status.NOT_CONFIRMED);
        companyEntity.setConfirmationToken(UUID.randomUUID());
        companyEntity.setRating(BigDecimal.ZERO);
        companyEntity.setVacancies(new ArrayList<>());
        companyEntity.setReviews(new ArrayList<>());

        CompanyEntity savedEntity = companyRepository.save(companyEntity);

        NotificationServiceOuterClass.MailMessageRequest mailMessageRequest = NotificationServiceOuterClass.MailMessageRequest
                .newBuilder()
                .setTo(companyEntity.getEmail())
                .setSubject("Confirm company registration")
                .setMessage(VACANCY_SERVICE_URL + "/company/confirm?token=" + companyEntity.getConfirmationToken().toString())
                .build();
        notificationServiceGrpcClient.sendMail(mailMessageRequest);

        return companyMapper.fromEntityToResponse(savedEntity);
    }

    private void checkCompanyWithThisNameIsNotExist(CreateCompanyRequest companyRequest) {

        Optional<CompanyEntity> company = companyRepository
                .findByNameAndStatus(companyRequest.getName(), CompanyEntity.Status.CONFIRMED);
        if(company.isEmpty()){
            company = companyRepository
                    .findByNameAndStatus(companyRequest.getName(), CompanyEntity.Status.MODERATED);
        }
        if(company.isPresent()){
            throw new CompanyAlreadyExistException();
        }
    }

    @Transactional
    @Override
    public CompanyResponse update(UpdateCompanyRequest updateCompanyRequest, String hrId) {

        checkHrCompany(hrId, updateCompanyRequest.getId());
        CompanyEntity companyEntity = companyRepository.findById(updateCompanyRequest.getId())
                .orElseThrow(CompanyNotFoundException::new);
        companyMapper.update(updateCompanyRequest, companyEntity);
        companyEntity.setUpdatedBy(UUID.fromString(hrId));

        companyEntity.setStatus(CompanyEntity.Status.MODERATED);

        return companyMapper.fromEntityToResponse(companyRepository.save(companyEntity));
    }

    @Override
    public void checkHrCompany(String hrId, UUID companyId) {

        HrServiceOuterClass.HrResponse hrResponse = hrServiceGrpcClient
                .getHrById(HrServiceOuterClass
                        .GetHrByIdRequest
                        .newBuilder()
                        .setId(hrId)
                        .build());

        if (!companyId.equals(UUID.fromString(hrResponse.getCompanyId()))) {
            throw new CompaniesNotMatchException(companyId, hrResponse.getCompanyId());
        }
    }

    @Transactional
    @Override
    public UUID delete(UUID id) {

        CompanyEntity companyEntity = companyRepository.findById(id)
                .orElseThrow(CompanyNotFoundException::new);
        companyEntity.setStatus(CompanyEntity.Status.DELETED);
        vacancyRepository.deleteAll(companyEntity.getVacancies());
        companyRepository.save(companyEntity);

        return companyEntity.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public CompanyResponse getById(UUID id) {

        CompanyEntity companyEntity = companyRepository.findById(id)
                .orElseThrow(CompanyNotFoundException::new);
        return companyMapper.fromEntityToResponse(companyEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<CompanyResponse> getAll(Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "rating");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        pageRequest.withSort(sort);
        Page<CompanyEntity> companies = companyRepository.findAll(pageRequest);
        return companies.map(companyMapper::fromEntityToResponse);
    }

    @Transactional
    @Override
    public CompanyResponse confirmCompany(String token) {

        CompanyEntity companyEntity = companyRepository
                .findByConfirmationToken(UUID.fromString(token))
                .orElseThrow(CompanyNotFoundException::new);
        companyEntity.setStatus(CompanyEntity.Status.MODERATED);

        return companyMapper.fromEntityToResponse(companyRepository.save(companyEntity));
    }

    @Transactional(readOnly = true)
    @Override
    public CompanyResponse getByName(String companyName) {

        CompanyEntity companyEntity = companyRepository
                .findByName(companyName)
                .orElseThrow(() -> new CompanyNotFoundException(companyName));

        return companyMapper.fromEntityToResponse(companyEntity);
    }

    @Transactional
    @Override
    public CompanyResponse updateCompanyStatusToConfirmed(UUID id) {

        CompanyResponse companyResponse = updateStatus(id, CompanyEntity.Status.CONFIRMED);
        notificationServiceRabbit.sentMailMessageThatCompanyIsConfirmed(companyResponse);
        return companyResponse;
    }

    @Transactional
    @Override
    public CompanyResponse updateCompanyStatusToBlocked(UUID id) {

        CompanyResponse companyResponse = updateStatus(id, CompanyEntity.Status.BLOCKED);
        notificationServiceRabbit.sentMailMessageThatCompanyNotPassedModeration(companyResponse);
        return companyResponse;
    }
}
