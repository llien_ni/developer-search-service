package com.technokratos.service.impl;

import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.dto.request.CreateVacancyRequest;
import com.technokratos.dto.request.VacancyFilter;
import com.technokratos.dto.request.UpdateVacancyRequest;
import com.technokratos.dto.response.VacancyResponse;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.exception.*;
import com.technokratos.mapper.VacancyMapper;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.VacancyRepository;
import com.technokratos.service.CompanyService;
import com.technokratos.service.VacancyService;
import com.technokratos.util.RequestParamUtil;
import hr.service.HrServiceOuterClass;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class VacancyServiceImpl implements VacancyService {

    private final VacancyRepository vacancyRepository;

    private final RequestParamUtil requestParamUtil;

    private final VacancyMapper vacancyMapper;

    private final CompanyRepository companyRepository;

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    private final CompanyService companyService;

    @Transactional
    @Override
    public VacancyResponse save(CreateVacancyRequest vacancyRequest, String hrId) {

        VacancyEntity vacancyEntity = vacancyMapper.fromRequestToEntity(vacancyRequest);

        HrServiceOuterClass.HrResponse hrResponse = authorizationServiceGrpcClient
                .getHrById(HrServiceOuterClass
                        .GetHrByIdRequest
                        .newBuilder()
                        .setId(hrId)
                        .build());
        CompanyEntity companyEntity = companyRepository.findById(UUID.fromString(hrResponse.getCompanyId()))
                .orElseThrow(CompanyNotFoundException::new);

        vacancyEntity.setHrId(UUID.fromString(hrResponse.getId()));
        vacancyEntity.setCompany(companyEntity);

        setDefaultVacancyStatus(vacancyEntity, vacancyRequest);

        vacancyEntity.setSpecializations(new ArrayList<>());
        vacancyEntity.setTags(new ArrayList<>());
        vacancyEntity.setReplies(new ArrayList<>());

        VacancyEntity savedEntity = vacancyRepository.save(vacancyEntity);

        companyEntity.getVacancies().add(savedEntity);
        companyRepository.save(companyEntity);

        return vacancyMapper.fromEntityToResponse(savedEntity);
    }

    private void setDefaultVacancyStatus(VacancyEntity vacancyEntity, CreateVacancyRequest vacancyRequest) {

        if (vacancyRequest.getStatus() != null) {
            try {
                vacancyEntity.setStatus(VacancyEntity.Status.valueOf(vacancyRequest.getStatus()));
            } catch (IllegalArgumentException e) {
                throw new BadRequestException("Entered invalid data");
            }
        } else {
            vacancyEntity.setStatus(VacancyEntity.Status.ACTIVE);
        }
    }

    @Transactional
    @Override
    public VacancyResponse update(UpdateVacancyRequest request, String hrId) {

        VacancyEntity vacancyEntity = vacancyRepository
                .findById(request.getId())
                .orElseThrow(VacancyNotFoundException::new);

        companyService.checkHrCompany(hrId, vacancyEntity.getCompany().getId());
        vacancyMapper.update(request, vacancyEntity);

        return vacancyMapper.fromEntityToResponse(vacancyRepository.save(vacancyEntity));
    }

    @Transactional
    @Override
    public UUID delete(UUID id, String hrId) {

        VacancyEntity vacancyEntity = vacancyRepository.findById(id).orElseThrow(VacancyNotFoundException::new);
        companyService.checkHrCompany(hrId, vacancyEntity.getCompany().getId());
        CompanyEntity company = vacancyEntity.getCompany();
        company.getVacancies().remove(vacancyEntity);
        vacancyRepository.delete(vacancyEntity);
        companyRepository.save(company);

        return vacancyEntity.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public VacancyResponse getById(UUID id) {

        VacancyEntity vacancyEntity = vacancyRepository.findById(id).orElseThrow(VacancyNotFoundException::new);
        return vacancyMapper.fromEntityToResponse(vacancyEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VacancyResponse> getAll(Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<VacancyEntity> vacancies = vacancyRepository.findAll(pageRequest.withSort(sort));

        return vacancies.map(vacancyMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VacancyResponse> getByCompany(UUID companyId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        CompanyEntity companyEntity = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<VacancyEntity> vacancies = vacancyRepository.findVacancyEntitiesByCompanyAndStatus(companyEntity, VacancyEntity.Status.ACTIVE, pageRequest.withSort(sort));

        return vacancies.map(vacancyMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VacancyResponse> getByHr(UUID hrId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<VacancyEntity> vacancies = vacancyRepository.findVacancyEntitiesByHrId(hrId, pageRequest.withSort(sort));

        return vacancies.map(vacancyMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VacancyResponse> getAllByStatus(VacancyEntity.Status status, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<VacancyEntity> vacancies = vacancyRepository.findAllByStatus(status, pageRequest.withSort(sort));
        return vacancies.map(vacancyMapper::fromEntityToResponse);
    }


    @Transactional(readOnly = true)
    @Override
    public Page<VacancyResponse> searchByFilter(VacancyFilter filter, Integer size, Integer number) {

        Specification<VacancyEntity> specification = Specification.where(byCity(filter.getCity()))
                .and(byRemote(filter.getRemote()))
                .and(bySalary(filter.getMinSalary(), filter.getMaxSalary()))
                .and(byTags(filter.getTags()))
                .and(bySpecializations(filter.getSpecializations()))
                .and(byCreatedDate(filter.getCreatedDate()))
                .and(byStatusActive());

        Sort sort = createSort(filter);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);

        Page<VacancyEntity> page = vacancyRepository.findAll(specification, pageRequest.withSort(sort));

        return page.map(vacancyMapper::fromEntityToResponse);
    }

    private Specification<VacancyEntity> byCreatedDate(LocalDate createdDate) {

        return (root, query, criterianBuilder) -> {

            if (nonNull(createdDate)) {

                return criterianBuilder.equal(root.get("createdDate").as(LocalDate.class), createdDate);
            }
            return null;
        };
    }

    private Specification<VacancyEntity> bySpecializations(List<String> specializations) {

        return (root, query, criteriaBuilder) -> {

            if (specializations != null && !specializations.isEmpty()) {
                query.distinct(true);
                return criteriaBuilder.and(root.join("specializations").get("specialization").in(specializations));

            }
            return null;
        };

    }

    private Specification<VacancyEntity> byTags(List<String> tags) {

        return (root, query, criteriaBuilder) -> {
            if (tags != null && !tags.isEmpty()) {

                query.distinct(true);
                return criteriaBuilder.and(root.join("tags").get("tag").in(tags));

            }
            return null;
        };
    }

    private Sort createSort(VacancyFilter filter) {

        Sort.Direction sortType;
        if (filter.getSortType() == null) {
            sortType = Sort.Direction.DESC;

        } else {
            sortType = Sort.Direction.fromString(filter.getSortType());
        }
        String sortBy = filter.getSortBy();
        if (filter.getSortBy() == null) {
            sortBy = "createdDate";
        }
        return Sort.by(sortType, sortBy);
    }

    public Specification<VacancyEntity> byCity(String city) {

        return (root, query, criterianBuilder) -> {
            if (nonNull(city)) {
                return criterianBuilder.equal(root.get("city"), city);
            }
            return null;
        };
    }

    private Specification<VacancyEntity> byStatusActive() {

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("status"), VacancyEntity.Status.ACTIVE);
    }

    public Specification<VacancyEntity> byRemote(Boolean remote) {

        return (root, query, criteriaBuilder) -> {
            if (nonNull(remote)) {
                return criteriaBuilder.equal(root.get("remote"), remote);
            }
            return null;
        };
    }

    public Specification<VacancyEntity> bySalary(Integer minSalary, Integer maxSalary) {

        return (root, query, criteriaBuilder) -> {
            Specification<VacancyEntity> specification = Specification.where(null);

            if (nonNull(minSalary)) {
                specification = specification.and((Specification<VacancyEntity>)
                        (root1, query1, criteriaBuilder1) ->
                                criteriaBuilder1.greaterThanOrEqualTo(root1.get("salary"), String.valueOf(minSalary)));

            }
            if (nonNull(maxSalary)) {
                specification = specification.and((Specification<VacancyEntity>)
                        (root12, query12, criteriaBuilder12) ->
                                criteriaBuilder12.lessThanOrEqualTo(root12.get("salary"), String.valueOf(maxSalary)));
            }
            return specification.toPredicate(root, query, criteriaBuilder);
        };
    }
}
