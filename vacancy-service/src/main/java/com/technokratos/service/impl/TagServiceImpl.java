package com.technokratos.service.impl;

import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListTagsRequest;
import com.technokratos.dto.response.TagResponse;
import com.technokratos.entity.TagEntity;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.exception.TagNotExistException;
import com.technokratos.exception.VacancyNotFoundException;
import com.technokratos.mapper.TagMapper;
import com.technokratos.repository.TagRepository;
import com.technokratos.repository.VacancyRepository;
import com.technokratos.service.CompanyService;
import com.technokratos.service.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    private final CompanyService companyService;

    private final VacancyRepository vacancyRepository;

    private final TagMapper tagMapper;

    @Transactional
    @Override
    public List<TagResponse> addList(ListTagsRequest listTagsRequest, String hrId) {

        VacancyEntity vacancyEntity = vacancyRepository
                .findById(listTagsRequest.getVacancyId())
                .orElseThrow(VacancyNotFoundException::new);

        companyService.checkHrCompany(hrId, vacancyEntity.getCompany().getId());

        List<TagEntity> tagsEntities = tagMapper.fromStringToEntity(listTagsRequest.getTags())
                .stream()
                .map(tagEntity -> {
                    tagEntity.setTag(tagEntity.getTag().toLowerCase(Locale.ROOT));
                    tagEntity.setVacancy(vacancyEntity);
                    tagRepository.save(tagEntity);
                    return tagEntity;
                }).collect(Collectors.toList());

        vacancyEntity.getTags().addAll(tagsEntities);
        vacancyRepository.save(vacancyEntity);
        return tagMapper.fromEntityToResponse(tagsEntities);
    }

    @Transactional
    @Override
    public void deleteList(ListOfIdsRequest list, String hrId) {

        VacancyEntity vacancyEntity = vacancyRepository
                .findById(list.getVacancyId())
                .orElseThrow(VacancyNotFoundException::new);

        companyService.checkHrCompany(hrId, vacancyEntity.getCompany().getId());

        List<UUID> listVacancyTags = vacancyEntity.getTags()
                .stream()
                .map(TagEntity::getId)
                .toList();

        list.getIds().forEach(id -> {
            if(!listVacancyTags.contains(id)){
                throw new TagNotExistException(id);
            }
            TagEntity tag = tagRepository.getReferenceById(id);

            tagRepository.delete(tag);
            vacancyEntity.getTags().remove(tag);
        });
        vacancyRepository.save(vacancyEntity);
    }
}
