package com.technokratos.service.impl;

import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListSpecializationsRequest;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.entity.SpecializationEntity;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.exception.SpecializationNotExistException;
import com.technokratos.exception.VacancyNotFoundException;
import com.technokratos.mapper.SpecializationMapper;
import com.technokratos.repository.SpecializationRepository;
import com.technokratos.repository.VacancyRepository;
import com.technokratos.service.CompanyService;
import com.technokratos.service.SpecializationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpecializationServiceImpl implements SpecializationService {

    private final SpecializationRepository specializationRepository;

    private final CompanyService companyService;

    private final VacancyRepository vacancyRepository;

    private final SpecializationMapper specializationMapper;

    @Transactional
    @Override
    public List<SpecializationResponse> addList(ListSpecializationsRequest list, String hrId) {

        VacancyEntity vacancyEntity = vacancyRepository
                .findById(list.getVacancyId())
                .orElseThrow(VacancyNotFoundException::new);

        companyService.checkHrCompany(hrId, vacancyEntity.getCompany().getId());

        List<SpecializationEntity> specializationsEntities = specializationMapper
                .fromStringToSpecializationEntity(list.getSpecializations())
                .stream()
                .map(specializationEntity -> {
                    specializationEntity.setVacancy(vacancyEntity);
                    specializationRepository.save(specializationEntity);
                    vacancyEntity.getSpecializations().add(specializationEntity);
                    return specializationEntity;
                })
                .collect(Collectors.toList());

        vacancyRepository.save(vacancyEntity);
        return specializationMapper.fromEntityToResponse(specializationsEntities);
    }

    @Transactional
    @Override
    public void deleteList(ListOfIdsRequest list, String hrId) {

        VacancyEntity vacancyEntity = vacancyRepository
                .findById(list.getVacancyId())
                .orElseThrow(VacancyNotFoundException::new);

        companyService.checkHrCompany(hrId, vacancyEntity.getCompany().getId());

        List<UUID> listProjectSpecializations = vacancyEntity.getSpecializations()
                .stream()
                .map(SpecializationEntity::getId).toList();

        list.getIds().forEach(id -> {
            if (!listProjectSpecializations.contains(id)) {
                throw new SpecializationNotExistException(id);
            }
            SpecializationEntity specializationEntity = specializationRepository.getReferenceById(id);

            specializationRepository.delete(specializationEntity);
            vacancyEntity.getSpecializations().remove(specializationEntity);
        });
        vacancyRepository.save(vacancyEntity);
    }
}
