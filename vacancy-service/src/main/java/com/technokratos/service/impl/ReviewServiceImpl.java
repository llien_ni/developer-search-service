package com.technokratos.service.impl;

import authorization.service.DeveloperServiceOuterClass;
import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.dto.request.ReviewRequest;
import com.technokratos.dto.request.UpdateReviewRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.ReviewResponse;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.ReviewEntity;
import com.technokratos.exception.AccountAlreadySentReviewToCompany;
import com.technokratos.exception.CompanyNotFoundException;
import com.technokratos.exception.ReviewNotExistException;
import com.technokratos.exception.ReviewNotFoundException;
import com.technokratos.mapper.ReviewMapper;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.ReviewRepository;
import com.technokratos.service.ReviewService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static com.technokratos.converter.DeveloperConverter.convertFromDeveloperResponseGrpc;


@Service
@RequiredArgsConstructor
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;

    private final CompanyRepository companyRepository;

    private final ReviewMapper reviewMapper;

    private final RequestParamUtil requestParamUtil;

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    @Transactional
    @Override
    public ReviewResponse add(ReviewRequest request, String authorId) {

        CompanyEntity company = companyRepository.findById(request.getCompanyId())
                .orElseThrow(CompanyNotFoundException::new);
        checkAccountNotSentReviewToCompany(company, authorId);
        ReviewEntity review = reviewMapper.fromRequestToEntity(request);
        review.setCompany(company);
        review.setAuthorId(UUID.fromString(authorId));

        ReviewEntity savedReview = reviewRepository.save(review);
        company.getReviews().add(savedReview);
        companyRepository.save(company);

        return reviewMapper.fromEntityToResponse(savedReview);
    }

    @Transactional
    @Override
    public ReviewResponse update(UpdateReviewRequest request, String authorId) {

        ReviewEntity review = reviewRepository.findByIdAndAuthorId(request.getId(), UUID.fromString(authorId))
                .orElseThrow(ReviewNotFoundException::new);
        reviewMapper.update(request, review);

        return reviewMapper.fromEntityToResponse(reviewRepository.save(review));
    }

    @Transactional
    @Override
    public UUID delete(UUID reviewId, String authorId) {

        ReviewEntity review = reviewRepository.findByIdAndAuthorId(reviewId, UUID.fromString(authorId))
                .orElseThrow(ReviewNotFoundException::new);
        CompanyEntity company = review.getCompany();
        company.getReviews().remove(review);
        reviewRepository.delete(review);
        companyRepository.save(company);

        return review.getId();
    }

    @Transactional
    @Override
    public UUID delete(UUID reviewId) {

        ReviewEntity review = reviewRepository.findById(reviewId)
                .orElseThrow(ReviewNotFoundException::new);
        CompanyEntity company = review.getCompany();
        company.getReviews().remove(review);
        reviewRepository.delete(review);
        companyRepository.save(company);

        return review.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public ReviewResponse findById(UUID reviewId) {

        ReviewEntity reviewEntity = reviewRepository
                .findById(reviewId)
                .orElseThrow(ReviewNotFoundException::new);
        return convertToReviewResponseWithDeveloper(reviewEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ReviewResponse> findByCompany(UUID companyId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        CompanyEntity companyEntity = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        Page<ReviewEntity> page = reviewRepository.findByCompany(companyEntity, pageRequest.withSort(sort));

        return page.map(this::convertToReviewResponseWithDeveloper);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ReviewResponse> findAll(Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<ReviewEntity> page = reviewRepository.findAll(pageRequest.withSort(sort));
        return page.map(this::convertToReviewResponseWithDeveloper);
    }

    @Transactional(readOnly = true)
    @Override
    public ReviewResponse findByCompanyAndAuthor(UUID companyId, String authorId) {

        CompanyEntity companyEntity = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        ReviewEntity reviewEntity = reviewRepository
                .findByCompanyAndAuthorId(companyEntity, UUID.fromString(authorId))
                .orElseThrow(ReviewNotExistException::new);
        return convertToReviewResponseWithDeveloper(reviewEntity);
    }

    private void checkAccountNotSentReviewToCompany(CompanyEntity company, String authorId) {

        Optional<ReviewEntity> review = reviewRepository.findByCompanyAndAuthorId(company, UUID.fromString(authorId));
        if (review.isPresent()) {
            throw new AccountAlreadySentReviewToCompany(company.getId(), authorId);
        }
    }

    private ReviewResponse convertToReviewResponseWithDeveloper(ReviewEntity review) {

        ReviewResponse response = reviewMapper.fromEntityToResponse(review);
        DeveloperResponse developer = getDeveloperResponse(review.getId().toString());
        response.setDeveloperResponse(developer);
        return response;
    }

    private DeveloperResponse getDeveloperResponse(String developerId) {

        DeveloperServiceOuterClass.DeveloperResponse developerResponse = authorizationServiceGrpcClient.getDeveloperById(DeveloperServiceOuterClass.GetDeveloperByIdRequest
                .newBuilder()
                .setId(developerId)
                .build());

        return convertFromDeveloperResponseGrpc(developerResponse);
    }
}
