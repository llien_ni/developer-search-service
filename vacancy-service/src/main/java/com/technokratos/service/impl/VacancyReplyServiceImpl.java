package com.technokratos.service.impl;

import authorization.service.DeveloperServiceOuterClass;
import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.VacancyReplyResponse;
import com.technokratos.entity.VacancyEntity;
import com.technokratos.entity.VacancyReplyEntity;
import com.technokratos.exception.*;
import com.technokratos.mapper.VacancyReplyMapper;
import com.technokratos.repository.VacancyRepository;
import com.technokratos.repository.VacancyReplyRepository;
import com.technokratos.service.VacancyReplyService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;
import static com.technokratos.converter.DeveloperConverter.convertFromDeveloperResponseGrpc;


@Service
@RequiredArgsConstructor
public class VacancyReplyServiceImpl implements VacancyReplyService {

    private final VacancyRepository vacancyRepository;

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    private final NotificationServiceRabbit notificationServiceRabbit;

    private final VacancyReplyRepository vacancyReplyRepository;

    private final RequestParamUtil requestParamUtil;

    private final VacancyReplyMapper vacancyReplyMapper;

    @Transactional
    @Override
    public VacancyReplyResponse replyToVacancy(UUID vacancyId, String developerId) {

        VacancyEntity vacancyEntity = vacancyRepository.
                findById(vacancyId)
                .orElseThrow(VacancyNotFoundException::new);

        checkReplyIsNotExist(developerId, vacancyEntity);

        VacancyReplyEntity vacancyRequest = VacancyReplyEntity.builder()
                .vacancy(vacancyEntity)
                .developerId(UUID.fromString(developerId))
                .state(VacancyReplyEntity.State.WAIT)
                .build();
        VacancyReplyEntity savedVacancyReply = vacancyReplyRepository.save(vacancyRequest);
        vacancyEntity.getReplies().add(savedVacancyReply);
        vacancyRepository.save(vacancyEntity);

        notificationServiceRabbit.sendMailMessageToHr(developerId, vacancyEntity.getHrId(), vacancyId);
        notificationServiceRabbit.sendMailMessageToDeveloper(developerId, vacancyId);

        return vacancyReplyMapper.fromEntityToResponse(savedVacancyReply);
    }

    private void checkReplyIsNotExist(String developerId, VacancyEntity vacancyEntity) {

        Optional<VacancyReplyEntity> vacancy = vacancyReplyRepository.findByVacancyAndDeveloperId(vacancyEntity, UUID.fromString(developerId));
        if(vacancy.isPresent()){
            throw new VacancyReplyAlreadyExist(vacancy.get().getId());
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VacancyReplyResponse> getAllRepliesByDeveloper(String developerId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<VacancyReplyEntity> replies = vacancyReplyRepository.findAllByDeveloperId(UUID.fromString(developerId), pageRequest.withSort(sort));

        return replies.map(vacancyReplyMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VacancyReplyResponse> getAllRepliesForHr(String hrId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<VacancyReplyEntity> replies = vacancyReplyRepository.findAllByVacancy_HrId(UUID.fromString(hrId), pageRequest.withSort(sort));

        return replies.map(this::convertToReplyResponseWithDeveloper);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VacancyReplyResponse> getAllRepliesByStateForHr(String hrId, String state, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<VacancyReplyEntity> replies;
        try {
            replies = vacancyReplyRepository.findAllByStateAndVacancy_HrId(VacancyReplyEntity.State.valueOf(state), UUID.fromString(hrId), pageRequest);
        } catch (IllegalArgumentException e) {
            throw new BadRequestException("State is invalid");
        }
        return replies.map(this::convertToReplyResponseWithDeveloper);
    }

    @Transactional
    @Override
    public UUID deleteReply(UUID replyId, String developerId) {

        VacancyReplyEntity reply = vacancyReplyRepository.findByIdAndDeveloperId(replyId, UUID.fromString(developerId)).orElseThrow(VacancyReplyDoesNotExist::new);
        VacancyEntity vacancy = reply.getVacancy();
        vacancy.getReplies().remove(reply);
        vacancyRepository.save(vacancy);
        vacancyReplyRepository.delete(reply);
        return replyId;
    }

    private VacancyReplyResponse convertToReplyResponseWithDeveloper(VacancyReplyEntity reply) {

        VacancyReplyResponse response = vacancyReplyMapper.fromEntityToResponse(reply);
        DeveloperResponse developer = getDeveloperResponse(reply.getDeveloperId().toString());
        response.setDeveloperResponse(developer);
        return response;
    }

    private DeveloperResponse getDeveloperResponse(String developerId) {

        DeveloperServiceOuterClass.DeveloperResponse developerResponse = authorizationServiceGrpcClient.getDeveloperById(DeveloperServiceOuterClass.GetDeveloperByIdRequest
                .newBuilder()
                .setId(developerId)
                .build());

        return convertFromDeveloperResponseGrpc(developerResponse);
    }

    @Transactional
    @Override
    public VacancyReplyResponse rejectVacancyReply(UUID id, String hrId) {

        VacancyReplyEntity reply = vacancyReplyRepository.findById(id)
                .orElseThrow(VacancyReplyNotFoundException::new);
        checkVacancyAddedByThisHr(reply.getVacancy(), hrId);
        reply.setState(VacancyReplyEntity.State.REJECTED);
        VacancyReplyEntity savedVacancyReply = vacancyReplyRepository.save(reply);

        notificationServiceRabbit.sendMailMessageToDeveloperReplyReject(reply.getVacancy().getId(), reply.getDeveloperId());

        return convertToReplyResponseWithDeveloper(savedVacancyReply);
    }

    private void checkVacancyAddedByThisHr(VacancyEntity vacancy, String hrId) {

        if (!vacancy.getHrId().equals(UUID.fromString(hrId))) {
            throw new VacancyNotFoundException("Hr didn't add this vacancy");
        }
    }

    @Transactional
    @Override
    public VacancyReplyResponse viewedVacancyReply(UUID id, String hrId) {

        VacancyReplyEntity reply = vacancyReplyRepository.findById(id)
                .orElseThrow(VacancyReplyNotFoundException::new);
        checkVacancyAddedByThisHr(reply.getVacancy(), hrId);
        reply.setState(VacancyReplyEntity.State.VIEWED);
        vacancyReplyRepository.save(reply);

        return convertToReplyResponseWithDeveloper(reply);
    }

    @Transactional(readOnly = true)
    @Override
    public VacancyReplyResponse getById(UUID id, String hrId) {

        VacancyReplyEntity reply = vacancyReplyRepository.findById(id)
                .orElseThrow(VacancyNotFoundException::new);
        checkVacancyAddedByThisHr(reply.getVacancy(), hrId);
        return convertToReplyResponseWithDeveloper(reply);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VacancyReplyResponse> getAllRepliesByVacancy(UUID vacancyId, String hrId, Integer size, Integer number) {

        VacancyEntity vacancy = vacancyRepository.findById(vacancyId)
                .orElseThrow(VacancyNotFoundException::new);
        checkVacancyAddedByThisHr(vacancy, hrId);

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<VacancyReplyEntity> replies = vacancyReplyRepository.findAllByVacancy(vacancy, pageRequest);

        return replies.map(this::convertToReplyResponseWithDeveloper);
    }
}
