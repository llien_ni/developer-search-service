package com.technokratos.service.impl;

import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.dto.request.LocationRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.LocationResponse;
import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.LocationEntity;
import com.technokratos.exception.CompanyNotFoundException;
import com.technokratos.exception.LocationNotFoundException;
import com.technokratos.mapper.LocationMapper;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.LocationRepository;
import com.technokratos.service.CompanyService;
import com.technokratos.service.LocationService;
import com.technokratos.util.RequestParamUtil;
import hr.service.HrServiceOuterClass;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;

    private final CompanyService companyService;

    private final LocationMapper locationMapper;

    private final RequestParamUtil requestParamUtil;

    private final CompanyRepository companyRepository;

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    @Transactional
    @Override
    public LocationResponse add(LocationRequest request, String hrId) {

        HrServiceOuterClass.HrResponse hr = authorizationServiceGrpcClient
                .getHrById(HrServiceOuterClass.GetHrByIdRequest.newBuilder()
                        .setId(hrId)
                        .build());
        CompanyEntity companyEntity = companyRepository.findById(UUID.fromString(hr.getCompanyId()))
                .orElseThrow(CompanyNotFoundException::new);

        LocationEntity locationEntity = locationMapper.fromRequestToEntity(request);
        locationEntity.setCompany(companyEntity);
        LocationEntity savedLocation = locationRepository.save(locationEntity);
        companyEntity.getLocations().add(savedLocation);
        companyRepository.save(companyEntity);

        return locationMapper.fromEntityToResponse(savedLocation);
    }

    @Transactional
    @Override
    public void delete(RequestId requestId, String hrId) {

        LocationEntity locationEntity = locationRepository
                .findById(requestId.getId())
                .orElseThrow(LocationNotFoundException::new);
        companyService.checkHrCompany(hrId, locationEntity.getCompany().getId());
        CompanyEntity company = locationEntity.getCompany();
        company.getLocations().remove(locationEntity);
        locationRepository.delete(locationEntity);
        companyRepository.save(company);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LocationResponse> getAllByCompany(UUID id, Integer size, Integer number) {

        CompanyEntity companyEntity = companyRepository
                .findById(id)
                .orElseThrow(CompanyNotFoundException::new);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<LocationEntity> locations = locationRepository.findAllByCompany(companyEntity, pageRequest);
        return locations.map(locationMapper::fromEntityToResponse);
    }
}
