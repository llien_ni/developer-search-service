package com.technokratos.service;

import com.technokratos.dto.request.ReviewRequest;
import com.technokratos.dto.request.UpdateReviewRequest;
import com.technokratos.dto.response.ReviewResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface ReviewService {

    ReviewResponse add(ReviewRequest request, String authorId);

    ReviewResponse update(UpdateReviewRequest request, String authorId);

    UUID delete(UUID reviewId, String authorId);

    UUID delete(UUID reviewId);

    ReviewResponse findById(UUID reviewId);

    Page<ReviewResponse> findByCompany(UUID companyId, Integer size, Integer number);

    Page<ReviewResponse> findAll(Integer size, Integer number);

    ReviewResponse findByCompanyAndAuthor(UUID companyId, String authorId);
}
