package com.technokratos.service;

import com.technokratos.dto.request.LocationRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.LocationResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface LocationService {

    LocationResponse add(LocationRequest request, String hrId);

    void delete(RequestId requestId, String hrId);

    Page<LocationResponse> getAllByCompany(UUID id, Integer size, Integer number);
}
