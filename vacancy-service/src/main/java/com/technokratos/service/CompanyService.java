package com.technokratos.service;

import com.technokratos.dto.request.CreateCompanyRequest;
import com.technokratos.dto.request.UpdateCompanyRequest;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.entity.CompanyEntity;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface CompanyService {

    Page<CompanyResponse> getCompanySortByAndStatus(String sortBy, CompanyEntity.Status notConfirmed, Integer size, Integer number);

    CompanyResponse updateStatus(UUID id, CompanyEntity.Status confirmed);

    void checkHrCompany(String hrId, UUID id);

    CompanyResponse save(CreateCompanyRequest companyRequest);

    CompanyResponse update(UpdateCompanyRequest updateCompanyRequest, String hrId);

    UUID delete(UUID id);

    CompanyResponse getById(UUID id);

    Page<CompanyResponse> getAll(Integer size, Integer number);

    CompanyResponse confirmCompany(String token);

    CompanyResponse getByName(String companyName);

    CompanyResponse updateCompanyStatusToConfirmed(UUID id);

    CompanyResponse updateCompanyStatusToBlocked(UUID id);
}
