package com.technokratos.service;

import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListSpecializationsRequest;
import com.technokratos.dto.response.SpecializationResponse;

import java.util.List;

public interface SpecializationService {
    
    List<SpecializationResponse> addList(ListSpecializationsRequest list, String hrId);

    void deleteList(ListOfIdsRequest list, String hrId);
}
