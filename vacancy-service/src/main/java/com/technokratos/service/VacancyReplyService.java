package com.technokratos.service;

import com.technokratos.dto.response.VacancyReplyResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface VacancyReplyService {

    VacancyReplyResponse replyToVacancy(UUID vacancyId, String developerId);

    Page<VacancyReplyResponse> getAllRepliesByDeveloper(String developerId, Integer size, Integer number);

    Page<VacancyReplyResponse> getAllRepliesForHr(String hrId, Integer size, Integer number);

    VacancyReplyResponse rejectVacancyReply(UUID id, String hrId);

    VacancyReplyResponse getById(UUID id, String hrId);

    Page<VacancyReplyResponse> getAllRepliesByVacancy(UUID vacancyId, String hrId, Integer size, Integer number);

    VacancyReplyResponse viewedVacancyReply(UUID id, String hrId);

    Page<VacancyReplyResponse> getAllRepliesByStateForHr(String hrId, String state, Integer size, Integer number);

    UUID deleteReply(UUID replyId, String developerId);
}
