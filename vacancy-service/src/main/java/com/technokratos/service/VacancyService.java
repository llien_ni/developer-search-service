package com.technokratos.service;

import com.technokratos.dto.request.CreateVacancyRequest;
import com.technokratos.dto.request.VacancyFilter;
import com.technokratos.dto.request.UpdateVacancyRequest;
import com.technokratos.dto.response.VacancyResponse;
import com.technokratos.entity.VacancyEntity;
import org.springframework.data.domain.Page;
import java.util.UUID;

public interface VacancyService {

    VacancyResponse save(CreateVacancyRequest request, String hrId);

    VacancyResponse update(UpdateVacancyRequest request, String hrId);

    UUID delete(UUID id, String hrId);

    VacancyResponse getById(UUID id);

    Page<VacancyResponse> getAll(Integer size, Integer number);

    Page<VacancyResponse> getByCompany(UUID companyId, Integer size, Integer number);

    Page<VacancyResponse> getByHr(UUID hrId, Integer size, Integer number);

    Page<VacancyResponse> searchByFilter(VacancyFilter filter, Integer size, Integer number);

    Page<VacancyResponse> getAllByStatus(VacancyEntity.Status status, Integer size, Integer number);
}
