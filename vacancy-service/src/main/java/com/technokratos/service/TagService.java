package com.technokratos.service;

import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListTagsRequest;
import com.technokratos.dto.response.TagResponse;

import java.util.List;

public interface TagService {

    List<TagResponse> addList(ListTagsRequest list, String hrId);

    void deleteList(ListOfIdsRequest list, String hrId);
}
