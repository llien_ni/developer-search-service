package com.technokratos.client;


import authorization.service.DeveloperServiceGrpc;
import authorization.service.DeveloperServiceOuterClass;
import hr.service.HrServiceGrpc;
import hr.service.HrServiceOuterClass;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthorizationServiceGrpcClient {

    @GrpcClient("authorization-service")
    private HrServiceGrpc.HrServiceBlockingStub hrServiceBlockingStub;

    @GrpcClient("authorization-service")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    public DeveloperServiceOuterClass.DeveloperResponse getDeveloperById(DeveloperServiceOuterClass.GetDeveloperByIdRequest request) {

        return developerServiceBlockingStub.getDeveloperById(request);
    }

    public DeveloperServiceOuterClass.DeveloperResponse getDeveloperByUsername(DeveloperServiceOuterClass.GetDeveloperByUsernameRequest request){

        return developerServiceBlockingStub.getDeveloperByUsername(request);
    }

    public HrServiceOuterClass.HrResponse getHrById(HrServiceOuterClass.GetHrByIdRequest request){
        return hrServiceBlockingStub.getHrById(request);
    }
}
