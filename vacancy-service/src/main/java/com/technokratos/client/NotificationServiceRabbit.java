package com.technokratos.client;

import com.technokratos.dto.model.MailMessageModel;
import com.technokratos.dto.response.CompanyResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import java.util.UUID;

import static com.technokratos.util.Constants.AUTHORIZATION_SERVICE_URL;
import static com.technokratos.util.Constants.VACANCY_SERVICE_URL;


@Service
@RequiredArgsConstructor
public class NotificationServiceRabbit {

    private final RabbitTemplate rabbitTemplate;

    public void sentMailMessageThatCompanyIsConfirmed(CompanyResponse companyResponse){
        MailMessageModel mailMessageModel = MailMessageModel.builder()
                .to(companyResponse.getEmail())
                .subject("Company is confirmed")
                .message("The company passed moderation successfully.")
                .build();
        sendMailMessage(mailMessageModel);
    }

    public void sentMailMessageThatCompanyNotPassedModeration(CompanyResponse companyResponse){

        MailMessageModel mailMessageModel = MailMessageModel.builder()
                .to(companyResponse.getEmail())
                .subject("Company is blocked")
                .message("The company didn't pass moderation")
                .build();
        sendMailMessage(mailMessageModel);
    }

    public void sendMailMessage(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend("direct-exchange", "vacancy-service-notification", mailMessageModel);
    }

    public void sendMailMessageToAuthService(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend("direct-exchange", "vacancy-service", mailMessageModel);
    }

    public void sendMailMessageToHr(String developerId, UUID hrId, UUID vacancyId) {

        MailMessageModel mailMessageModelToHr = MailMessageModel.builder()
                .message("Developer: " + AUTHORIZATION_SERVICE_URL + "/developer/" + developerId + " " +
                        "Vacancy: " + VACANCY_SERVICE_URL + "/vacancy/" + vacancyId)
                .to(hrId.toString())
                .subject("Developer respond to a vacancy")
                .build();

        sendMailMessageToAuthService(mailMessageModelToHr);
    }


    public void sendMailMessageToDeveloper(String developerId, UUID vacancyId) {

        MailMessageModel mailMessageModelToDeveloper = MailMessageModel.builder()
                .message("Vacancy: " + VACANCY_SERVICE_URL + "/vacancy/" + vacancyId)
                .to(developerId)
                .subject("You respond to vacancy")
                .build();

        sendMailMessageToAuthService(mailMessageModelToDeveloper);
    }

    public void sendMailMessageToDeveloperReplyReject(UUID vacancyId, UUID developerId) {

        MailMessageModel mailMessageModelToDeveloper = MailMessageModel.builder()
                .message("Hello, unfortunately we have to refuse you, but you can try your hand in the future. Vacancy: " + VACANCY_SERVICE_URL + "/vacancy/" + vacancyId)
                .to(developerId.toString())
                .subject("REJECT")
                .build();

        sendMailMessageToAuthService(mailMessageModelToDeveloper);
    }
}
