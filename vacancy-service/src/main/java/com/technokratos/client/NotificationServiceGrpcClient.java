package com.technokratos.client;


import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import notification.service.NotificationServiceOuterClass;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class NotificationServiceGrpcClient {

    @GrpcClient("notification-service")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    public void sendMail(NotificationServiceOuterClass.MailMessageRequest request) {

        notificationServiceBlockingStub.sendMail(request);
    }
}
