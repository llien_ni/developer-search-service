package com.technokratos.util.converter;

import com.technokratos.dto.response.CompanyResponse;
import company.service.CompanyServiceOuterClass;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CompanyResponseConverter {

    public static CompanyServiceOuterClass.CompanyResponse toGrpcResponse(CompanyResponse companyResponse){

        return CompanyServiceOuterClass.CompanyResponse
                .newBuilder()
                .setId(companyResponse.getId().toString())
                .setName(companyResponse.getName())
                .setDescription(companyResponse.getDescription() == null ? "" : companyResponse.getDescription())
                .setEmail(companyResponse.getEmail())
                .setStatus(companyResponse.getStatus())
                .setType(companyResponse.getType())
                .setWebsiteLink(companyResponse.getWebsiteLink())
                .setUpdatedBy(companyResponse.getUpdatedBy() == null ? "" : companyResponse.getUpdatedBy())
                .build();

    }
}
