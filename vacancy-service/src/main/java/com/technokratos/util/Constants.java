package com.technokratos.util;

public class Constants {

    public static final String VACANCY_SERVICE_URL = "http://localhost:8090";

    public static final String AUTHORIZATION_SERVICE_URL = "http://localhost:8080";
}
