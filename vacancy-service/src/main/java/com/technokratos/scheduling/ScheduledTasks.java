package com.technokratos.scheduling;


import com.technokratos.entity.CompanyEntity;
import com.technokratos.entity.ReviewEntity;
import com.technokratos.exception.CompanyNotFoundException;
import com.technokratos.repository.CompanyRepository;
import com.technokratos.repository.ReviewRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;
import java.util.*;

@Component
@RequiredArgsConstructor
public class ScheduledTasks {

    private final CompanyRepository companyRepository;

    private final ReviewRepository reviewRepository;

    //3600000 == 1h
    @Scheduled(fixedRate = 3600000)
    public void calculateRating(){

        //первое значение в листе это сумма второе это колво оценок
        //ключ это id компании
        Map<UUID, List<Integer>> map = new HashMap<>();
        List<ReviewEntity> reviews = reviewRepository.findAll();
        for(ReviewEntity reviewEntity:reviews){
            UUID companyId = reviewEntity.getCompany().getId();

            if(map.get(companyId)==null){
                List<Integer> list = new ArrayList<>();
                list.add(0);
                list.add(0);
                map.put(companyId,list);
            }
            List<Integer> list = map.get(companyId);
            list.set(0, list.get(0)+reviewEntity.getEvaluation());
            list.set(1, list.get(1)+1);
            map.put(companyId, list);
        }
        map.forEach((key, value) -> {
            CompanyEntity company = companyRepository.findById(key).orElseThrow(CompanyNotFoundException::new);
            float sum = value.get(0);
            float count = value.get(1);
            company.setRating(BigDecimal.valueOf(sum / count));
            companyRepository.save(company);
        });
    }
}
