package com.technokratos.api;


import com.technokratos.dto.request.*;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.VacancyResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/vacancy")
public interface VacancyApi {

    @Operation(summary = "add vacancy")
    @ApiResponse(responseCode = "201", description = "vacancy successfully added",
            content = @Content(schema = @Schema(implementation = VacancyResponse.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    VacancyResponse add(@RequestBody @Valid CreateVacancyRequest request, @AuthenticationPrincipal String hrId);

    @Operation(summary = "update vacancy")
    @ApiResponse(responseCode = "202", description = "vacancy successfully updated",
            content = @Content(schema = @Schema(implementation = VacancyResponse.class)))
    @ApiResponse(responseCode = "400", description = "company id and hr company id didn't match",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "vacancy not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    VacancyResponse update(@RequestBody @Valid UpdateVacancyRequest request, @AuthenticationPrincipal String hrId);

    @Operation(summary = "update vacancy")
    @ApiResponse(responseCode = "202", description = "vacancy successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "company id and hr company id didn't match",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "vacancy not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestId request, @AuthenticationPrincipal String hrId);

    @Operation(summary = "get vacancy by id")
    @ApiResponse(responseCode = "200", description = "vacancy successfully deleted",
            content = @Content(schema = @Schema(implementation = VacancyResponse.class)))
    @ApiResponse(responseCode = "404", description = "vacancy not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    VacancyResponse getById(@PathVariable("id") UUID id);

    @Operation(summary = "get all vacancies and sort by vacancy created date")
    @ApiResponse(responseCode = "200", description = "vacancy successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR')")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyResponse> getAll(@RequestParam(value = "size", required = false) Integer size,
                                 @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get all vacancies with status ACTIVE")
    @ApiResponse(responseCode = "200", description = "vacancy successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/active")
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyResponse> getAllActive(@RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get vacancies by company with status ACTIVE")
    @ApiResponse(responseCode = "200", description = "vacancy successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/by/company/{id}")
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyResponse> getByCompany(@PathVariable("id") UUID id,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get vacancies by hr with status ACTIVE")
    @ApiResponse(responseCode = "200", description = "vacancy successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @GetMapping("/by/hr")
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyResponse> getByHr(@AuthenticationPrincipal String hrId,
                                  @RequestParam(value = "size", required = false) Integer size,
                                  @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "search vacancies by filter")
    @ApiResponse(responseCode = "200", description = "vacancy successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PostMapping("/by/filter")
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyResponse> getByFilter(@RequestBody VacancyFilter filter,
                                      @RequestParam(value = "size", required = false) Integer size,
                                      @RequestParam(value = "number", required = false) Integer number);
}
