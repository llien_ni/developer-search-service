package com.technokratos.api;

import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.VacancyReplyResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/vacancy/reply")
public interface VacancyReplyApi {

    @Operation(summary = "get all developer replies to vacancies and sort by reply created date")
    @ApiResponse(responseCode = "200", description = "replies successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @GetMapping("/my")
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyReplyResponse> getAllRepliesByDeveloper(@AuthenticationPrincipal String developerId,
                                                        @RequestParam(value = "size", required = false) Integer size,
                                                        @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "reply to a vacancy")
    @ApiResponse(responseCode = "200", description = "reply successfully add",
            content = @Content(schema = @Schema(implementation = VacancyReplyResponse.class)))
    @ApiResponse(responseCode = "400", description = "reply already exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "vacancy not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping("/{vacancy-id}")
    @ResponseStatus(HttpStatus.CREATED)
    VacancyReplyResponse replyToVacancy(@PathVariable("vacancy-id") UUID vacancyId,
                                        @AuthenticationPrincipal String developerId);

    @Operation(summary = "cancel the reply to the vacancy")
    @ApiResponse(responseCode = "202", description = "reply successfully deleted",
            content = @Content(schema = @Schema(implementation = VacancyReplyResponse.class)))
    @ApiResponse(responseCode = "400", description = "reply does not exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping("/{reply-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse deleteVacancyReply(@PathVariable("reply-id") UUID replyId,
                                       @AuthenticationPrincipal String developerId);

    @Operation(summary = "get replies to all vacancies that the hr has added")
    @ApiResponse(responseCode = "200", description = "replies successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyReplyResponse> getAllReplies(@AuthenticationPrincipal String hrId,
                                             @RequestParam(value = "size", required = false) Integer size,
                                             @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get replies to all vacancies that the hr has added and certain state")
    @ApiResponse(responseCode = "200", description = "replies successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @GetMapping("/by/state")
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyReplyResponse> getAllRepliesByState(@RequestParam("state") String state,
                                                    @AuthenticationPrincipal String hrId,
                                                    @RequestParam(value = "size", required = false) Integer size,
                                                    @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get reply by id")
    @ApiResponse(responseCode = "200", description = "replies successfully received",
            content = @Content(schema = @Schema(implementation = VacancyReplyResponse.class)))
    @ApiResponse(responseCode = "404", description = "reply not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @GetMapping("/{vacancy-request-id}")
    @ResponseStatus(HttpStatus.OK)
    VacancyReplyResponse getById(@PathVariable("vacancy-request-id") UUID id,
                                 @AuthenticationPrincipal String hrId);

    @Operation(summary = "get all replies by vacancy")
    @ApiResponse(responseCode = "200", description = "replies successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "vacancy not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @GetMapping("/by/vacancy/{vacancy-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyReplyResponse> getAllRepliesByVacancy(@PathVariable("vacancy-id") UUID vacancyId,
                                                      @AuthenticationPrincipal String hrId,
                                                      @RequestParam(value = "size", required = false) Integer size,
                                                      @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "reject vacancy reply, vacancy reply status changed to REJECTED")
    @ApiResponse(responseCode = "202", description = "reply successfully rejected",
            content = @Content(schema = @Schema(implementation = VacancyReplyResponse.class)))
    @ApiResponse(responseCode = "404", description = "vacancy reply not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @PatchMapping("/reject")
    @ResponseStatus(HttpStatus.ACCEPTED)
    VacancyReplyResponse rejectVacancyReply(@RequestBody @Valid RequestId requestId,
                                            @AuthenticationPrincipal String hrId);

    @Operation(summary = "view vacancy reply, vacancy reply status changed to VIEWED")
    @ApiResponse(responseCode = "202", description = "reply successfully viewed",
            content = @Content(schema = @Schema(implementation = VacancyReplyResponse.class)))
    @ApiResponse(responseCode = "404", description = "vacancy reply not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @PatchMapping("/viewed")
    @ResponseStatus(HttpStatus.ACCEPTED)
    VacancyReplyResponse viewedVacancyReply(@RequestBody @Valid RequestId requestId,
                                            @AuthenticationPrincipal String hrId);
}
