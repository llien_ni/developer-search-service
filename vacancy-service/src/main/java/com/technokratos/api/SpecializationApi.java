package com.technokratos.api;

import com.technokratos.dto.request.ListOfIdsRequest;
import com.technokratos.dto.request.ListSpecializationsRequest;
import com.technokratos.dto.response.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@PreAuthorize("hasRole('ROLE_HR')")
@RequestMapping("/vacancy/specialization")
public interface SpecializationApi {

    @Operation(summary = "add specializations as list to vacancy")
    @ApiResponse(responseCode = "201", description = "specializations successfully added",
            content = @Content(schema = @Schema(implementation = ListResponse.class)))
    @ApiResponse(responseCode = "400", description = "company id and hr company id didn't match",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "vacancy not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ListResponse<SpecializationResponse> addSpecializationListToVacancy(@RequestBody @Valid ListSpecializationsRequest list,
                                                                        @AuthenticationPrincipal String accountID);

    @Operation(summary = "delete specializations as list from vacancy")
    @ApiResponse(responseCode = "202", description = "specializations successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "company id and hr company id didn't match",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "400", description = "specialization does not exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "vacancy not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse deleteSpecializationFromVacancy(@RequestBody @Valid ListOfIdsRequest list,
                                                    @AuthenticationPrincipal String accountID);
}
