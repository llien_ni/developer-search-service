package com.technokratos.api;

import com.technokratos.dto.request.CreateCompanyRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateCompanyRequest;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/company")
public interface CompanyApi {

    @Operation(summary = "confirm company")
    @ApiResponse(responseCode = "200", description = "company confirmed status changed to MODERATED",
            content = @Content(schema = @Schema(implementation = CompanyResponse.class)))
    @ApiResponse(responseCode = "404", description = "Company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/confirm")
    @ResponseStatus(HttpStatus.OK)
    CompanyResponse confirm(@RequestParam("token") String token);

    @Operation(summary = "add company")
    @ApiResponse(responseCode = "201", description = "company successfully added company status is NOT_CONFIRMED",
            content = @Content(schema = @Schema(implementation = CompanyResponse.class)))
    @ApiResponse(responseCode = "400", description = "company with this name already exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    CompanyResponse add(@RequestBody @Valid CreateCompanyRequest companyRequest);

    @Operation(summary = "update company")
    @ApiResponse(responseCode = "202", description = "company successfully updated, company status is MODERATED",
            content = @Content(schema = @Schema(implementation = CompanyResponse.class)))
    @ApiResponse(responseCode = "400", description = "company id and hr company id didn't match",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    CompanyResponse update(@RequestBody @Valid UpdateCompanyRequest updateCompanyRequest,
                           @AuthenticationPrincipal String hrId);

    @Operation(summary = "admin delete company")
    @ApiResponse(responseCode = "202", description = "company successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestId request);

    @Operation(summary = "get company by id")
    @ApiResponse(responseCode = "200", description = "company successfully received",
            content = @Content(schema = @Schema(implementation = CompanyResponse.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    CompanyResponse getById(@PathVariable("id") UUID id);

    @Operation(summary = "get all confirmed companies with sort by rating")
    @ApiResponse(responseCode = "200", description = "companies successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<CompanyResponse> getConfirmedCompanies(@RequestParam(value = "size", required = false) Integer size,
                                                @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "admin and moderator get all companies with sort by rating")
    @ApiResponse(responseCode = "200", description = "companies successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR')")
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    Page<CompanyResponse> getAll(@RequestParam(value = "size", required = false) Integer size,
                                 @RequestParam(value = "number", required = false) Integer number);
}
