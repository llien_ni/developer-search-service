package com.technokratos.api;

import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.ReviewRequest;
import com.technokratos.dto.request.UpdateReviewRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ReviewResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/review")
public interface ReviewApi {

    @Operation(summary = "add review")
    @ApiResponse(responseCode = "201", description = "review successfully added",
            content = @Content(schema = @Schema(implementation = ReviewResponse.class)))
    @ApiResponse(responseCode = "400", description = "account already sent review to this company",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @ResponseStatus(HttpStatus.CREATED)
    ReviewResponse add(@RequestBody @Valid ReviewRequest request, @AuthenticationPrincipal String authorId);

    @Operation(summary = "update review")
    @ApiResponse(responseCode = "202", description = "review successfully updated",
            content = @Content(schema = @Schema(implementation = ReviewResponse.class)))
    @ApiResponse(responseCode = "404", description = "review not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PatchMapping
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    ReviewResponse update(@RequestBody @Valid UpdateReviewRequest request, @AuthenticationPrincipal String authorId);

    @Operation(summary = "delete review")
    @ApiResponse(responseCode = "202", description = "review successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "review not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping("/my")
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestId request, @AuthenticationPrincipal String authorId);

    @Operation(summary = "admin delete review")
    @ApiResponse(responseCode = "202", description = "review successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "review not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestId request);

    @Operation(summary = "get review by id")
    @ApiResponse(responseCode = "200", description = "review successfully received",
            content = @Content(schema = @Schema(implementation = ReviewResponse.class)))
    @ApiResponse(responseCode = "404", description = "review not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{review-id}")
    ReviewResponse findById(@PathVariable("review-id") UUID reviewId);

    @Operation(summary = "get reviews by company and sort by review create date")
    @ApiResponse(responseCode = "200", description = "reviews successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/company/{company-id}")
    Page<ReviewResponse> findByCompany(@PathVariable("company-id") UUID companyId,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get all and sort by review create date")
    @ApiResponse(responseCode = "200", description = "reviews successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping()
    Page<ReviewResponse> findAll(@RequestParam(value = "size", required = false) Integer size,
                                 @RequestParam(value = "number", required = false) Integer number);

    //это для нахождения пользователем отзыва, который он отправил на компанию
    @Operation(summary = "user get a review that he sent to the company")
    @ApiResponse(responseCode = "200", description = "review successfully received",
            content = @Content(schema = @Schema(implementation = ReviewResponse.class)))
    @ApiResponse(responseCode = "400", description = "review does not exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @GetMapping("/company/{company-id}/my_review")
    ReviewResponse findByCompanyAndAuthor(@PathVariable("company-id") UUID companyId,
                                          @AuthenticationPrincipal String authorId,
                                          @RequestParam(value = "size", required = false) Integer size,
                                          @RequestParam(value = "number", required = false) Integer number);
}
