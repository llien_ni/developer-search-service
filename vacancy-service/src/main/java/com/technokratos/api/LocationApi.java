package com.technokratos.api;


import com.technokratos.dto.request.LocationRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.LocationResponse;
import com.technokratos.dto.response.MessageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/company/location")
public interface LocationApi {

    @Operation(summary = "add location to company")
    @ApiResponse(responseCode = "201", description = "location successfully added",
            content = @Content(schema = @Schema(implementation = LocationResponse.class)))
    @ApiResponse(responseCode = "404", description = "Company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    LocationResponse addLocationToCompany(@RequestBody @Valid LocationRequest request,
                                          @AuthenticationPrincipal String hrId);

    @Operation(summary = "delete location")
    @ApiResponse(responseCode = "202", description = "location successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "company id and hr company id didn't match",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "location not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse deleteLocationFromCompany(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String hrId);

    @Operation(summary = "get all company locations")
    @ApiResponse(responseCode = "200", description = "company locations successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{company-id}")
    Page<LocationResponse> getByCompany(@PathVariable("company-id") UUID id,
                                        @RequestParam(value = "size", required = false) Integer size,
                                        @RequestParam(value = "number", required = false) Integer number);
}
