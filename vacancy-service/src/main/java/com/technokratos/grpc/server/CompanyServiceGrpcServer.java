package com.technokratos.grpc.server;


import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.service.CompanyService;
import com.technokratos.util.converter.CompanyResponseConverter;
import company.service.CompanyServiceGrpc;
import company.service.CompanyServiceOuterClass;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.UUID;

@Slf4j
@GrpcService
@RequiredArgsConstructor
public class CompanyServiceGrpcServer extends CompanyServiceGrpc.CompanyServiceImplBase {

    private final CompanyService companyService;

    @Override
    public void getCompanyByName(CompanyServiceOuterClass.GetCompanyByNameRequest request, StreamObserver<CompanyServiceOuterClass.CompanyResponse> responseObserver) {

        CompanyResponse companyResponse = companyService.getByName(request.getCompanyName());

        CompanyServiceOuterClass.CompanyResponse response = CompanyResponseConverter.toGrpcResponse(companyResponse);

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getCompanyById(CompanyServiceOuterClass.GetCompanyByIdRequest request, StreamObserver<CompanyServiceOuterClass.CompanyResponse> responseObserver){

        CompanyResponse companyResponse = companyService.getById(UUID.fromString(request.getCompanyId()));

        CompanyServiceOuterClass.CompanyResponse response = CompanyResponseConverter.toGrpcResponse(companyResponse);

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
