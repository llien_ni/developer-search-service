package com.technokratos.mapper;

import com.technokratos.dto.request.ReviewRequest;
import com.technokratos.dto.request.UpdateReviewRequest;
import com.technokratos.dto.response.ReviewResponse;
import com.technokratos.entity.ReviewEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface ReviewMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    ReviewEntity fromRequestToEntity(ReviewRequest request);

    ReviewResponse fromEntityToResponse(ReviewEntity reviewEntity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "company", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(UpdateReviewRequest request, @MappingTarget ReviewEntity review);
}
