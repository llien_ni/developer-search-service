package com.technokratos.mapper;


import com.technokratos.dto.request.LocationRequest;
import com.technokratos.dto.response.LocationResponse;
import com.technokratos.entity.LocationEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface LocationMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    LocationEntity fromRequestToEntity(LocationRequest request);

    @Mapping(source = "company.id", target = "companyId")
    LocationResponse fromEntityToResponse(LocationEntity locationEntity);
}
