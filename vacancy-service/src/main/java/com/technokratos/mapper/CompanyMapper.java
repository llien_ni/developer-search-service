package com.technokratos.mapper;


import com.technokratos.dto.request.CreateCompanyRequest;
import com.technokratos.dto.request.UpdateCompanyRequest;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.entity.CompanyEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface CompanyMapper {

    CompanyResponse fromEntityToResponse(CompanyEntity companyEntity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    CompanyEntity fromRequestToEntity(CreateCompanyRequest companyRequest);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(UpdateCompanyRequest projectRequest, @MappingTarget CompanyEntity companyEntity);
}
