package com.technokratos.mapper;

import com.technokratos.dto.request.SpecializationRequest;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.entity.SpecializationEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SpecializationMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    SpecializationEntity fromStringToSpecializationEntity(String specialization);

    @Mapping(source = "specialization", target = "specialization")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    List<SpecializationEntity> fromStringToSpecializationEntity(List<String> specialization);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    SpecializationEntity fromRequestToSpecialization(SpecializationRequest request);

    @Mapping(source = "vacancy.id", target = "vacancyId")
    SpecializationResponse fromEntityToResponse(SpecializationEntity specializationEntity);

    @Mapping(source = "vacancy.id", target = "vacancyId")
    List<SpecializationResponse> fromEntityToResponse(List<SpecializationEntity> specializationsEntities);
}
