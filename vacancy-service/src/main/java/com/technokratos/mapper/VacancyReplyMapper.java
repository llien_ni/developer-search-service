package com.technokratos.mapper;


import com.technokratos.dto.response.VacancyReplyResponse;
import com.technokratos.entity.VacancyReplyEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {VacancyMapper.class})
public interface VacancyReplyMapper {

    @Mapping(target = "developerResponse.id", source = "developerId")
    @Mapping(target = "vacancyResponse", source = "vacancy")
    VacancyReplyResponse fromEntityToResponse(VacancyReplyEntity vacancyReplyEntity);
}
