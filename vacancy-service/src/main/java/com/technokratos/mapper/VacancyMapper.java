package com.technokratos.mapper;

import com.technokratos.dto.request.CreateVacancyRequest;
import com.technokratos.dto.request.UpdateVacancyRequest;
import com.technokratos.dto.response.VacancyResponse;
import com.technokratos.entity.VacancyEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {SpecializationMapper.class, TagMapper.class})
public interface VacancyMapper {

    @Mapping(source = "status", target = "status", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    VacancyEntity fromRequestToEntity(CreateVacancyRequest vacancyRequest);

    @Mapping(source = "company", target = "companyResponse")
    VacancyResponse fromEntityToResponse(VacancyEntity savedEntity);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(UpdateVacancyRequest request, @MappingTarget VacancyEntity vacancyEntity);
}
