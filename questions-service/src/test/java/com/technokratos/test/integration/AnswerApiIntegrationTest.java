package com.technokratos.test.integration;


import authorization.service.AccountServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.AnswerRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateAnswerRequest;
import com.technokratos.dto.response.AnswerResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.entity.AnswerEntity;
import com.technokratos.entity.QuestionEntity;
import com.technokratos.repository.AnswerRepository;
import com.technokratos.repository.QuestionRepository;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/answer-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class AnswerApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    //2669cbf8-32c1-4796-bf43-2f36a8a80cc8
    private static final String FIRST_DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    //117f2c92-7735-48fe-a0de-7ce15ff10937
    private static final String SECOND_DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxMTdmMmM5Mi03NzM1LTQ4ZmUtYTBkZS03Y2UxNWZmMTA5MzciLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTkyNDUyMzEsImp0aSI6IjE2ODM2NjkyMzE1MzUifQ.cZYTB1ifODL14KExKom0BpomSayTPqzr5XEaJgyM39_RMStlfttN3cUUFnezOyf5HQYYVrGzf7BoKbsLrKUz4ApiSx8G22nkReqMz5Gl6bM3hchGJopxnkbO6E6z0g1CEwFhuC-9MUXXuFwncxv8iQk-G5uR5Hu-Zqkc_i74uKxlibt_-59XtV2mH-W4GQrV-ZSLW5dwfwoKJJ_XSM-YoPgWhukud6M2dXzk08pRjw3IkKa5O1LHo3Rs9bySZ4Ck_pM5DRvzwB4LMluLrNd8jL7wl5se85HK8CDLg9BFasfwKc1NQKLtTuUkE1L2KVJ9EtUGD_ypvEnwN0e3fbLGuw";

    private static final String ANSWER_PREFIX = "/answer";

    private static final String BEARER_PREFIX = "Bearer ";

    @GrpcClient("inProcess")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;

    @Autowired
    private ObjectMapper objectMapper;

    @LocalServerPort
    private int port;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Test
    void addAnswerSuccessfully() throws Exception{

        UUID questionId = UUID.fromString("2869cbf8-32c1-4796-bf43-2f36a8a80cc8");
        AnswerRequest answerRequest = AnswerRequest.builder()
                .questionId(questionId)
                .text("my answer")
                .build();

        AnswerResponse answerResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + ANSWER_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(answerRequest))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        QuestionEntity questionEntity = questionRepository.getReferenceById(questionId);

        AnswerEntity answerEntity = answerRepository.getReferenceById(answerResponse.getId());

        Assertions.assertEquals(answerRequest.getText(), answerResponse.getText());
        Assertions.assertNotNull(answerResponse.getQuestionId());
        Assertions.assertNotNull(answerResponse.getId());
        Assertions.assertEquals(1, questionEntity.getAnswers().size());
        Assertions.assertNotNull(answerEntity.getQuestion());
        Assertions.assertNotNull(answerEntity.getAccountId());
    }

    @Test
    void updateAnswerSuccessfully() throws Exception{

        UUID answerId = UUID.fromString("2969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        UpdateAnswerRequest request = UpdateAnswerRequest.builder()
                .id(answerId)
                .text("Answer text")
                .build();

        AnswerResponse answerResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ANSWER_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(request.getText(), answerResponse.getText());
    }

    @Test
    void updateAnswerFailedAnswerNotFound() throws Exception{

        UUID answerId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        UpdateAnswerRequest request = UpdateAnswerRequest.builder()
                .id(answerId)
                .text("Answer text")
                .build();

       ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ANSWER_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteAnswerSuccessfully() throws Exception{

        UUID answerId = UUID.fromString("2969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId request = new RequestId(answerId);
        AnswerResponse answerResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + ANSWER_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        AnswerEntity answerEntity = answerRepository.getReferenceById(answerId);
        Assertions.assertTrue(answerEntity.isDeleted());
        Assertions.assertNotNull(answerResponse.getId());
        Assertions.assertNotNull(answerResponse.getText());
    }

    @Test
    void deleteAnswerFailedAnswerNotFound() throws Exception{

        UUID answerId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId request = new RequestId(answerId);
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + ANSWER_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void restoreAnswerSuccessfully() throws Exception{

        UUID answerId = UUID.fromString("3069cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId request = new RequestId(answerId);

        AnswerEntity oldAnswerEntity = answerRepository.getReferenceById(answerId);
        Assertions.assertTrue(oldAnswerEntity.isDeleted());

        AnswerResponse answerResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ANSWER_PREFIX+"/restore")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        AnswerEntity answerEntity = answerRepository.getReferenceById(answerId);
        Assertions.assertFalse(answerEntity.isDeleted());
        Assertions.assertNotNull(answerEntity.getAnswerVotes());
        Assertions.assertNotNull(answerEntity.getQuestion());
        Assertions.assertNotNull(answerEntity.getText());
        Assertions.assertNotNull(answerEntity.getAccountId());
        Assertions.assertEquals(0, answerEntity.getVotes());
        Assertions.assertNotNull(answerResponse.getText());
        Assertions.assertNotNull(answerResponse.getId());
    }

    @Test
    void getByAccountSuccessfully() throws Exception{

        UUID accountId = UUID.fromString("2669cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ResponsePage<AnswerResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + ANSWER_PREFIX+"/by/account/"+accountId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(1, page.getTotalElements());
    }

    @Test
    void getByIdSuccessfully() throws Exception{

        UUID answerId = UUID.fromString("2969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        AnswerResponse answerResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + ANSWER_PREFIX+"/"+ answerId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(answerResponse.getText());
        Assertions.assertNotNull(answerResponse.getQuestionId());
        Assertions.assertNotNull(answerResponse.getAccount());
        Assertions.assertEquals("2669cbf8-32c1-4796-bf43-2f36a8a80cc8", answerResponse.getAccount().getId().toString());
        Assertions.assertNotNull(answerResponse.getAccount().getFirstName());
    }

    @Test
    void getByIdFailedAnswerNotFound() throws Exception{

        UUID answerId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + ANSWER_PREFIX+"/"+ answerId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void markAnswerAsTheBest() throws Exception{

        UUID answerId = UUID.fromString("3169cbf8-32c1-4796-bf43-2f36a8a80cc8");

        AnswerEntity oldAnswerEntity = answerRepository.getReferenceById(answerId);
        Assertions.assertFalse(oldAnswerEntity.isBestSolution());

        AnswerResponse answerResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ANSWER_PREFIX+"/"+answerId+"/mark_best_solution")
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        AnswerEntity answerEntity = answerRepository.getReferenceById(answerId);
        Assertions.assertTrue(answerEntity.isBestSolution());
        Assertions.assertTrue(answerResponse.isBestSolution());
    }

    @Test
    void markAnswerAsTheBestFailed() throws Exception{

        UUID answerId = UUID.fromString("3469cbf8-32c1-4796-bf43-2f36a8a80cc8");

        AnswerEntity oldAnswerEntity = answerRepository.getReferenceById(answerId);
        Assertions.assertFalse(oldAnswerEntity.isBestSolution());

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ANSWER_PREFIX+"/"+answerId+"/mark_best_solution")
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
