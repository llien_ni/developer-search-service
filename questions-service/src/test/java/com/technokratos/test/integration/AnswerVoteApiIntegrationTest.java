package com.technokratos.test.integration;

import authorization.service.AccountServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.AnswerVoteRequest;
import com.technokratos.dto.response.AnswerVoteResponse;
import com.technokratos.entity.AnswerEntity;
import com.technokratos.entity.enums.Vote;
import com.technokratos.repository.AnswerRepository;
import com.technokratos.repository.QuestionRepository;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/answer-vote-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class AnswerVoteApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    //2669cbf8-32c1-4796-bf43-2f36a8a80cc8
    private static final String FIRST_DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    //117f2c92-7735-48fe-a0de-7ce15ff10937
    private static final String SECOND_DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxMTdmMmM5Mi03NzM1LTQ4ZmUtYTBkZS03Y2UxNWZmMTA5MzciLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTkyNDUyMzEsImp0aSI6IjE2ODM2NjkyMzE1MzUifQ.cZYTB1ifODL14KExKom0BpomSayTPqzr5XEaJgyM39_RMStlfttN3cUUFnezOyf5HQYYVrGzf7BoKbsLrKUz4ApiSx8G22nkReqMz5Gl6bM3hchGJopxnkbO6E6z0g1CEwFhuC-9MUXXuFwncxv8iQk-G5uR5Hu-Zqkc_i74uKxlibt_-59XtV2mH-W4GQrV-ZSLW5dwfwoKJJ_XSM-YoPgWhukud6M2dXzk08pRjw3IkKa5O1LHo3Rs9bySZ4Ck_pM5DRvzwB4LMluLrNd8jL7wl5se85HK8CDLg9BFasfwKc1NQKLtTuUkE1L2KVJ9EtUGD_ypvEnwN0e3fbLGuw";

    private static final String ANSWER_VOTE_PREFIX = "/answer/vote";

    private static final String BEARER_PREFIX = "Bearer ";

    @GrpcClient("inProcess")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;

    @Autowired
    private ObjectMapper objectMapper;

    @LocalServerPort
    private int port;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AnswerRepository answerRepository;

    @Test
    void voteUpSuccessfully() throws Exception{

        UUID answerId = UUID.fromString("2969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        AnswerVoteRequest request = AnswerVoteRequest.builder()
                .answerId(answerId)
                .vote(Vote.UP.toString())
                .build();

        AnswerVoteResponse answerVoteResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ANSWER_VOTE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(answerId, answerVoteResponse.getAnswerId());
        Assertions.assertEquals(Vote.UP.toString(), answerVoteResponse.getVote());

        AnswerEntity answer = answerRepository.getReferenceById(answerId);
        Assertions.assertEquals(1, answer.getAnswerVotes().size());
        Assertions.assertEquals(1, answer.getVotes());
    }

    @Test
    void voteUpSuccessfullyVoteAlreadyExist() throws Exception{

        UUID answerId = UUID.fromString("3069cbf8-32c1-4796-bf43-2f36a8a80cc8");

        AnswerVoteRequest request = AnswerVoteRequest.builder()
                .answerId(answerId)
                .vote(Vote.UP.toString())
                .build();

        AnswerVoteResponse answerVoteResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ANSWER_VOTE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(answerId, answerVoteResponse.getAnswerId());
        Assertions.assertEquals(Vote.NOT_MARKED.toString(), answerVoteResponse.getVote());

        AnswerEntity answer = answerRepository.getReferenceById(answerId);
        Assertions.assertEquals(0, answer.getAnswerVotes().size());
        Assertions.assertEquals(0, answer.getVotes());
    }

    @Test
    void voteDownSuccessfully() throws Exception{

        UUID answerId = UUID.fromString("2969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        AnswerVoteRequest request = AnswerVoteRequest.builder()
                .answerId(answerId)
                .vote(Vote.DOWN.toString())
                .build();

        AnswerVoteResponse answerVoteResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ANSWER_VOTE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(answerId, answerVoteResponse.getAnswerId());
        Assertions.assertEquals(Vote.DOWN.toString(), answerVoteResponse.getVote());

        AnswerEntity answer = answerRepository.getReferenceById(answerId);
        Assertions.assertEquals(1, answer.getAnswerVotes().size());
        Assertions.assertEquals(-1, answer.getVotes());
    }


}
