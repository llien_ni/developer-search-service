package com.technokratos.test.integration;


import authorization.service.AccountServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.QuestionRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateQuestionRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.QuestionResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.entity.QuestionEntity;
import com.technokratos.entity.enums.Vote;
import com.technokratos.repository.QuestionRepository;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/question-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class QuestionApiIntegrationTest extends AbstractIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";

    //2669cbf8-32c1-4796-bf43-2f36a8a80cc8
    private static final String FIRST_DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    private static final String QUESTION_PREFIX = "/question";

    private static final String BEARER_PREFIX = "Bearer ";

    @GrpcClient("inProcess")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;

    @Autowired
    private ObjectMapper objectMapper;

    @LocalServerPort
    private int port;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private QuestionRepository questionRepository;

    //проверь что все поля с проставились
    @Test
    void addQuestionSuccessfully() throws Exception {

        QuestionRequest questionRequest = QuestionRequest.builder()
                .title("My question")
                .text("Text my question")
                .build();

        QuestionResponse questionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + QUESTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(questionRequest))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        QuestionEntity questionEntity = questionRepository.getReferenceById(questionResponse.getId());

        Assertions.assertNotNull(questionResponse.getId());
        Assertions.assertEquals(questionRequest.getText(), questionResponse.getText());
        Assertions.assertEquals(questionRequest.getTitle(), questionResponse.getTitle());
        Assertions.assertEquals("2669cbf8-32c1-4796-bf43-2f36a8a80cc8", questionEntity.getIssuerId().toString());
        Assertions.assertNotNull(questionEntity.getText());
        Assertions.assertNotNull(questionEntity.getTitle());
        Assertions.assertNotNull(questionEntity.getQuestionVotes());
        Assertions.assertNotNull(questionResponse.getIssuer().getId());
        Assertions.assertEquals(0, questionEntity.getVotes());
        Assertions.assertFalse(questionEntity.isDeleted());
    }

    @Test
    void updateQuestionSuccessfully() throws Exception {

        UUID questionId = UUID.fromString("2669cbf8-32c1-4796-bf43-2f36a8a80cc8");

        UpdateQuestionRequest request = UpdateQuestionRequest.builder()
                .id(questionId)
                .text("New text")
                .build();

        QuestionResponse questionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + QUESTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        QuestionEntity questionEntity = questionRepository.getReferenceById(questionResponse.getId());
        Assertions.assertEquals(request.getText(), questionEntity.getText());
        Assertions.assertNotNull(questionEntity.getTitle());
        Assertions.assertEquals(request.getText(), questionResponse.getText());
        Assertions.assertNotNull(questionResponse.getIssuer().getId());

    }

    @Test
    void updateQuestionFailedQuestionNotFound() throws Exception {

        UUID questionId = UUID.fromString("2769cbf8-32c1-4796-bf43-2f36a8a80cc8");

        UpdateQuestionRequest request = UpdateQuestionRequest.builder()
                .id(questionId)
                .text("New text")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + QUESTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteQuestionSuccessfully() throws Exception {

        UUID questionId = UUID.fromString("2669cbf8-32c1-4796-bf43-2f36a8a80cc8");

        RequestId request = new RequestId(questionId);

        QuestionResponse questionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + QUESTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        QuestionEntity questionEntity = questionRepository.getReferenceById(questionId);
        Assertions.assertTrue(questionEntity.isDeleted());
        Assertions.assertTrue(questionResponse.isDeleted());

    }

    @Test
    void deleteQuestionFailedQuestionNotFound() throws Exception {

        UUID questionId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        RequestId request = new RequestId(questionId);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + QUESTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void restoreQuestionSuccessfully() throws Exception {

        UUID questionId = UUID.fromString("2869cbf8-32c1-4796-bf43-2f36a8a80cc8");

        RequestId request = new RequestId(questionId);

        QuestionResponse questionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + QUESTION_PREFIX + "/restore")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertFalse(questionResponse.isDeleted());

        QuestionEntity questionEntity = questionRepository.getReferenceById(questionId);
        Assertions.assertFalse(questionEntity.isDeleted());
    }

    @Test
    void getByIdSuccessfully() throws Exception {

        UUID questionId = UUID.fromString("3069cbf8-32c1-4796-bf43-2f36a8a80cc8");
        QuestionResponse questionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + QUESTION_PREFIX + "/" + questionId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(questionResponse.getId());
        Assertions.assertNotNull(questionResponse.getText());
        Assertions.assertNotNull(questionResponse.getTitle());
        Assertions.assertEquals(1, questionResponse.getVotes());
        Assertions.assertNotNull(questionResponse.getAnswers().get(0));
        Assertions.assertNotNull(questionResponse.getAnswers().get(0).getText());
    }

    @Test
    void getByIdFailedQuestionNotFound() throws Exception {

        UUID questionId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + QUESTION_PREFIX + "/" + questionId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getQuestionWithAnswersWithThisAccountVote() throws Exception {

        UUID questionId = UUID.fromString("3269cbf8-32c1-4796-bf43-2f36a8a80cc8");

        QuestionResponse questionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + QUESTION_PREFIX + "/" + questionId + "/answers/votes")
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + FIRST_DEVELOPER_TOKEN))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(questionResponse.getText());
        Assertions.assertNotNull(questionResponse.getTitle());
        Assertions.assertNotNull(questionResponse.getIssuer().getId());
        Assertions.assertNotNull(questionResponse.getIssuer().getFirstName());
        Assertions.assertNotNull(questionResponse.getIssuer().getLastName());
        Assertions.assertEquals(3, questionResponse.getAnswers().size());
        Assertions.assertEquals("3269cbf8-32c1-4796-bf43-2f36a8a80cc8", questionResponse.getAnswers().get(0).getId().toString());
        Assertions.assertEquals(Vote.UP.toString(), questionResponse.getAnswers().get(0).getVoteThisAccount());
        Assertions.assertEquals(Vote.NOT_MARKED.toString(), questionResponse.getAnswers().get(1).getVoteThisAccount());

    }

    @Test
    void getAllWithParamSuccessfully() throws Exception {

        ResponsePage<QuestionResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + QUESTION_PREFIX)
                                .param("sortBy", "createdDate")
                                .param("sortType", Sort.Direction.DESC.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(4, page.getTotalElements());
        Assertions.assertEquals("2669cbf8-32c1-4796-bf43-2f36a8a80cc8", page.getContent().get(0).getId().toString());
        Assertions.assertNotNull(page.getContent().get(0).getTitle());
        Assertions.assertNotNull(page.getContent().get(0).getText());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer().getFirstName());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer().getId());
    }

    @Test
    void getAllSuccessfully() throws Exception {

        ResponsePage<QuestionResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + QUESTION_PREFIX))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(4, page.getTotalElements());
        Assertions.assertEquals("3069cbf8-32c1-4796-bf43-2f36a8a80cc8", page.getContent().get(0).getId().toString());
        Assertions.assertNotNull(page.getContent().get(0).getTitle());
        Assertions.assertNotNull(page.getContent().get(0).getText());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer().getFirstName());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer().getId());
    }

    @Test
    void getByIssuerSuccessfully() throws Exception {

        String issuerId = "2669cbf8-32c1-4796-bf43-2f36a8a80cc8";
        ResponsePage<QuestionResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + QUESTION_PREFIX+"/by/issuer/"+ issuerId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertEquals("3069cbf8-32c1-4796-bf43-2f36a8a80cc8", page.getContent().get(0).getId().toString());
        Assertions.assertNotNull(page.getContent().get(0).getTitle());
        Assertions.assertNotNull(page.getContent().get(0).getText());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer().getId());

    }

    @Test
    void search() throws Exception {

        String query = "hibernate";

        ResponsePage<QuestionResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + QUESTION_PREFIX+"/search")
                                .param("query", query))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getTitle());
        Assertions.assertNotNull(page.getContent().get(0).getText());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer().getFirstName());
        Assertions.assertNotNull(page.getContent().get(0).getIssuer().getId());
    }
}
