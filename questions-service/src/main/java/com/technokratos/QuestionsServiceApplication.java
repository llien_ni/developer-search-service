package com.technokratos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@ConfigurationPropertiesScan("com.technokratos")
@SpringBootApplication
public class QuestionsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuestionsServiceApplication.class, args);
    }

}
