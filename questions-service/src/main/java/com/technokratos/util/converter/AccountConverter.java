package com.technokratos.util.converter;

import authorization.service.AccountServiceOuterClass;
import com.technokratos.dto.response.AccountResponse;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class AccountConverter {

    public static AccountResponse getAccountResponse(AccountServiceOuterClass.AccountResponse account) {

            return AccountResponse.builder()
                    .id(UUID.fromString(account.getId()))
                    .firstName(account.getFirstName())
                    .lastName(account.getLastname())
                    .role(account.getRole())
                    .status(account.getStatus())
                    .photo(account.getPhoto())
                    .build();
    }
}
