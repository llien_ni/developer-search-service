package com.technokratos.util;

import com.technokratos.entity.enums.Vote;
import com.technokratos.exception.IdentifyVoteException;
import org.springframework.stereotype.Component;

@Component
public class ValidationUtil {

    public static void validateVote(String vote) {
        try{
            Vote.valueOf(vote);
        }catch (IllegalArgumentException e){
            throw new IdentifyVoteException();
        }
    }
}
