package com.technokratos.client;

import com.technokratos.dto.request.RatingChangeRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorizationServiceRabbitClient {

    private final RabbitTemplate rabbitTemplate;

    public void sendMessageToChangeDeveloperRating(RatingChangeRequest ratingChangeRequest) {

        rabbitTemplate.convertAndSend("direct-exchange", "questions-service", ratingChangeRequest);
        log.info("sent");
    }
}
