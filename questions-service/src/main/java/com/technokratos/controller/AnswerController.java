package com.technokratos.controller;

import com.technokratos.api.AnswerApi;
import com.technokratos.dto.request.AnswerRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateAnswerRequest;
import com.technokratos.dto.response.AnswerResponse;
import com.technokratos.service.AnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AnswerController implements AnswerApi {

    private final AnswerService answerService;

    @Override
    public AnswerResponse add(AnswerRequest request, String accountId) {

        return answerService.add(request, accountId);
    }

    @Override
    public AnswerResponse update(UpdateAnswerRequest request, String accountId) {

        return answerService.update(request, accountId);
    }

    @Override
    public AnswerResponse delete(RequestId requestId, String accountId) {

        return answerService.delete(requestId.getId(), accountId);
    }

    @Override
    public AnswerResponse restore(RequestId requestId, String accountId) {

        return answerService.restore(requestId.getId(), accountId);
    }

    @Override
    public Page<AnswerResponse> getByAccount(UUID accountId, Integer size, Integer number) {

        return answerService.getByAccount(accountId, size, number);
    }

    @Override
    public AnswerResponse getById(UUID id) {

        return answerService.getById(id);
    }

    @Override
    public AnswerResponse markAnswerAsTheBest(UUID answerId, String accountId) {

        return answerService.markAnswerAsTheBest(answerId, accountId);
    }
}
