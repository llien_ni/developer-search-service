package com.technokratos.controller;

import com.technokratos.api.QuestionVoteApi;
import com.technokratos.dto.request.QuestionVoteRequest;
import com.technokratos.dto.response.QuestionVoteResponse;
import com.technokratos.service.QuestionVoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class QuestionVoteController implements QuestionVoteApi {

    private final QuestionVoteService questionVoteService;

    @Override
    public QuestionVoteResponse vote(QuestionVoteRequest request, String accountId) {

        return questionVoteService.vote(request, accountId);
    }
}
