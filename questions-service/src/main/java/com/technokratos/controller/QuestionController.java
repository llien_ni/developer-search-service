package com.technokratos.controller;

import com.technokratos.api.QuestionApi;
import com.technokratos.dto.request.QuestionRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateQuestionRequest;
import com.technokratos.dto.response.QuestionResponse;
import com.technokratos.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class QuestionController implements QuestionApi {

    private final QuestionService questionService;

    @Override
    public QuestionResponse add(QuestionRequest request, String issuerId) {

        return questionService.add(request, issuerId);
    }

    @Override
    public QuestionResponse update(UpdateQuestionRequest request, String issuerId) {

        return questionService.update(request, issuerId);
    }

    @Override
    public QuestionResponse delete(RequestId request, String issuerId) {

        return questionService.delete(request.getId(), issuerId);
    }

    @Override
    public QuestionResponse restore(RequestId requestId, String issuerId) {

        return questionService.restoreDeletedQuestion(requestId.getId(), issuerId);
    }

    @Override
    public QuestionResponse getById(UUID id) {

        return questionService.getById(id);
    }

    @Override
    public QuestionResponse getQuestionWithAnswersWithThisAccountVote(UUID questionId, String accountId) {

        return questionService.getQuestionWithAnswersWithThisAccountVote(questionId, accountId);
    }

    @Override
    public Page<QuestionResponse> getAll(String sortBy, String sortType, Integer size, Integer number) {

        return questionService.getAll(sortBy, sortType, size, number);
    }

    @Override
    public Page<QuestionResponse> getByIssuer(String sortBy, String sortType, UUID issuerId, Integer size, Integer number) {

        return questionService.getByIssuer(sortBy, sortType, issuerId, size, number);
    }

    @Override
    public Page<QuestionResponse> search(String sortBy, String sortType, String query, Integer size, Integer number) {

        return questionService.search(sortBy, sortType, query, size, number);
    }
}
