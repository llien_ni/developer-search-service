package com.technokratos.controller;

import com.technokratos.api.AnswerVoteApi;
import com.technokratos.dto.request.AnswerVoteRequest;
import com.technokratos.dto.response.AnswerVoteResponse;
import com.technokratos.service.AnswerVoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class AnswerVoteController implements AnswerVoteApi {

    private final AnswerVoteService answerVoteService;

    @Override
    public AnswerVoteResponse vote(AnswerVoteRequest request, String accountId) {

        return answerVoteService.vote(request, accountId);
    }
}
