package com.technokratos.api;

import com.technokratos.dto.request.AnswerVoteRequest;
import com.technokratos.dto.response.AnswerVoteResponse;
import com.technokratos.dto.response.ExceptionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@PreAuthorize("hasRole('ROLE_DEVELOPER')")
@RequestMapping("/answer/vote")
public interface AnswerVoteApi {

    @Operation(summary = "add a vote to the answer, if the vote already exists, then it is deleted")
    @ApiResponse(responseCode = "202", description = "vote successfully added or deleted",
            content = @Content(schema = @Schema(implementation = AnswerVoteResponse.class)))
    @ApiResponse(responseCode = "404", description = "answer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    AnswerVoteResponse vote(@RequestBody @Valid AnswerVoteRequest request,
                            @AuthenticationPrincipal String accountId);
}
