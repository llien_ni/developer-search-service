package com.technokratos.api;

import com.technokratos.dto.request.AnswerRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateAnswerRequest;
import com.technokratos.dto.response.AnswerResponse;
import com.technokratos.dto.response.ExceptionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/answer")
public interface AnswerApi {

    @Operation(summary = "add answer")
    @ApiResponse(responseCode = "201", description = "answer successfully added",
            content = @Content(schema = @Schema(implementation = AnswerResponse.class)))
    @ApiResponse(responseCode = "404", description = "question not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    AnswerResponse add(@RequestBody @Valid AnswerRequest request, @AuthenticationPrincipal String accountId);

    @Operation(summary = "update answer")
    @ApiResponse(responseCode = "202", description = "answer successfully updated",
            content = @Content(schema = @Schema(implementation = AnswerResponse.class)))
    @ApiResponse(responseCode = "404", description = "answer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    AnswerResponse update(@RequestBody @Valid UpdateAnswerRequest request, @AuthenticationPrincipal String accountId);

    @Operation(summary = "delete answer")
    @ApiResponse(responseCode = "202", description = "answer successfully deleted",
            content = @Content(schema = @Schema(implementation = AnswerResponse.class)))
    @ApiResponse(responseCode = "404", description = "answer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    AnswerResponse delete(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String accountId);

    @Operation(summary = "restore deleted answer")
    @ApiResponse(responseCode = "202", description = "answer successfully restored",
            content = @Content(schema = @Schema(implementation = AnswerResponse.class)))
    @ApiResponse(responseCode = "404", description = "answer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PatchMapping("/restore")
    @ResponseStatus(HttpStatus.ACCEPTED)
    AnswerResponse restore(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String accountId);

    @Operation(summary = "get user answers")
    @ApiResponse(responseCode = "200", description = "answers successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/by/account/{account-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<AnswerResponse> getByAccount(@PathVariable("account-id") UUID accountId,
                                      @RequestParam(value = "size", required = false) Integer size,
                                      @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get answer by id")
    @ApiResponse(responseCode = "200", description = "answer successfully received",
            content = @Content(schema = @Schema(implementation = AnswerResponse.class)))
    @ApiResponse(responseCode = "404", description = "answer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    AnswerResponse getById(@PathVariable("id") UUID id);

    @Operation(summary = "author of the question marks the answer as the best, " +
            "if the answer is already marked as the best, then the mark is removed")
    @ApiResponse(responseCode = "202", description = "answer mark as the best, if the answer has already been marked as " +
            "the best then the mark has been removed",
            content = @Content(schema = @Schema(implementation = AnswerResponse.class)))
    @ApiResponse(responseCode = "400", description = "can't mark answer as the best",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "answer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PatchMapping("/{id}/mark_best_solution")
    @ResponseStatus(HttpStatus.ACCEPTED)
    AnswerResponse markAnswerAsTheBest(@PathVariable("id") UUID answerId,
                                       @AuthenticationPrincipal String accountId);

}
