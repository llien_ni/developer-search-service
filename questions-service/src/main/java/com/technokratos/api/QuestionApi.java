package com.technokratos.api;

import com.technokratos.dto.request.QuestionRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateQuestionRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.QuestionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/question")
public interface QuestionApi {

    @Operation(summary = "add question")
    @ApiResponse(responseCode = "201", description = "question successfully added",
            content = @Content(schema = @Schema(implementation = QuestionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    QuestionResponse add(@RequestBody @Valid QuestionRequest request, @AuthenticationPrincipal String issuerId);

    @Operation(summary = "update question")
    @ApiResponse(responseCode = "202", description = "question successfully updated",
            content = @Content(schema = @Schema(implementation = QuestionResponse.class)))
    @ApiResponse(responseCode = "404", description = "question not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    QuestionResponse update(@RequestBody @Valid UpdateQuestionRequest request, @AuthenticationPrincipal String issuerId);

    @Operation(summary = "delete question")
    @ApiResponse(responseCode = "202", description = "question successfully deleted",
            content = @Content(schema = @Schema(implementation = QuestionResponse.class)))
    @ApiResponse(responseCode = "404", description = "question not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    QuestionResponse delete(@RequestBody @Valid RequestId request, @AuthenticationPrincipal String issuerId);

    @Operation(summary = "restore deleted question")
    @ApiResponse(responseCode = "202", description = "question successfully restored",
            content = @Content(schema = @Schema(implementation = QuestionResponse.class)))
    @ApiResponse(responseCode = "404", description = "question not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PatchMapping("/restore")
    @ResponseStatus(HttpStatus.ACCEPTED)
    QuestionResponse restore(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String issuerId);

    @Operation(summary = "get question by id with answers and sort answers by votes")
    @ApiResponse(responseCode = "200", description = "question successfully received",
            content = @Content(schema = @Schema(implementation = QuestionResponse.class)))
    @ApiResponse(responseCode = "404", description = "question not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    QuestionResponse getById(@PathVariable("id") UUID id);

    @Operation(summary = "get question by id with answers and marks if the user marked the answers and sort answers by votes")
    @ApiResponse(responseCode = "200", description = "question successfully received",
            content = @Content(schema = @Schema(implementation = QuestionResponse.class)))
    @ApiResponse(responseCode = "404", description = "question not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @GetMapping("/{id}/answers/votes")
    @ResponseStatus(HttpStatus.OK)
    QuestionResponse getQuestionWithAnswersWithThisAccountVote(@PathVariable("id") UUID questionId, @AuthenticationPrincipal() String accountId);

    @Operation(summary = "get all questions")
    @ApiResponse(responseCode = "200", description = "questions successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<QuestionResponse> getAll(@RequestParam(value = "sortBy", required = false)String sortBy,
                                  @RequestParam(value = "sortType", required = false) String sortType,
                                  @RequestParam(value = "size", required = false) Integer size,
                                  @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get questions by issuer")
    @ApiResponse(responseCode = "200", description = "questions successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/by/issuer/{issuer-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<QuestionResponse> getByIssuer(@RequestParam(value = "sortBy", required = false)String sortBy,
                                       @RequestParam (value = "sortType", required = false)String sortType,
                                       @PathVariable("issuer-id")UUID issuerId,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "search questions by words in title or question text")
    @ApiResponse(responseCode = "200", description = "questions successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    Page<QuestionResponse> search(@RequestParam(value = "sortBy", required = false)String sortBy,
                                  @RequestParam(value = "sortType", required = false) String sortType,
                                  @RequestParam(value = "query") String query,
                                  @RequestParam(value = "size", required = false) Integer size,
                                  @RequestParam(value = "number", required = false) Integer number);
}
