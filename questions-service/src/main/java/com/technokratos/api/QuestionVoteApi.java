package com.technokratos.api;

import com.technokratos.dto.request.QuestionVoteRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.QuestionVoteResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@PreAuthorize("hasRole('ROLE_DEVELOPER')")
@RequestMapping("/question/vote")
public interface QuestionVoteApi {

    @Operation(summary = "add a vote to the question, if the vote already exists, then it is deleted")
    @ApiResponse(responseCode = "202", description = "vote successfully added or deleted",
            content = @Content(schema = @Schema(implementation = QuestionVoteResponse.class)))
    @ApiResponse(responseCode = "404", description = "question not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    QuestionVoteResponse vote(@RequestBody @Valid QuestionVoteRequest request,
                              @AuthenticationPrincipal String accountId);
}
