package com.technokratos.mapper;


import com.technokratos.dto.request.QuestionRequest;
import com.technokratos.dto.request.UpdateQuestionRequest;
import com.technokratos.dto.response.QuestionResponse;
import com.technokratos.entity.QuestionEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface QuestionMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    QuestionEntity fromRequestToEntity(QuestionRequest request);

    @Mapping(source = "issuerId", target = "issuer.id")
    QuestionResponse fromEntityToResponse(QuestionEntity entity);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(UpdateQuestionRequest request, @MappingTarget QuestionEntity questionEntity);
}
