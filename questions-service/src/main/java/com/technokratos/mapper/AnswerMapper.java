package com.technokratos.mapper;


import com.technokratos.dto.request.AnswerRequest;
import com.technokratos.dto.request.UpdateAnswerRequest;
import com.technokratos.dto.response.AnswerResponse;
import com.technokratos.entity.AnswerEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface AnswerMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    AnswerEntity fromRequestToEntity(AnswerRequest request);

    @Mapping(source = "question.id", target = "questionId")
    @Mapping(source = "accountId", target = "account.id")
    AnswerResponse fromEntityToResponse(AnswerEntity answerEntity);

    @Mapping(target = "id", ignore = true)
    void update(UpdateAnswerRequest request, @MappingTarget AnswerEntity answerEntity);
}
