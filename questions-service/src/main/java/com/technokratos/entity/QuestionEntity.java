package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "question")
public class QuestionEntity extends AbstractBaseEntity{

    private String title;

    private String text;

    @Column(name = "issuer_id")
    private UUID issuerId;

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;

    @Column(name = "updated_date")
    @UpdateTimestamp
    private OffsetDateTime updateDate;

    private Integer votes;

    private boolean deleted;

    @OneToMany(mappedBy = "question", cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<AnswerEntity> answers;

    @OneToMany(mappedBy = "question", cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<QuestionVoteEntity> questionVotes;
}
