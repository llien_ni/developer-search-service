package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "answer")
public class AnswerEntity extends AbstractBaseEntity{

    private String text;

    @Column(name ="account_id")
    private UUID accountId;

    @Column(name = "best_solution")
    private boolean bestSolution;

    @ManyToOne
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    private QuestionEntity question;

    @OneToMany(mappedBy = "answer")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<AnswerVoteEntity> answerVotes;

    private Integer votes;

    private boolean deleted;

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;

    @Column(name = "updated_date")
    @UpdateTimestamp
    private OffsetDateTime updatedDate;
}
