package com.technokratos.entity.enums;

public enum Vote {
    UP, DOWN, NOT_MARKED
}
