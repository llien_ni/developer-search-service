package com.technokratos.entity;


import com.technokratos.entity.enums.Vote;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "question_vote")
public class QuestionVoteEntity extends AbstractBaseEntity {

    @Enumerated(value = EnumType.STRING)
    private Vote vote;

    @Column(name = "account_id")
    private UUID accountId;

    @ManyToOne
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    private QuestionEntity question;
}
