package com.technokratos.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AnswerResponse {

    private UUID id;

    private AccountResponse account;

    private boolean bestSolution;

    private UUID questionId;

    private Integer votes;

    private OffsetDateTime createdDate;

    private OffsetDateTime updatedDate;

    private String text;

    private boolean deleted;

    private String voteThisAccount;
}
