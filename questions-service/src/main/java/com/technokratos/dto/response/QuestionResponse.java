package com.technokratos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuestionResponse {

    private UUID id;

    private String title;

    private String text;

    private AccountResponse issuer;

    private OffsetDateTime createdDate;

    private OffsetDateTime updateDate;

    private Integer votes;

    private Integer answersCount;

    private List<AnswerResponse> answers;

    private boolean deleted;

    private String voteThisAccount;
}
