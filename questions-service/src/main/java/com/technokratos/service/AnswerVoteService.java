package com.technokratos.service;

import com.technokratos.dto.request.AnswerVoteRequest;
import com.technokratos.dto.response.AnswerVoteResponse;


public interface AnswerVoteService {

    AnswerVoteResponse vote(AnswerVoteRequest request, String accountId);

}
