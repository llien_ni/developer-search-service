package com.technokratos.service;

import com.technokratos.dto.request.QuestionVoteRequest;
import com.technokratos.dto.response.QuestionVoteResponse;


public interface QuestionVoteService {

    QuestionVoteResponse vote(QuestionVoteRequest request, String accountId);
}
