package com.technokratos.service;

import com.technokratos.dto.request.AnswerRequest;
import com.technokratos.dto.request.UpdateAnswerRequest;
import com.technokratos.dto.response.AnswerResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface AnswerService {

    AnswerResponse add(AnswerRequest request, String accountId);

    AnswerResponse update(UpdateAnswerRequest request, String accountId);

    AnswerResponse delete(UUID id, String accountId);

    AnswerResponse restore(UUID id, String accountId);

    Page<AnswerResponse> getByAccount(UUID accountId, Integer size, Integer number);

    AnswerResponse getById(UUID id);

    AnswerResponse markAnswerAsTheBest(UUID answerId, String accountId);
}
