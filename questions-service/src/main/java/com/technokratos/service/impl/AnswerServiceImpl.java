package com.technokratos.service.impl;

import authorization.service.AccountServiceOuterClass;
import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.dto.request.AnswerRequest;
import com.technokratos.dto.request.UpdateAnswerRequest;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.AnswerResponse;
import com.technokratos.entity.AnswerEntity;
import com.technokratos.entity.QuestionEntity;
import com.technokratos.exception.AnswerNotFoundException;
import com.technokratos.exception.MarkAnswerException;
import com.technokratos.exception.QuestionNotFoundException;
import com.technokratos.mapper.AnswerMapper;
import com.technokratos.repository.AnswerRepository;
import com.technokratos.repository.QuestionRepository;
import com.technokratos.service.AnswerService;
import com.technokratos.util.RequestParamUtil;
import com.technokratos.util.converter.AccountConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;

    private final QuestionRepository questionRepository;

    private final AnswerMapper answerMapper;

    private final RequestParamUtil requestParamUtil;

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    @Transactional
    @Override
    public AnswerResponse add(AnswerRequest request, String accountId) {

        QuestionEntity question = questionRepository.findById(request.getQuestionId()).orElseThrow(QuestionNotFoundException::new);
        AnswerEntity answerEntity = answerMapper.fromRequestToEntity(request);
        answerEntity.setVotes(0);
        answerEntity.setAnswerVotes(new ArrayList<>());
        answerEntity.setQuestion(question);
        answerEntity.setAccountId(UUID.fromString(accountId));
        AnswerEntity savedAnswer = answerRepository.save(answerEntity);
        question.getAnswers().add(savedAnswer);
        questionRepository.save(question);
        return answerMapper.fromEntityToResponse(savedAnswer);
    }

    @Transactional
    @Override
    public AnswerResponse update(UpdateAnswerRequest request, String accountId) {

        AnswerEntity answerEntity = answerRepository.findByIdAndAccountIdAndDeletedIsFalse(request.getId(), UUID.fromString(accountId))
                .orElseThrow(AnswerNotFoundException::new);
        answerMapper.update(request, answerEntity);
        return answerMapper.fromEntityToResponse(answerRepository.save(answerEntity));
    }

    @Transactional
    @Override
    public AnswerResponse delete(UUID answerId, String accountId) {

        AnswerEntity answerEntity = answerRepository.findByIdAndAccountId(answerId, UUID.fromString(accountId))
                .orElseThrow(AnswerNotFoundException::new);
        answerEntity.setDeleted(true);
        return answerMapper.fromEntityToResponse(answerRepository.save(answerEntity));
    }

    @Transactional
    @Override
    public AnswerResponse restore(UUID answerId, String accountId) {

        AnswerEntity answerEntity = answerRepository.findByIdAndAccountId(answerId, UUID.fromString(accountId)).orElseThrow(AnswerNotFoundException::new);
        answerEntity.setDeleted(false);
        return answerMapper.fromEntityToResponse(answerRepository.save(answerEntity));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<AnswerResponse> getByAccount(UUID accountId, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Sort sort = Sort.by(Sort.Direction.DESC, "updatedDate");
        Page<AnswerEntity> page = answerRepository.findAllByAccountIdAndDeletedIsFalse(accountId, pageRequest.withSort(sort));
        return page.map(answerMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public AnswerResponse getById(UUID id) {

        AnswerResponse answerResponse = answerMapper.fromEntityToResponse(answerRepository.findById(id)
                .orElseThrow(AnswerNotFoundException::new));
        return convertToAnswerResponseWithAccountResponse(answerResponse);
    }

    @Transactional
    @Override
    public AnswerResponse markAnswerAsTheBest(UUID answerId, String accountId) {

        AnswerEntity answerEntity = answerRepository.findByIdAndDeletedIsFalse(answerId).orElseThrow(AnswerNotFoundException::new);
        if (answerEntity.getQuestion().getIssuerId().equals(UUID.fromString(accountId))) {

            answerEntity.setBestSolution(!answerEntity.isBestSolution());
        } else {
            throw new MarkAnswerException();
        }
        return answerMapper.fromEntityToResponse(answerRepository.save(answerEntity));
    }

    private AnswerResponse convertToAnswerResponseWithAccountResponse(AnswerResponse answerResponse) {
        AccountServiceOuterClass.AccountResponse account = authorizationServiceGrpcClient.getAccountById(AccountServiceOuterClass.GetAccountByIdRequest
                .newBuilder()
                .setId(answerResponse.getAccount().getId().toString())
                .build());
        AccountResponse accountResponse = AccountConverter.getAccountResponse(account);
        answerResponse.setAccount(accountResponse);
        return answerResponse;
    }

}
