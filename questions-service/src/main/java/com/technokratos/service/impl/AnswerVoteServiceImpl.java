package com.technokratos.service.impl;

import com.technokratos.client.AuthorizationServiceRabbitClient;
import com.technokratos.dto.request.AnswerVoteRequest;
import com.technokratos.dto.request.RatingChangeRequest;
import com.technokratos.dto.response.AnswerVoteResponse;
import com.technokratos.entity.AnswerEntity;
import com.technokratos.entity.AnswerVoteEntity;
import com.technokratos.entity.enums.Vote;
import com.technokratos.exception.AddVoteToAnswerException;
import com.technokratos.exception.AnswerNotFoundException;
import com.technokratos.repository.AnswerRepository;
import com.technokratos.repository.AnswerVoteRepository;
import com.technokratos.service.AnswerVoteService;
import com.technokratos.util.ValidationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AnswerVoteServiceImpl implements AnswerVoteService {

    private final AnswerRepository answerRepository;

    private final AnswerVoteRepository answerVoteRepository;

    private final AuthorizationServiceRabbitClient authorizationServiceRabbitClient;

    @Override
    public AnswerVoteResponse vote(AnswerVoteRequest request, String accountId) {

        ValidationUtil.validateVote(request.getVote());

        AnswerEntity answerEntity = answerRepository.findById(request.getAnswerId()).orElseThrow(AnswerNotFoundException::new);
        checkAnswerIssuerId(accountId, answerEntity);

        AnswerVoteResponse answerVoteResponse;

        Optional<AnswerVoteEntity> answerVote = answerVoteRepository.findByAnswerAndAccountId(answerEntity, UUID.fromString(accountId));
        //еще не ставил оценку
        if (answerVote.isEmpty()) {
            AnswerVoteEntity answerVoteEntity = AnswerVoteEntity.builder()
                    .accountId(UUID.fromString(accountId))
                    .answer(answerEntity)
                    .vote(Vote.valueOf(request.getVote()))
                    .build();
            answerVoteRepository.save(answerVoteEntity);
            answerEntity.getAnswerVotes().add(answerVoteEntity);
            answerRepository.save(answerEntity);

            if (request.getVote().equals(Vote.UP.toString())) {
                answerEntity.setVotes(answerEntity.getVotes() + 1);
                answerVoteResponse = AnswerVoteResponse.builder()
                        .answerId(request.getAnswerId())
                        .vote(Vote.UP.toString())
                        .build();

                authorizationServiceRabbitClient.sendMessageToChangeDeveloperRating(RatingChangeRequest.builder()
                        .developerId(answerEntity.getAccountId())
                        .value(10)
                        .build());
            } else {
                answerEntity.setVotes(answerEntity.getVotes() - 1);
                answerVoteResponse = AnswerVoteResponse.builder()
                        .answerId(request.getAnswerId())
                        .vote(Vote.DOWN.toString())
                        .build();

                authorizationServiceRabbitClient.sendMessageToChangeDeveloperRating(RatingChangeRequest.builder()
                        .developerId(answerEntity.getAccountId())
                        .value(-2)
                        .build());
            }
            answerRepository.save(answerEntity);
        } else {
            //уже ставил оценку она удаляется
            AnswerVoteEntity answerVoteEntity = answerVote.get();
            if (answerVoteEntity.getVote().equals(Vote.UP)) {
                answerEntity.setVotes(answerEntity.getVotes() - 1);

                authorizationServiceRabbitClient.sendMessageToChangeDeveloperRating(RatingChangeRequest.builder()
                        .developerId(answerEntity.getAccountId())
                        .value(-10)
                        .build());
            } else {
                answerEntity.setVotes(answerEntity.getVotes() + 1);

                authorizationServiceRabbitClient.sendMessageToChangeDeveloperRating(RatingChangeRequest.builder()
                        .developerId(answerEntity.getAccountId())
                        .value(2)
                        .build());
            }
            answerEntity.getAnswerVotes().remove(answerVoteEntity);
            answerRepository.save(answerEntity);
            answerVoteRepository.delete(answerVoteEntity);

            answerVoteResponse = AnswerVoteResponse.builder()
                    .answerId(request.getAnswerId())
                    .vote(Vote.NOT_MARKED.toString())
                    .build();
        }
        return answerVoteResponse;
    }

    private void checkAnswerIssuerId(String accountId, AnswerEntity answerEntity) {

        if (answerEntity.getAccountId().equals(UUID.fromString(accountId))) {
            throw new AddVoteToAnswerException();
        }
    }
}
