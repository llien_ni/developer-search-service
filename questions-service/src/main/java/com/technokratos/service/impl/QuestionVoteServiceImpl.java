package com.technokratos.service.impl;


import com.technokratos.client.AuthorizationServiceRabbitClient;
import com.technokratos.dto.request.QuestionVoteRequest;
import com.technokratos.dto.request.RatingChangeRequest;
import com.technokratos.dto.response.QuestionVoteResponse;
import com.technokratos.entity.QuestionEntity;
import com.technokratos.entity.QuestionVoteEntity;
import com.technokratos.entity.enums.Vote;
import com.technokratos.exception.AddVoteToQuestionException;
import com.technokratos.exception.QuestionNotFoundException;
import com.technokratos.repository.QuestionRepository;
import com.technokratos.repository.QuestionVoteRepository;
import com.technokratos.service.QuestionVoteService;
import com.technokratos.util.ValidationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class QuestionVoteServiceImpl implements QuestionVoteService {

    private final QuestionVoteRepository questionVoteRepository;

    private final QuestionRepository questionRepository;

    private final AuthorizationServiceRabbitClient authorizationServiceRabbitClient;

    @Transactional
    @Override
    public QuestionVoteResponse vote(QuestionVoteRequest request, String accountId) {

        ValidationUtil.validateVote(request.getVote());

        QuestionEntity question = questionRepository.findById(request.getQuestionId()).orElseThrow(QuestionNotFoundException::new);
        checkQuestionIssuerId(accountId, question);

        QuestionVoteResponse questionVoteResponse;
        Optional<QuestionVoteEntity> questionVote = questionVoteRepository.findByQuestionAndAccountId(question, UUID.fromString(accountId));

        if(questionVote.isEmpty()){

            QuestionVoteEntity questionVoteEntity = QuestionVoteEntity.builder()
                    .accountId(UUID.fromString(accountId))
                    .question(question)
                    .vote(Vote.valueOf(request.getVote()))
                    .build();
            questionVoteRepository.save(questionVoteEntity);

            question.getQuestionVotes().add(questionVoteEntity);
            questionRepository.save(question);

            if(request.getVote().equals(Vote.UP.toString())){
                question.setVotes(question.getVotes()+1);
                questionVoteResponse = QuestionVoteResponse.builder()
                        .questionId(request.getQuestionId())
                        .vote(Vote.UP.toString())
                        .build();
                //девелопер rating +10 если UP
                authorizationServiceRabbitClient.sendMessageToChangeDeveloperRating(RatingChangeRequest.builder()
                        .developerId(question.getIssuerId())
                        .value(10)
                        .build());
            }else {
                question.setVotes(question.getVotes()-1);
                questionVoteResponse = QuestionVoteResponse.builder()
                        .questionId(request.getQuestionId())
                        .vote(Vote.DOWN.toString())
                        .build();
                //если DOWN то не меняется
            }
        }else{
            //то есть уже ставил оценку она удаляется
            QuestionVoteEntity questionVoteEntity = questionVote.get();
            if(questionVoteEntity.getVote().equals(Vote.UP)){
                //девелопер rating -10, так как оценка удаляется
                question.setVotes(question.getVotes()-1);
                authorizationServiceRabbitClient.sendMessageToChangeDeveloperRating(RatingChangeRequest.builder()
                        .developerId(question.getIssuerId())
                        .value(-10)
                        .build());
            }else {
                //не меняем потому что если оценка была DOWN то это не влияет на rating
                question.setVotes(question.getVotes()+1);
            }
            question.getQuestionVotes().remove(questionVoteEntity);
            questionVoteRepository.delete(questionVoteEntity);

            questionVoteResponse = QuestionVoteResponse.builder()
                    .questionId(request.getQuestionId())
                    .vote(Vote.NOT_MARKED.toString())
                    .build();
        }
        return questionVoteResponse;
    }

    private void checkQuestionIssuerId(String accountId, QuestionEntity question) {

        if(question.getIssuerId().equals(UUID.fromString(accountId))){
            throw new AddVoteToQuestionException();
        }
    }
}
