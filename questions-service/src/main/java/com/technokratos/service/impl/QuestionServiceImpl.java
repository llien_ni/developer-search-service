package com.technokratos.service.impl;

import authorization.service.AccountServiceOuterClass;
import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.dto.request.QuestionRequest;
import com.technokratos.dto.request.UpdateQuestionRequest;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.QuestionResponse;
import com.technokratos.entity.AnswerVoteEntity;
import com.technokratos.entity.QuestionEntity;
import com.technokratos.entity.QuestionVoteEntity;
import com.technokratos.entity.enums.Vote;
import com.technokratos.exception.QuestionNotFoundException;
import com.technokratos.mapper.QuestionMapper;
import com.technokratos.repository.AnswerVoteRepository;
import com.technokratos.repository.QuestionRepository;
import com.technokratos.repository.QuestionVoteRepository;
import com.technokratos.service.QuestionService;
import com.technokratos.util.RequestParamUtil;
import com.technokratos.util.converter.AccountConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;

    private final QuestionMapper questionMapper;

    private final RequestParamUtil requestParamUtil;

    private final AnswerVoteRepository answerVoteRepository;

    private final QuestionVoteRepository questionVoteRepository;

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    @Transactional
    @Override
    public QuestionResponse add(QuestionRequest request, String issuerId) {

        QuestionEntity questionEntity = questionMapper.fromRequestToEntity(request);
        questionEntity.setAnswers(new ArrayList<>());
        questionEntity.setQuestionVotes(new ArrayList<>());
        questionEntity.setIssuerId(UUID.fromString(issuerId));
        questionEntity.setVotes(0);

        QuestionResponse questionResponse = questionMapper.fromEntityToResponse(questionRepository.save(questionEntity));
        questionResponse.setAnswersCount(questionEntity.getAnswers().size());

        return questionResponse;
    }

    @Transactional
    @Override
    public QuestionResponse update(UpdateQuestionRequest request, String issuerId) {

        QuestionEntity questionEntity = questionRepository.findByIdAndIssuerIdAndDeletedIsFalse(request.getId(), UUID.fromString(issuerId))
                .orElseThrow(QuestionNotFoundException::new);
        questionMapper.update(request, questionEntity);

        QuestionResponse questionResponse = questionMapper.fromEntityToResponse(questionRepository.save(questionEntity));
        questionResponse.setAnswersCount(questionEntity.getAnswers().size());
        return questionResponse;
    }

    @Transactional
    @Override
    public QuestionResponse delete(UUID questionId, String issuerId) {

        QuestionEntity questionEntity = questionRepository.findByIdAndIssuerId(questionId, UUID.fromString(issuerId))
                .orElseThrow(QuestionNotFoundException::new);
        questionEntity.setDeleted(true);

        QuestionResponse questionResponse = questionMapper.fromEntityToResponse(questionRepository.save(questionEntity));
        questionResponse.setAnswersCount(questionEntity.getAnswers().size());
        return questionResponse;
    }

    @Transactional(readOnly = true)
    @Override
    public QuestionResponse getById(UUID id) {

        QuestionResponse questionResponse = questionMapper.fromEntityToResponse(questionRepository.findById(id)
                .orElseThrow(QuestionNotFoundException::new));
        questionResponse.getAnswers().sort((answer1, answer2) -> answer2.getVotes()-answer1.getVotes());
        questionResponse.setAnswersCount(questionResponse.getAnswers().size());
        return convertToQuestionResponseWithIssuerResponse(questionResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<QuestionResponse> getAll(String sortBy, String sortType, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Sort sort = createSort(sortBy, sortType);
        PageRequest pageRequestWithSort = pageRequest.withSort(sort);
        Page<QuestionEntity> page = questionRepository.findAllByDeletedIsFalse(pageRequestWithSort);

        Page<QuestionResponse> responses = page.map(questionMapper::fromEntityToResponse);
        responses.forEach(questionResponse -> questionResponse.setAnswersCount(questionResponse.getAnswers().size()));

        return responses.map(this::convertToQuestionResponseWithIssuerResponse);
    }

    private Sort createSort(String by, String type) {

        Sort.Direction sortType;
        if (type == null) {
            sortType = Sort.Direction.DESC;

        } else {
            sortType = Sort.Direction.fromString(type);
        }
        String sortBy = null;
        if (by == null) {
            sortBy = "votes";
        }else{
            sortBy = by;
        }

        return Sort.by(sortType, sortBy);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<QuestionResponse> getByIssuer(String sortBy, String sortType, UUID issuerId, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Sort sort = createSort(sortBy, sortType);
        Page<QuestionEntity> page = questionRepository.findAllByDeletedIsFalseAndIssuerId(issuerId, pageRequest.withSort(sort));

        Page<QuestionResponse> responses = page.map(questionMapper::fromEntityToResponse);
        responses.forEach(questionResponse -> questionResponse.setAnswersCount(questionResponse.getAnswers().size()));
        return responses;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<QuestionResponse> search(String sortBy, String sortType, String query, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Sort sort = createSort(sortBy, sortType);
        Page<QuestionEntity> page = questionRepository.findAllByTitleLikeOrTextLike('%' + query + '%', '%' + query + '%', pageRequest.withSort(sort));

        Page<QuestionResponse> responses = page.map(questionMapper::fromEntityToResponse);
        responses.forEach(questionResponse -> questionResponse.setAnswersCount(questionResponse.getAnswers().size()));
        return responses.map(this::convertToQuestionResponseWithIssuerResponse);
    }

    @Transactional
    @Override
    public QuestionResponse restoreDeletedQuestion(UUID questionId, String issuerId) {

        QuestionEntity questionEntity = questionRepository.findByIdAndIssuerId(questionId, UUID.fromString(issuerId))
                .orElseThrow(QuestionNotFoundException::new);
        questionEntity.setDeleted(false);

        QuestionResponse questionResponse = questionMapper.fromEntityToResponse(questionRepository.save(questionEntity));
        questionResponse.setAnswersCount(questionEntity.getAnswers().size());
        return questionResponse;
    }

    @Transactional(readOnly = true)
    @Override
    public QuestionResponse getQuestionWithAnswersWithThisAccountVote(UUID questionId, String accountId) {

        QuestionEntity question = questionRepository.findById(questionId).orElseThrow(QuestionNotFoundException::new);
        QuestionResponse questionResponse = questionMapper.fromEntityToResponse(question);

        questionResponse.getAnswers().sort((answer1, answer2) -> answer2.getVotes()-answer1.getVotes());

        questionResponse.getAnswers().forEach(answer -> {
            Optional<AnswerVoteEntity> answerVote = answerVoteRepository
                    .findByAnswer_IdAndAccountId(answer.getId(), UUID.fromString(accountId));

            if (answerVote.isEmpty()) {
                answer.setVoteThisAccount(Vote.NOT_MARKED.toString());
            } else {
                answer.setVoteThisAccount(answerVote.get().getVote().toString());
            }
        });
        questionResponse.setAnswersCount(question.getAnswers().size());

        Optional<QuestionVoteEntity> questionVote = questionVoteRepository.findByQuestionAndAccountId(question, UUID.fromString(accountId));
        if(questionVote.isEmpty()){
            questionResponse.setVoteThisAccount(Vote.NOT_MARKED.toString());
        }else {
            questionResponse.setVoteThisAccount(questionVote.get().getVote().toString());
        }
        return convertToQuestionResponseWithIssuerResponse(questionResponse);
    }

    private QuestionResponse convertToQuestionResponseWithIssuerResponse(QuestionResponse questionResponse) {
        AccountServiceOuterClass.AccountResponse account = authorizationServiceGrpcClient.getAccountById(AccountServiceOuterClass.GetAccountByIdRequest
                .newBuilder()
                .setId(questionResponse.getIssuer().getId().toString())
                .build());
        AccountResponse accountResponse = AccountConverter.getAccountResponse(account);
        questionResponse.setIssuer(accountResponse);
        return questionResponse;
    }
}
