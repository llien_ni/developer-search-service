package com.technokratos.service;

import com.technokratos.dto.request.QuestionRequest;
import com.technokratos.dto.request.UpdateQuestionRequest;
import com.technokratos.dto.response.QuestionResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface QuestionService {

    QuestionResponse add(QuestionRequest request, String issuerId);

    QuestionResponse update(UpdateQuestionRequest request, String issuerId);

    QuestionResponse delete(UUID id, String issuerId);

    QuestionResponse getById(UUID id);

    Page<QuestionResponse> getAll(String sortBy, String sortType, Integer size, Integer number);

    Page<QuestionResponse> getByIssuer(String sortBy, String sortType, UUID issuerId, Integer size, Integer number);

    Page<QuestionResponse> search(String query, String sortType, String s, Integer size, Integer number);

    QuestionResponse restoreDeletedQuestion(UUID id, String issuerId);

    QuestionResponse getQuestionWithAnswersWithThisAccountVote(UUID questionId, String accountId);
}
