package com.technokratos.exception.handler;



import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.ResponseErrorMessage;
import com.technokratos.exception.BadRequestException;
import com.technokratos.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public <T extends NotFoundException> ExceptionResponse handleNotFoundException(T exception) {
        return ExceptionResponse.builder()
                .message(exception.getMessage())
                .build();
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public <T extends BadRequestException> ExceptionResponse handleAlreadyExistException(T exception) {
        return ExceptionResponse.builder()
                .message(exception.getMessage())
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ResponseErrorMessage handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return ResponseErrorMessage.builder()
                .status(HttpStatus.BAD_REQUEST)
                .errors(ex.getBindingResult()
                        .getFieldErrors()
                        .stream()
                        .map(fieldError ->
                                new ResponseErrorMessage.Error(fieldError.getField(),
                                        fieldError.getCode(), fieldError.getDefaultMessage()))
                        .collect(Collectors.toList()))
                .build();
    }
}
