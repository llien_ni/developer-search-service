package com.technokratos.exception;

public class AddVoteToAnswerException extends BadRequestException {

    private static final String MESSAGE = "Failed add vote to answer";

    public AddVoteToAnswerException() {
        super(MESSAGE);
    }
}
