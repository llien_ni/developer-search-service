package com.technokratos.exception;

public class MarkAnswerException extends BadRequestException{

    private static final String MESSAGE = "Can't mark answer as the best";

    public MarkAnswerException() {
        super(MESSAGE);
    }
}
