package com.technokratos.exception;

public class QuestionNotFoundException extends NotFoundException {

    private static final String MESSAGE = "Question not found";

    public QuestionNotFoundException() {

        super(MESSAGE);
    }
}
