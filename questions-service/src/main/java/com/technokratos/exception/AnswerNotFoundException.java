package com.technokratos.exception;

public class AnswerNotFoundException extends NotFoundException{

    private static final String MESSAGE = "Answer not found";

    public AnswerNotFoundException() {
        super(MESSAGE);
    }
}
