package com.technokratos.exception;

public class AddVoteToQuestionException extends BadRequestException {

    private static final String MESSAGE = "Failed add vote to answer";

    public AddVoteToQuestionException() {

        super(MESSAGE);
    }
}
