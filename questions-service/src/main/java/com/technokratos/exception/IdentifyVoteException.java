package com.technokratos.exception;

public class IdentifyVoteException extends BadRequestException{

    private static final String MESSAGE = "Failed to identify vote";

    public IdentifyVoteException() {
        super(MESSAGE);
    }
}
