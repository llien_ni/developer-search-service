package com.technokratos.repository;

import com.technokratos.entity.AnswerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AnswerRepository extends JpaRepository<AnswerEntity, UUID> {

    Optional<AnswerEntity> findByIdAndAccountIdAndDeletedIsFalse(UUID answerId, UUID accountId);

    Optional<AnswerEntity> findByIdAndAccountId(UUID answerId, UUID accountId);

    Page<AnswerEntity> findAllByAccountIdAndDeletedIsFalse(UUID accountId, Pageable pageable);

    Optional<AnswerEntity> findByIdAndDeletedIsFalse(UUID id);
}
