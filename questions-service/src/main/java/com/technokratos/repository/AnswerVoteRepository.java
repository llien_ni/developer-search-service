package com.technokratos.repository;

import com.technokratos.entity.AnswerEntity;
import com.technokratos.entity.AnswerVoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AnswerVoteRepository extends JpaRepository<AnswerVoteEntity, UUID> {

    Optional<AnswerVoteEntity> findByAnswerAndAccountId(AnswerEntity answer, UUID accountId);

    Optional<AnswerVoteEntity> findByAnswer_IdAndAccountId(UUID answerId, UUID accountId);
}
