package com.technokratos.repository;

import com.technokratos.entity.QuestionEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;
import java.util.UUID;

public interface QuestionRepository extends JpaRepository<QuestionEntity, UUID> {

    Optional<QuestionEntity> findByIdAndIssuerId(UUID questionId, UUID issuerId);

    Optional<QuestionEntity> findByIdAndIssuerIdAndDeletedIsFalse(UUID questionId, UUID issuerId);

    Page<QuestionEntity> findAllByDeletedIsFalse(Pageable pageable);

    Page<QuestionEntity> findAllByDeletedIsFalseAndIssuerId(UUID issuerId, Pageable pageable);

    Page<QuestionEntity> findAllByTitleLikeOrTextLike(String queryTitle, String queryText,  Pageable pageable);
}
