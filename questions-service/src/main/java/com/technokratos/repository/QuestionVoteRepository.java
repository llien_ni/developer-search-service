package com.technokratos.repository;

import com.technokratos.entity.QuestionEntity;
import com.technokratos.entity.QuestionVoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface QuestionVoteRepository extends JpaRepository<QuestionVoteEntity, UUID> {

    Optional<QuestionVoteEntity> findByQuestionAndAccountId(QuestionEntity question, UUID accountId);
}
