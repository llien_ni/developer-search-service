package com.technokratos.test.integration;

import authorization.service.AccountServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.MessageModel;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.entity.ChatEntity;
import com.technokratos.entity.MessageEntity;
import com.technokratos.repository.ChatRepository;
import com.technokratos.repository.MessageRepository;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Sql(scripts = "/sql/message-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class MessageApiIntegrationTest extends AbstractIntegrationTest {

    @LocalServerPort
    private int port;

    BlockingQueue<MessageResponse> blockingQueue;

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @GrpcClient("inProcess")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;

    private static final String WEBSOCEKET_LOCALHOST = "ws://localhost:";

    private static final String LOCALHOST = "http://localhost:";

    private static final String CHAT_MESSAGE_PREFIX = "/chat/message";

    //2669cbf8-32c1-4796-bf43-2f36a8a80cc8
    private static final String FIRST_ACCOUNT_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Test
    void sendMessageToChatSuccessfully() throws Exception {

        UUID chatId = UUID.fromString("9740e91e-cdb5-447b-a9d8-904075ec4d37");
        ChatEntity chatEntity = chatRepository.getReferenceById(chatId);

        Assertions.assertEquals(0, chatEntity.getMessages().size());
        MessageModel messageModel = MessageModel.builder()
                .chatId(chatId)
                .content("Helllooooooooo")
                .build();

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + CHAT_MESSAGE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(messageModel))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        ChatEntity updatedChat = chatRepository.getReferenceById(chatId);
        Assertions.assertEquals(1, updatedChat.getMessages().size());
        Assertions.assertNotNull(messageResponse);
    }

    @Test
    void sendMessageToChatFailedChatNotFound() throws Exception {

        UUID chatId = UUID.fromString("1040e91e-cdb5-447b-a9d8-904075ec4d37");

        MessageModel messageModel = MessageModel.builder()
                .chatId(chatId)
                .content("Helllooooooooo")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + CHAT_MESSAGE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(messageModel))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void acceptMessageFromChat() throws Exception {

        UUID chatId = UUID.fromString("9740e91e-cdb5-447b-a9d8-904075ec4d37");

        blockingQueue = new LinkedBlockingDeque<>();

        String connectToChat = WEBSOCEKET_LOCALHOST + port + "/connect";

        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(asList(new WebSocketTransport(new StandardWebSocketClient()))));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession stompSession = stompClient.connect(connectToChat, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        stompSession.subscribe("/topic/messages/" + chatId, new DefaultStompFrameHandler());

        ChatEntity chatEntity = chatRepository.getReferenceById(chatId);

        Assertions.assertEquals(0, chatEntity.getMessages().size());
        MessageModel messageModel = MessageModel.builder()
                .chatId(chatId)
                .content("Helllooooooooo")
                .build();

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + CHAT_MESSAGE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(messageModel))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        MessageResponse messageFromAnotherAccount = blockingQueue.poll(1, SECONDS);
        Assertions.assertNotNull(messageFromAnotherAccount);
        Assertions.assertEquals(messageResponse, messageFromAnotherAccount);
        Assertions.assertFalse(messageFromAnotherAccount.isDeleted());
    }

    private class DefaultStompFrameHandler implements StompFrameHandler {

        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {

            System.out.println(stompHeaders.toString());
            return byte[].class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {

            try {
                blockingQueue.offer(objectMapper.readValue((byte[]) o, MessageResponse.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    void acceptMessageThatMessageDeleteFromChat() throws Exception {

        UUID chatId = UUID.fromString("9840e91e-cdb5-447b-a9d8-904075ec4d37");

        blockingQueue = new LinkedBlockingDeque<>();

        String connectToChat = "ws://localhost:" + port + "/connect";

        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(asList(new WebSocketTransport(new StandardWebSocketClient()))));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession stompSession = stompClient.connect(connectToChat, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        stompSession.subscribe("/topic/messages/" + chatId, new DefaultStompFrameHandler());

        ChatEntity chatEntity = chatRepository.getReferenceById(chatId);

        Assertions.assertEquals(3, chatEntity.getMessages().size());
        Assertions.assertFalse(chatEntity.getMessages().get(0).isDeleted());
        Assertions.assertFalse(chatEntity.getMessages().get(1).isDeleted());
        Assertions.assertFalse(chatEntity.getMessages().get(2).isDeleted());

        UUID messageId = UUID.fromString("9040e91e-cdb5-447b-a9d8-904075ec4d37");
        RequestId requestId = new RequestId(messageId);

        UUID deletedMessageId = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + CHAT_MESSAGE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        MessageResponse messageFromAnotherAccount = blockingQueue.poll(1, SECONDS);
        Assertions.assertNotNull(messageFromAnotherAccount);
        Assertions.assertEquals(messageId, messageFromAnotherAccount.getId());
        Assertions.assertTrue(messageFromAnotherAccount.isDeleted());

        MessageEntity deletedMessage = messageRepository.getReferenceById(messageId);
        Assertions.assertTrue(deletedMessage.isDeleted());
    }

    @Test
    void deleteMessageSuccessfully() throws Exception {

        UUID messageId = UUID.fromString("9840e91e-cdb5-447b-a9d8-904075ec4d37");
        MessageEntity messageEntity = messageRepository.getReferenceById(messageId);
        Assertions.assertFalse(messageEntity.isDeleted());
        UUID chatId = messageEntity.getChat().getId();

        RequestId requestId = new RequestId(messageId);

        ChatEntity chatEntity = chatRepository.getReferenceById(chatId);
        Assertions.assertFalse(chatEntity.getMessages().get(0).isDeleted());

        UUID deletedMessageId = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + CHAT_MESSAGE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        ChatEntity updatedChat = chatRepository.getReferenceById(chatId);
        Assertions.assertTrue(updatedChat.getMessages().get(0).isDeleted());

        MessageEntity message = messageRepository.getReferenceById(messageId);
        Assertions.assertTrue(message.isDeleted());
    }

    @Test
    void deleteMessageFailed() throws Exception {

        UUID messageId = UUID.fromString("8740e91e-cdb5-447b-a9d8-904075ec4d37");
        MessageEntity messageEntity = messageRepository.getReferenceById(messageId);
        UUID chatId = messageEntity.getChat().getId();

        RequestId requestId = new RequestId(messageId);

        ChatEntity chatEntity = chatRepository.getReferenceById(chatId);
        Assertions.assertEquals(3, chatEntity.getMessages().size());

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + CHAT_MESSAGE_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllMessagesInChatSuccessfully() throws Exception {

        UUID chatId = UUID.fromString("3340e91e-cdb5-447b-a9d8-904075ec4d37");

        ResponsePage<MessageResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + CHAT_MESSAGE_PREFIX + "/" + chatId)
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getContent());
        Assertions.assertNotNull(page.getContent().get(0).getChatId());
        Assertions.assertNotNull(page.getContent().get(0).getId());
        Assertions.assertNotNull(page.getContent().get(0).getCreatedDate());
        Assertions.assertNotNull(page.getContent().get(0).getSender());
        Assertions.assertNotNull(page.getContent().get(0).getSender().getId());
        Assertions.assertNotNull(page.getContent().get(0).getSender().getFirstName());

        Assertions.assertNotNull(page.getContent().get(1).getContent());
        Assertions.assertNotNull(page.getContent().get(1).getChatId());
        Assertions.assertNotNull(page.getContent().get(1).getId());
        Assertions.assertNotNull(page.getContent().get(1).getCreatedDate());
        Assertions.assertNotNull(page.getContent().get(1).getSender());
        Assertions.assertNotNull(page.getContent().get(1).getSender().getId());
        Assertions.assertNotNull(page.getContent().get(1).getSender().getFirstName());
    }

    @Test
    void getAllMessagesInChatSuccessfullyChatIsEmpty() throws Exception {

        UUID chatId = UUID.fromString("3440e91e-cdb5-447b-a9d8-904075ec4d37");

        ResponsePage<MessageResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + CHAT_MESSAGE_PREFIX + "/" + chatId)
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(0, page.getTotalElements());
    }

    @Test
    void getAllMessagesInChatFailedChatNotFound() throws Exception {

        UUID chatId = UUID.fromString("5540e91e-cdb5-447b-a9d8-904075ec4d37");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + CHAT_MESSAGE_PREFIX + "/" + chatId)
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
