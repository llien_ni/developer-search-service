package com.technokratos.test.integration;

import authorization.service.AccountServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.*;
import com.technokratos.entity.ChatAccountEntity;
import com.technokratos.entity.ChatEntity;
import com.technokratos.repository.ChatRepository;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


@Sql(scripts = "/sql/chat-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class ChatApiIntegrationTest extends AbstractIntegrationTest {

    @LocalServerPort
    private int port;

    BlockingQueue<MessageResponse> blockingQueue;

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @GrpcClient("inProcess")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;

    private static final String LOCALHOST = "http://localhost:";

    private static final String CHAT_PREFIX = "/chat";

    //2669cbf8-32c1-4796-bf43-2f36a8a80cc8
    private static final String FIRST_ACCOUNT_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    //117f2c92-7735-48fe-a0de-7ce15ff10937
    private static final String SECOND_ACCOUNT_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxMTdmMmM5Mi03NzM1LTQ4ZmUtYTBkZS03Y2UxNWZmMTA5MzciLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTkyNDUyMzEsImp0aSI6IjE2ODM2NjkyMzE1MzUifQ.cZYTB1ifODL14KExKom0BpomSayTPqzr5XEaJgyM39_RMStlfttN3cUUFnezOyf5HQYYVrGzf7BoKbsLrKUz4ApiSx8G22nkReqMz5Gl6bM3hchGJopxnkbO6E6z0g1CEwFhuC-9MUXXuFwncxv8iQk-G5uR5Hu-Zqkc_i74uKxlibt_-59XtV2mH-W4GQrV-ZSLW5dwfwoKJJ_XSM-YoPgWhukud6M2dXzk08pRjw3IkKa5O1LHo3Rs9bySZ4Ck_pM5DRvzwB4LMluLrNd8jL7wl5se85HK8CDLg9BFasfwKc1NQKLtTuUkE1L2KVJ9EtUGD_ypvEnwN0e3fbLGuw";

    @Autowired
    private ChatRepository chatRepository;

    @Test
    void createChatSuccessfully() throws Exception {

        UUID anotherAccountId = UUID.fromString("2869cbf8-32c1-4796-bf43-2f36a8a80cc8");
        RequestId requestId = new RequestId(anotherAccountId);
        ChatResponse chatResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + CHAT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        ChatEntity createdChat = chatRepository.getReferenceById(chatResponse.getId());
        Assertions.assertEquals(2, createdChat.getChatAccounts().size());
        Iterator<ChatAccountEntity> iterator = createdChat.getChatAccounts().iterator();
        ChatAccountEntity chatAccountEntity = iterator.next();
        Assertions.assertNotNull(chatAccountEntity.getChat());
        Assertions.assertNotNull(chatAccountEntity.getChat().getId());
        Assertions.assertNotNull(chatAccountEntity.getAccountId());

        ChatAccountEntity anotherChatAccountEntity = iterator.next();
        Assertions.assertEquals(chatAccountEntity.getChat().getId(), anotherChatAccountEntity.getChat().getId());
    }

    @Test
    void createChatSuccessfullyChatAlreadyExist() throws Exception {

        UUID secondAccountId = UUID.fromString("307f2c92-7735-48fe-a0de-7ce15ff10937");
        RequestId requestId = new RequestId(secondAccountId);

        ChatResponse chatResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + CHAT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals("1340e91e-cdb5-447b-a9d8-904075ec4d37", chatResponse.getId().toString());
    }

    @Test
    void deleteChatSuccessfully() throws Exception {

        UUID chatId = UUID.fromString("1340e91e-cdb5-447b-a9d8-904075ec4d37");
        RequestId requestId = new RequestId(chatId);

        Message message = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + CHAT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        ChatEntity chatEntity = chatRepository.getReferenceById(chatId);
        Assertions.assertEquals(ChatEntity.State.DELETED, chatEntity.getState());
        Assertions.assertNotNull(message.getData());
    }

    @Test
    void deleteChatFailedChatNotExist() throws Exception {

        UUID chatId = UUID.fromString("1440e91e-cdb5-447b-a9d8-904075ec4d37");
        RequestId requestId = new RequestId(chatId);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + CHAT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getChatByIdSuccessfully() throws Exception {

        UUID chatId = UUID.fromString("1340e91e-cdb5-447b-a9d8-904075ec4d37");

        ChatResponse chatResponse =  objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + CHAT_PREFIX+"/"+ chatId)
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(chatId, chatResponse.getId());
        Assertions.assertEquals("307f2c92-7735-48fe-a0de-7ce15ff10937", chatResponse.getAccount().getId().toString());
        Assertions.assertEquals(ChatEntity.State.ACTIVE.toString(), chatResponse.getState());
    }
    @Test
    void getChatByIdFailedChatNotFound() throws Exception{

        UUID chatId = UUID.fromString("1440e91e-cdb5-447b-a9d8-904075ec4d37");
        ExceptionResponse exceptionResponse =  objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + CHAT_PREFIX+"/"+ chatId)
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllByAccount() throws Exception {

        ResponsePage<ChatResponse> page =  objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + CHAT_PREFIX)
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(3, page.getTotalElements());
        ChatResponse chatResponse = page.getContent().get(0);

        Assertions.assertEquals("1540e91e-cdb5-447b-a9d8-904075ec4d37", chatResponse.getId().toString());
        Assertions.assertEquals("337f2c92-7735-48fe-a0de-7ce15ff10937", chatResponse.getAccount().getId().toString());
        Assertions.assertNotNull(chatResponse.getAccount().getFirstName());
        Assertions.assertNotNull(chatResponse.getAccount().getLastName());
        Assertions.assertEquals(ChatEntity.State.ACTIVE.toString(), chatResponse.getState());
    }
}
