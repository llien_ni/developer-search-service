package com.technokratos.config;

import authorization.service.AccountServiceGrpc;
import authorization.service.AccountServiceOuterClass;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;


@GrpcService
public class AccountServiceImpl extends AccountServiceGrpc.AccountServiceImplBase {

    @Override
    public void getAccountById(AccountServiceOuterClass.GetAccountByIdRequest request, StreamObserver<AccountServiceOuterClass.AccountResponse> responseObserver) {

        AccountServiceOuterClass.AccountResponse accountResponse = AccountServiceOuterClass.AccountResponse
                .newBuilder()
                .setId(request.getId())
                .setFirstName("account1")
                .setLastname("pokemon")
                .setEmail("account@gmail.com")
                .setRole("ROLE_DEVELOPER")
                .setStatus("CONFIRMED")
                .setPhoto("http://my-photo")
                .build();
        responseObserver.onNext(accountResponse);
        responseObserver.onCompleted();
    }
}
