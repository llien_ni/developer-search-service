package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "chat")
public class ChatEntity extends AbstractBaseEntity {

    public enum State{
        ACTIVE, DELETED, NOT_ACTIVE
    }

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @Column(name = "last_message_date")
    private OffsetDateTime lastMessageDate;

    @OneToMany(mappedBy = "chat", cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<MessageEntity> messages;

    @OneToMany(mappedBy = "chat", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<ChatAccountEntity> chatAccounts;
}
