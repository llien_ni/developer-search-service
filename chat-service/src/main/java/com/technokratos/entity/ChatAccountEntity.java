package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "chat_account")
public class ChatAccountEntity extends AbstractBaseEntity{

    @Column(name = "account_id")
    private UUID accountId;

    @ManyToOne
    @JoinColumn(name = "chat_id", referencedColumnName = "id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private ChatEntity chat;
}
