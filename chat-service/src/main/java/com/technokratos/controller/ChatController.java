package com.technokratos.controller;

import com.technokratos.api.ChatApi;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ChatResponse;
import com.technokratos.dto.response.Message;
import com.technokratos.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ChatController implements ChatApi {

    private final ChatService chatService;

    @Override
    public ChatResponse create(RequestId requestId, String accountId) {

        return chatService.create(requestId.getId(), UUID.fromString(accountId));
    }

    @Override
    public ChatResponse getById(UUID chatId, String accountId) {

        return chatService.getById(chatId, accountId);
    }

    @Override
    public Page<ChatResponse> getAllByAccount(String accountId, Integer size, Integer number) {

        return chatService.getAllByAccountAndStateIsNotDeleted(accountId, size, number);
    }

    @Override
    public Message delete(RequestId requestId, String accountId) {

        chatService.delete(requestId.getId(), accountId);
        return Message.builder()
                .data("Chat successfully deleted")
                .build();
    }
}
