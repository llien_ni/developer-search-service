package com.technokratos.controller;

import com.technokratos.api.MessageApi;
import com.technokratos.dto.request.MessageModel;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@CrossOrigin
@Slf4j
@RestController
@RequiredArgsConstructor
public class MessageController implements MessageApi {

    private final MessageService messageService;

    @Override
    public MessageResponse sendToChat(MessageModel request, String accountId) {

        return messageService.add(request.getChatId(), request.getContent(), accountId);
    }

    @Override
    public UUID deleteMessage(RequestId requestId, String accountId) {

        return messageService.delete(requestId.getId(), accountId);
    }

    @Override
    public Page<MessageResponse> getAllInChat(UUID chatId, String accountId, Integer size, Integer number) {

        return messageService.getAllInChat(chatId, accountId, size, number);
    }
}
