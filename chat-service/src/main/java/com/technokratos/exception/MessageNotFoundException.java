package com.technokratos.exception;

public class MessageNotFoundException extends NotFoundException{

    private static final String MESSAGE = "Message not found";

    public MessageNotFoundException() {
        super(MESSAGE);
    }
}
