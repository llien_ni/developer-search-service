package com.technokratos.exception;

public class ChatNotFoundException extends NotFoundException{

    private static final String MESSAGE = "Chat not found";

    public ChatNotFoundException() {
        super(MESSAGE);
    }
}
