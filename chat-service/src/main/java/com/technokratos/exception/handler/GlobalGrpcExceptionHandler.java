package com.technokratos.exception.handler;


import com.google.protobuf.Any;
import com.google.rpc.Code;
import com.google.rpc.Status;
import com.technokratos.exception.NotFoundException;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.StatusProto;
import error.service.Error;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;
import org.springframework.http.HttpStatus;

@Slf4j
@GrpcAdvice
public class GlobalGrpcExceptionHandler {

    @GrpcExceptionHandler(NotFoundException.class)
    public StatusRuntimeException handleNotFoundException(NotFoundException exception) {

        log.info("call grpc exception handler");

        Error.ErrorInfo errorInfo = Error.ErrorInfo.newBuilder()
                .setCode(HttpStatus.NOT_FOUND.value())
                .setMsg(exception.getMessage())
                .build();
        Status status = Status.newBuilder()
                .setCode(Code.INVALID_ARGUMENT.getNumber())
                .addDetails(Any.pack(errorInfo))
                .build();

        return StatusProto.toStatusRuntimeException(status);
    }
}
