package com.technokratos.repository;

import com.technokratos.entity.ChatEntity;
import com.technokratos.entity.MessageEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MessageRepository extends JpaRepository<MessageEntity, UUID> {

    Page<MessageEntity> findAllByChatAndDeleted(ChatEntity chat, boolean deleted, Pageable pageable);
}
