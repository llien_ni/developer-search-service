package com.technokratos.repository;

import com.technokratos.entity.ChatAccountEntity;
import com.technokratos.entity.ChatEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.UUID;

public interface ChatAccountRepository extends JpaRepository<ChatAccountEntity, UUID> {

    Page<ChatAccountEntity> findAllByAccountIdAndChat_State(UUID accountId, ChatEntity.State state, Pageable pageable);

    List<ChatAccountEntity> findAllByAccountIdAndChat_State(UUID accountId, ChatEntity.State state);

    List<ChatAccountEntity> findAllByAccountId(UUID accountId);
}
