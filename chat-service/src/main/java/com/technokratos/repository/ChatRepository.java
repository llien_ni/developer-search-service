package com.technokratos.repository;

import com.technokratos.entity.ChatEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ChatRepository extends JpaRepository<ChatEntity, UUID> {

    Optional<ChatEntity> findByIdAndState(UUID id, ChatEntity.State state);

    @Query("select chat from ChatEntity chat join chat.chatAccounts as accounts where accounts.accountId = (:accountId) and chat.state not in(:state)")
    Page<ChatEntity> findAllByAccountAndStateNotEqual(UUID accountId, ChatEntity.State state, Pageable pageable);

    @Query("select chat from ChatEntity chat join chat.chatAccounts as accounts where accounts.accountId = (:accountId) and chat.state in(:state)")
    List<ChatEntity> findAllByAccountAndState(UUID accountId, ChatEntity.State state);

    @Query("select chat from ChatEntity chat join chat.chatAccounts as accounts where accounts.accountId in (:accountsIds) group by chat.id having count(accounts.id) = 2")
    Optional<ChatEntity> findChatBetweenAccounts(List<UUID> accountsIds);
}
