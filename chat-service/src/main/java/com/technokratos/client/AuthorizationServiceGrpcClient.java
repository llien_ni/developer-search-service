package com.technokratos.client;


import authorization.service.AccountServiceGrpc;
import authorization.service.AccountServiceOuterClass;

import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthorizationServiceGrpcClient {

    @GrpcClient("authorization-service")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;

    public AccountServiceOuterClass.AccountResponse getAccountById(AccountServiceOuterClass.GetAccountByIdRequest request) {

        return accountServiceBlockingStub.getAccountById(request);
    }
}
