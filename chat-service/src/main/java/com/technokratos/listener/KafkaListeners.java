package com.technokratos.listener;

import com.technokratos.dto.response.MessageResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaListeners {

    private final SimpMessagingTemplate template;

    @KafkaListener(topics = "chat-topic", groupId = "group")
    public void listener(@Payload MessageResponse message) {

        template.convertAndSend("/topic/messages/" + message.getChatId(), message);
    }
}
