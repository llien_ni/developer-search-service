package com.technokratos.service;

import com.technokratos.dto.response.MessageResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface MessageService {

    MessageResponse add(UUID chatId, String messageRequest, String accountId);

    UUID delete(UUID id, String accountId);

    Page<MessageResponse> getAllInChat(UUID chatId, String accountId, Integer size, Integer number);
}
