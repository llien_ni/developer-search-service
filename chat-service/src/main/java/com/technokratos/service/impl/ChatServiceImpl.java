package com.technokratos.service.impl;

import authorization.service.AccountServiceOuterClass;
import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.ChatResponse;
import com.technokratos.entity.ChatAccountEntity;
import com.technokratos.entity.ChatEntity;
import com.technokratos.exception.ChatNotFoundException;
import com.technokratos.mapper.ChatMapper;
import com.technokratos.repository.ChatAccountRepository;
import com.technokratos.repository.ChatRepository;
import com.technokratos.service.ChatService;
import com.technokratos.util.converter.AccountConverter;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {

    private final ChatRepository chatRepository;

    private final ChatAccountRepository chatAccountRepository;

    private final ChatMapper chatMapper;

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    private final RequestParamUtil requestParamUtil;

    @Transactional
    @Override
    public ChatResponse create(UUID anotherAccountId, UUID issuerId) {

        Optional<ChatEntity> existedChat = chatRepository.findChatBetweenAccounts(List.of(anotherAccountId, issuerId));

        if (existedChat.isPresent() && !existedChat.get().getState().equals(ChatEntity.State.DELETED)) {

            return chatMapper.fromEntityToResponse(existedChat.get());
        }

        ChatEntity chat = ChatEntity.builder()
                .state(ChatEntity.State.ACTIVE)
                .chatAccounts(new HashSet<>())
                .messages(new ArrayList<>())
                .build();

        ChatAccountEntity chatAccount = ChatAccountEntity.builder()
                .accountId(issuerId)
                .chat(chat)
                .build();
        ChatAccountEntity anotherChatAccount = ChatAccountEntity.builder()
                .accountId(anotherAccountId)
                .chat(chat)
                .build();

        chat.getChatAccounts().add(chatAccount);
        chat.getChatAccounts().add(anotherChatAccount);

        ChatEntity savedChat = chatRepository.save(chat);
        ChatResponse chatResponse = chatMapper.fromEntityToResponse(savedChat);
        return convertToChatResponseWithAccountResponse(anotherAccountId, chatResponse);
    }


    private ChatResponse convertToChatResponseWithAccountResponse(UUID anotherAccountId, ChatResponse chatResponse) {

        AccountServiceOuterClass.AccountResponse account = authorizationServiceGrpcClient.getAccountById(AccountServiceOuterClass.GetAccountByIdRequest.newBuilder()
                .setId(anotherAccountId.toString())
                .build());
        AccountResponse accountResponse = AccountConverter.getAccountResponse(account);
        chatResponse.setAccount(accountResponse);
        return chatResponse;
    }

    @Transactional(readOnly = true)
    @Override
    public ChatResponse getById(UUID chatId, String accountId) {

        ChatEntity chat = chatRepository.findById(chatId).orElseThrow(ChatNotFoundException::new);
        checkAccountHaveThisChat(chat, accountId);
        return convertToChatResponseWithAccountResponse(chat, accountId);
    }

    private ChatResponse convertToChatResponseWithAccountResponse(ChatEntity chat, String accountId) {

        ChatResponse chatResponse = chatMapper.fromEntityToResponse(chat);
        Set<UUID> accountsIds = chat.getChatAccounts()
                .stream()
                .map(ChatAccountEntity::getAccountId)
                .collect(Collectors.toSet());

        for (UUID anotherAccountId : accountsIds) {
            if (!anotherAccountId.equals(UUID.fromString(accountId))) {
                AccountServiceOuterClass.AccountResponse account = authorizationServiceGrpcClient.getAccountById(AccountServiceOuterClass.GetAccountByIdRequest
                        .newBuilder()
                        .setId(anotherAccountId.toString())
                        .build());
                AccountResponse accountResponse = AccountConverter.getAccountResponse(account);
                chatResponse.setAccount(accountResponse);
            }
        }
        return chatResponse;
    }

    @Override
    public void checkAccountHaveThisChat(ChatEntity chat, String accountId) {

        Set<UUID> accountsInThisChat = chat.getChatAccounts()
                .stream()
                .map(ChatAccountEntity::getAccountId).collect(Collectors.toSet());
        if (!accountsInThisChat.contains(UUID.fromString(accountId))) {
            throw new ChatNotFoundException();
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ChatResponse> getAllByAccountAndStateIsNotDeleted(String accountId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "lastMessageDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<ChatEntity> chats = chatRepository.findAllByAccountAndStateNotEqual(UUID.fromString(accountId), ChatEntity.State.DELETED, pageRequest.withSort(sort));

        return chats.map(chat -> convertToChatResponseWithAccountResponse(chat, accountId));
    }

    @Transactional
    @Override
    public void delete(UUID chatId, String accountId) {

        ChatEntity chat = chatRepository.findById(chatId).orElseThrow(ChatNotFoundException::new);
        checkAccountHaveThisChat(chat, accountId);
        chat.setState(ChatEntity.State.DELETED);
        chatRepository.save(chat);
    }

    @Transactional
    @Override
    public void updateChatStateToNotActiveByAccount(UUID accountId) {

        List<ChatEntity> chats = chatRepository.findAllByAccountAndState(accountId, ChatEntity.State.ACTIVE);

        for (ChatEntity chat : chats) {
            chat.setState(ChatEntity.State.NOT_ACTIVE);
            chatRepository.save(chat);
        }
    }

    @Transactional
    @Override
    public void updateChatStateToActiveByAccount(UUID accountId) {

        List<ChatEntity> chats = chatRepository.findAllByAccountAndState(accountId, ChatEntity.State.NOT_ACTIVE);
        for (ChatEntity chat : chats) {
            chat.setState(ChatEntity.State.ACTIVE);
            chatRepository.save(chat);
        }
    }
}
