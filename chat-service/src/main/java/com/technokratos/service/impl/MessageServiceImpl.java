package com.technokratos.service.impl;


import authorization.service.AccountServiceOuterClass;
import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.entity.ChatEntity;
import com.technokratos.entity.MessageEntity;
import com.technokratos.exception.ChatNotFoundException;
import com.technokratos.exception.MessageNotFoundException;
import com.technokratos.mapper.MessageMapper;
import com.technokratos.repository.ChatRepository;
import com.technokratos.repository.MessageRepository;
import com.technokratos.service.ChatService;
import com.technokratos.service.MessageService;
import com.technokratos.util.converter.AccountConverter;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    private final MessageRepository messageRepository;

    private final ChatRepository chatRepository;

    private final ChatService chatService;

    private final MessageMapper messageMapper;

    private final RequestParamUtil requestParamUtil;

    private final KafkaTemplate<String, MessageResponse> kafkaTemplate;

    @Transactional
    @Override
    public MessageResponse add(UUID chatId, String text, String accountId) {

        ChatEntity chat = chatRepository.findByIdAndState(chatId, ChatEntity.State.ACTIVE)
                .orElseThrow(ChatNotFoundException::new);
        chatService.checkAccountHaveThisChat(chat, accountId);
        MessageEntity message = MessageEntity.builder()
                .text(text)
                .createdDate(OffsetDateTime.now())
                .accountId(UUID.fromString(accountId))
                .chat(chat)
                .build();
        messageRepository.save(message);

        chat.setLastMessageDate(message.getCreatedDate());
        chat.getMessages().add(message);
        chatRepository.save(chat);

        MessageResponse messageResponse = messageMapper.fromEntityToResponse(message);
        AccountResponse accountResponse = getAccountResponse(accountId);
        messageResponse.setSender(accountResponse);

        kafkaTemplate.send("chat-topic", messageResponse.getChatId().toString(), messageResponse);
        return messageResponse;
    }

    @Transactional
    @Override
    public UUID delete(UUID id, String accountId) {

        MessageEntity message = messageRepository
                .findById(id)
                .orElseThrow(MessageNotFoundException::new);
        checkAccountSentThisMessage(message, accountId);
        message.setDeleted(true);
        messageRepository.save(message);

        MessageResponse messageResponse = messageMapper.fromEntityToResponse(message);
        AccountResponse accountResponse = getAccountResponse(accountId);
        messageResponse.setSender(accountResponse);

        kafkaTemplate.send("chat-topic", message.getChat().getId().toString(), messageResponse);
        return message.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public Page<MessageResponse> getAllInChat(UUID chatId, String accountId, Integer size, Integer number) {

        ChatEntity chat = chatRepository.findById(chatId).orElseThrow(ChatNotFoundException::new);
        chatService.checkAccountHaveThisChat(chat, accountId);

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<MessageEntity> messages = messageRepository.findAllByChatAndDeleted(chat, false, pageRequest.withSort(sort));

        return messages.map(this::convertToMessageResponseWithAccount);
    }

    private MessageResponse convertToMessageResponseWithAccount(MessageEntity message) {

        MessageResponse messageResponse = messageMapper.fromEntityToResponse(message);
        AccountResponse accountResponse = getAccountResponse(message.getAccountId().toString());
        messageResponse.setSender(accountResponse);

        return messageResponse;
    }

    private AccountResponse getAccountResponse(String accountId) {

        AccountServiceOuterClass.AccountResponse account = authorizationServiceGrpcClient.getAccountById(AccountServiceOuterClass.GetAccountByIdRequest
                .newBuilder()
                .setId(accountId)
                .build());
        return AccountConverter.getAccountResponse(account);
    }

    private void checkAccountSentThisMessage(MessageEntity message, String accountId) {

        if (!message.getAccountId().equals(UUID.fromString(accountId))) {
            throw new MessageNotFoundException();
        }
    }

}
