package com.technokratos.service;

import com.technokratos.dto.response.ChatResponse;
import com.technokratos.entity.ChatEntity;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface ChatService {

    ChatResponse create(UUID anotherAccountId, UUID issuerId);

    ChatResponse getById(UUID chatId, String accountId);

    void checkAccountHaveThisChat(ChatEntity chat, String accountId);

    Page<ChatResponse> getAllByAccountAndStateIsNotDeleted(String accountId, Integer size, Integer number);

    void delete(UUID id, String accountId);

    void updateChatStateToNotActiveByAccount(UUID accountId);

    void updateChatStateToActiveByAccount(UUID accountId);
}
