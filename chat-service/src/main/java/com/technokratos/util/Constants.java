package com.technokratos.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public  static final String FRONT_CHAT_SERVICE_ADDRESS = "http://localhost:4000";
}
