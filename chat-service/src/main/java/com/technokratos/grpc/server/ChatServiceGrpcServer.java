package com.technokratos.grpc.server;

import chat.service.ChatServiceGrpc;
import chat.service.ChatServiceOuterClass;
import com.technokratos.dto.response.ChatResponse;
import com.technokratos.service.ChatService;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.UUID;


@Slf4j
@GrpcService
@RequiredArgsConstructor
public class ChatServiceGrpcServer extends ChatServiceGrpc.ChatServiceImplBase {

    private final ChatService chatService;

    @Override
    public void createChat(ChatServiceOuterClass.CreateChatRequest request, StreamObserver<ChatServiceOuterClass.ChatResponse> responseObserver) {

        ChatResponse chatResponse = chatService.create(UUID.fromString(request.getAnotherAccountId()), UUID.fromString(request.getIssuerId()));

        ChatServiceOuterClass.ChatResponse response = ChatServiceOuterClass.ChatResponse.newBuilder()
                .setChatId(chatResponse.getId().toString())
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void setChatStateToNotActiveByAccount(ChatServiceOuterClass.AccountIdRequest request, StreamObserver<ChatServiceOuterClass.Empty> responseObserver) {

        chatService.updateChatStateToNotActiveByAccount(UUID.fromString(request.getAccountId()));
        responseObserver.onNext(ChatServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }

    @Override
    public void setChatStateToActiveByAccount(ChatServiceOuterClass.AccountIdRequest request, StreamObserver<ChatServiceOuterClass.Empty> responseObserver) {

        chatService.updateChatStateToActiveByAccount(UUID.fromString(request.getAccountId()));
        responseObserver.onNext(ChatServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }
}
