package com.technokratos.grpc.server;

import com.technokratos.service.MessageService;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import message.service.MessageServiceGrpc;
import message.service.MessageServiceOuterClass;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.UUID;


@Slf4j
@GrpcService
@RequiredArgsConstructor
public class MessageServiceGrpcServer extends MessageServiceGrpc.MessageServiceImplBase {

    private final MessageService messageService;

    @Override
    public void sendMessageToChat(MessageServiceOuterClass.MessageRequest request, StreamObserver<MessageServiceOuterClass.Empty> responseObserver) {

        messageService.add(UUID.fromString(request.getChatId()), request.getText(), request.getIssuerId());
        responseObserver.onNext(MessageServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }
}
