package com.technokratos.mapper;


import com.technokratos.dto.response.ChatResponse;
import com.technokratos.entity.ChatEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ChatMapper {

    @Mapping(target = "account", ignore = true)
    ChatResponse fromEntityToResponse(ChatEntity savedChat);
}
