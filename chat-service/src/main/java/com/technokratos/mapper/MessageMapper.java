package com.technokratos.mapper;


import com.technokratos.dto.response.MessageResponse;
import com.technokratos.entity.MessageEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface MessageMapper {

    @Mapping(source = "chat.id", target = "chatId")
    @Mapping(source = "text", target = "content")
    MessageResponse fromEntityToResponse(MessageEntity message);
}
