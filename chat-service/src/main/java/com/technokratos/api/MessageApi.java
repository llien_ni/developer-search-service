package com.technokratos.api;


import com.technokratos.dto.request.MessageModel;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/chat/message")
@PreAuthorize("isAuthenticated()")
public interface MessageApi {

    @Operation(summary = "send message to chat")
    @ApiResponse(responseCode = "201", description = "message successfully sent",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "chat not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    MessageResponse sendToChat(@RequestBody @Valid MessageModel messageRequest, @AuthenticationPrincipal String accountId);

    @Operation(summary = "delete message")
    @ApiResponse(responseCode = "202", description = "message successfully deleted",
            content = @Content(schema = @Schema(implementation = UUID.class)))
    @ApiResponse(responseCode = "404", description = "message not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    UUID deleteMessage(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String accountId);

    @Operation(summary = "get all messages in chat")
    @ApiResponse(responseCode = "200", description = "messages successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "chat not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{chat-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<MessageResponse> getAllInChat(@PathVariable("chat-id") UUID chatId,
                                       @AuthenticationPrincipal String accountId,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "number", required = false) Integer number);
}
