package com.technokratos.api;

import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ChatResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.Message;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/chat")
@PreAuthorize("isAuthenticated()")
public interface ChatApi {

    @Operation(summary = "create chat this user by user id")
    @ApiResponse(responseCode = "201", description = "chat successfully created or return already existing chat",
            content = @Content(schema = @Schema(implementation = ChatResponse.class)))
    @ApiResponse(responseCode = "404", description = "account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ChatResponse create(@RequestBody @Valid RequestId requestId,
                        @AuthenticationPrincipal String accountId);

    @Operation(summary = "get chat by id")
    @ApiResponse(responseCode = "200", description = "chat successfully received",
            content = @Content(schema = @Schema(implementation = ChatResponse.class)))
    @ApiResponse(responseCode = "404", description = "chat not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{chat-id}")
    @ResponseStatus(HttpStatus.OK)
    ChatResponse getById(@PathVariable("chat-id") UUID id,
                         @AuthenticationPrincipal String accountId);

    @Operation(summary = "get all chats by account with chat state is not DELETED and sort by last message date")
    @ApiResponse(responseCode = "200", description = "chats successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<ChatResponse> getAllByAccount(@AuthenticationPrincipal String accountId,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "delete chat")
    @ApiResponse(responseCode = "202", description = "chat successfully deleted",
            content = @Content(schema = @Schema(implementation = Message.class)))
    @ApiResponse(responseCode = "404", description = "chat not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    Message delete(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String accountId);
}
