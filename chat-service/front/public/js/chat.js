const url = 'http://localhost:8091';
const urlLogin = 'http://localhost:8080/login';
const urlLogout = 'http://localhost:8080/logout';
const urlRefreshToken = 'http://localhost:8080/token/refresh';
let subscribedChannels = [];

let selectedChat
let $textarea;
let stompClient;

function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}


function openLoginBlock() {
    let loginBlock = document.getElementById('login-block');
    loginBlock.style.display = 'block'
}

function login(email, password) {

    fetch(urlLogin, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            login: email,
            password: password
        })
    }).then(response => {

        let status = response.status;

        let data = response.json()

        if (status === 200) {

            data.then((promise) => {

                localStorage.setItem('accessToken', promise.accessToken)
                localStorage.setItem('refreshToken', promise.refreshToken)


                document.getElementById('login-block').style.display = 'none'
                document.getElementById('login-link').style.display = 'none'
                document.getElementById('logout-link').style.display = 'block'

                document.getElementById("chat-block").style.display = 'block'
                getAllChatsByUser()
                setTimeout(refreshAccessToken, 70000)

            })
        } else {

            document.getElementById('login-error').style.display = 'block'

        }
    }).catch(error => {
        console.error('Registration error:', error);
    });
}

function logout() {

    fetch(urlLogout, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        }
    }).then(e => {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken")
        document.getElementById('logout-link').style.display = 'none'
        document.getElementById('login-link').style.display = 'block'

        selectedChat = null;
        subscribedChannels.forEach(a => a.remove())
        document.getElementById('usersList').innerHTML = ''
        document.getElementById('chat-history').innerHTML = ''
        document.getElementById('selectedUserFirstName').innerHTML = ''
        document.getElementById('selectedUserLastName').innerHTML = ''
        document.getElementById('anotherAccountPhoto').innerHTML = ''
    }).catch(e => console.log("error::", e))

}

function refreshAccessToken() {

    const expirationTime = parseJwt(localStorage.getItem('accessToken')).exp;

    let s = Date.now().toString().slice(0, 10);

    if (expirationTime <= s) {
        console.log('пытается обновить токены')
        fetch(urlRefreshToken, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('refreshToken')
            }
        })
            .then(response => response.json())
            .then(response => {

                localStorage.setItem('accessToken', response.accessToken)
                localStorage.setItem('refreshToken', response.refreshToken)

                let role = parseJwt(response.accessToken).role;
                let id = parseJwt(response.accessToken).sub;
                setTimeout(refreshAccessToken, 30000)
            })

    } else {
        console.log("elese block")
        setTimeout(refreshAccessToken, 50000)
    }

}

function clearChildElements(parentElement) {

    const childElements = parentElement.children;
    for (let i = 0; i < childElements.length; i++) {
        const childElement = childElements[i];
        clearChildElements(childElement);
        childElement.innerHTML = '';
    }
}

function connectToChat(chat) {

    let socket = new SockJS(url + '/connect', {});

    thisUser = parseJwt(localStorage.getItem('accessToken')).sub;

    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {

        console.log(subscribedChannels.length)

        stompClient.subscribe("/topic/messages/" + chat, function (response) {

            let data = JSON.parse(response.body);

            console.log(data)
            if (data.deleted === true) {
                console.log("ДАААА")

                document.getElementById(data.id).remove();
                if (selectedChat === data.chatId) {
                    console.log(data)
                    renderReceivedMessage(data);

                    document.getElementById(data.id).remove()
                }
            } else {
                if (thisUser !== data.sender.id) {

                    if (selectedChat === data.chatId) {
                        console.log(data)
                        renderReceivedMessage(data);
                    }

                }
            }
        });

    });
}


function renderReceivedMessage(message) {

    let chatHistory = document.getElementById('chat-history');


    const messageTemplate = document.getElementById("message-response-template");

    console.log(messageTemplate)
    let newMessage = messageTemplate.content.cloneNode(true);

    newMessage.querySelector('#messageId').id = message.id
    newMessage.querySelector('#messageDate').textContent = convertToLocalDateTime(message.createdDate)
    newMessage.querySelector('#messageChatId').textContent = message.chatId
    newMessage.querySelector('.message-content').textContent = message.content

    chatHistory.appendChild(newMessage)

    scrollToBottom();
}

function openMenu(messageId) {
    let message = document.getElementById(messageId);
    let menu = message.querySelector('.menu')

    if(menu.style.display ==='none'){
        menu.style.display = 'block'

        menu.addEventListener('click', function (){
            deleteMessage(messageId)
        })

    }else{
        menu.style.display = 'none'
    }

}

function getChatById(chatId) {

    document.querySelector('.chat').style.display = 'block'
    document.querySelector('#chat-history').innerHTML = ''

    if (!subscribedChannels.includes(chatId)) {
        subscribedChannels.push(chatId)
        console.log("еще не было")
        connectToChat(chatId)
    }
    console.log("уже было")
    selectedChat = chatId;

    const chatAbout = document.getElementById('chat-about');

    fetch(url+"/chat/"+ chatId, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
        }  })
        .then(response => response.json())
        .then(response => {
            let data = response;

            console.log(response)

            chatAbout.querySelector('#selectedUserLastName').textContent =  data.account.firstName
            chatAbout.querySelector('#selectedUserFirstName').textContent = data.account.lastName


        })

    fetch(url + "/chat/message/" + chatId, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
        }
    })
        .then(response => response.json())
        .then(response => {
            let messages = response;

            console.log(response)
            let chatHeader = document.getElementById('chat-header');
            const chatAbout = document.getElementById('chat-about');
            const chatHistory = document.getElementById('chat-history');

            let thisUser = parseJwt(localStorage.getItem('accessToken')).sub

            let anotherAccountMessageTemplate = document.getElementById('message-response-template');
            let thisAccountMessageTemplate = document.getElementById('message-template');

            let anotherAccountFirstName;
            let anotherAccountLastName;
            for (let i = messages.content.length - 1; i >= 0; i--) {

                let data = messages.content[i]

                if (thisUser !== data.sender.id) {

                    let message = anotherAccountMessageTemplate.content.cloneNode(true);
                    message.querySelector('#messageId').id = data.id
                    message.querySelector('#messageDate').textContent = convertToLocalDateTime(data.createdDate)
                    message.querySelector('#messageChatId').textContent = data.chatId
                    message.querySelector('.message-content').textContent = data.content


                    chatHistory.appendChild(message)

                    if (data.sender.photo !== '') {

                        chatHeader.querySelector("#anotherAccountPhoto").setAttribute('src', data.sender.photo)

                    }


                } else {

                    let message = thisAccountMessageTemplate.content.cloneNode(true);
                    message.querySelector("#myMessageId").id = data.id
                    message.querySelector(".message-data-time").textContent = convertToLocalDateTime(data.createdDate)
                    message.querySelector(".message-content").textContent = data.content

                    message.querySelector("#link-to-menu").addEventListener('click', function () {
                        console.log(data.id)
                        openMenu(data.id)
                    })

                    chatHistory.appendChild(message)
                }
            }

            scrollToBottom()
        })

}

function convertToLocalDateTime(offsetDateTime) {

    const isoString = offsetDateTime.toString();
    const date = new Date(Date.parse(isoString));
    const options = {timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone, hour12: false};
    const localDateTime = date.toLocaleString('en-US', options);
    return localDateTime;
}


function getAllChatsByUser() {

    console.log("getAllCompanies")
    fetch(url + "/chat", {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
        }
    })
        .then(response => response.json())
        .then(data => {
            console.log('Chats:', data.content);
            let chats = document.getElementById('usersList');
            chats.innerHTML = ''

            for (let i = 0; i < data.content.length; i++) {


                let account = data.content[i].account;
                const chatTemplate = document.getElementById('chat-template');

                let chat = chatTemplate.content.cloneNode(true);

                chat.querySelector('#user-firstName').textContent = account.firstName
                chat.querySelector('#user-lastName').textContent = account.lastName

                if (account.photo !== '') {
                    chat.querySelector('#user-photo').setAttribute('src', account.photo)

                }
                chat.querySelector("#link-to-chat").addEventListener('click', function () {
                    getChatById(data.content[i].id)
                })

                chats.appendChild(chat);
            }
        })
        .catch(error => {
            console.error('get companies error:', error);
        });
}

function init() {
    cacheDOM();
    bindEvents();
}

function bindEvents() {
    $button.on('click', addMessage.bind(this));
    $textarea.on('keyup', addMessageEnter.bind(this));
}

function cacheDOM() {
    $button = $('#sendBtn');
    $textarea = $('#message-to-send');
}

function scrollToBottom() {

    let chatHistory = document.getElementById('chat-history');

    chatHistory.scrollTo(chatHistory[0])

    chatHistory.scrollTop = chatHistory.scrollHeight;

}


document.addEventListener('DOMContentLoaded', function () {

    let token = localStorage.getItem("accessToken");

    if (token !== null) {
        setTimeout(refreshAccessToken, 10000);
        document.getElementById('login-link').style.display = 'none'
        document.getElementById('logout-link').style.display = 'block'

        let parsedToken = parseJwt(token);
        let role = parsedToken.role;

        getAllChatsByUser()


    } else {
        document.getElementById('login-link').style.display = 'block'
        document.getElementById('logout-link').style.display = 'none'
    }
});

async function sendMsg(content) {

    let result = await fetch(url + "/chat/message", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken'),
        },
        body: JSON.stringify({
            chatId: selectedChat,
            content: content
        })
    })
    return result.json()
}

function getCurrentTime() {
    const options = {timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone, hour12: false};
    return new Date().toLocaleString('en-US', options);
}

async function sendMessage(content) {
    let chatHistory = document.getElementById('chat-history');

    let data = await sendMsg(content);

    scrollToBottom();
    if (content.trim() !== '') {

        let template = document.getElementById("message-template");
        let message = template.content.cloneNode(true);

        message.querySelector("#myMessageId").id = data.id
        message.querySelector(".message-data-time").textContent = convertToLocalDateTime(data.createdDate)
        message.querySelector(".message-content").textContent = data.content
        message.querySelector("#link-to-menu").addEventListener('click', function () {
            console.log(data.id)
            openMenu(data.id)
        })

        chatHistory.appendChild(message)

        scrollToBottom();
        $textarea.val('');
    }
}

function addMessage() {
    sendMessage($textarea.val());
}

function deleteMessage(messageId) {
    console.log(messageId)

    fetch(url + "/chat/message", {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken'),

        },
        body: JSON.stringify({
            id: messageId
        })
    }).then(response => response.json())

}

function addMessageEnter(event) {

    if (event.keyCode === 13) {
        addMessage();
    }
}

init();
