package com.technokratos.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "sms")
public class SmsConfigurationProperties {

    private String email;

    private String apiKey;
}
