package com.technokratos.config;


import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

@Configuration
public class RabbitMQConfig implements RabbitListenerConfigurer {

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Override
    public void configureRabbitListeners(
            RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(myHandlerMethodFactory());
    }

    @Bean
    public DefaultMessageHandlerMethodFactory myHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(new MappingJackson2MessageConverter());
        return factory;
    }


    @Bean
    public DirectExchange exchange() {

        return new DirectExchange("direct-exchange");
    }

    @Bean
    public Binding vacancyServiceBinding(Queue queueSendNotification, DirectExchange exchange) {
        return BindingBuilder.bind(queueSendNotification).to(exchange).with("vacancy-service-notification");
    }

    @Bean
    public Queue queueSendNotification() {
        return QueueBuilder.durable("q.sending-notification-from-vacancy-service")
                .withArgument("x-dead-letter-exchange", "x.vacancy-notification-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }
    @Bean
    public Declarables createDeadLetterSchema() {
        return new Declarables(
                new DirectExchange("x.vacancy-notification-failure"),
                new Queue("q.fall-back-vacancy-notification"),
                new Binding("q.fall-back-vacancy-notification", Binding.DestinationType.QUEUE, "x.vacancy-notification-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public Binding projectServiceBinding(Queue queueSendProjectNotification, DirectExchange exchange) {
        return BindingBuilder.bind(queueSendProjectNotification).to(exchange).with("project-service-notification");
    }

    @Bean
    public Queue queueSendProjectNotification() {
        return QueueBuilder.durable("q.project-service-notification")
                .withArgument("x-dead-letter-exchange", "x.project-notification-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaForProjectNotification() {
        return new Declarables(
                new DirectExchange("x.project-notification-failure"),
                new Queue("q.fall-back-project-notification"),
                new Binding("q.fall-back-project-notification", Binding.DestinationType.QUEUE, "x.project-notification-failure",
                        "fall-back", null)
        );
    }
}
