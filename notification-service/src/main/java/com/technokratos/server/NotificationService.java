package com.technokratos.server;

import com.technokratos.dto.MailMessageModel;
import com.technokratos.dto.SmsModel;
import com.technokratos.service.EmailSenderService;
import com.technokratos.service.SmsSenderService;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import net.devh.boot.grpc.server.service.GrpcService;
import notification.service.NotificationServiceGrpc;
import notification.service.NotificationServiceOuterClass;

@Slf4j
@GrpcService
@RequiredArgsConstructor
public class NotificationService extends NotificationServiceGrpc.NotificationServiceImplBase {

    private final EmailSenderService emailSenderService;

    private final SmsSenderService smsSenderService;

    @Override
    public void sendMail(NotificationServiceOuterClass.MailMessageRequest request, StreamObserver<NotificationServiceOuterClass.Empty> responseObserver) {

        log.info("call sendMail method");
        MailMessageModel mailMessageModel = MailMessageModel.builder()
                .subject(request.getSubject())
                .to(request.getTo())
                .message(request.getMessage())
                .build();

        emailSenderService.sendEmail(mailMessageModel);

        log.info("emailSenderService call method sendMail");
        responseObserver.onNext(NotificationServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }

    @Override
    public void sendSms(NotificationServiceOuterClass.SmsModel request, StreamObserver<NotificationServiceOuterClass.Empty> responseObserver) {

        log.info("Call sendSms method");
        SmsModel smsModel = SmsModel.builder()
                .to(request.getTo())
                .message(request.getMessage())
                .build();
        smsSenderService.sendSms(smsModel);

        responseObserver.onNext(NotificationServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }
}
