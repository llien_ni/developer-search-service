package com.technokratos.service;

import com.technokratos.dto.SmsModel;

public interface SmsSenderService {

    void sendSms(SmsModel smsModel);
}
