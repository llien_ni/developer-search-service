package com.technokratos.service;

import com.technokratos.dto.MailMessageModel;

public interface EmailSenderService {

    void sendEmail(MailMessageModel mailMessage);
}
