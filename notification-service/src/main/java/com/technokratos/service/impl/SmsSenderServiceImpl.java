package com.technokratos.service.impl;

import com.technokratos.config.SmsConfigurationProperties;
import com.technokratos.dto.SmsModel;
import com.technokratos.service.SmsSenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class SmsSenderServiceImpl implements SmsSenderService {

    private static final String SMS_AERO = "https://gate.smsaero.ru/send/?user=%s&password=%s&to=%s&text=%s&from=Sms Aero";

    private final RestTemplate restTemplate;

    private final SmsConfigurationProperties smsConfigurationProperties;

    @Override
    public void sendSms(SmsModel smsModel){

        String url = String.format(SMS_AERO, smsConfigurationProperties.getEmail(), smsConfigurationProperties.getApiKey(), smsModel.getTo(), smsModel.getMessage());
        restTemplate.getForObject(url, String.class);
    }
}
