package com.technokratos.listener;


import com.technokratos.dto.MailMessageModel;
import com.technokratos.service.EmailSenderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SendingNotificationListener {

    private final EmailSenderService emailSenderService;

    @RabbitListener(queues = {"q.project-service-notification",
            "q.vacancy-service-notification",
            "q.sending-notification-from-authorization-service",
            "q.notification-about-event"})
    public void handleMessage(@Payload MailMessageModel mailMessage) {

        log.info("Get mail message request");
        if (mailMessage.getTo() != null) {
            emailSenderService.sendEmail(mailMessage);
        }
        log.info("Sent");
    }
}
