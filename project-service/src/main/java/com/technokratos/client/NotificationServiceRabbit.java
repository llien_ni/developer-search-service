package com.technokratos.client;

import com.technokratos.dto.model.MailMessageModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import java.util.UUID;
import static com.technokratos.util.constants.Constants.AUTHORIZATION_SERVICE_URL;
import static com.technokratos.util.constants.Constants.PROJECT_SERVICE_URL;



@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationServiceRabbit {

    private final RabbitTemplate rabbitTemplate;

    private static final String PROJECT = "Project: ";

    public void sendMailMessageToAuthService(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend("direct-exchange", "project-service", mailMessageModel);
    }

    public void sendMailMessageToProjectAuthor(String senderId, UUID projectId, UUID projectAuthorId) {

        MailMessageModel mailMessageModelToProjectAuthor = MailMessageModel.builder()
                .message("Developer: " + AUTHORIZATION_SERVICE_URL + "/developer/" + senderId + " " +
                        PROJECT + PROJECT_SERVICE_URL + "/project/" + projectId)
                .to(projectAuthorId.toString())
                .subject("You have new request")
                .build();

        sendMailMessageToAuthService(mailMessageModelToProjectAuthor);
    }


    public void sendMailMessageToDeveloper(String senderId, UUID projectId) {

        MailMessageModel mailMessageModelToSender = MailMessageModel.builder()
                .message("You add request to project" + " " +
                        PROJECT + PROJECT_SERVICE_URL + "/project/" + projectId)
                .to(senderId)
                .subject("You sent request")
                .build();

        sendMailMessageToAuthService(mailMessageModelToSender);
    }

    public void sendMailMessageToNotificationService(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend("direct-exchange", "project-service-notification", mailMessageModel);
    }

    public void sendMailMessageDeveloperAddedToProject(UUID projectId, String email) {

        MailMessageModel mailMessageModelToDeveloper = MailMessageModel.builder()
                .message(PROJECT + PROJECT_SERVICE_URL + "/project/" + projectId)
                .to(email)
                .subject("You have been added to project!")
                .build();

        sendMailMessageToNotificationService(mailMessageModelToDeveloper);
    }

    public void sendMailMessageDeveloperAddedToProject(UUID projectId, UUID developerId) {

        MailMessageModel mailMessageModelToDeveloper = MailMessageModel.builder()
                .message(PROJECT + PROJECT_SERVICE_URL + "/project/" + projectId)
                .to(developerId.toString())
                .subject("You have been added to project!")
                .build();

        sendMailMessageToAuthService(mailMessageModelToDeveloper);
    }
}
