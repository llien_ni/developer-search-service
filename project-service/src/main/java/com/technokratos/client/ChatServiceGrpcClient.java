package com.technokratos.client;


import chat.service.ChatServiceGrpc;
import chat.service.ChatServiceOuterClass;
import message.service.MessageServiceGrpc;
import message.service.MessageServiceOuterClass;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

@Service
public class ChatServiceGrpcClient {

    @GrpcClient("chat-service")
    private ChatServiceGrpc.ChatServiceBlockingStub chatServiceBlockingStub;

    @GrpcClient("chat-service")
    private MessageServiceGrpc.MessageServiceBlockingStub messageServiceBlockingStub;

    public ChatServiceOuterClass.ChatResponse createChat(ChatServiceOuterClass.CreateChatRequest request){

        return chatServiceBlockingStub.createChat(request);
    }

    public MessageServiceOuterClass.Empty sendMessageToChat(MessageServiceOuterClass.MessageRequest request){

        return messageServiceBlockingStub.sendMessageToChat(request);
    }
}
