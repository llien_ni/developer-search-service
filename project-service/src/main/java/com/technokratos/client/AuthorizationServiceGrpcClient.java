package com.technokratos.client;


import authorization.service.AccountServiceGrpc;
import authorization.service.AccountServiceOuterClass;
import authorization.service.DeveloperServiceGrpc;
import authorization.service.DeveloperServiceOuterClass;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;


@Service
public class AuthorizationServiceGrpcClient {

    @GrpcClient("authorization-service")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("authorization-service")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;


    public DeveloperServiceOuterClass.DeveloperResponse getDeveloperById(DeveloperServiceOuterClass.GetDeveloperByIdRequest request) {

        return developerServiceBlockingStub.getDeveloperById(request);
    }

    public DeveloperServiceOuterClass.DeveloperResponse getDeveloperByUsername(DeveloperServiceOuterClass.GetDeveloperByUsernameRequest request){

        return developerServiceBlockingStub.getDeveloperByUsername(request);
    }

    public AccountServiceOuterClass.AccountResponse getAccountById(AccountServiceOuterClass.GetAccountByIdRequest request) {

        return accountServiceBlockingStub.getAccountById(request);
    }
}
