package com.technokratos.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProjectResponse {

    private UUID id;

    private UUID authorId;

    private String name;

    private String description;

    private String linkToRepository;

    private List<SpecializationResponse> specializations;

    private List<TagResponse> tags;

    private String status;

    private OffsetDateTime createdDate;

    private OffsetDateTime updateDate;
}
