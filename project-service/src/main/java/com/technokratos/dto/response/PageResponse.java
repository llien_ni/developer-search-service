package com.technokratos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageResponse<T extends Collection>{

    private Integer page;

    private Integer size;

    private Long totalElements;

    private Integer totalPages;

    private T data;
}
