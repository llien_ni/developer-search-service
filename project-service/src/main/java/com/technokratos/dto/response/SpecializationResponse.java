package com.technokratos.dto.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SpecializationResponse {

    private UUID id;

    private UUID projectId;

    private String specializationName;
}
