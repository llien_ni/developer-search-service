package com.technokratos.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UpdateProjectRequest {

    @NotNull(message = "id cannot be empty")
    private UUID id;

    @Size(min = 3, max = 100)
    private String name;

    @Size(min = 10, max = 1000)
    private String description;

    private String linkToRepository;

    private String status;
}
