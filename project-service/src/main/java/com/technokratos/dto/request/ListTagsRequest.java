package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ListTagsRequest {

    @NotNull(message = "project id cannot be empty")
    private UUID projectId;

    @Size(min = 1, max = 100)
    private List<@NotBlank(message = "Tag cannot be empty") String> tags;
}
