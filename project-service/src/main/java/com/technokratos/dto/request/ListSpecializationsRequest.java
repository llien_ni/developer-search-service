package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ListSpecializationsRequest {

    @NotNull(message = "project id cannot be empty")
    private UUID projectId;

    @Size(max = 50)
    private List<@NotBlank(message = "Specialization cannot be empty")String> specializations;
}
