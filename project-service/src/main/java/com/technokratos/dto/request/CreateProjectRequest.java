package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CreateProjectRequest {

    @NotBlank(message = "Project name cannot be empty")
    @Size(min = 3, max = 100)
    private String name;

    @NotBlank(message = "Project description cannot be empty")
    @Size(min = 10, max = 1000)
    private String description;

    private String linkToRepository;

    private List<String> specializations;

    private List<String> tags;
}
