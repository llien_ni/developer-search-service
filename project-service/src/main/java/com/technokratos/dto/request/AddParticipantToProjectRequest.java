package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AddParticipantToProjectRequest {

    @NotNull(message = "project id cannot be empty")
    private UUID projectId;

    @NotBlank(message = "username cannot be empty")
    private String developerUsername;
}
