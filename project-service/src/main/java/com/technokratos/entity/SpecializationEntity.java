package com.technokratos.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.persistence.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "specialization")
public class SpecializationEntity extends AbstractBaseEntity {

    @Column(name = "specialization_name")
    private String specializationName;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_project_id"))
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private ProjectEntity project;
}
