package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.persistence.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tag")
public class TagEntity extends AbstractBaseEntity{

    @Column(name = "tag")
    private String tag;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_project_id"))
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private ProjectEntity project;
}
