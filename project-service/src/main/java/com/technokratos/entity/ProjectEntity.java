package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "project")
public class ProjectEntity extends AbstractBaseEntity {

    public enum Status {
        ACTIVE, NOT_ACTIVE
    }

    private String name;

    private String description;

    @Column(name = "link_to_Repository")
    private String linkToRepository;

    @Column(name = "author_id")
    private UUID authorId;

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;

    @Column(name = "updated_date")
    @UpdateTimestamp
    private OffsetDateTime updateDate;

    @Enumerated(value = EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "project", cascade = {CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<TagEntity> tags;

    @OneToMany(mappedBy = "project", cascade = {CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<SpecializationEntity> specializations;

    @OneToMany(mappedBy = "project")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<ProjectDeveloper> developers;
}
