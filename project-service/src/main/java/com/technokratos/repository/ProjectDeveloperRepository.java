package com.technokratos.repository;

import com.technokratos.entity.ProjectDeveloper;
import com.technokratos.entity.ProjectEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProjectDeveloperRepository extends JpaRepository<ProjectDeveloper, UUID> {

    Page<ProjectDeveloper> findByDeveloperIdAndState(UUID developerId, ProjectDeveloper.State state, Pageable pageable);

    List<ProjectDeveloper> findAllByProject(ProjectEntity project);

    List<ProjectDeveloper> findAllByProjectAndState(ProjectEntity project, ProjectDeveloper.State state);

    Optional<ProjectDeveloper> findByDeveloperIdAndProject(UUID developerId, ProjectEntity project);

    Page<ProjectDeveloper> findAllByDeveloperId(UUID developerId, Pageable pageable);

    Page<ProjectDeveloper> findAllByProject_AuthorId(UUID authorId, Pageable pageable);
}
