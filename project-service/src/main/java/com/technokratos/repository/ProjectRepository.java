package com.technokratos.repository;

import com.technokratos.entity.ProjectEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectEntity, UUID> {

    Page<ProjectEntity> findAllByAuthorId(UUID authorId, Pageable pageable);

    Optional<ProjectEntity> findByIdAndAuthorId(UUID id, UUID authorId);
}
