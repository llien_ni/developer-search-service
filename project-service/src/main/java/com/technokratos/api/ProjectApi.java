package com.technokratos.api;

import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateProjectRequest;
import com.technokratos.dto.response.*;
import com.technokratos.dto.request.CreateProjectRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/project")
public interface ProjectApi {

    @Operation(summary = "add project")
    @ApiResponse(responseCode = "201", description = "project successfully added",
            content = @Content(schema = @Schema(implementation = ProjectResponse.class)))
    @ApiResponse(responseCode = "400", description = "more elements in specializations list or in tags list",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ProjectResponse add(@RequestBody @Valid CreateProjectRequest projectRequest, @AuthenticationPrincipal String accountId);

    @Operation(summary = "update project")
    @ApiResponse(responseCode = "202", description = "project successfully updated",
            content = @Content(schema = @Schema(implementation = ProjectResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    ProjectResponse update(@RequestBody @Valid UpdateProjectRequest projectRequest, @AuthenticationPrincipal String accountId);

    @Operation(summary = "delete project")
    @ApiResponse(responseCode = "202", description = "project successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "the project could not be deleted because there are other participants in it",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String accountId);

    @Operation(summary = "get project by id")
    @ApiResponse(responseCode = "200", description = "project successfully received",
            content = @Content(schema = @Schema(implementation = ProjectResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    ProjectResponse getById(@PathVariable UUID id);

    @Operation(summary = "get developers by project")
    @ApiResponse(responseCode = "200", description = "developers successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/by/project/{project-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<DeveloperResponse> getDevelopersByProject(@PathVariable("project-id") UUID projectId,
                                                   @RequestParam(value = "size", required = false) Integer size,
                                                   @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get projects by user, where user is author of the project")
    @ApiResponse(responseCode = "200", description = "projects successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/by/author/{author-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<ProjectResponse> getByAuthor(@PathVariable("author-id") UUID authorId,
                                      @RequestParam(value = "size", required = false) Integer size,
                                      @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get projects by user, where user is a project participant")
    @ApiResponse(responseCode = "200", description = "projects successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/by/developer/{developer-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<ProjectResponse> getByAccount(@PathVariable("developer-id") UUID developerId,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get all projects")
    @ApiResponse(responseCode = "200", description = "projects successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<ProjectResponse> getAll(@RequestParam(value = "size", required = false) Integer size,
                                 @RequestParam(value = "number", required = false) Integer number);
}
