package com.technokratos.api;

import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.ListSpecializationsRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.ListResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.SpecializationResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RequestMapping("/project/specialization")
public interface SpecializationApi {

    @Operation(summary = "add specializations as list to the project")
    @ApiResponse(responseCode = "201", description = "specializations successfully added",
            content = @Content(schema = @Schema(implementation = ListResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ListResponse<SpecializationResponse> addSpecializationToProject(@RequestBody @Valid ListSpecializationsRequest list, @AuthenticationPrincipal String accountID);

    @Operation(summary = "delete specializations as list from the project")
    @ApiResponse(responseCode = "202", description = "specializations successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "specialization does not exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse deleteSpecializationFromProject(@RequestBody @Valid ListIdsRequest list, @AuthenticationPrincipal String accountID);
}
