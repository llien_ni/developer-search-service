package com.technokratos.api;

import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.ListTagsRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.ListResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.TagResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/project/tag")
public interface TagApi {

    @Operation(summary = "add tags as list to the project")
    @ApiResponse(responseCode = "201", description = "tags successfully added",
            content = @Content(schema = @Schema(implementation = ListResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ListResponse<TagResponse> addTagsToProject(@RequestBody @Valid ListTagsRequest list, @AuthenticationPrincipal String accountID);

    @Operation(summary = "delete tags as list from the project")
    @ApiResponse(responseCode = "202", description = "tags successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "tag does not exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse deleteTagsFromProject(@RequestBody @Valid ListIdsRequest list, @AuthenticationPrincipal String accountID);
}
