package com.technokratos.api;


import com.technokratos.dto.request.AddParticipantToProjectRequest;
import com.technokratos.dto.request.DeleteDeveloperFromProjectRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ProjectDeveloperResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RequestMapping("/project/participation")
public interface ProjectParticipationApi {

    @Operation(summary = "get all user requests to participate in the projects and sort by created date")
    @ApiResponse(responseCode = "200", description = "requests to participate in the project successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @GetMapping("/by/developer")
    @ResponseStatus(HttpStatus.OK)
    Page<ProjectDeveloperResponse> getAllByDeveloper(@AuthenticationPrincipal String developerId,
                                                     @RequestParam(value = "size", required = false) Integer size,
                                                     @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get all requests to participate in the projects for the author of the projects and sort by created date")
    @ApiResponse(responseCode = "200", description = "requests to participate in the project successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @GetMapping("/by/author")
    @ResponseStatus(HttpStatus.OK)
    Page<ProjectDeveloperResponse> getAllByAuthor(@AuthenticationPrincipal String authorId,
                                                  @RequestParam(value = "size", required = false) Integer size,
                                                  @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "send a request to participate in the project")
    @ApiResponse(responseCode = "201", description = "request successfully sent, created chat and sent notification by email",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "the developer has already sent a request to participate in the project",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping("/send-request")
    @ResponseStatus(HttpStatus.CREATED)
    MessageResponse sendRequestToTakePartInProject(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String accountId);

    @Operation(summary = "add participant to project, participant find by username")
    @ApiResponse(responseCode = "201", description = "participant successfully added",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "developer already added to project",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    MessageResponse addParticipantToProject(@RequestBody @Valid AddParticipantToProjectRequest request, @AuthenticationPrincipal String accountId);

    @Operation(summary = "add participant to project")
    @ApiResponse(responseCode = "201", description = "participant successfully added",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "request to participate in project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @PatchMapping
    @ResponseStatus(HttpStatus.CREATED)
    MessageResponse addParticipantByIdToProject(@RequestBody @Valid RequestId request, @AuthenticationPrincipal String accountId);

    @Operation(summary = "developer wants to leave the project")
    @ApiResponse(responseCode = "202", description = "developer successfully left the project",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found in project",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping()
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse deleteFromProject(@RequestBody @Valid RequestId request, @AuthenticationPrincipal String accountId);

    @Operation(summary = "the author of the project wants to delete the developer from the project")
    @ApiResponse(responseCode = "202", description = "developer successfully deleted from the project",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "project not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found in project",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_DEVELOPER', 'ROLE_SIMPLE_ACCOUNT')")
    @DeleteMapping("/developer")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse deleteDeveloperFromProjectForProjectAuthor(@RequestBody @Valid DeleteDeveloperFromProjectRequest request, @AuthenticationPrincipal String accountId);
}
