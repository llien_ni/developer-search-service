package com.technokratos.controller;

import com.technokratos.api.ProjectParticipationApi;
import com.technokratos.dto.request.AddParticipantToProjectRequest;
import com.technokratos.dto.request.DeleteDeveloperFromProjectRequest;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ProjectDeveloperResponse;
import com.technokratos.service.ProjectParticipationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

import static com.technokratos.util.constants.Constants.PROJECT_SERVICE_URL;

@RestController
@RequiredArgsConstructor
public class ProjectParticipationController implements ProjectParticipationApi {

    private final ProjectParticipationService projectParticipationService;

    @Override
    public Page<ProjectDeveloperResponse> getAllByDeveloper(String developerId, Integer size, Integer number) {

        return projectParticipationService.getAllByDeveloper(developerId, size, number);
    }

    @Override
    public Page<ProjectDeveloperResponse> getAllByAuthor(String authorId, Integer size, Integer number) {

        return projectParticipationService.getAllByAuthor(authorId, size, number);
    }

    @Override
    public MessageResponse sendRequestToTakePartInProject(RequestId requestId, String accountId) {

        UUID projectId = projectParticipationService.sendParticipationRequest(requestId.getId(), accountId);
        String projectUrl = "Project: " + PROJECT_SERVICE_URL + "/project/" + projectId;

        return MessageResponse.builder()
                .message(String.format("The request to add to the project %s has been sent", projectUrl))
                .build();
    }

    @Override
    public MessageResponse addParticipantToProject(AddParticipantToProjectRequest request, String accountId) {

        projectParticipationService.addParticipantToProject(request, accountId);

        return MessageResponse.builder()
                .message(String.format("Developer %s successfully added", request.getDeveloperUsername()))
                .build();
    }

    @Override
    public MessageResponse addParticipantByIdToProject(RequestId request, String accountId) {

        projectParticipationService.addParticipantToProject(request.getId(), accountId);

        return MessageResponse.builder()
                .message("Developer successfully added")
                .build();
    }

    @Override
    public MessageResponse deleteFromProject(RequestId request, String accountId) {

        UUID projectId = projectParticipationService.deleteForAccount(request.getId(), UUID.fromString(accountId));

        return MessageResponse.builder()
                .message(String.format("Developer successfully deleted from project %s", projectId))
                .build();
    }

    @Override
    public MessageResponse deleteDeveloperFromProjectForProjectAuthor(DeleteDeveloperFromProjectRequest request, String accountId) {

        UUID projectId = projectParticipationService.deleteDeveloperFromProjectForProjectAuthor(request, accountId);

        return MessageResponse.builder()
                .message(String.format("Developer successfully deleted from project %s", projectId))
                .build();
    }
}
