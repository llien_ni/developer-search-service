package com.technokratos.controller;

import com.technokratos.api.ProjectApi;
import com.technokratos.dto.request.CreateProjectRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateProjectRequest;
import com.technokratos.dto.response.*;
import com.technokratos.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
public class ProjectController implements ProjectApi {

    private final ProjectService projectService;

    @Override
    public ProjectResponse getById(UUID id) {

        return projectService.getById(id);
    }

    @Override
    public ProjectResponse add(CreateProjectRequest projectRequest, String accountId) {

        return projectService.add(projectRequest, UUID.fromString(accountId));
    }

    @Override
    public ProjectResponse update(UpdateProjectRequest projectRequest, String accountId) {

        return projectService.update(projectRequest, accountId);
    }

    @Override
    public MessageResponse delete(RequestId requestId, String accountId) {

        UUID projectId  = projectService.delete(requestId.getId(), accountId);
        return MessageResponse.builder()
                .message(String.format("Project with id %s successfully deleted", projectId))
                .build();
    }

    @Override
    public Page<DeveloperResponse> getDevelopersByProject(UUID projectId, Integer size, Integer number) {

        return projectService.getByProject(projectId, size, number);
    }

    @Override
    public Page<ProjectResponse> getByAuthor(UUID authorId, Integer size, Integer number) {

        return projectService.getByAuthor(authorId, size, number);
    }

    @Override
    public Page<ProjectResponse> getByAccount(UUID accountId, Integer size, Integer number) {

        return projectService.getByAccount(accountId, size, number);
    }

    @Override
    public Page<ProjectResponse> getAll(Integer size, Integer number) {

        return projectService.getAll(size, number);
    }
}
