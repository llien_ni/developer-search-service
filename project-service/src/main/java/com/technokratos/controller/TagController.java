package com.technokratos.controller;

import com.technokratos.api.TagApi;
import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.ListTagsRequest;
import com.technokratos.dto.response.ListResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.TagResponse;
import com.technokratos.service.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TagController implements TagApi {

    private final TagService tagService;

    @Override
    public ListResponse<TagResponse> addTagsToProject(ListTagsRequest list, String accountId) {

        List<TagResponse> data = tagService.addList(list, accountId);

        return ListResponse.<TagResponse>builder()
                .size(data.size())
                .data(data)
                .build();
    }

    @Override
    public MessageResponse deleteTagsFromProject(ListIdsRequest list, String accountId) {

        tagService.deleteList(list, accountId);

        return MessageResponse.builder()
                .message("Tags successfully deleted from project")
                .build();
    }
}
