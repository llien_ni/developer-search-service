package com.technokratos.controller;

import com.technokratos.api.SpecializationApi;
import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.ListSpecializationsRequest;
import com.technokratos.dto.response.ListResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.service.SpecializationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class SpecializationController implements SpecializationApi {

    private final SpecializationService specializationService;

    @Override
    public ListResponse<SpecializationResponse> addSpecializationToProject(ListSpecializationsRequest list, String accountId) {

        List<SpecializationResponse> data = specializationService.addList(list, accountId);

        return ListResponse.<SpecializationResponse>builder()
                .size(data.size())
                .data(data)
                .build();
    }

    @Override
    public MessageResponse deleteSpecializationFromProject(ListIdsRequest list, String accountId) {

        specializationService.deleteList(list, accountId);

        return MessageResponse.builder()
                .message("Specializations successfully deleted from project")
                .build();
    }
}
