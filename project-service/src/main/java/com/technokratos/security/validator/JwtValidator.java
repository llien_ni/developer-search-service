package com.technokratos.security.validator;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.X509CertUtils;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import com.technokratos.exception.ParseTokenException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.Collections;

@Slf4j
@Component
public class JwtValidator {

    @Value("${jwt.certificate}")
    private String certificateFileName;

    private RSAKey getPublicKey() {

        RSAKey publicKey = null;
        try {
            String content = new String(Files.readAllBytes(Paths.get(certificateFileName)));
            X509Certificate certificate = X509CertUtils.parse(content);
            publicKey = RSAKey.parse(certificate);
        } catch (IOException | JOSEException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public Authentication buildAuthentication(String token) {

        ParsedToken parsedToken = parse(token);

        return new UsernamePasswordAuthenticationToken(parsedToken.id, null,
                Collections.singleton(new SimpleGrantedAuthority(parsedToken.getRole())));
    }

    private ParsedToken parse(String token) {

        try {
            JWT jwt = JWTParser.parse(token);
            SignedJWT jws = (SignedJWT) jwt;

            RSAKey publicKey = getPublicKey();
            RSASSAVerifier signVerifier = new RSASSAVerifier(publicKey);

            if (jws.verify(signVerifier)) {
                log.info("Token is valid");

                JWTClaimsSet claims = jws.getJWTClaimsSet();
                String role = claims.getClaim("role").toString();
                String id = claims.getSubject();

                return ParsedToken.builder()
                        .id(id)
                        .role(role)
                        .build();

            } else {
                log.info("Token is invalid");
                throw new JWTVerificationException("Token is invalid");
            }

        } catch (ParseException | JOSEException e) {
            throw new ParseTokenException();
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @lombok.Builder
    private static class ParsedToken {
        private String id;
        private String role;
    }
}
