package com.technokratos.service;

import com.technokratos.dto.request.AddParticipantToProjectRequest;
import com.technokratos.dto.request.DeleteDeveloperFromProjectRequest;
import com.technokratos.dto.response.ProjectDeveloperResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface ProjectParticipationService {

    UUID sendParticipationRequest(UUID projectId, String fromString);

    void addParticipantToProject(AddParticipantToProjectRequest request, String accountId);

    void addParticipantToProject(UUID id, String accountId);

    Page<ProjectDeveloperResponse> getAllByDeveloper(String developerId, Integer size, Integer number);

    Page<ProjectDeveloperResponse> getAllByAuthor(String authorId, Integer size, Integer number);

    UUID deleteDeveloperFromProjectForProjectAuthor(DeleteDeveloperFromProjectRequest request, String accountId);

    UUID deleteForAccount(UUID id, UUID fromString);
}
