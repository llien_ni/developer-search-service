package com.technokratos.service.impl;



import authorization.service.DeveloperServiceOuterClass;
import chat.service.ChatServiceOuterClass;
import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.client.ChatServiceGrpcClient;
import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.converter.DeveloperConverter;
import com.technokratos.dto.request.AddParticipantToProjectRequest;
import com.technokratos.dto.request.DeleteDeveloperFromProjectRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.entity.ProjectDeveloper;
import com.technokratos.entity.ProjectEntity;
import com.technokratos.exception.ProjectDeveloperAlreadyExistException;
import com.technokratos.exception.ProjectDeveloperNotFoundException;
import com.technokratos.exception.ProjectNotFoundException;
import com.technokratos.mapper.ProjectDeveloperMapper;
import com.technokratos.repository.ProjectDeveloperRepository;
import com.technokratos.repository.ProjectRepository;
import com.technokratos.dto.response.ProjectDeveloperResponse;
import com.technokratos.service.ProjectParticipationService;
import com.technokratos.service.ProjectService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import message.service.MessageServiceOuterClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static com.technokratos.util.constants.Constants.PROJECT_SERVICE_URL;


@Service
@RequiredArgsConstructor
public class ProjectParticipationServiceImpl implements ProjectParticipationService {

    private final ProjectDeveloperRepository projectDeveloperRepository;

    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    private final ProjectService projectService;

    private final ProjectRepository projectRepository;

    private final RequestParamUtil requestParamUtil;

    private final ProjectDeveloperMapper projectDeveloperMapper;

    private final ChatServiceGrpcClient chatServiceGrpcClient;

    private final NotificationServiceRabbit notificationServiceRabbit;

    @Transactional
    @Override
    public UUID sendParticipationRequest(UUID projectId, String accountId) {

        ProjectEntity project = projectRepository.findById(projectId)
                .orElseThrow(() -> new ProjectNotFoundException(projectId));

        checkProjectDeveloperNotExist(UUID.fromString(accountId), project);

        ProjectDeveloper projectDeveloper = ProjectDeveloper.builder()
                .developerId(UUID.fromString(accountId))
                .project(project)
                .state(ProjectDeveloper.State.WAIT)
                .build();
        projectDeveloperRepository.save(projectDeveloper);
        project.getDevelopers().add(projectDeveloper);
        projectRepository.save(project);

        createChatAndSendMessageToChat(project, project.getAuthorId(), accountId);

        notificationServiceRabbit.sendMailMessageToProjectAuthor(accountId, project.getId(), project.getAuthorId());
        notificationServiceRabbit.sendMailMessageToDeveloper(accountId, project.getId());

        return project.getId();
    }

    @Transactional
    @Override
    public void addParticipantToProject(AddParticipantToProjectRequest request, String authorId) {

        ProjectEntity projectEntity = projectRepository
                .findByIdAndAuthorId(request.getProjectId(), UUID.fromString(authorId))
                .orElseThrow(() -> new ProjectNotFoundException(request.getProjectId()));

        DeveloperServiceOuterClass.DeveloperResponse developerResponse = getDeveloperByUsername(request.getDeveloperUsername());

        checkProjectDeveloperNotExist(UUID.fromString(developerResponse.getId()), projectEntity);
        ProjectDeveloper projectDeveloper = ProjectDeveloper.builder()
                .project(projectEntity)
                .developerId(UUID.fromString(developerResponse.getId()))
                .state(ProjectDeveloper.State.ACCEPTED)
                .build();
        projectDeveloperRepository.save(projectDeveloper);
        projectEntity.getDevelopers().add(projectDeveloper);
        projectRepository.save(projectEntity);


        notificationServiceRabbit.sendMailMessageDeveloperAddedToProject(request.getProjectId(), developerResponse.getEmail());
    }

    private DeveloperServiceOuterClass.DeveloperResponse getDeveloperByUsername(String developerUsername) {

        return authorizationServiceGrpcClient
                .getDeveloperByUsername(DeveloperServiceOuterClass.GetDeveloperByUsernameRequest
                        .newBuilder()
                        .setUsername(developerUsername)
                        .build());
    }

    @Transactional
    @Override
    public void addParticipantToProject(UUID id, String accountId) {

        ProjectDeveloper projectDeveloper = projectDeveloperRepository.findById(id)
                .orElseThrow(ProjectDeveloperNotFoundException::new);

        ProjectEntity projectEntity = projectDeveloper.getProject();
        projectService.checkAccountIsAuthorProject(projectEntity, accountId);
        projectDeveloper.setState(ProjectDeveloper.State.ACCEPTED);
        projectDeveloperRepository.save(projectDeveloper);

        notificationServiceRabbit.sendMailMessageDeveloperAddedToProject(projectDeveloper.getProject().getId(), projectDeveloper.getDeveloperId());
    }

    private void checkProjectDeveloperNotExist(UUID developerId, ProjectEntity projectEntity) {

        Optional<ProjectDeveloper> projectDeveloper = projectDeveloperRepository
                .findByDeveloperIdAndProject(developerId, projectEntity);
        if (projectDeveloper.isPresent()) {
            throw new ProjectDeveloperAlreadyExistException();
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ProjectDeveloperResponse> getAllByDeveloper(String developerId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<ProjectDeveloper> projectDevelopers = projectDeveloperRepository.findAllByDeveloperId(UUID.fromString(developerId), pageRequest.withSort(sort));

        return projectDevelopers.map(projectDeveloperMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ProjectDeveloperResponse> getAllByAuthor(String authorId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<ProjectDeveloper> projectDevelopers = projectDeveloperRepository.findAllByProject_AuthorId(UUID.fromString(authorId), pageRequest.withSort(sort));
        return projectDevelopers.map(this::convertToProjectDeveloperResponse);
    }

    @Transactional
    @Override
    public UUID deleteDeveloperFromProjectForProjectAuthor(DeleteDeveloperFromProjectRequest request, String authorId) {

        ProjectEntity projectEntity = projectRepository
                .findByIdAndAuthorId(request.getProjectId(), UUID.fromString(authorId))
                .orElseThrow(() -> new ProjectNotFoundException(request.getProjectId()));

        ProjectDeveloper projectDeveloper = projectDeveloperRepository
                .findByDeveloperIdAndProject(request.getDeveloperId(), projectEntity)
                .orElseThrow(ProjectDeveloperNotFoundException::new);
        projectEntity.getDevelopers().remove(projectDeveloper);
        projectDeveloperRepository.delete(projectDeveloper);
        projectRepository.save(projectEntity);

        return projectEntity.getId();
    }

    @Transactional
    @Override
    public UUID deleteForAccount(UUID id, UUID accountId) {

        ProjectEntity projectEntity = projectRepository
                .findById(id)
                .orElseThrow(() -> new ProjectNotFoundException(id));

        ProjectDeveloper projectDeveloper = projectDeveloperRepository
                .findByDeveloperIdAndProject(accountId, projectEntity)
                .orElseThrow(ProjectDeveloperNotFoundException::new);
        projectEntity.getDevelopers().remove(projectDeveloper);
        projectDeveloperRepository.delete(projectDeveloper);
        projectRepository.save(projectEntity);

        return projectEntity.getId();
    }

    private ProjectDeveloperResponse convertToProjectDeveloperResponse(ProjectDeveloper projectDeveloper){

        ProjectDeveloperResponse projectDeveloperResponse = projectDeveloperMapper.fromEntityToResponse(projectDeveloper);
        DeveloperResponse developerResponse = DeveloperConverter.convertFromDeveloperResponseGrpc(getDeveloperById(projectDeveloper.getDeveloperId().toString()));
        projectDeveloperResponse.setDeveloper(developerResponse);
        return projectDeveloperResponse;
    }

    private DeveloperServiceOuterClass.DeveloperResponse getDeveloperById(String accountId) {

        return authorizationServiceGrpcClient
                .getDeveloperById(DeveloperServiceOuterClass.GetDeveloperByIdRequest
                        .newBuilder()
                        .setId(accountId)
                        .build());
    }

    private void createChatAndSendMessageToChat(ProjectEntity project, UUID authorId, String senderId) {

        ChatServiceOuterClass.ChatResponse chat = chatServiceGrpcClient.createChat(ChatServiceOuterClass.CreateChatRequest.newBuilder()
                .setIssuerId(senderId)
                .setAnotherAccountId(authorId.toString())
                .build());
        chatServiceGrpcClient.sendMessageToChat(MessageServiceOuterClass.MessageRequest
                .newBuilder()
                .setIssuerId(senderId)
                .setChatId(chat.getChatId())
                .setText("Want to participate in project: "+PROJECT_SERVICE_URL + "/project/" + project.getId())
                .build());
    }
}
