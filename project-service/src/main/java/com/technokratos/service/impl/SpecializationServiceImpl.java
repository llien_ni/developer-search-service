package com.technokratos.service.impl;

import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.ListSpecializationsRequest;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.entity.ProjectEntity;
import com.technokratos.entity.SpecializationEntity;
import com.technokratos.exception.ProjectNotFoundException;
import com.technokratos.exception.SpecializationNotExistException;
import com.technokratos.mapper.SpecializationMapper;
import com.technokratos.repository.ProjectRepository;
import com.technokratos.repository.SpecializationRepository;
import com.technokratos.service.SpecializationService;
import com.technokratos.util.ConstraintUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpecializationServiceImpl implements SpecializationService {

    private final SpecializationRepository specializationRepository;

    private final ProjectRepository projectRepository;

    private final SpecializationMapper specializationMapper;

    @Transactional
    @Override
    public List<SpecializationResponse> addList(ListSpecializationsRequest list, String accountId) {

        ProjectEntity project = projectRepository.findByIdAndAuthorId(list.getProjectId(), UUID.fromString(accountId))
                .orElseThrow(() -> new ProjectNotFoundException(list.getProjectId()));

        ConstraintUtil.checkListNotNull(list.getSpecializations());

        ConstraintUtil.checkTargetTagListSize(project, list.getSpecializations(), 50);

        List<SpecializationEntity> specializationsEntities = specializationMapper
                .fromStringToSpecializationEntity(list.getSpecializations())
                .stream()
                .map(specializationEntity -> {
                    specializationEntity.setProject(project);
                    specializationRepository.save(specializationEntity);
                    return specializationEntity;
                }).collect(Collectors.toList());

        project.getSpecializations().addAll(specializationsEntities);
        projectRepository.save(project);
        return specializationMapper.fromEntityToResponse(specializationsEntities);
    }

    @Transactional
    @Override
    public void deleteList(ListIdsRequest list, String accountId) {

        ProjectEntity projectEntity = projectRepository.findByIdAndAuthorId(list.getProjectId(), UUID.fromString(accountId))
                .orElseThrow(() -> new ProjectNotFoundException(list.getProjectId()));

        ConstraintUtil.checkListNotNull(list.getIds());

        List<UUID> listProjectSpecializations = projectEntity.getSpecializations()
                .stream()
                .map(SpecializationEntity::getId)
                .toList();

        list.getIds().forEach(id -> {
            if (!listProjectSpecializations.contains(id)) {
                throw new SpecializationNotExistException(id);
            }

            SpecializationEntity specializationEntity = specializationRepository.getReferenceById(id);

            specializationRepository.delete(specializationEntity);
            projectEntity.getSpecializations().remove(specializationEntity);
        });
        projectRepository.save(projectEntity);
    }
}
