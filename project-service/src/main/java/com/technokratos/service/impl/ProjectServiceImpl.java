package com.technokratos.service.impl;

import authorization.service.DeveloperServiceOuterClass;
import com.technokratos.client.AuthorizationServiceGrpcClient;
import com.technokratos.converter.DeveloperConverter;
import com.technokratos.dto.request.CreateProjectRequest;
import com.technokratos.dto.request.UpdateProjectRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.ProjectResponse;
import com.technokratos.entity.ProjectDeveloper;
import com.technokratos.entity.ProjectEntity;
import com.technokratos.exception.DeleteProjectException;
import com.technokratos.exception.MoreElementsInListException;
import com.technokratos.exception.ProjectNotFoundException;
import com.technokratos.mapper.ProjectMapper;
import com.technokratos.repository.ProjectDeveloperRepository;
import com.technokratos.repository.ProjectRepository;
import com.technokratos.repository.SpecializationRepository;
import com.technokratos.repository.TagRepository;
import com.technokratos.service.ProjectService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    private final SpecializationRepository specializationRepository;

    private final TagRepository tagRepository;

    private final ProjectMapper projectMapper;

    private final RequestParamUtil requestParamUtil;

    private final ProjectDeveloperRepository projectDeveloperRepository;

    private static final String CREATED_DATE = "createdDate";

    @Transactional(readOnly = true)
    @Override
    public ProjectResponse getById(UUID id) {

        ProjectEntity projectEntity = projectRepository.findById(id)
                .orElseThrow(() -> new ProjectNotFoundException(id));
        return projectMapper.fromEntityToResponse(projectEntity);
    }

    @Transactional
    @Override
    public ProjectResponse add(CreateProjectRequest projectRequest, UUID accountId) {

        if (projectRequest.getSpecializations() != null && projectRequest.getSpecializations().size() > 50) {
            throw new MoreElementsInListException();
        }
        if (projectRequest.getTags() != null && projectRequest.getTags().size() > 50) {
            throw new MoreElementsInListException();
        }
        ProjectEntity projectEntity = projectMapper.fromRequestToEntity(projectRequest);
        projectEntity.setAuthorId(accountId);
        projectEntity.setStatus(ProjectEntity.Status.ACTIVE);
        projectEntity.setDevelopers(new ArrayList<>());
        projectEntity.setCreatedDate(OffsetDateTime.now());
        ProjectEntity savedEntity = projectRepository.save(projectEntity);

        if (savedEntity.getSpecializations() != null) {
            saveSpecializationsForProject(savedEntity);
        }else {
            projectEntity.setSpecializations(new ArrayList<>());
        }

        if (savedEntity.getTags() != null) {
            saveTagsForProject(savedEntity);
        }else {
            projectEntity.setTags(new ArrayList<>());
        }

        return projectMapper.fromEntityToResponse(savedEntity);
    }

    private void saveSpecializationsForProject(ProjectEntity savedEntity) {

        savedEntity.getSpecializations().forEach(specializationEntity -> {
            specializationEntity.setProject(savedEntity);
            specializationRepository.save(specializationEntity);
        });
    }

    private void saveTagsForProject(ProjectEntity savedEntity) {

        savedEntity.getTags().forEach(tagEntity -> {
            tagEntity.setProject(savedEntity);
            tagRepository.save(tagEntity);
        });
    }

    @Override
    public void checkAccountIsAuthorProject(ProjectEntity project, String accountId) {

        if (!project.getAuthorId().equals(UUID.fromString(accountId))) {
            throw new ProjectNotFoundException(project.getId());
        }
    }

    @Transactional
    @Override
    public ProjectResponse update(UpdateProjectRequest projectRequest, String accountId) {

        ProjectEntity projectEntity = projectRepository.findByIdAndAuthorId(projectRequest.getId(), UUID.fromString(accountId))
                .orElseThrow(() -> new ProjectNotFoundException(projectRequest.getId()));
        projectMapper.update(projectRequest, projectEntity);

        if(projectRequest.getStatus()!=null){
            projectEntity.setStatus(ProjectEntity.Status.valueOf(projectRequest.getStatus()));
        }

        return projectMapper.fromEntityToResponse(projectRepository.save(projectEntity));
    }

    @Transactional
    @Override
    public UUID delete(UUID id, String accountId) {

        ProjectEntity projectEntity = projectRepository.findByIdAndAuthorId(id, UUID.fromString(accountId))
                .orElseThrow(() -> new ProjectNotFoundException(id));
        List<ProjectDeveloper> developers = projectDeveloperRepository.findAllByProject(projectEntity);
        if (developers.isEmpty()) {
            projectRepository.delete(projectEntity);
            return id;
        }else{
            throw new DeleteProjectException();
        }
    }
    private final AuthorizationServiceGrpcClient authorizationServiceGrpcClient;

    @Transactional(readOnly = true)
    @Override
    public Page<DeveloperResponse> getByProject(UUID projectId, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        ProjectEntity projectEntity = projectRepository
                .findById(projectId)
                .orElseThrow(() -> new ProjectNotFoundException(projectId));

        List<UUID> listDevelopersIds = projectDeveloperRepository.findAllByProjectAndState(projectEntity, ProjectDeveloper.State.ACCEPTED)
                .stream()
                .map(ProjectDeveloper::getDeveloperId)
                .toList();

        List<DeveloperResponse>  developerResponses = new ArrayList<>();
        for(UUID developerId: listDevelopersIds){

            DeveloperServiceOuterClass.DeveloperResponse developerGrpcResponse = authorizationServiceGrpcClient.getDeveloperById(DeveloperServiceOuterClass.GetDeveloperByIdRequest.newBuilder()
                    .setId(developerId.toString())
                    .build());

            DeveloperResponse developerResponse = DeveloperConverter.convertFromDeveloperResponseGrpc(developerGrpcResponse);
            developerResponses.add(developerResponse);
        }
        return new PageImpl<>(developerResponses, pageRequest, developerResponses.size());
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ProjectResponse> getByAuthor(UUID authorId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<ProjectEntity> projects = projectRepository.findAllByAuthorId(authorId, pageRequest.withSort(sort));

        return projects.map(projectMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ProjectResponse> getByAccount(UUID accountId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<ProjectDeveloper> projectDevelopers = projectDeveloperRepository
                .findByDeveloperIdAndState(accountId,ProjectDeveloper.State.ACCEPTED, pageRequest.withSort(sort));
        Page<ProjectEntity> page = projectDevelopers.map(ProjectDeveloper::getProject);

        return page.map(projectMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ProjectResponse> getAll(Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<ProjectEntity> projects = projectRepository.findAll(pageRequest.withSort(sort));

        return projects.map(projectMapper::fromEntityToResponse);
    }
}
