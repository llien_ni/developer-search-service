package com.technokratos.service.impl;

import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.ListTagsRequest;
import com.technokratos.dto.response.TagResponse;
import com.technokratos.entity.ProjectEntity;
import com.technokratos.entity.TagEntity;
import com.technokratos.exception.ProjectNotFoundException;
import com.technokratos.exception.TagNotExistException;
import com.technokratos.mapper.TagMapper;
import com.technokratos.repository.ProjectRepository;
import com.technokratos.repository.TagRepository;
import com.technokratos.service.TagService;
import com.technokratos.util.ConstraintUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    private final ProjectRepository projectRepository;

    private final TagMapper tagMapper;

    @Transactional
    @Override
    public List<TagResponse> addList(ListTagsRequest listTagsRequest, String accountId) {

        ProjectEntity project = projectRepository.findByIdAndAuthorId(listTagsRequest.getProjectId(), UUID.fromString(accountId))
                .orElseThrow(() -> new ProjectNotFoundException(listTagsRequest.getProjectId()));

        ConstraintUtil.checkListNotNull(listTagsRequest.getTags());

        ConstraintUtil.checkTargetTagListSize(project, listTagsRequest.getTags(), 50);

        List<TagEntity> tagsEntities = tagMapper.fromStringToEntity(listTagsRequest.getTags())
                .stream()
                .map(tagEntity -> {
            tagEntity.setProject(project);
            tagRepository.save(tagEntity);
            return tagEntity;
        }).collect(Collectors.toList());

        project.getTags().addAll(tagsEntities);
        projectRepository.save(project);
        return tagMapper.fromEntityToResponse(tagsEntities);
    }

    @Transactional
    @Override
    public void deleteList(ListIdsRequest list, String accountId) {

        ProjectEntity projectEntity = projectRepository
                .findByIdAndAuthorId(list.getProjectId(), UUID.fromString(accountId))
                .orElseThrow(() -> new ProjectNotFoundException(list.getProjectId()));

        ConstraintUtil.checkListNotNull(list.getIds());

        List<UUID> listProjectTags = projectEntity.getTags()
                .stream()
                .map(TagEntity::getId)
                .toList();

        list.getIds().forEach(id -> {
            if(!listProjectTags.contains(id)){

                throw new TagNotExistException(id);
            }
            TagEntity tag = tagRepository.getReferenceById(id);

            tagRepository.delete(tag);
            projectEntity.getTags().remove(tag);

        });
        projectRepository.save(projectEntity);
    }
}
