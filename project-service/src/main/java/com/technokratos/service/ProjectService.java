package com.technokratos.service;

import com.technokratos.dto.request.CreateProjectRequest;
import com.technokratos.dto.request.UpdateProjectRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.ProjectResponse;
import com.technokratos.entity.ProjectEntity;
import org.springframework.data.domain.Page;
import java.util.UUID;

public interface ProjectService {

    ProjectResponse add(CreateProjectRequest projectRequest, UUID accountId);

    void checkAccountIsAuthorProject(ProjectEntity project, String accountId);

    ProjectResponse update(UpdateProjectRequest projectRequest, String accountId);

    UUID delete(UUID id, String accountId);

    Page<DeveloperResponse> getByProject(UUID projectId, Integer size, Integer number);

    Page<ProjectResponse> getByAuthor(UUID authorId, Integer size, Integer number);

    Page<ProjectResponse> getByAccount(UUID accountId, Integer size, Integer number);

    Page<ProjectResponse> getAll(Integer size, Integer number);

    ProjectResponse getById(UUID id);
}
