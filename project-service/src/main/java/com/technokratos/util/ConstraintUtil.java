package com.technokratos.util;

import com.technokratos.entity.ProjectEntity;
import com.technokratos.exception.ListIsNullException;
import com.technokratos.exception.MoreElementsInListException;
import lombok.experimental.UtilityClass;

import java.util.List;

@UtilityClass
public class ConstraintUtil {

    public static void checkTargetTagListSize(ProjectEntity project, List<String> list, int maxValue) {
        if(project.getTags().size()+list.size()>maxValue){
            throw new MoreElementsInListException();
        }
    }

    public static <T> void checkListNotNull(List<T> list) {
        if(list == null){
            throw new ListIsNullException();
        }
    }
}
