package com.technokratos.util.constants;

public class Constants {

    public static final String AUTHORIZATION_SERVICE_URL = "http://localhost:8080";

    public static final String PROJECT_SERVICE_URL = "http://localhost:8089";
}
