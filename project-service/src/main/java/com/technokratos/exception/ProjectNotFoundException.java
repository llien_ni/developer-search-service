package com.technokratos.exception;

import java.util.UUID;

public class ProjectNotFoundException extends NotFoundException {

    private static final String MESSAGE = "Project with id %s not found";

    public ProjectNotFoundException(UUID id) {
        super(String.format(MESSAGE, id));
    }
}
