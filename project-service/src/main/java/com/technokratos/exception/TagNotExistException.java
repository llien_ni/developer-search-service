package com.technokratos.exception;

import java.util.UUID;

public class TagNotExistException extends BadRequestException {

    private static final String MESSAGE = "Tag with id %s is not exist";

    public TagNotExistException(UUID id) {
        super(String.format(MESSAGE, id));
    }
}
