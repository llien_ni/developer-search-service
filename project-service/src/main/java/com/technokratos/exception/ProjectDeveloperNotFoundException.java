package com.technokratos.exception;

public class ProjectDeveloperNotFoundException extends NotFoundException {

    private static final String MESSAGE = "Developer not found in this project";

    public ProjectDeveloperNotFoundException() {
        super(MESSAGE);
    }
}
