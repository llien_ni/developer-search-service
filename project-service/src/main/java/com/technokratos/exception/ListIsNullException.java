package com.technokratos.exception;

public class ListIsNullException extends BadRequestException{

    private static final String MESSAGE = "List is empty";

    public ListIsNullException() {
        super(MESSAGE);
    }
}
