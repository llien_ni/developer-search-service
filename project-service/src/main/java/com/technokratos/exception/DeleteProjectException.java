package com.technokratos.exception;

public class DeleteProjectException extends BadRequestException {

    private static final String MESSAGE = "The project could not be deleted because there are other participants in it";

    public DeleteProjectException() {
        super(MESSAGE);
    }
}
