package com.technokratos.exception;

public class ProjectDeveloperAlreadyExistException extends BadRequestException {

    private static final String MESSAGE = "Developer already added to project";

    public ProjectDeveloperAlreadyExistException() {
        super(MESSAGE);
    }
}
