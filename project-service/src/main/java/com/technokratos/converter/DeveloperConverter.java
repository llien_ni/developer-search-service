package com.technokratos.converter;


import authorization.service.DeveloperServiceOuterClass;
import com.technokratos.dto.response.DeveloperResponse;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class DeveloperConverter {

    public static DeveloperResponse convertFromDeveloperResponseGrpc(DeveloperServiceOuterClass.DeveloperResponse developerResponse){

        return DeveloperResponse.builder()
                .id(UUID.fromString(developerResponse.getId()))
                .firstName(developerResponse.getFirstName())
                .lastName(developerResponse.getLastname())
                .username(developerResponse.getUsername())
                .email(developerResponse.getEmail())
                .photo(developerResponse.getPhoto())
                .build();
    }
}
