package com.technokratos.config;


import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;

@Configuration
@RequiredArgsConstructor
public class RabbitMQConfig {

    private final CachingConnectionFactory cachingConnectionFactory;

    @Bean
    public DirectExchange exchange() {

        return new DirectExchange("direct-exchange");
    }

    @Bean
    public Jackson2JsonMessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(Jackson2JsonMessageConverter converter) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(cachingConnectionFactory);
        rabbitTemplate.setMessageConverter(converter);
        return rabbitTemplate;
    }

    @Bean
    public Binding projectServiceBinding(Queue queueFromProjectServiceToAuthService, DirectExchange exchange) {
        return BindingBuilder.bind(queueFromProjectServiceToAuthService).to(exchange).with("project-service");
    }

    @Bean
    public Queue queueFromProjectServiceToAuthService() {
        return QueueBuilder.durable("q.from-project-service-to-auth-service")
                .withArgument("x-dead-letter-exchange", "x.from-project-service-to-auth-service-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchema() {
        return new Declarables(
                new DirectExchange("x.from-project-service-to-auth-service-failure"),
                new Queue("q.fall-back-from-project-service-to-auth-service"),
                new Binding("q.fall-back-from-project-service-to-auth-service", Binding.DestinationType.QUEUE, "x.from-project-service-to-auth-service-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(SimpleRabbitListenerContainerFactoryConfigurer configurer) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, cachingConnectionFactory);
        factory.setAcknowledgeMode(AcknowledgeMode.AUTO);
        factory.setAdviceChain(retryInterceptor());
        factory.setDefaultRequeueRejected(false);
        return factory;
    }


    @Bean
    public Binding projectNotificationServiceBinding(Queue queueProjectNotification, DirectExchange exchange) {
        return BindingBuilder.bind(queueProjectNotification).to(exchange).with("project-service-notification");
    }

    @Bean
    public Queue queueProjectNotification() {
        return QueueBuilder.durable("q.project-service-notification")
                .withArgument("x-dead-letter-exchange", "x.project-notification-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaForProjectNotification() {
        return new Declarables(
                new DirectExchange("x.project-notification-failure"),
                new Queue("q.fall-back-project-notification"),
                new Binding("q.fall-back-project-notification", Binding.DestinationType.QUEUE, "x.project-notification-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public RetryOperationsInterceptor retryInterceptor() {
        return RetryInterceptorBuilder.stateless().maxAttempts(3)
                .backOffOptions(2000, 2.0, 100000)
                .recoverer(new RejectAndDontRequeueRecoverer())
                .build();
    }
}
