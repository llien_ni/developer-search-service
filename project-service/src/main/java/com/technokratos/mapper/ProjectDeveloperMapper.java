package com.technokratos.mapper;


import com.technokratos.entity.ProjectDeveloper;
import com.technokratos.dto.response.ProjectDeveloperResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProjectDeveloperMapper {

    @Mapping(source = "developerId", target = "developer.id")
    ProjectDeveloperResponse fromEntityToResponse(ProjectDeveloper projectDeveloper);
}
