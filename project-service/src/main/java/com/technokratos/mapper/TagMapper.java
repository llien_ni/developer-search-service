package com.technokratos.mapper;

import com.technokratos.dto.request.TagRequest;
import com.technokratos.dto.response.TagResponse;
import com.technokratos.entity.TagEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TagMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    TagEntity fromStringToEntity(String tag);

    @Mapping(source = "tag", target = "tag")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    List<TagEntity> fromStringToEntity(List<String> tag);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    TagEntity fromRequestToEntity(TagRequest request);

    @Mapping(source = "project.id", target = "projectId")
    TagResponse fromEntityToResponse(TagEntity tagEntity);

    @Mapping(source = "project.id", target = "projectId")
    List<TagResponse> fromEntityToResponse(List<TagEntity> tagsEntities);
}
