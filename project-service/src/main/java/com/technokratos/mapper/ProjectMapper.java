package com.technokratos.mapper;


import com.technokratos.dto.request.CreateProjectRequest;
import com.technokratos.dto.request.UpdateProjectRequest;
import com.technokratos.dto.response.ProjectResponse;
import com.technokratos.entity.ProjectEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {SpecializationMapper.class, TagMapper.class})
public interface ProjectMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    ProjectEntity fromRequestToEntity(CreateProjectRequest projectRequest);

    ProjectResponse fromEntityToResponse(ProjectEntity entity);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(UpdateProjectRequest projectRequest, @MappingTarget ProjectEntity projectEntity);
}
