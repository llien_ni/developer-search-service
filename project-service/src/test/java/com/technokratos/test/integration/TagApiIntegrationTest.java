package com.technokratos.test.integration;


import authorization.service.AccountServiceGrpc;
import authorization.service.DeveloperServiceGrpc;
import chat.service.ChatServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.ListTagsRequest;
import com.technokratos.dto.response.*;
import com.technokratos.entity.ProjectEntity;
import com.technokratos.repository.ProjectDeveloperRepository;
import com.technokratos.repository.ProjectRepository;
import message.service.MessageServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/tag-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class TagApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String PROJECT_TAG_PREFIX = "/project/tag";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String ADMIN_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9BRE1JTiIsImV4cCI6MTk5ODkzMTc4OSwianRpIjoiMTY4MzM1NTc4OTA1NyJ9.SrwnQ3nyazSAtG94WT3c40Ijc_ItZwv58Ap2nlhaAdbezjoE-_HB4B-wPWJ0IctG3FKU9d1_RCe6xay9FrIqSersIaNOPf5MktKvxVshka0iJsrbqVoLCpSusR07l0YsyVwXi32w69aOaXMnMvqKQL5Qke5Pmu7Z4dfQnM94klt6QZ0NnvKR9CLwQ0wkDFpQRYQRkqrCUGfqkfDyFG3k3Vb3La287H_VkAVu55Kb1rgM4B1L18Dte78-eVcYhVyod8DUI_645T4UjRakO1jGWvAoy8JIk2z4Ges3EtcwCQ5_OB0U30xsohPjXWgeLzdBUfxJgtm5kIdtrDdIEwwlOg";

    //2669cbf8-32c1-4796-bf43-2f36a8a80cc8
    private static final String DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    //117f2c92-7735-48fe-a0de-7ce15ff10937
    private static final String DEVELOPER_PROJECT_AUTHOR_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxMTdmMmM5Mi03NzM1LTQ4ZmUtYTBkZS03Y2UxNWZmMTA5MzciLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTkyNDUyMzEsImp0aSI6IjE2ODM2NjkyMzE1MzUifQ.cZYTB1ifODL14KExKom0BpomSayTPqzr5XEaJgyM39_RMStlfttN3cUUFnezOyf5HQYYVrGzf7BoKbsLrKUz4ApiSx8G22nkReqMz5Gl6bM3hchGJopxnkbO6E6z0g1CEwFhuC-9MUXXuFwncxv8iQk-G5uR5Hu-Zqkc_i74uKxlibt_-59XtV2mH-W4GQrV-ZSLW5dwfwoKJJ_XSM-YoPgWhukud6M2dXzk08pRjw3IkKa5O1LHo3Rs9bySZ4Ck_pM5DRvzwB4LMluLrNd8jL7wl5se85HK8CDLg9BFasfwKc1NQKLtTuUkE1L2KVJ9EtUGD_ypvEnwN0e3fbLGuw";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private MessageServiceGrpc.MessageServiceBlockingStub messageServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;

    @GrpcClient("inProcess")
    private ChatServiceGrpc.ChatServiceBlockingStub chatServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectDeveloperRepository projectDeveloperRepository;

    @Test
    void addTagsToProjectSuccessfully() throws Exception{

        UUID projectId = UUID.fromString("9a19406f-39a4-40f8-8cb4-4b0844a67f25");
        ProjectEntity project = projectRepository.getReferenceById(projectId);

        Assertions.assertEquals(1, project.getTags().size());

        List<String> tags = new ArrayList<>();
        tags.add("java");
        tags.add("spring");
        tags.add("python");
        ListTagsRequest listTagsRequest = ListTagsRequest.builder()
                .projectId(projectId)
                .tags(tags)
                .build();

        ListResponse<TagResponse> listResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + PROJECT_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listTagsRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_PROJECT_AUTHOR_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        ProjectEntity updatedProject = projectRepository.getReferenceById(projectId);
        Assertions.assertEquals(4, updatedProject.getTags().size());
        Assertions.assertNotNull(updatedProject.getTags().get(0).getTag());
        Assertions.assertNotNull(updatedProject.getTags().get(0).getProject());
        Assertions.assertNotNull(updatedProject.getTags().get(0).getId());

        Assertions.assertNotNull(updatedProject.getTags().get(1).getTag());
        Assertions.assertNotNull(updatedProject.getTags().get(1).getProject());
        Assertions.assertNotNull(updatedProject.getTags().get(1).getId());

        Assertions.assertEquals(3, listResponse.getSize());
    }

    @Test
    void addTagsToProjectFailedProjectNotFound() throws Exception{

        UUID projectId = UUID.fromString("9919406f-39a4-40f8-8cb4-4b0844a67f25");

        List<String> tags = new ArrayList<>();
        tags.add("java");
        tags.add("spring");
        tags.add("python");
        ListTagsRequest listTagsRequest = ListTagsRequest.builder()
                .projectId(projectId)
                .tags(tags)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + PROJECT_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listTagsRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_PROJECT_AUTHOR_TOKEN))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void addTagsToProjectFailedListTagsIsNull() throws Exception{

        UUID projectId = UUID.fromString("9a19406f-39a4-40f8-8cb4-4b0844a67f25");
        ProjectEntity project = projectRepository.getReferenceById(projectId);

        Assertions.assertEquals(1, project.getTags().size());

        ListTagsRequest listTagsRequest = ListTagsRequest.builder()
                .projectId(projectId)
                .tags(null)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + PROJECT_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listTagsRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_PROJECT_AUTHOR_TOKEN))
                        .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteTagsFromProjectSuccessfully() throws Exception{

        UUID projectId = UUID.fromString("1119406f-39a4-40f8-8cb4-4b0844a67f25");
        ProjectEntity projectEntity = projectRepository.getReferenceById(projectId);
        Assertions.assertEquals(4, projectEntity.getTags().size());

        List<UUID> list = new ArrayList<>();
        list.add(UUID.fromString("1569cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        list.add(UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        list.add(UUID.fromString("1469cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        ListIdsRequest listIdsRequest = ListIdsRequest.builder()
                .projectId(projectId)
                .ids(list)
                .build();

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + PROJECT_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listIdsRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_PROJECT_AUTHOR_TOKEN))
                        .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());

        ProjectEntity updatedProject = projectRepository.getReferenceById(projectId);
        Assertions.assertEquals(1, updatedProject.getTags().size());
        Assertions.assertEquals("1369cbf8-32c1-4796-bf43-2f36a8a80cc8", updatedProject.getTags().get(0).getId().toString());
        Assertions.assertNotNull(updatedProject.getTags().get(0).getProject());
        Assertions.assertNotNull(updatedProject.getTags().get(0).getTag());
    }

    @Test
    void deleteTagsFromProjectFailedProjectNotFound() throws Exception{

        UUID projectId = UUID.fromString("9919406f-39a4-40f8-8cb4-4b0844a67f25");

        List<UUID> list = new ArrayList<>();
        list.add(UUID.fromString("1569cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        list.add(UUID.fromString("1669cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        list.add(UUID.fromString("1469cbf8-32c1-4796-bf43-2f36a8a80cc8"));
        ListIdsRequest listIdsRequest = ListIdsRequest.builder()
                .projectId(projectId)
                .ids(list)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + PROJECT_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listIdsRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_PROJECT_AUTHOR_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteTagsFromProjectFailedListIsNull() throws Exception{

        UUID projectId = UUID.fromString("1119406f-39a4-40f8-8cb4-4b0844a67f25");
        ProjectEntity projectEntity = projectRepository.getReferenceById(projectId);
        Assertions.assertEquals(4, projectEntity.getTags().size());

        ListIdsRequest listIdsRequest = ListIdsRequest.builder()
                .projectId(projectId)
                .ids(null)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + PROJECT_TAG_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listIdsRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_PROJECT_AUTHOR_TOKEN))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
