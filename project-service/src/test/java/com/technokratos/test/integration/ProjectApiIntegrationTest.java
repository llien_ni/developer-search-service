package com.technokratos.test.integration;


import authorization.service.AccountServiceGrpc;
import authorization.service.DeveloperServiceGrpc;
import chat.service.ChatServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.CreateProjectRequest;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateProjectRequest;
import com.technokratos.dto.response.*;
import com.technokratos.entity.ProjectEntity;
import com.technokratos.repository.ProjectRepository;
import liquibase.pro.packaged.R;
import message.service.MessageServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/project-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class ProjectApiIntegrationTest extends AbstractIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";

    private static final String PROJECT_PREFIX = "/project";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String ADMIN_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9BRE1JTiIsImV4cCI6MTk5ODkzMTc4OSwianRpIjoiMTY4MzM1NTc4OTA1NyJ9.SrwnQ3nyazSAtG94WT3c40Ijc_ItZwv58Ap2nlhaAdbezjoE-_HB4B-wPWJ0IctG3FKU9d1_RCe6xay9FrIqSersIaNOPf5MktKvxVshka0iJsrbqVoLCpSusR07l0YsyVwXi32w69aOaXMnMvqKQL5Qke5Pmu7Z4dfQnM94klt6QZ0NnvKR9CLwQ0wkDFpQRYQRkqrCUGfqkfDyFG3k3Vb3La287H_VkAVu55Kb1rgM4B1L18Dte78-eVcYhVyod8DUI_645T4UjRakO1jGWvAoy8JIk2z4Ges3EtcwCQ5_OB0U30xsohPjXWgeLzdBUfxJgtm5kIdtrDdIEwwlOg";

    private static final String DEVELOPER_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private MessageServiceGrpc.MessageServiceBlockingStub messageServiceBlockingStub;

    @GrpcClient("inProcess")
    private DeveloperServiceGrpc.DeveloperServiceBlockingStub developerServiceBlockingStub;

    @GrpcClient("inProcess")
    private AccountServiceGrpc.AccountServiceBlockingStub accountServiceBlockingStub;

    @GrpcClient("inProcess")
    private ChatServiceGrpc.ChatServiceBlockingStub chatServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    void addProjectSuccessfully() throws Exception {

        List<String> tags = new ArrayList<>();
        tags.add("java");
        tags.add("postgtressql");

        List<String> specializations = new ArrayList<>();
        specializations.add("backend");
        CreateProjectRequest createProjectRequest = CreateProjectRequest.builder()
                .name("create  website")
                .description("We work and you will work")
                .linkToRepository("http://gitlab/my-project")
                .tags(tags)
                .specializations(specializations)
                .build();

        ProjectResponse projectResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + PROJECT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createProjectRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(projectResponse.getAuthorId());
        Assertions.assertEquals(createProjectRequest.getName(), projectResponse.getName());
        Assertions.assertEquals(createProjectRequest.getDescription(), projectResponse.getDescription());
        Assertions.assertEquals(createProjectRequest.getLinkToRepository(), projectResponse.getLinkToRepository());
        Assertions.assertEquals(2, projectResponse.getTags().size());
        Assertions.assertEquals(1, projectResponse.getSpecializations().size());
    }

    @Test
    void updateProjectSuccessfully() throws Exception {

        UUID projectId = UUID.fromString("9a19406f-39a4-40f8-8cb4-4b0844a67f25");

        ProjectEntity project = projectRepository.getReferenceById(projectId);

        UpdateProjectRequest updateProjectRequest = UpdateProjectRequest.builder()
                .id(projectId)
                .description("We need to create project")
                .status("NOT_ACTIVE")
                .build();

        ProjectResponse projectResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + PROJECT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateProjectRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        ProjectEntity updatedProject = projectRepository.getReferenceById(projectId);
        Assertions.assertEquals(updateProjectRequest.getDescription(), updatedProject.getDescription());
        Assertions.assertEquals(project.getName(), updatedProject.getName());
        Assertions.assertEquals(project.getLinkToRepository(), updatedProject.getLinkToRepository());

        Assertions.assertEquals(updateProjectRequest.getDescription(), projectResponse.getDescription());
        Assertions.assertEquals(project.getName(), projectResponse.getName());
    }

    @Test
    void updateProjectFailedProjectNotFound() throws Exception {

        UUID projectId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        UpdateProjectRequest updateProjectRequest = UpdateProjectRequest.builder()
                .id(projectId)
                .description("We need to create project")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + PROJECT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateProjectRequest))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteProjectSuccessfully() throws Exception {

        UUID projectId = UUID.fromString("9a19406f-39a4-40f8-8cb4-4b0844a67f25");

        RequestId requestId = new RequestId(projectId);

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + PROJECT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());

        Assertions.assertEquals(Optional.empty(), projectRepository.findById(projectId));
    }

    @Test
    void deleteProjectFailedProjectNotFound() throws Exception {

        UUID projectId = UUID.fromString("9969cbf8-32c1-4796-bf43-2f36a8a80cc8");

        RequestId requestId = new RequestId(projectId);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + PROJECT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteProjectFailedAnotherAccountsAlreadyAddedToThisProject() throws Exception {

        UUID projectId = UUID.fromString("1169cbf8-32c1-4796-bf43-2f36a8a80cc8");

        RequestId requestId = new RequestId(projectId);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + PROJECT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + DEVELOPER_TOKEN))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getById() throws Exception {

        UUID projectId = UUID.fromString("1269cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ProjectResponse projectResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + PROJECT_PREFIX + "/" + projectId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(projectResponse.getId());
        Assertions.assertNotNull(projectResponse.getName());
        Assertions.assertNotNull(projectResponse.getAuthorId());
        Assertions.assertNotNull(projectResponse.getTags());
        Assertions.assertNotNull(projectResponse.getTags().get(0).getTag());
        Assertions.assertNotNull(projectResponse.getSpecializations());
        Assertions.assertNotNull(projectResponse.getSpecializations().get(0).getSpecializationName());
    }

    @Test
    void getByAuthorSuccessfully() throws Exception {

        UUID authorId = UUID.fromString("2669cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ResponsePage<ProjectResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + PROJECT_PREFIX + "/by/author/" + authorId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(7, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getName());
        Assertions.assertNotNull(page.getContent().get(0).getId());
        Assertions.assertNotNull(page.getContent().get(0).getAuthorId());
    }

    @Test
    void getByAccountSuccessfully() throws Exception {

        UUID developerId = UUID.fromString("1169cbf8-32c1-4796-bf43-2f36a8a80cc8");

        ResponsePage<ProjectResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + PROJECT_PREFIX + "/by/developer/" + developerId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(1, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getName());
        Assertions.assertNotNull(page.getContent().get(0).getId());
        Assertions.assertNotNull(page.getContent().get(0).getAuthorId());
    }

    @Test
    void getAllSuccessfully() throws Exception {

        ResponsePage<ProjectResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + PROJECT_PREFIX))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(7, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getName());
        Assertions.assertNotNull(page.getContent().get(0).getId());
        Assertions.assertNotNull(page.getContent().get(0).getAuthorId());

    }

    @Test
    void getDevelopersByProject() throws Exception {

        UUID projectId = UUID.fromString("2319406f-39a4-40f8-8cb4-4b0844a67f25");
        ResponsePage<DeveloperResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + PROJECT_PREFIX + "/by/project/" + projectId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getId());
        Assertions.assertNotNull(page.getContent().get(0).getUsername());
        Assertions.assertNotNull(page.getContent().get(0).getEmail());
        Assertions.assertNotNull(page.getContent().get(0).getFirstName());
        Assertions.assertNotNull(page.getContent().get(0).getLastName());
        Assertions.assertNotNull(page.getContent().get(0).getPhoto());
    }

    @Test
    void getDevelopersByProjectFailedProjectNotFound() throws Exception {

        UUID projectId = UUID.fromString("9919406f-39a4-40f8-8cb4-4b0844a67f25");
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + PROJECT_PREFIX + "/by/project/" + projectId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
