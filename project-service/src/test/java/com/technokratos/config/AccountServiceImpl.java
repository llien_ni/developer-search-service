package com.technokratos.config;

import authorization.service.AccountServiceGrpc;
import authorization.service.AccountServiceOuterClass;
import com.technokratos.dto.response.AccountResponse;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.UUID;

@GrpcService
public class AccountServiceImpl extends AccountServiceGrpc.AccountServiceImplBase {

    @Override
    public void getAccountById(AccountServiceOuterClass.GetAccountByIdRequest request, StreamObserver<AccountServiceOuterClass.AccountResponse> responseObserver) {

        AccountServiceOuterClass.AccountResponse accountResponse = AccountServiceOuterClass.AccountResponse
                .newBuilder()
                .setId(UUID.randomUUID().toString())
                .setFirstName("account1")
                .setLastname("pokemon")
                .setEmail("account@gmail.com")
                .setRole("ROLE_DEVELOPER")
                .setStatus("CONFIRMED")
                .setPhoto("http://my-photo")
                .build();
        responseObserver.onNext(accountResponse);
        responseObserver.onCompleted();
    }
}
