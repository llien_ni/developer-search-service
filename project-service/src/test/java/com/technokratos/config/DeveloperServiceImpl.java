package com.technokratos.config;


import authorization.service.DeveloperServiceGrpc;
import authorization.service.DeveloperServiceOuterClass;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class DeveloperServiceImpl extends DeveloperServiceGrpc.DeveloperServiceImplBase {

    @Override
    public void getDeveloperById(DeveloperServiceOuterClass.GetDeveloperByIdRequest request, StreamObserver<DeveloperServiceOuterClass.DeveloperResponse> responseObserver) {

        if (request.getId().equals("4469cbf8-32c1-4796-bf43-2f36a8a80cc8")) {
            responseObserver.onNext(DeveloperServiceOuterClass.DeveloperResponse.newBuilder()
                    .setId("4469cbf8-32c1-4796-bf43-2f36a8a80cc8")
                    .setPhoto("http://link to photo")
                    .setFirstName("Lena")
                    .setLastname("Lll")
                    .setUsername("ll")
                    .setEmail("lena@gmail.com")
                    .build());
            responseObserver.onCompleted();
        } else if (request.getId().equals("4569cbf8-32c1-4796-bf43-2f36a8a80cc8")) {
            responseObserver.onNext(DeveloperServiceOuterClass.DeveloperResponse.newBuilder()
                    .setId("4569cbf8-32c1-4796-bf43-2f36a8a80cc8")
                    .setPhoto("http://link to photo")
                    .setFirstName("Lena")
                    .setLastname("Lll")
                    .setUsername("ll")
                    .setEmail("lena@gmail.com")
                    .build());
            responseObserver.onCompleted();
        } else {
            responseObserver.onNext(DeveloperServiceOuterClass.DeveloperResponse.newBuilder()
                    .setId("2669cbf8-32c1-4796-bf43-2f36a8a80cc8")
                    .setPhoto("http://link to photo")
                    .setFirstName("Lena")
                    .setLastname("Lll")
                    .setUsername("ll")
                    .setEmail("lena@gmail.com")
                    .build());
            responseObserver.onCompleted();
        }
    }

    @Override
    public void getDeveloperByUsername(DeveloperServiceOuterClass.GetDeveloperByUsernameRequest request, StreamObserver<DeveloperServiceOuterClass.DeveloperResponse> responseObserver) {

        if (request.getUsername().equals("pokemon")) {
            responseObserver.onNext(DeveloperServiceOuterClass.DeveloperResponse.newBuilder()
                    .setId("2669cbf8-32c1-4796-bf43-2f36a8a80cc8")
                    .setPhoto("http://link to photo")
                    .setFirstName("Lena")
                    .setLastname("Lll")
                    .setUsername("pokemon")
                    .setEmail("lena@gmail.com")
                    .build());
            responseObserver.onCompleted();
        }
    }
}
