package com.technokratos.config;

import io.grpc.stub.StreamObserver;
import message.service.MessageServiceGrpc;
import message.service.MessageServiceOuterClass;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class MessageServiceImpl extends MessageServiceGrpc.MessageServiceImplBase {

    @Override
    public void sendMessageToChat(MessageServiceOuterClass.MessageRequest request, StreamObserver<MessageServiceOuterClass.Empty> responseObserver) {

        responseObserver.onNext(MessageServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }
}
