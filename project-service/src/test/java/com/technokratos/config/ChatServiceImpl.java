package com.technokratos.config;


import chat.service.ChatServiceGrpc;
import chat.service.ChatServiceOuterClass;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.UUID;

@GrpcService
public class ChatServiceImpl extends ChatServiceGrpc.ChatServiceImplBase {

    @Override
    public void createChat(ChatServiceOuterClass.CreateChatRequest request, StreamObserver<ChatServiceOuterClass.ChatResponse> responseObserver) {

        ChatServiceOuterClass.ChatResponse response = ChatServiceOuterClass.ChatResponse.newBuilder()
                .setChatId(UUID.randomUUID().toString())
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
