package com.technokratos.handler.impl;

import com.technokratos.entity.BotState;
import com.technokratos.handler.InputMessageHandler;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import static com.technokratos.bot.TelegramBot.HELP_MESSAGE;

@Slf4j
@Component
@RequiredArgsConstructor
public class ShowMainMenuHandler implements InputMessageHandler {

    @Override
    public SendMessage handle(Message message) {

        return MessageUtil.createSendMessage(message.getChatId(), HELP_MESSAGE);
    }

    @Override
    public BotState getHandlerName() {

        return BotState.SHOW_MAIN_MENU;
    }
}
