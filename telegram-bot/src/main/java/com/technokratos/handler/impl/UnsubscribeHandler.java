package com.technokratos.handler.impl;

import com.technokratos.dto.ChatResponse;
import com.technokratos.entity.BotState;
import com.technokratos.handler.InputMessageHandler;
import com.technokratos.service.ChatService;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
@Slf4j
@RequiredArgsConstructor
public class UnsubscribeHandler implements InputMessageHandler {

    private static final String MESSAGE = "Ты отписался от автоматической отправки новых вакансий";

    private final ChatService chatService;

    @Override
    public SendMessage handle(Message message) {

        ChatResponse chat = chatService.getChatById(message.getChatId());
        chat.setSubscribed(false);
        chat.setBotState(BotState.SHOW_MAIN_MENU);
        chatService.updateChat(chat);

        return MessageUtil.createSendMessage(message.getChatId(), MESSAGE);
    }

    @Override
    public BotState getHandlerName() {
        return BotState.UNSUBSCRIBE;
    }
}
