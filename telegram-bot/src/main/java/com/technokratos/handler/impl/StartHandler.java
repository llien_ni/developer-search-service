package com.technokratos.handler.impl;

import com.technokratos.entity.BotState;
import com.technokratos.entity.ChatEntity;
import com.technokratos.handler.InputMessageHandler;
import com.technokratos.repository.ChatRepository;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.Optional;

import static com.technokratos.bot.TelegramBot.HELP_MESSAGE;

@Component
@RequiredArgsConstructor
public class StartHandler implements InputMessageHandler {

    private final ChatRepository chatRepository;

    @Override
    public SendMessage handle(Message message) {

        Optional<ChatEntity> user = chatRepository.findById(message.getChatId());
        if (user.isEmpty()) {
            ChatEntity chat = ChatEntity.builder()
                    .id(message.getChatId())
                    .build();
            chatRepository.save(chat);
        }
        return MessageUtil.createSendMessage(message.getChatId(), HELP_MESSAGE);
    }

    @Override
    public BotState getHandlerName() {
        return BotState.START;
    }
}
