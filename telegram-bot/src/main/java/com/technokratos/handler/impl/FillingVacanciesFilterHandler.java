package com.technokratos.handler.impl;


import com.technokratos.client.VacancyApiClient;
import com.technokratos.config.PageConfigurationProperties;
import com.technokratos.dto.ChatResponse;
import com.technokratos.dto.VacancyFilter;
import com.technokratos.entity.BotState;
import com.technokratos.handler.InputMessageHandler;
import com.technokratos.service.ChatService;
import com.technokratos.util.BotQuestionUtil;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

import static com.technokratos.util.MessageUtil.createListSpecializations;

@Slf4j
@Component
@RequiredArgsConstructor
public class FillingVacanciesFilterHandler implements InputMessageHandler {

    private final VacancyApiClient vacancyApiClient;

    private final ChatService chatService;

    private final PageConfigurationProperties pageConfigurationProperties;

    @Override
    public SendMessage handle(Message message) {

        ChatResponse chat = chatService.getChatById(message.getChatId());

        if (chat.getBotState().equals(BotState.FILLING_VACANCIES_FILTER)) {
            chat = chatService.updateBotState(chat.getId(), BotState.ASK_SPECIALIZATIONS);
        }
        return processUserInput(message, chat);
    }

    private SendMessage processUserInput(Message message, ChatResponse chat) {
        String messageText = message.getText();
        long chatId = message.getChatId();

        BotState botState = chat.getBotState();

        SendMessage replyToUser = null;

        if (botState.equals(BotState.ASK_SPECIALIZATIONS)) {
            replyToUser = MessageUtil.createSendMessage(chatId, BotQuestionUtil.QUESTION_SPECIALIZATIONS);

            ReplyKeyboardMarkup replyKeyboard = createReplyKeyboard();
            replyToUser.setReplyMarkup(replyKeyboard);

            chat.setBotState(BotState.ASK_REMOTE);
        }
        if (botState.equals(BotState.ASK_REMOTE)) {

            if (messageText != null && messageText.equals("Пропустить")) {
                messageText = null;
            }

            chat.setVacancySpecializations(messageText);
            replyToUser = MessageUtil.createSendMessage(chatId, BotQuestionUtil.QUESTION_REMOTE);
            ReplyKeyboardMarkup keyboardMarkup = createReplyKeyBoardForRemote();
            replyToUser.setReplyMarkup(keyboardMarkup);

            chat.setBotState(BotState.ASK_CITY);
        }
        if (botState.equals(BotState.ASK_CITY)) {
            if (messageText != null) {
                if (messageText.equals("Да")) {
                    chat.setVacancyRemote(true);
                } else if (messageText.equals("Нет")) {

                    chat.setVacancyRemote(false);
                } else {
                    chat.setVacancyRemote(null);
                }
            }


            replyToUser = MessageUtil.createSendMessage(chatId, BotQuestionUtil.QUESTION_CITY);
            replyToUser.setReplyMarkup(new ReplyKeyboardRemove(true));

            chat.setBotState(BotState.FILTER_IS_FULL);
        }
        if (botState.equals(BotState.FILTER_IS_FULL)) {
            chat.setVacancyCity(messageText);

            chat.setBotState(BotState.FIND_VACANCIES_BY_FILTER);
            VacancyFilter filter = VacancyFilter.builder()
                    .remote(chat.getVacancyRemote())
                    .specializations(createListSpecializations(chat))
                    .city(chat.getVacancyCity())
                    .build();

            replyToUser = MessageUtil.createSendMessageWithPagination(chatId,
                    vacancyApiClient.getByFilter(filter, pageConfigurationProperties.getSize(),
                            pageConfigurationProperties.getNumber()), BotState.FIND_VACANCIES_BY_FILTER.toString());
        }

        chatService.updateChat(chat);
        return replyToUser;
    }

    private ReplyKeyboardMarkup createReplyKeyBoardForRemote() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboardRows = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();
        row.add("Да");
        row.add("Нет");
        row.add("Пропустить");
        keyboardRows.add(row);

        keyboardMarkup.setKeyboard(keyboardRows);
        keyboardMarkup.setResizeKeyboard(true);
        return keyboardMarkup;
    }

    private ReplyKeyboardMarkup createReplyKeyboard() {

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboardRows = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();
        row.add("Пропустить");
        keyboardRows.add(row);

        keyboardMarkup.setKeyboard(keyboardRows);
        keyboardMarkup.setResizeKeyboard(true);
        return keyboardMarkup;
    }

    @Override
    public BotState getHandlerName() {
        return BotState.FILLING_VACANCIES_FILTER;
    }
}
