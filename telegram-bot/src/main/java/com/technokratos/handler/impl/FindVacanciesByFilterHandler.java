package com.technokratos.handler.impl;

import com.technokratos.client.VacancyApiClient;
import com.technokratos.config.PageConfigurationProperties;
import com.technokratos.dto.ChatResponse;
import com.technokratos.dto.VacancyFilter;
import com.technokratos.entity.BotState;
import com.technokratos.handler.InputMessageHandler;
import com.technokratos.service.ChatService;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import static com.technokratos.util.MessageUtil.createListSpecializations;


@Component
@RequiredArgsConstructor
public class FindVacanciesByFilterHandler implements InputMessageHandler {

    private final ChatService chatService;

    private final VacancyApiClient vacancyApiClient;

    private final PageConfigurationProperties pageConfigurationProperties;

    @Override
    public SendMessage handle(Message message) {

        ChatResponse chat = chatService.getChatById(message.getChatId());

        VacancyFilter filter = VacancyFilter.builder()
                .remote(chat.getVacancyRemote())
                .specializations(createListSpecializations(chat))
                .city(chat.getVacancyCity())
                .build();

        return MessageUtil.createSendMessageWithPagination(message.getChatId(), vacancyApiClient.getByFilter(filter,
                pageConfigurationProperties.getSize(), pageConfigurationProperties.getNumber()),
                BotState.FIND_VACANCIES_BY_FILTER.toString());
    }

    @Override
    public BotState getHandlerName() {
        return BotState.FIND_VACANCIES_BY_FILTER;
    }
}
