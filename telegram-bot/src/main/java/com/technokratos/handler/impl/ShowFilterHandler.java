package com.technokratos.handler.impl;

import com.technokratos.dto.ChatResponse;
import com.technokratos.entity.BotState;
import com.technokratos.handler.InputMessageHandler;
import com.technokratos.service.ChatService;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
@RequiredArgsConstructor
public class ShowFilterHandler implements InputMessageHandler {

    private final ChatService chatService;

    @Override
    public SendMessage handle(Message message) {

        ChatResponse chat = chatService.getChatById(message.getChatId());

        StringBuilder text = new StringBuilder();
        if(chat.getVacancySpecializations()!=null){

            text.append("Специализации: ").append(chat.getVacancySpecializations()).append("\n");
        }
        if(chat.getVacancyRemote()!=null){
            text.append("Удаленно: ").append(Boolean.TRUE.equals(chat.getVacancyRemote())?"да":"нет").append("\n");
        }
        if(chat.getVacancyCity()!=null){
            text.append("Город: ").append(chat.getVacancyCity()).append("\n");
        }
        if(text.isEmpty()){
            text.append("У тебя нет выбранных параметров для поиска");
        }

        return MessageUtil.createSendMessage(message.getChatId(), text.toString());
    }

    @Override
    public BotState getHandlerName() {
        return BotState.SHOW_FILTER;
    }
}
