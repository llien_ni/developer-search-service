package com.technokratos.handler.impl;

import com.technokratos.bot.BotStateContext;
import com.technokratos.client.VacancyApiClient;
import com.technokratos.config.PageConfigurationProperties;
import com.technokratos.dto.ChatResponse;
import com.technokratos.dto.VacancyFilter;
import com.technokratos.dto.VacancyResponse;
import com.technokratos.entity.BotState;
import com.technokratos.handler.UpdateHandler;
import com.technokratos.service.ChatService;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.time.LocalDate;

import static com.technokratos.util.MessageUtil.createListSpecializations;


@Slf4j
@Component
@RequiredArgsConstructor
public class UpdateHandlerImpl implements UpdateHandler {

    private final VacancyApiClient vacancyApiClient;

    private final BotStateContext botStateContext;

    private final ChatService chatService;

    private final PageConfigurationProperties pageConfigurationProperties;

    @Override
    public EditMessageText handleCallbackQuery(CallbackQuery callbackQuery) {

        ChatResponse chat = chatService.getChatById(callbackQuery.getMessage().getChatId());
        Page<VacancyResponse> page = null;

        String callbackData = callbackQuery.getData();
        int currentPage = Integer.parseInt(callbackData.split("-")[1]);
        String pageType = String.valueOf(callbackData.split("-")[0]);

        if(pageType.equals(BotState.GET_ALL_VACANCIES.toString())){

            page = vacancyApiClient.getAllActive(3, currentPage);
        }
        if(pageType.equals(BotState.FIND_VACANCIES_BY_FILTER.toString())){

            VacancyFilter vacancyFilter = VacancyFilter.builder()
                    .city(chat.getVacancyCity())
                    .specializations(createListSpecializations(chat))
                    .remote(chat.getVacancyRemote())
                    .build();
            page = vacancyApiClient.getByFilter(vacancyFilter, pageConfigurationProperties.getSize(), currentPage);
        }
        if(pageType.equals(BotState.SUBSCRIBE.toString())){

            VacancyFilter vacancyFilter = VacancyFilter.builder()
                    .city(chat.getVacancyCity())
                    .specializations(createListSpecializations(chat))
                    .remote(chat.getVacancyRemote())
                    .createdDate(LocalDate.now().minusDays(1))
                    .build();
            page = vacancyApiClient.getByFilter(vacancyFilter, pageConfigurationProperties.getSize(), currentPage);
        }

        log.info("Current page " + currentPage);

        return MessageUtil.editMessageWithPagination(callbackQuery.getMessage(), page, pageType);
    }

    @Override
    public SendMessage handleInput(Message message) {

        String messageText = message.getText();
        long chatId = message.getChatId();

        BotState botState;

        switch (messageText) {
            case "/start":
                botState = BotState.START;
                break;
            case "/get_all_vacancies":
                botState = BotState.GET_ALL_VACANCIES;
                chatService.updateBotState(chatId, botState);
                break;
            case "/help":
                botState = BotState.SHOW_MAIN_MENU;
                chatService.updateBotState(chatId, botState);
                break;
            case "/subscribe":
                botState = BotState.SUBSCRIBE;
                chatService.updateBotState(chatId, botState);
                break;
            case "/unsubscribe":
                botState = BotState.UNSUBSCRIBE;
                chatService.updateBotState(chatId, botState);
                break;
            case "/filling_filter":
                botState = BotState.FILLING_VACANCIES_FILTER;
                chatService.updateBotState(chatId, botState);
                break;
            case "/find_vacancies_by_filter":
                botState = BotState.FIND_VACANCIES_BY_FILTER;
                chatService.updateBotState(chatId, botState);
                break;
            case "/show_filter":
                botState = BotState.SHOW_FILTER;
                chatService.updateBotState(chatId, botState);
                break;
            default:
                botState = chatService.getChatById(chatId).getBotState();
                chatService.updateBotState(chatId, botState);
                break;
        }

        return botStateContext.processInputMessage(botState, message);
    }
}
