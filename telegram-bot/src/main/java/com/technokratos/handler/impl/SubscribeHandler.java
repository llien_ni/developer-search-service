package com.technokratos.handler.impl;

import com.technokratos.dto.ChatResponse;
import com.technokratos.entity.BotState;
import com.technokratos.handler.InputMessageHandler;
import com.technokratos.service.ChatService;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Slf4j
@Component
@RequiredArgsConstructor
public class SubscribeHandler implements InputMessageHandler {

    private static  final String MESSAGE = "Ты подписался на отправку новых вакансий, чтобы отписаться используй команду /unsubscribe";

    private final ChatService chatService;

    @Override
    public SendMessage handle(Message message) {

        ChatResponse chat = chatService.getChatById(message.getChatId());
        chat.setSubscribed(Boolean.TRUE);
        chatService.updateChat(chat);

        return MessageUtil.createSendMessage(message.getChatId(), MESSAGE);
    }

    @Override
    public BotState getHandlerName() {

        return BotState.SUBSCRIBE;
    }
}
