package com.technokratos.handler.impl;

import com.technokratos.client.VacancyApiClient;
import com.technokratos.config.PageConfigurationProperties;
import com.technokratos.dto.VacancyResponse;
import com.technokratos.entity.BotState;
import com.technokratos.handler.InputMessageHandler;
import com.technokratos.service.ChatService;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
@RequiredArgsConstructor
public class GetAllVacanciesHandler implements InputMessageHandler {

    private final VacancyApiClient vacancyApiClient;

    private final ChatService chatService;

    private final PageConfigurationProperties pageConfigurationProperties;

    @Override
    public SendMessage handle(Message message) {

        Page<VacancyResponse> page = vacancyApiClient.getAllActive(pageConfigurationProperties.getSize(),
                pageConfigurationProperties.getNumber());

        chatService.updateBotState(message.getChatId(), BotState.GET_ALL_VACANCIES);
        return MessageUtil.createSendMessageWithPagination(message.getChatId(), page, BotState.GET_ALL_VACANCIES.toString());
    }

    @Override
    public BotState getHandlerName() {
        return BotState.GET_ALL_VACANCIES;
    }
}
