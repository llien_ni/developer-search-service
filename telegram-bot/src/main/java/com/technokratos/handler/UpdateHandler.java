package com.technokratos.handler;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface UpdateHandler {

    EditMessageText handleCallbackQuery(CallbackQuery callbackQuery);

    SendMessage handleInput(Message message);
}
