package com.technokratos.client;


import com.technokratos.dto.VacancyFilter;
import com.technokratos.dto.VacancyResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "vacancy-service", url = "http://vacancy-service:8090/vacancy")
public interface VacancyApiClient {

    @GetMapping(  "/active")
    Page<VacancyResponse> getAllActive(@RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "number", required = false) Integer number);

    @PostMapping("/by/filter")
    @ResponseStatus(HttpStatus.OK)
    Page<VacancyResponse> getByFilter(@RequestBody VacancyFilter filter,
                                      @RequestParam(value = "size", required = false) Integer size,
                                      @RequestParam(value = "number", required = false) Integer number);

}
