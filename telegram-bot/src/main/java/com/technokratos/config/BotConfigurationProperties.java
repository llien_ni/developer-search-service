package com.technokratos.config;



import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "bot")
public class BotConfigurationProperties {

    private String name;

    private String token;
}
