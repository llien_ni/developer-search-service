package com.technokratos.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@Data
@ConfigurationProperties(prefix = "default.page")
public class PageConfigurationProperties {

    private Integer size;

    private Integer number;
}
