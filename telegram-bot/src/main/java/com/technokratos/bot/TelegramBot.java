package com.technokratos.bot;

import com.technokratos.config.BotConfigurationProperties;
import com.technokratos.handler.impl.UpdateHandlerImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Component
public class TelegramBot extends TelegramLongPollingBot {

    public static final String HELP_MESSAGE = "Бот для поиска вакансий. \n\n" +
            "Mожет показывать список всех доступных вакансий \n /get_all_vacancies \n\n" +
            "Создавать фильтр для поиска вакансий \n /filling_filter \n\n" +
            "Показать текущий фильтр \n /show_filter \n\n"+
            "Искать вакансии используя фильтр \n /find_vacancies_by_filter \n\n" +
            "Автоматически отправлять новые вакансии подходящие под фильтр \n /subscribe \n\n"+
            "Отписаться от отправки уведомлений \n /unsubscribe \n\n"+
            "Помощь /help ";

    private final BotConfigurationProperties configurationProperties;

    private final UpdateHandlerImpl updateHandler;

    public TelegramBot(BotConfigurationProperties configurationProperties, UpdateHandlerImpl updateHandler) {


        this.configurationProperties = configurationProperties;

        this.updateHandler = updateHandler;

        List<BotCommand> listOfCommands = new ArrayList<>();
        listOfCommands.add(new BotCommand("/start", "start bot"));
        listOfCommands.add(new BotCommand("/get_all_vacancies", "get all vacancies"));
        listOfCommands.add(new BotCommand("/filling_filter", "filling vacancies filter"));
        listOfCommands.add(new BotCommand("/find_vacancies_by_filter", "find vacancies by filter"));
        listOfCommands.add(new BotCommand("/show_filter", "show filter"));
        listOfCommands.add(new BotCommand("/subscribe", "subscribe"));
        listOfCommands.add(new BotCommand("/unsubscribe", "unsubscribe"));
        listOfCommands.add(new BotCommand("/help", "info about bot"));

        try {
            this.execute(new SetMyCommands(listOfCommands, new BotCommandScopeDefault(), null));

        } catch (TelegramApiException e) {

            log.info("Error when add menu");
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {

        return configurationProperties.getName();
    }

    @Override
    public String getBotToken() {
        return configurationProperties.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasMessage() && update.getMessage().hasText()) {

            SendMessage sendMessage = updateHandler.handleInput(update.getMessage());
            sendMessage(sendMessage);
        }

        if (update.hasCallbackQuery()) {

            CallbackQuery callbackQuery = update.getCallbackQuery();
            EditMessageText editMessageText = updateHandler.handleCallbackQuery(callbackQuery);
            try {
                execute(editMessageText);
            } catch (TelegramApiException e) {
                log.info("exception send message");
                e.printStackTrace();
            }
        }

    }

    public void sendMessage(SendMessage sendMessage) {

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.info("exception send message");
            e.printStackTrace();
        }
    }
}
