package com.technokratos.bot;

import com.technokratos.entity.BotState;
import com.technokratos.handler.InputMessageHandler;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class BotStateContext {

    private Map<BotState, InputMessageHandler> messageHandlers = new HashMap<>();

    public BotStateContext(List<InputMessageHandler> messageHandlers){
        messageHandlers.forEach(handler-> this.messageHandlers.put(handler.getHandlerName(), handler));
    }

    public SendMessage processInputMessage(BotState botState, Message message) {

        InputMessageHandler currentMessageHandler = findMessageHandler(botState);
        return currentMessageHandler.handle(message);
    }

    private InputMessageHandler findMessageHandler(BotState botState) {
        if (isFindVacanciesByFilterState(botState)) {
            return messageHandlers.get(BotState.FILLING_VACANCIES_FILTER);
        }
        return messageHandlers.get(botState);
    }

    private boolean isFindVacanciesByFilterState(BotState botState) {
        switch (botState) {
            case ASK_SPECIALIZATIONS:
            case ASK_CITY:
            case ASK_REMOTE:
            case FILTER_IS_FULL:
                return true;
            default:
                return false;
        }
    }
}
