package com.technokratos.scheduled;


import com.technokratos.bot.TelegramBot;
import com.technokratos.client.VacancyApiClient;
import com.technokratos.config.PageConfigurationProperties;
import com.technokratos.dto.ChatResponse;
import com.technokratos.dto.VacancyFilter;
import com.technokratos.dto.VacancyResponse;
import com.technokratos.entity.BotState;
import com.technokratos.service.ChatService;
import com.technokratos.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.time.LocalDate;
import java.util.List;

import static com.technokratos.util.MessageUtil.createListSpecializations;

@Slf4j
@Component
@RequiredArgsConstructor
public class ScheduledTasks {

    private final ChatService chatService;

    private final VacancyApiClient vacancyApiClient;

    private final TelegramBot telegramBot;

    private final PageConfigurationProperties pageConfigurationProperties;


    @Scheduled(cron = "0 0 10 * * *")
    private void sendNotification() {

        List<ChatResponse> chats = chatService.findAllBySubscribedTrue();
        chats.forEach(chat -> {

            chat.setBotState(BotState.FIND_VACANCIES_BY_FILTER);
            chatService.updateChat(chat);
            VacancyFilter filter = VacancyFilter.builder()
                    .remote(chat.getVacancyRemote())
                    .specializations(createListSpecializations(chat))
                    .city(chat.getVacancyCity())
                    .createdDate(LocalDate.now().minusDays(1))
                    .build();

            Page<VacancyResponse> page = vacancyApiClient.getByFilter(filter, pageConfigurationProperties.getSize(), pageConfigurationProperties.getNumber());

            if (!page.getContent().isEmpty()) {

                SendMessage sendMessage = MessageUtil.createSendMessageWithPagination(chat.getId(),page , BotState.SUBSCRIBE.toString());
                telegramBot.sendMessage(sendMessage);
            }
        });
    }
}
