package com.technokratos.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class VacancyFilter {

    private List<String> specializations;

    private String city;

    private Boolean remote;

    private LocalDate createdDate;
}
