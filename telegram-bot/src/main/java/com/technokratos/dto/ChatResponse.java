package com.technokratos.dto;


import com.technokratos.entity.BotState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
public class ChatResponse {

    private Long id;

    private BotState botState;

    private String vacancyCity;

    private String vacancySpecializations;

    private Boolean vacancyRemote;

    private Boolean subscribed;
}
