package com.technokratos.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyResponse {

    private UUID id;

    private String name;

    private String description;

    private String websiteLink;

    private String email;

    private String type;

    private String status;

    private OffsetDateTime createdDate;

    private OffsetDateTime updateDate;

    private String updatedBy;

    private Float rating;
}
