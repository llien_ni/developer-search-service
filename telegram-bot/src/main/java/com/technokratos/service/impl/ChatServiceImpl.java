package com.technokratos.service.impl;

import com.technokratos.dto.ChatResponse;
import com.technokratos.entity.BotState;
import com.technokratos.entity.ChatEntity;
import com.technokratos.mapper.ChatMapper;
import com.technokratos.repository.ChatRepository;
import com.technokratos.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {

    private final ChatRepository chatRepository;

    private final ChatMapper chatMapper;

    @Transactional
    @Override
    public ChatResponse updateBotState(long chatId, BotState botState) {

        ChatEntity chatEntity = createChatIfNotExist(chatId);
        chatEntity.setBotState(botState);
        ChatEntity savedEntity = chatRepository.save(chatEntity);
        return chatMapper.fromEntityToResponse(savedEntity);
    }

    private ChatEntity createChatIfNotExist(long chatId) {
        Optional<ChatEntity> chat = chatRepository.findById(chatId);
        ChatEntity chatEntity=null;
        if(chat.isEmpty()){
            chatEntity = ChatEntity.builder().id(chatId).build();

        }else{
            chatEntity = chat.get();
        }
        return chatEntity;
    }

    @Transactional(readOnly = true)
    @Override
    public ChatResponse getChatById(Long chatId) {

        ChatEntity chatEntity = createChatIfNotExist(chatId);
        return chatMapper.fromEntityToResponse(chatEntity);
    }

    @Transactional
    @Override
    public void updateChat(ChatResponse chat) {

        ChatEntity chatEntity = createChatIfNotExist(chat.getId());
        chatMapper.update(chat, chatEntity);
        chatRepository.save(chatEntity);
    }

    @Override
    public List<ChatResponse> findAllBySubscribedTrue() {

        List<ChatEntity> chats = chatRepository.findAllBySubscribedTrue();
        return chats.stream().map(chatMapper::fromEntityToResponse).collect(Collectors.toList());
    }
}
