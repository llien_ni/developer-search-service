package com.technokratos.service;

import com.technokratos.dto.ChatResponse;
import com.technokratos.entity.BotState;

import java.util.List;

public interface ChatService {

    ChatResponse updateBotState(long chatId, BotState botState);

    ChatResponse getChatById(Long chatId);

    void updateChat(ChatResponse chat);

    List<ChatResponse> findAllBySubscribedTrue();
}
