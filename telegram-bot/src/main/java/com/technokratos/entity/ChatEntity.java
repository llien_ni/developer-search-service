package com.technokratos.entity;


import jakarta.persistence.*;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
@Setter
@Builder
@Entity
@Table(name ="chat")
public class ChatEntity {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "bot_state")
    @Enumerated(EnumType.STRING)
    private BotState botState;

    private String vacancyCity;

    private String vacancySpecializations;

    private Boolean vacancyRemote;

    private Boolean subscribed;
}
