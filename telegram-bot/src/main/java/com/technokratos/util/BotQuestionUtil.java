package com.technokratos.util;

public class BotQuestionUtil {

    public static final String QUESTION_CITY = "Введи свой город";
    public static final String QUESTION_REMOTE = "Ты хочешь найти работу удаленно?";
    public static final String QUESTION_SPECIALIZATIONS = "Введи названия профессий через запятую";
}
