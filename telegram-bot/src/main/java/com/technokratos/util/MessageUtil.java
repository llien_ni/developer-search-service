package com.technokratos.util;


import com.technokratos.dto.ChatResponse;
import com.technokratos.dto.VacancyResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Slf4j
@Service
public class MessageUtil {

    public static SendMessage createSendMessage(long chatId, String replyMessage) {

        return new SendMessage(String.valueOf(chatId), replyMessage);
    }

    public static SendMessage createSendMessageWithPagination(long chatId, Page<VacancyResponse> pageData, String pageType) {

        List<InlineKeyboardButton> keyboardButtons = createInlineKeyboardButtons(pageData, pageType);
        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();

        SendMessage message = new SendMessage();
        message.setText("Найденные вакансии");
        message.setChatId(chatId);
        for (VacancyResponse vacancyResponse : pageData.getContent()) {
            message.setText(message.getText() + "\n\n" + createTextMessage(vacancyResponse));
        }

        keyboardMarkup.setKeyboard(Collections.singletonList(keyboardButtons));

        message.setReplyMarkup(keyboardMarkup);

        return message;
    }

    public static String createTextMessage(VacancyResponse vacancyResponse) {
        StringBuilder text = new StringBuilder();

        text.append(vacancyResponse.getName()).append("\n")
                .append("Компания: ").append(vacancyResponse.getCompanyResponse().getName()).append("\n")
                .append("Дата: ").append(vacancyResponse.getCreatedDate().toLocalDate()).append("\n")
                .append("З/п: ").append(vacancyResponse.getSalary()).append("\n");

        if(vacancyResponse.getCity()!=null){
            text.append("Город: ").append(vacancyResponse.getCity()).append("\n");
        }
        if(vacancyResponse.getSpecializations()!=null && !vacancyResponse.getSpecializations().isEmpty()){
            text.append("Специализации: ");
            vacancyResponse.getSpecializations().forEach(specializationResponse ->
                    text.append(specializationResponse.getSpecialization()).append(" "));
            text.append("\n");
        }

        if(vacancyResponse.getTags()!=null && !vacancyResponse.getTags().isEmpty()){
            text.append("Теги: ");
            vacancyResponse.getTags().forEach(tag -> text.append(tag.getTag()).append(" "));
            text.append("\n");

        }
        text.append("Удаленно: ");
        if (Boolean.TRUE.equals(vacancyResponse.getRemote())) {
            text.append("да");
        } else if (Boolean.FALSE.equals(vacancyResponse.getRemote())) {
            text.append("нет");
        } else {
            text.append("не указано");
        }

        text.append("\n\n");
        return text.toString();
    }


    public static List<InlineKeyboardButton> createInlineKeyboardButtons(Page<VacancyResponse> pageData, String pageType) {

        List<InlineKeyboardButton> keyboardButtons = new ArrayList<>();
        if (pageData.getNumber() > 0) {

            InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
            inlineKeyboardButton.setText("<Назад");
            inlineKeyboardButton.setCallbackData(pageType + "-" + (pageData.getNumber() - 1));

            keyboardButtons.add(inlineKeyboardButton);
        }
        if (pageData.getNumber() < pageData.getTotalPages()) {

            InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
            inlineKeyboardButton.setText("Вперед>");
            inlineKeyboardButton.setCallbackData(pageType + "-" + (pageData.getNumber() + 1));

            keyboardButtons.add(inlineKeyboardButton);
        }
        return keyboardButtons;

    }


    public static EditMessageText editMessageWithPagination(Message message, Page<VacancyResponse> pageData, String pageType) {

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();

        List<InlineKeyboardButton> keyboardButtons = createInlineKeyboardButtons(pageData, pageType);
        log.info("page number " + pageData.getNumber());

        EditMessageText editMessage = new EditMessageText();
        editMessage.setChatId(message.getChatId());
        editMessage.setMessageId(message.getMessageId());
        editMessage.setText("Вакансии");

        for (VacancyResponse vacancyResponse : pageData.getContent()) {
            editMessage.setText(editMessage.getText() + "\n\n" + createTextMessage(vacancyResponse));
        }

        editMessage.setReplyMarkup(keyboardMarkup);
        keyboardMarkup.setKeyboard(Collections.singletonList(keyboardButtons));

        return editMessage;
    }

    public static List<String> createListSpecializations(ChatResponse chat) {

        if (chat.getVacancySpecializations() != null) {

            return Arrays.stream(chat.getVacancySpecializations().split(",")).map(String::trim).distinct().toList();
        }
        return null;
    }
}
