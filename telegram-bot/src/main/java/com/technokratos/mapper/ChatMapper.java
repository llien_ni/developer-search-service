package com.technokratos.mapper;


import com.technokratos.dto.ChatResponse;
import com.technokratos.entity.ChatEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ChatMapper {

    void update(ChatResponse chat, @MappingTarget ChatEntity chatEntity);

    ChatResponse fromEntityToResponse(ChatEntity savedEntity);
}
