package com.technokratos.test.integration;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.dto.response.SubscriptionResponse;
import com.technokratos.entity.EventEntity;
import com.technokratos.entity.SubscriptionEntity;
import com.technokratos.repository.EventRepository;
import com.technokratos.repository.SubscriptionRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/subscription-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class SubscriptionApiIntegrationTest extends AbstractIntegrationTest {

    @LocalServerPort
    private int port;

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    private static final String LOCALHOST = "http://localhost:";

    private static final String SUBSCRIPTION_PREFIX = "/subscription";

    //2669cbf8-32c1-4796-bf43-2f36a8a80cc8
    private static final String FIRST_ACCOUNT_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIyNjY5Y2JmOC0zMmMxLTQ3OTYtYmY0My0yZjM2YThhODBjYzgiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTg5MzE4MjUsImp0aSI6IjE2ODMzNTU4MjU5NDYifQ.AWKBZA7906JQFq8kU51R86cuspEN3qhLtss9bYLj0OIuDRb_0cY9NkuS9SxTfWBqCofawfhwV2YYCPxxDEWDimsUUQ5w0mA-KTKb6JWELCS0msvtbwgvWPUna_cogASirZMC_uzMz093uURkN-s-UV8xr0sH8X1ciHZPN9XB1gGa-kWZRu1euao7cPiTXUYkqjxtGzsQ1fk8tP5AW-QUeCGsySV4_kOmlE0mn4eaSpZWxXcGN7gMMMhTWYa-6Y5yN_VsJ-fISJFDM3rgQVTieUKLES5aX3r8P9TcmagSkhzuQ4PlyPy3JVUEZUzkcdR4kMAcwiJZh-dlAQyUH7Uycw";

    //117f2c92-7735-48fe-a0de-7ce15ff10937
    private static final String SECOND_ACCOUNT_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIxMTdmMmM5Mi03NzM1LTQ4ZmUtYTBkZS03Y2UxNWZmMTA5MzciLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE5OTkyNDUyMzEsImp0aSI6IjE2ODM2NjkyMzE1MzUifQ.cZYTB1ifODL14KExKom0BpomSayTPqzr5XEaJgyM39_RMStlfttN3cUUFnezOyf5HQYYVrGzf7BoKbsLrKUz4ApiSx8G22nkReqMz5Gl6bM3hchGJopxnkbO6E6z0g1CEwFhuC-9MUXXuFwncxv8iQk-G5uR5Hu-Zqkc_i74uKxlibt_-59XtV2mH-W4GQrV-ZSLW5dwfwoKJJ_XSM-YoPgWhukud6M2dXzk08pRjw3IkKa5O1LHo3Rs9bySZ4Ck_pM5DRvzwB4LMluLrNd8jL7wl5se85HK8CDLg9BFasfwKc1NQKLtTuUkE1L2KVJ9EtUGD_ypvEnwN0e3fbLGuw";

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;


    @Test
    void subscribeSuccessfully() throws Exception {

        UUID id = UUID.fromString("11059303-38d0-4c36-bc84-4024141ae361");

        RequestId eventId = new RequestId(id);
        SubscriptionResponse subscriptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + SUBSCRIPTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(eventId))
                                .header("Authorization", BEARER_PREFIX + SECOND_ACCOUNT_TOKEN))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        SubscriptionEntity subscriptionEntity = subscriptionRepository.getReferenceById(subscriptionResponse.getId());
        EventEntity eventEntity = eventRepository.getReferenceById(id);
        Assertions.assertEquals(2, eventEntity.getSubscriptions().size());
        Assertions.assertNotNull(subscriptionEntity.getEvent());
        Assertions.assertNotNull(subscriptionEntity.getAccountId());

        Assertions.assertNotNull(subscriptionResponse.getId());
        Assertions.assertNotNull(subscriptionResponse.getAccountId());
        Assertions.assertNotNull(subscriptionResponse.getEvent());
        Assertions.assertNotNull(subscriptionResponse.getEvent().getId());
    }

    @Test
    void subscribeFailedSubscriptionAlreadyExist() throws Exception {


        UUID id = UUID.fromString("11059303-38d0-4c36-bc84-4024141ae361");

        RequestId eventId = new RequestId(id);
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + SUBSCRIPTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(eventId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllSubscriptionsByUserSuccessfully() throws Exception {

        ResponsePage<SubscriptionResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + SUBSCRIPTION_PREFIX)
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
    }

    @Test
    void deleteSubscriptionSuccessfully() throws Exception {

        UUID eventId = UUID.fromString("11059303-38d0-4c36-bc84-4024141ae361");
        EventEntity oldEventEntity = eventRepository.getReferenceById(eventId);
        Assertions.assertEquals(1, oldEventEntity.getSubscriptions().size());


        RequestId subscriptionId = new RequestId(UUID.fromString("ae7fcd98-b8fd-47dc-8a0d-ea0a1b342afe"));
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + SUBSCRIPTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(subscriptionId))
                                .header("Authorization", BEARER_PREFIX + FIRST_ACCOUNT_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });


        EventEntity updatedEntity = eventRepository.getReferenceById(eventId);
        Assertions.assertEquals(0, updatedEntity.getSubscriptions().size());

        Assertions.assertNotNull(messageResponse.getData());

    }

    @Test
    void deleteSubscriptionFailedSubscriptionIsNotExist() throws Exception {

        RequestId subscriptionId = new RequestId(UUID.fromString("ae7fcd98-b8fd-47dc-8a0d-ea0a1b342afe"));
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + SUBSCRIPTION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(subscriptionId))
                                .header("Authorization", BEARER_PREFIX + SECOND_ACCOUNT_TOKEN))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

}

