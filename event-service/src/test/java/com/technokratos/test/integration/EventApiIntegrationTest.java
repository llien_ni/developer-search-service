package com.technokratos.test.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.CreateEventRequest;
import com.technokratos.dto.request.EventFilter;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateEventRequest;
import com.technokratos.dto.response.EventResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.entity.EventEntity;
import com.technokratos.repository.EventRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Sql(scripts = "/sql/event-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class EventApiIntegrationTest extends AbstractIntegrationTest {

    @LocalServerPort
    private int port;

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    private static final String LOCALHOST = "http://localhost:";

    private static final String EVENT_PREFIX = "/event";

    private static final String ADMIN_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiIwMzdhZGU0YS05NzMxLTQ4NzktYmZjOC05ZjVkMjJiZmZlOWIiLCJyb2xlIjoiUk9MRV9BRE1JTiIsImV4cCI6MjAwMDc4NDY0OSwianRpIjoiMTY4NTIwODY0OTgzMSJ9.c5y1No40PyGKVFRMosYFemZ5bHGQk1F2LW2q6DYpxraW-HIEwmCoCTEASRWPESZLJs9_mkFYuy82SiXqAdE0ft-4y2sx3s4sB9AgxP_4QPiwDKpoosl0h_-rqBshGDim5hi-3mWhVDalmqP8JBzrkphMJHTqbzvkODVx8hupC7C2b6D6HBShzYsJBIU-r1ocijLXaxCQO3P7rXfDGIEFMc-G0idv_Yap8iZ-FSAFlzXX6XpJyGhLmNKAfrk2DXBfxJuskvFwbffMo9UEvPB91Y635h3hCn-mBEVvViG4buZx3mfjI90SVJDIeYGT5Nm5xadRTaxokt4RKQ0TT3ipJA";

    @Autowired
    private EventRepository eventRepository;

    @Test
    void createEventSuccessfully() throws Exception {

        CreateEventRequest request = CreateEventRequest.builder()
                .title("New event")
                .description("You will develop game")
                .finishDate(LocalDate.of(2023, 10, 7))
                .startDate(LocalDate.of(2023, 9, 6))
                .build();

        EventResponse eventResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + EVENT_PREFIX).contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        EventEntity eventEntity = eventRepository.getReferenceById(eventResponse.getId());
        Assertions.assertEquals(request.getTitle(), eventEntity.getTitle());
        Assertions.assertEquals(request.getDescription(), eventEntity.getDescription());
        Assertions.assertEquals(request.getStartDate(), eventEntity.getStartDate());
        Assertions.assertEquals(request.getFinishDate(), eventEntity.getFinishDate());
    }

    @Test
    void updateEventSuccessfully() throws Exception {

        UUID eventId = UUID.fromString("11059303-38d0-4c36-bc84-4024141ae361");

        EventEntity oldEventEntity = eventRepository.getReferenceById(eventId);

        UpdateEventRequest request = UpdateEventRequest.builder()
                .id(eventId)
                .description("new description")
                .build();

        EventResponse eventResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + EVENT_PREFIX).contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        EventEntity updatedEvent = eventRepository.getReferenceById(eventId);
        Assertions.assertEquals(request.getDescription(), updatedEvent.getDescription());
        Assertions.assertEquals(oldEventEntity.getTitle(), updatedEvent.getTitle());
        Assertions.assertNotNull(eventResponse.getId());
    }

    @Test
    void updateEventFailedEventNotFound() throws Exception {

        UUID eventId = UUID.fromString("99059303-38d0-4c36-bc84-4024141ae361");
        UpdateEventRequest request = UpdateEventRequest.builder()
                .id(eventId)
                .description("new description")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + EVENT_PREFIX).contentType("application/json").content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());

    }

    @Test
    void deleteEventSuccessfully() throws Exception {

        UUID eventId = UUID.fromString("11059303-38d0-4c36-bc84-4024141ae361");
        RequestId request = new RequestId(eventId);

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + EVENT_PREFIX).contentType("application/json").content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Optional<EventEntity> deletedEvent = eventRepository.findById(eventId);
        Assertions.assertEquals(Optional.empty(), deletedEvent);
        Assertions.assertNotNull(messageResponse.getData());
    }

    @Test
    void deleteEventFailed() throws Exception {

        UUID eventId = UUID.fromString("99059303-38d0-4c36-bc84-4024141ae361");
        RequestId request = new RequestId(eventId);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + EVENT_PREFIX).contentType("application/json").content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());

    }

    @Test
    void proposeEventSuccessfully() throws Exception {

        CreateEventRequest request = CreateEventRequest.builder()
                .title("New event")
                .description("You will develop game")
                .finishDate(LocalDate.of(2023, 10, 7))
                .startDate(LocalDate.of(2023, 9, 6))
                .build();

        EventResponse eventResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + EVENT_PREFIX + "/propose").contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        EventEntity eventEntity = eventRepository.getReferenceById(eventResponse.getId());
        Assertions.assertEquals(request.getTitle(), eventEntity.getTitle());
        Assertions.assertEquals(request.getDescription(), eventEntity.getDescription());
        Assertions.assertEquals(request.getStartDate(), eventEntity.getStartDate());
        Assertions.assertEquals(request.getFinishDate(), eventEntity.getFinishDate());
        Assertions.assertEquals(EventEntity.State.MODERATED, eventEntity.getState());
    }

    @Test
    void addProposedEventSuccessfully() throws Exception {
//12059303-38d0-4c36-bc84-4024141ae361

        UUID eventId = UUID.fromString("12059303-38d0-4c36-bc84-4024141ae361");
        RequestId request = new RequestId(eventId);

        EventResponse eventResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + EVENT_PREFIX + "/proposed").contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        EventEntity eventEntity = eventRepository.getReferenceById(eventResponse.getId());
        Assertions.assertEquals(EventEntity.State.ACTIVE, eventEntity.getState());
    }

    @Test
    void addProposedEventFailedEventNotFound() throws Exception {


        UUID eventId = UUID.fromString("99059303-38d0-4c36-bc84-4024141ae361");
        RequestId request = new RequestId(eventId);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + EVENT_PREFIX + "/proposed").contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getEventsByStateSuccessfully() throws Exception {

        ResponsePage<EventResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + EVENT_PREFIX + "/by/state")
                                .param("state", "MODERATED")
//                                .contentType("application/json")
//                                .content(objectMapper.writeValueAsString(request))
                                .header("Authorization", BEARER_PREFIX + ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(1, page.getTotalElements());
    }

    @Test
    void getAllSuccessfully() throws Exception {

        ResponsePage<EventResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + EVENT_PREFIX))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());

    }

    @Test
    void getByIdSuccessfully() throws Exception {

        UUID eventId = UUID.fromString("11059303-38d0-4c36-bc84-4024141ae361");

        EventResponse eventResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + EVENT_PREFIX + "/"+eventId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(eventResponse.getTitle());
        Assertions.assertNotNull(eventResponse.getDescription());
        Assertions.assertNotNull(eventResponse.getStartDate());
        Assertions.assertNotNull(eventResponse.getFinishDate());
    }

    @Test
    void searchByCreatedDateSuccessfully() throws Exception {

        EventFilter eventFilter = EventFilter.builder()
                .startDate(LocalDate.of(2023, 8, 11))
                .startDateType(EventFilter.Operator.BEFORE)
                .build();

        ResponsePage<EventResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + EVENT_PREFIX + "/search")
//                                .param("state", "MODERATED")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(eventFilter)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());

    }

    @Test
    void searchByStartAndFinishDateAndTitleSuccessfully() throws Exception {

        EventFilter eventFilter = EventFilter.builder()
                .startDate(LocalDate.of(2023, 8, 11))
                .startDateType(EventFilter.Operator.BEFORE)
                .finishDate(LocalDate.of(2023, 10, 10))
                .finishDateType(EventFilter.Operator.AFTER)
                .title("Pokemon")
                .build();

        ResponsePage<EventResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + EVENT_PREFIX + "/search")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(eventFilter)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(1, page.getTotalElements());

    }

    @Test
    void searchByFinishDateBefore() throws Exception{

        EventFilter eventFilter = EventFilter.builder()
                .finishDate(LocalDate.of(2023, 11, 10))
                .finishDateType(EventFilter.Operator.BEFORE)
                .build();

        ResponsePage<EventResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + EVENT_PREFIX + "/search")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(eventFilter)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());

    }

    @Test
    void searchByStartDateAfter() throws Exception{

        EventFilter eventFilter = EventFilter.builder()
                .startDate(LocalDate.of(2023, 6, 10))
                .startDateType(EventFilter.Operator.AFTER)
                .build();

        ResponsePage<EventResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + EVENT_PREFIX + "/search")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(eventFilter)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
    }

    @Test
    void searchByStartDateMatch() throws Exception{

        EventFilter eventFilter = EventFilter.builder()
                .startDate(LocalDate.of(2023, 7, 10))
                .startDateType(EventFilter.Operator.MATCH)
                .build();

        ResponsePage<EventResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + EVENT_PREFIX + "/search")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(eventFilter)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(1, page.getTotalElements());
    }

    @Test
    void searchByFinishDateMatch() throws Exception{

        EventFilter eventFilter = EventFilter.builder()
                .finishDate(LocalDate.of(2023, 10, 12))
                .finishDateType(EventFilter.Operator.MATCH)
                .build();

        ResponsePage<EventResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + EVENT_PREFIX + "/search")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(eventFilter)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(1, page.getTotalElements());
    }

}
