package com.technokratos.client;

import com.technokratos.dto.model.MailMessageModel;
import com.technokratos.dto.response.SubscriptionResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class NotificationServiceRabbit {

    private final RabbitTemplate rabbitTemplate;

    public void sentMailMessageAccountSubscribedToEvent(SubscriptionResponse subscriptionResponse) {


        MailMessageModel mailMessageModel = MailMessageModel.builder()
                .to(subscriptionResponse.getAccountId().toString())
                .subject("You subscribed to event")
                .message(subscriptionResponse.getEvent().getTitle() + " " + subscriptionResponse.getEvent().getDescription() + " " + subscriptionResponse.getEvent().getStartDate() + " " + subscriptionResponse.getEvent().getFinishDate())
                .build();
        sendMailMessage(mailMessageModel);
    }

    public void sendMailMessage(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend("direct-exchange", "event-service", mailMessageModel);
    }

    public void sentMailMessageEventUpdate(SubscriptionResponse subscriptionResponse) {

        MailMessageModel mailMessageModel = MailMessageModel.builder()
                .to(subscriptionResponse.getAccountId().toString())
                .subject("Event updated")
                .message(subscriptionResponse.getEvent().getTitle() + " " + subscriptionResponse.getEvent().getDescription() + " " + subscriptionResponse.getEvent().getStartDate() + " " + subscriptionResponse.getEvent().getFinishDate())
                .build();
        sendMailMessage(mailMessageModel);
    }

    public void sentMailMessageEventDeleted(SubscriptionResponse subscriptionResponse) {

        MailMessageModel mailMessageModel = MailMessageModel.builder()
                .to(subscriptionResponse.getAccountId().toString())
                .subject("Event deleted")
                .message(subscriptionResponse.getEvent().getTitle() + " " + subscriptionResponse.getEvent().getDescription() + " " + subscriptionResponse.getEvent().getStartDate() + " " + subscriptionResponse.getEvent().getFinishDate())
                .build();
        sendMailMessage(mailMessageModel);
    }

    public void sendMailMessageThatEventFinishSoon(List<SubscriptionResponse> subscriptions) {

        subscriptions.forEach(subscription -> {

            MailMessageModel mailMessageModel = MailMessageModel.builder()
                    .to(subscription.getAccountId().toString())
                    .subject("The event will end soon")
                    .message(subscription.getEvent().getTitle() + " " + subscription.getEvent().getDescription() + " " + subscription.getEvent().getStartDate() + " " + subscription.getEvent().getFinishDate())
                    .build();
            sendMailMessage(mailMessageModel);
        });
    }
}
