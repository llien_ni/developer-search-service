package com.technokratos.repository;

import com.technokratos.entity.EventEntity;
import com.technokratos.entity.SubscriptionEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface SubscriptionRepository extends JpaRepository<SubscriptionEntity, UUID> {

    Optional<SubscriptionEntity> findByEventAndAccountId(EventEntity event, UUID accountId);

    Optional<SubscriptionEntity> findByIdAndAccountId(UUID id, UUID accountId);

    Page<SubscriptionEntity> findAllByAccountId(UUID accountId, Pageable pageable);
}
