package com.technokratos.scheduling;

import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.dto.response.SubscriptionResponse;
import com.technokratos.entity.EventEntity;
import com.technokratos.mapper.SubscriptionMapper;
import com.technokratos.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class ScheduledTasks {

    private final EventRepository eventRepository;

    private final NotificationServiceRabbit notificationServiceRabbit;

    private final SubscriptionMapper subscriptionMapper;

    //уведомляет о том, что через 3 дня закончится эвент и удаляет эвенты, которые уже закончились
    @Scheduled(fixedRate = 86400000)
    public void unblockAccount() {

        LocalDate now = LocalDate.now();
        List<EventEntity> events = eventRepository.findAllByState(EventEntity.State.ACTIVE);
        events.forEach(event -> {

            LocalDate finishDate = event.getFinishDate();

            if(finishDate!=null && finishDate.minusDays(3).equals(now)){

                List<SubscriptionResponse> subscriptions = event.getSubscriptions().stream().map(subscriptionMapper::fromEntityToResponse).collect(Collectors.toList());

                notificationServiceRabbit.sendMailMessageThatEventFinishSoon(subscriptions);
            }

            if(finishDate!=null && finishDate.isBefore(now)){
                eventRepository.delete(event);
            }
        });
    }
}
