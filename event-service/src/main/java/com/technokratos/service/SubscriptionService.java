package com.technokratos.service;

import com.technokratos.dto.response.SubscriptionResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface SubscriptionService {

    SubscriptionResponse createSubscription(UUID id, String userId);

    Page<SubscriptionResponse> getAllByUser(String userId, Integer size, Integer number);

    UUID delete(UUID id, String userId);
}
