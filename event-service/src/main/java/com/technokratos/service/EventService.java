package com.technokratos.service;

import com.technokratos.dto.request.CreateEventRequest;
import com.technokratos.dto.request.EventFilter;
import com.technokratos.dto.request.UpdateEventRequest;
import com.technokratos.dto.response.EventResponse;
import com.technokratos.entity.EventEntity;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface EventService {

    EventResponse create(CreateEventRequest request, EventEntity.State state);

    EventResponse update(UpdateEventRequest request);

    UUID delete(UUID id);

    Page<EventResponse> getByState(String state, Integer size, Integer number);

    Page<EventResponse> getAllActiveEvents(Integer size, Integer number);

    EventResponse getById(UUID id);

    Page<EventResponse> searchByFilter(EventFilter filter, Integer size, Integer number);

    EventResponse updateEventState(UUID id, EventEntity.State active);
}
