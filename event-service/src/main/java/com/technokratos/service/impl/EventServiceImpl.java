package com.technokratos.service.impl;

import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.dto.request.CreateEventRequest;
import com.technokratos.dto.request.EventFilter;
import com.technokratos.dto.request.UpdateEventRequest;
import com.technokratos.dto.response.EventResponse;
import com.technokratos.dto.response.SubscriptionResponse;
import com.technokratos.entity.EventEntity;
import com.technokratos.exception.EventNotFoundException;
import com.technokratos.mapper.EventMapper;
import com.technokratos.mapper.SubscriptionMapper;
import com.technokratos.repository.EventRepository;
import com.technokratos.service.EventService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;

    private final EventMapper eventMapper;

    private final RequestParamUtil requestParamUtil;

    private final NotificationServiceRabbit notificationServiceRabbit;

    private final SubscriptionMapper subscriptionMapper;

    private static final String CREATED_DATE = "createdDate";

    private static final String START_DATE = "startDate";

    private static final String FINISH_DATE = "finishDate";

    @Transactional
    @Override
    public EventResponse create(CreateEventRequest request, EventEntity.State state) {

        EventEntity event = eventMapper.fromRequestToEntity(request);
        event.setSubscriptions(new ArrayList<>());
        event.setState(state);
        EventEntity savedEvent = eventRepository.save(event);

        return eventMapper.fromEntityToResponse(savedEvent);
    }

    @Transactional
    @Override
    public EventResponse update(UpdateEventRequest request) {

        EventEntity event = eventRepository.findById(request.getId()).orElseThrow(EventNotFoundException::new);
        eventMapper.update(request, event);
        EventEntity savedEvent = eventRepository.save(event);
        List<SubscriptionResponse> subscriptions = savedEvent.getSubscriptions()
                .stream().map(subscriptionMapper::fromEntityToResponse)
                .collect(Collectors.toList());
        notifySubscribersThatEventUpdated(subscriptions);

        return eventMapper.fromEntityToResponse(savedEvent);
    }

    private void notifySubscribersThatEventUpdated(List<SubscriptionResponse> subscriptions) {

        subscriptions.forEach(notificationServiceRabbit::sentMailMessageEventUpdate);
    }

    @Transactional
    @Override
    public UUID delete(UUID id) {

        EventEntity event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
        eventRepository.delete(event);
        List<SubscriptionResponse> subscriptions = event.getSubscriptions()
                .stream().map(subscriptionMapper::fromEntityToResponse)
                .collect(Collectors.toList());
        notifySubscribersThatEventDeleted(subscriptions);

        return event.getId();
    }

    private void notifySubscribersThatEventDeleted(List<SubscriptionResponse> subscriptions) {

        subscriptions.forEach(notificationServiceRabbit::sentMailMessageEventDeleted);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EventResponse> getByState(String state, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<EventEntity> page = eventRepository.findAllByState(EventEntity.State.valueOf(state), pageRequest.withSort(sort));

        return page.map(eventMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EventResponse> getAllActiveEvents(Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<EventEntity> page = eventRepository.findAllByState(EventEntity.State.ACTIVE, pageRequest.withSort(sort));

        return page.map(eventMapper::fromEntityToResponse);
    }

    @Transactional
    @Override
    public EventResponse updateEventState(UUID id, EventEntity.State state) {

        EventEntity event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
        event.setState(state);
        EventEntity savedEntity = eventRepository.save(event);

        return eventMapper.fromEntityToResponse(savedEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public EventResponse getById(UUID id) {

        EventEntity event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
        return eventMapper.fromEntityToResponse(event);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EventResponse> searchByFilter(EventFilter filter, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);


        Specification<EventEntity> specification = Specification.where(byTitle(filter.getTitle()))
                .and(byStartDate(filter.getStartDate(),filter.getStartDateType()))
                .and(byFinishDate(filter.getFinishDate(), filter.getFinishDateType()))
                .and(byStateActive());

        Page<EventEntity> page = eventRepository.findAll(specification, pageRequest.withSort(sort));

        return page.map(eventMapper::fromEntityToResponse);
    }

    private Specification<EventEntity> byStateActive() {

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("state"), EventEntity.State.ACTIVE);
    }

    private Specification<EventEntity> byStartDate(LocalDate startDate, EventFilter.Operator startDateType) {

        return (root, query, criteriaBuilder) -> {
            if (nonNull(startDate)&& nonNull(startDateType)) {
                if(startDateType.equals(EventFilter.Operator.MATCH)){
                   return criteriaBuilder.equal(root.get(START_DATE), startDate);
                }
                if(startDateType.equals(EventFilter.Operator.BEFORE)){
                    return criteriaBuilder.lessThan(root.get(START_DATE), startDate);
                }
                if(startDateType.equals(EventFilter.Operator.AFTER)){
                    return criteriaBuilder.greaterThan(root.get(START_DATE), startDate);
                }
            }
            return null;
        };
    }

    private Specification<EventEntity> byFinishDate(LocalDate finishDate, EventFilter.Operator startDateType) {

        return (root, query, criteriaBuilder) -> {
            if (nonNull(finishDate)&& nonNull(startDateType)) {
                if(startDateType.equals(EventFilter.Operator.MATCH)){
                    return criteriaBuilder.equal(root.get(FINISH_DATE), finishDate);
                }
                if(startDateType.equals(EventFilter.Operator.BEFORE)){
                    return criteriaBuilder.lessThan(root.get(FINISH_DATE), finishDate);
                }
                if(startDateType.equals(EventFilter.Operator.AFTER)){
                    return criteriaBuilder.greaterThan(root.get(FINISH_DATE), finishDate);
                }
            }
            return null;
        };
    }

    private Specification<EventEntity> byTitle(String title) {

        return (root, query, criteriaBuilder) -> {
            if (nonNull(title)) {
                return criteriaBuilder.like(root.get("title"), '%'+title+'%');
            }
            return null;
        };
    }
}
