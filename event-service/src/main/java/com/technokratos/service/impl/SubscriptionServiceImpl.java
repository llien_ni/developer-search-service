package com.technokratos.service.impl;

import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.dto.response.SubscriptionResponse;
import com.technokratos.entity.EventEntity;
import com.technokratos.entity.SubscriptionEntity;
import com.technokratos.exception.EventNotFoundException;
import com.technokratos.exception.SubscriptionAlreadyExist;
import com.technokratos.exception.SubscriptionIsNotExistException;
import com.technokratos.mapper.SubscriptionMapper;
import com.technokratos.repository.EventRepository;
import com.technokratos.repository.SubscriptionRepository;
import com.technokratos.service.SubscriptionService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;

    private final EventRepository eventRepository;

    private final SubscriptionMapper subscriptionMapper;

    private final RequestParamUtil requestParamUtil;

    private final NotificationServiceRabbit notificationServiceRabbit;

    @Transactional
    @Override
    public SubscriptionResponse createSubscription(UUID id, String userId) {

        EventEntity event = eventRepository.findById(id)
                .orElseThrow(EventNotFoundException::new);
        checkSubscriptionIsNotExist(event, UUID.fromString(userId));
        SubscriptionEntity subscriptionEntity = SubscriptionEntity.builder()
                .accountId(UUID.fromString(userId))
                .event(event)
                .build();

        SubscriptionEntity savedEntity = subscriptionRepository.save(subscriptionEntity);
        event.getSubscriptions().add(savedEntity);
        eventRepository.save(event);
        SubscriptionResponse subscriptionResponse = subscriptionMapper.fromEntityToResponse(savedEntity);
        notificationServiceRabbit.sentMailMessageAccountSubscribedToEvent(subscriptionResponse);

        return subscriptionResponse;
    }

    private void checkSubscriptionIsNotExist(EventEntity event, UUID userId) {

        Optional<SubscriptionEntity> subscription = subscriptionRepository.findByEventAndAccountId(event, userId);
        if (subscription.isPresent()) {
            throw new SubscriptionAlreadyExist(subscription.get().getId().toString());
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<SubscriptionResponse> getAllByUser(String userId, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<SubscriptionEntity> page = subscriptionRepository.findAllByAccountId(UUID.fromString(userId), pageRequest.withSort(sort));

        return page.map(subscriptionMapper::fromEntityToResponse);
    }

    @Transactional
    @Override
    public UUID delete(UUID id, String userId) {

        SubscriptionEntity subscription = subscriptionRepository.findByIdAndAccountId(id, UUID.fromString(userId))
                .orElseThrow(SubscriptionIsNotExistException::new);
        EventEntity event = subscription.getEvent();
        event.getSubscriptions().remove(subscription);
        eventRepository.save(event);
        subscriptionRepository.delete(subscription);

        return subscription.getId();
    }
}
