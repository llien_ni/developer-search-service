package com.technokratos.controller;

import com.technokratos.api.EventApi;
import com.technokratos.dto.request.CreateEventRequest;
import com.technokratos.dto.request.EventFilter;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateEventRequest;
import com.technokratos.dto.response.EventResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.entity.EventEntity;
import com.technokratos.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class EventController implements EventApi {

    private final EventService eventService;

    @Override
    public EventResponse create(CreateEventRequest request) {

        return eventService.create(request, EventEntity.State.ACTIVE);
    }

    @Override
    public EventResponse update(UpdateEventRequest request) {

        return eventService.update(request);
    }

    @Override
    public MessageResponse delete(RequestId request) {

        UUID id = eventService.delete(request.getId());
        return MessageResponse.builder().data(String.format("Event %s successfully deleted", id)).build();
    }

    @Override
    public EventResponse proposeEvent(CreateEventRequest request) {

        return eventService.create(request, EventEntity.State.MODERATED);
    }

    @Override
    public EventResponse addProposedEvent(RequestId request) {

        return eventService.updateEventState(request.getId(), EventEntity.State.ACTIVE);
    }

    @Override
    public Page<EventResponse> getByState(String state, Integer size, Integer number) {

        return eventService.getByState(state, size, number);
    }

    @Override
    public Page<EventResponse> getAllActiveEvents(Integer size, Integer number) {

        return eventService.getAllActiveEvents(size, number);
    }

    @Override
    public EventResponse getById(UUID id) {

        return eventService.getById(id);
    }

    @Override
    public Page<EventResponse> search(EventFilter filter, Integer size, Integer number) {

        return eventService.searchByFilter(filter, size, number);
    }
}
