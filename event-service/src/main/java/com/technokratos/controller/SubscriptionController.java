package com.technokratos.controller;

import com.technokratos.api.SubscriptionApi;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.SubscriptionResponse;
import com.technokratos.service.SubscriptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class SubscriptionController implements SubscriptionApi {

    private final SubscriptionService subscriptionService;

    @Override
    public SubscriptionResponse subscribe(RequestId requestId, String userId) {

        return subscriptionService.createSubscription(requestId.getId(), userId);
    }

    @Override
    public Page<SubscriptionResponse> getAllSubscriptionsByUser(String userId, Integer size, Integer number) {

        return subscriptionService.getAllByUser(userId, size, number);
    }

    @Override
    public MessageResponse delete(RequestId requestId, String userId) {

        UUID id = subscriptionService.delete(requestId.getId(), userId);
        return MessageResponse.builder()
                .data(String.format("Subscription %s successfully deleted", id))
                .build();
    }
}
