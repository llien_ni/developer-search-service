package com.technokratos.mapper;

import com.technokratos.dto.request.CreateEventRequest;
import com.technokratos.dto.request.UpdateEventRequest;
import com.technokratos.dto.response.EventResponse;
import com.technokratos.entity.EventEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface EventMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    EventEntity fromRequestToEntity(CreateEventRequest request);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(UpdateEventRequest request, @MappingTarget EventEntity eventEntity);

    EventResponse fromEntityToResponse(EventEntity eventEntity);
}
