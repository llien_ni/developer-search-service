package com.technokratos.mapper;


import com.technokratos.dto.response.SubscriptionResponse;
import com.technokratos.entity.SubscriptionEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = EventMapper.class)
public interface SubscriptionMapper {

    SubscriptionResponse fromEntityToResponse(SubscriptionEntity savedEntity);

}
