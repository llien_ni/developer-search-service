package com.technokratos.security.util;


import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthorizationHeaderUtil {

    private static final String AUTH_HEADER_NAME = "Authorization";

    private static final String BEARER_NAME = "Bearer ";

    public static boolean hasAuthorizationToken(HttpServletRequest request) {
        String header = request.getHeader(AUTH_HEADER_NAME);

        return header != null && header.startsWith(BEARER_NAME);
    }

    public static String getToken(HttpServletRequest request) {
        String authHeader = request.getHeader(AUTH_HEADER_NAME);

        return authHeader.substring(BEARER_NAME.length());
    }
}
