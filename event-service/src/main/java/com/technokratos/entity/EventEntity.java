package com.technokratos.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;


@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "event")
public class EventEntity extends AbstractBaseEntity{


    public enum State{
        ACTIVE, MODERATED, NOT_ACTIVE
    }

    private String title;

    private String description;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name="finish_date")
    private LocalDate finishDate;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @OneToMany(mappedBy = "event", cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<SubscriptionEntity> subscriptions;
}
