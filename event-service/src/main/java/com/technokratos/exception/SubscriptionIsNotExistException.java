package com.technokratos.exception;

public class SubscriptionIsNotExistException extends BadRequestException{

    private static final String MESSAGE = "Subscription is not exist";

    public SubscriptionIsNotExistException() {
        super(MESSAGE);
    }
}
