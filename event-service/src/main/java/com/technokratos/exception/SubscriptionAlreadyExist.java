package com.technokratos.exception;


public class SubscriptionAlreadyExist extends BadRequestException {

    private static final String MESSAGE = "Subscription already exist. Subscription id %s";
    public SubscriptionAlreadyExist(String message) {

        super(String.format(MESSAGE, message));
    }
}
