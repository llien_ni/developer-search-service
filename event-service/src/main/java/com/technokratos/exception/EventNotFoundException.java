package com.technokratos.exception;

public class EventNotFoundException extends NotFoundException{

    private static final String MESSAGE = "Event not found";

    public EventNotFoundException() {
        super(MESSAGE);
    }
}
