package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EventFilter {

    public enum Operator{
        BEFORE, MATCH, AFTER
    }

    private String title;

    private LocalDate startDate;

    private Operator startDateType;//BEFORE, AFTER, MATCH

    private LocalDate finishDate;

    private Operator finishDateType;//BEFORE, AFTER, MATCH
}
