package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CreateEventRequest {

    @NotBlank
    private String title;

    private String description;

    @DateTimeFormat
    private LocalDate startDate;

    @DateTimeFormat
    private LocalDate finishDate;
}
