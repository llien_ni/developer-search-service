package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UpdateEventRequest {

    @NotNull
    private UUID id;

    private String title;

    private String description;

    @DateTimeFormat
    private LocalDate startDate;

    @DateTimeFormat
    private LocalDate finishDate;

    private String state;
}
