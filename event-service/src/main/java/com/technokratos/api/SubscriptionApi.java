package com.technokratos.api;

import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.SubscriptionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/subscription")
@PreAuthorize("isAuthenticated()")
public interface SubscriptionApi {

    @Operation(summary = "the user subscribes to the event")
    @ApiResponse(responseCode = "201", description = "user successfully subscribed",
            content = @Content(schema = @Schema(implementation = SubscriptionResponse.class)))
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    SubscriptionResponse subscribe(@RequestBody @Valid RequestId eventId, @AuthenticationPrincipal String userId);

    @Operation(summary = "get all subscriptions by user")
    @ApiResponse(responseCode = "202", description = "subscriptions successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<SubscriptionResponse> getAllSubscriptionsByUser(@AuthenticationPrincipal String userId,
                                                         @RequestParam(value = "size", required = false) Integer size,
                                                         @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "delete subscription by id")
    @ApiResponse(responseCode = "202", description = "subscription successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "subscription is not exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestId requestId, @AuthenticationPrincipal String userId);

}
