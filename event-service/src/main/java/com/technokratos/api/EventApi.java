package com.technokratos.api;


import com.technokratos.dto.request.CreateEventRequest;
import com.technokratos.dto.request.EventFilter;
import com.technokratos.dto.request.RequestId;
import com.technokratos.dto.request.UpdateEventRequest;
import com.technokratos.dto.response.EventResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/event")
public interface EventApi {

    @Operation(summary = "admin create event")
    @ApiResponse(responseCode = "201", description = "event successfully created",
            content = @Content(schema = @Schema(implementation = EventResponse.class)))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    EventResponse create(@RequestBody @Valid CreateEventRequest request);

    @Operation(summary = "admin update event")
    @ApiResponse(responseCode = "202", description = "event successfully updated",
            content = @Content(schema = @Schema(implementation = EventResponse.class)))
    @ApiResponse(responseCode = "404", description = "event not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    EventResponse update(@RequestBody @Valid UpdateEventRequest request);

    @Operation(summary = "admin delete event")
    @ApiResponse(responseCode = "202", description = "event successfully updated",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "event not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestId request);

    @Operation(summary = "user propose event")
    @ApiResponse(responseCode = "201", description = "event successfully created, event state is MODERATED",
            content = @Content(schema = @Schema(implementation = EventResponse.class)))
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/propose")
    @ResponseStatus(HttpStatus.CREATED)
    EventResponse proposeEvent(@RequestBody @Valid CreateEventRequest request);

    @Operation(summary = "admin add event that was proposed by the user")
    @ApiResponse(responseCode = "202", description = "event successfully added",
            content = @Content(schema = @Schema(implementation = EventResponse.class)))
    @ApiResponse(responseCode = "404", description = "event not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PatchMapping("/proposed")
    @ResponseStatus(HttpStatus.ACCEPTED)
    EventResponse addProposedEvent(@RequestBody @Valid RequestId request);

    @Operation(summary = "admin get events by state and sort events by created date")
    @ApiResponse(responseCode = "200", description = "events successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/by/state")
    @ResponseStatus(HttpStatus.OK)
    Page<EventResponse> getByState(@RequestParam(value = "state")String state,
                               @RequestParam(value = "size", required = false) Integer size,
                               @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get all events with state ACTIVE")
    @ApiResponse(responseCode = "200", description = "events successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<EventResponse> getAllActiveEvents(@RequestParam(value = "size", required = false) Integer size,
                                           @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get event by id")
    @ApiResponse(responseCode = "200", description = "event successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    EventResponse getById(@PathVariable("id")UUID id);

    @Operation(summary = "search events by filter")
    @ApiResponse(responseCode = "200", description = "events successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    Page<EventResponse> search(@RequestBody EventFilter filter,
                               @RequestParam(value = "size", required = false) Integer size,
                               @RequestParam(value = "number", required = false) Integer number);
}
