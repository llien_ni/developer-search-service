package com.technokratos.service;

import com.technokratos.dto.request.DocumentRequest;
import com.technokratos.dto.response.DocumentResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public interface DocumentService {

    DocumentResponse uploadFile(MultipartFile file, DocumentRequest request, UUID developerId);

    void downloadDocument(UUID documentId, HttpServletResponse response);

    UUID delete(UUID id, UUID developerId);

    DocumentResponse getDocumentById(UUID documentId);

    Page<DocumentResponse> getDocumentsByDeveloper(UUID developerId, Integer size, Integer number);
}
