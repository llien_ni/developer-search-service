package com.technokratos.service;

import com.technokratos.dto.request.*;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface AccountService {

    void checkAccountWithSuchEmailIsNotExist(String email);

    AccountResponse confirmAccount(String token);

    AccountEntity save(AccountEntity accountEntity);

    UUID delete(UUID id);

    AccountResponse getById(UUID id);

    AccountResponse convertToAccountResponse(AccountEntity account);

    AccountResponse setPhotoInResponse(AccountEntity accountEntity, AccountResponse accountResponse);

    String resetPassword(PhoneOrEmailRequest request);

    AccountResponse resetPasswordConfirm(ResetPasswordRequest request);

    void changePassword(ChangePasswordRequest request, UUID account);

    String restoreAccount(PhoneOrEmailRequest request);

    AccountResponse update(UUID account, UpdateAccountRequest request);

    String changeEmail(ChangeEmailRequest request, UUID account);

    AccountResponse changeEmailConfirm(String token);

    AccountResponse updateAccountStatus(UUID id, Status confirmed);

    Page<AccountResponse> getAll(String status, Integer size, Integer number);

    AccountResponse save(CreateAccountRequest request);
}
