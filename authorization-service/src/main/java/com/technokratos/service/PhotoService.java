package com.technokratos.service;

import com.technokratos.dto.request.PhotoRequest;
import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.model.entity.PhotoEntity;

import java.util.UUID;

public interface PhotoService {

    PhotoResponse uploadPhoto(PhotoRequest request, UUID account);

    UUID delete(UUID account);

    PhotoResponse getPhotoResponse(PhotoEntity photo);
}
