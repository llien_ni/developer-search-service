package com.technokratos.service.impl;

import com.technokratos.client.NotificationServiceGrpcClient;
import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.client.VacancyServiceGrpcClient;
import com.technokratos.converter.CompanyResponseConverter;
import com.technokratos.converter.HrConverter;
import com.technokratos.dto.model.MailMessageModel;
import com.technokratos.dto.request.CreateHrRequest;
import com.technokratos.dto.request.UpdateHrRequest;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.exception.HrNotFoundException;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.mapper.HrMapper;
import com.technokratos.model.constant.Role;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.BlockInfoEntity;
import com.technokratos.model.entity.HrEntity;
import com.technokratos.repository.BlockInfoRepository;
import com.technokratos.repository.HrRepository;
import com.technokratos.service.AccountService;
import com.technokratos.service.ConfirmationTokenService;
import com.technokratos.service.HrService;
import com.technokratos.util.NotificationUtil;
import com.technokratos.util.RequestParamUtil;
import company.service.CompanyServiceOuterClass;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class HrServiceImpl implements HrService {

    private final HrConverter hrConverter;

    private final HrRepository hrRepository;

    private final HrMapper hrMapper;

    private final PasswordEncoder passwordEncoder;

    private final AccountService accountService;

    private final RequestParamUtil requestParamUtil;

    private final VacancyServiceGrpcClient vacancyServiceGrpcClient;

    private final ConfirmationTokenService confirmationTokenService;

    private final NotificationServiceGrpcClient notificationServiceClient;

    private final BlockInfoRepository blockInfoRepository;

    private final NotificationServiceRabbit notificationServiceRabbit;

    @Transactional
    @Override
    public HrResponse save(CreateHrRequest request) {

        CompanyServiceOuterClass.GetCompanyByNameRequest getCompanyByNameRequest = CompanyServiceOuterClass.GetCompanyByNameRequest
                .newBuilder()
                .setCompanyName(request.getCompanyName())
                .build();
        CompanyServiceOuterClass.CompanyResponse companyResponse = vacancyServiceGrpcClient.getCompanyByName(getCompanyByNameRequest);

        HrEntity hrEntity = hrMapper.fromRequestToEntity(request);
        hrEntity.setRole(Role.ROLE_HR);
        hrEntity.setCompanyId(UUID.fromString(companyResponse.getId()));
        AccountEntity account = accountService.save(hrEntity);
        hrEntity.setId(account.getId());
        HrEntity savedEntity = hrRepository.save(hrEntity);

        HrResponse hrResponse = hrConverter.convertToHrResponse(savedEntity);
        hrResponse.setCompanyResponse(CompanyResponseConverter.fromGrpcResponse(companyResponse));
        return hrResponse;
    }

    @Transactional
    @Override
    public HrResponse update(UUID id, UpdateHrRequest request) {

        HrEntity hrEntity = hrRepository.findById(id).orElseThrow(UserNotFoundException::new);
        hrMapper.updateHrEntity(request, hrEntity);
        HrEntity savedEntity = hrRepository.save(hrEntity);

        HrResponse hrResponse = hrConverter.convertToHrResponse(savedEntity);

        return hrConverter.addCompanyResponseToHrResponse(hrResponse, hrEntity.getCompanyId());
    }

    @Transactional(readOnly = true)
    @Override
    public HrResponse getByIdAndStatus(UUID id, Status status) {

        HrEntity hrEntity = hrRepository.findByIdAndStatus(id, status)
                .orElseThrow(HrNotFoundException::new);

        HrResponse hrResponse = hrConverter.convertToHrResponse(hrEntity);
        return hrConverter.addCompanyResponseToHrResponse(hrResponse, hrEntity.getCompanyId());
    }

    @Transactional(readOnly = true)
    @Override
    public Page<HrResponse> getAllByStatus(Status status, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<HrEntity> entities = hrRepository.findAllByStatus(status, pageRequest);

        return entities.map(entity->{
            HrResponse hrResponse = hrConverter.convertToHrResponse(entity);
            return hrConverter.addCompanyResponseToHrResponse(hrResponse, entity.getCompanyId());
        });
    }

    @Transactional
    @Override
    public HrResponse confirmHr(UUID id) {

        HrEntity hr = hrRepository.findById(id).orElseThrow(HrNotFoundException::new);
        hr.setStatus(Status.CONFIRMED);
        HrEntity updatedEntity = hrRepository.save(hr);

        Optional<BlockInfoEntity> blockInfo = blockInfoRepository.findByAccountAndActiveTrue(hr);
        if(blockInfo.isPresent()){
            blockInfo.get().setActive(false);
            blockInfoRepository.save(blockInfo.get());
        }

        HrResponse hrResponse = hrConverter.convertToHrResponse(updatedEntity);
        MailMessageModel mailMessageThatHrConfirmed = NotificationUtil.createMailMessageThatHrConfirmed(updatedEntity);
        notificationServiceRabbit.sendMailMessage(mailMessageThatHrConfirmed);

        return hrConverter.addCompanyResponseToHrResponse(hrResponse, updatedEntity.getCompanyId());
    }

    @Transactional(readOnly = true)
    @Override
    public Page<HrResponse> getAllByStatusAndCompanyName(String companyName, Status status, Integer size, Integer number) {

        CompanyServiceOuterClass.GetCompanyByNameRequest getCompanyByNameRequest = CompanyServiceOuterClass.GetCompanyByNameRequest
                .newBuilder()
                .setCompanyName(companyName)
                .build();
        CompanyServiceOuterClass.CompanyResponse companyResponseGrpc = vacancyServiceGrpcClient.getCompanyByName(getCompanyByNameRequest);
        CompanyResponse companyResponse = CompanyResponseConverter.fromGrpcResponse(companyResponseGrpc);


        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<HrEntity> entities = hrRepository.findAllByStatusAndCompanyId(status, companyResponse.getId(), pageRequest);

        return entities.map(entity->{
            HrResponse hrResponse = hrConverter.convertToHrResponse(entity);
            hrResponse.setCompanyResponse(companyResponse);
            return hrResponse;
        });
    }
}
