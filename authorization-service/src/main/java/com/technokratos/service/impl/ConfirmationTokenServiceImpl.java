package com.technokratos.service.impl;

import com.technokratos.exception.TokenNotFoundException;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.ConfirmationTokenEntity;
import com.technokratos.repository.ConfirmationTokenRepository;
import com.technokratos.service.ConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.security.SecureRandom;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    private final ConfirmationTokenRepository confirmationTokenRepository;

    private final Random random = new SecureRandom();

    private static final int MIN_VALUE = 100000;

    private static final int MAX_VALUE = 999999;

    @Transactional
    @Override
    public ConfirmationTokenEntity createTokenForAccount(AccountEntity account) {

        ConfirmationTokenEntity token = ConfirmationTokenEntity
                .builder()
                .token(UUID.randomUUID().toString())
                .account(account)
                .expiredDate(OffsetDateTime.now().plusMinutes(10))
                .build();

        account.getConfirmationTokens().add(token);
        return token;
    }

    @Transactional(readOnly = true)
    @Override
    public ConfirmationTokenEntity getByToken(String token) {

        return confirmationTokenRepository
                .findByToken(token)
                .orElseThrow(TokenNotFoundException::new);
    }

    @Transactional
    @Override
    public void delete(ConfirmationTokenEntity confirmationToken) {

        confirmationTokenRepository.delete(confirmationToken);
    }

    @Transactional
    @Override
    public void deleteExpiredTokens(){

        List<ConfirmationTokenEntity> tokens = confirmationTokenRepository
                .findConfirmationTokenEntitiesByExpiredDateLessThan(OffsetDateTime.now());
        confirmationTokenRepository.deleteAll(tokens);
    }

    @Override
    public ConfirmationTokenEntity createCodeConfirmationForAccount(AccountEntity account) {

        ConfirmationTokenEntity token = ConfirmationTokenEntity
                .builder()
                .token(String.valueOf(generateRandomValue()))
                .account(account)
                .expiredDate(OffsetDateTime.now().plusMinutes(10))
                .build();

        account.getConfirmationTokens().add(token);
        return token;
    }


    private Integer generateRandomValue(){

        return random.nextInt(MAX_VALUE - MIN_VALUE + 1) + MIN_VALUE;
    }
}
