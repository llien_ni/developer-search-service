package com.technokratos.service.impl;

import com.technokratos.exception.RefreshTokenNotFoundException;
import com.technokratos.model.entity.RefreshTokenEntity;
import com.technokratos.repository.RefreshTokenRepository;
import com.technokratos.security.authentification.RefreshTokenAuthentication;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.security.model.AccessAndRefreshTokens;
import com.technokratos.security.provider.RefreshTokenAuthenticationProvider;
import com.technokratos.security.util.AuthorizationHeaderUtil;
import com.technokratos.security.util.JwtUtil;
import com.technokratos.service.RefreshTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

import static com.technokratos.security.util.constants.Constants.REFRESH_TOKEN_URL;


@Service
@RequiredArgsConstructor
public class RefreshTokenServiceImpl implements RefreshTokenService {

    private final RefreshTokenRepository refreshTokenRepository;

    private final JwtUtil jwtUtil;

    private final RefreshTokenAuthenticationProvider refreshTokenAuthenticationProvider;

    @Override
    public AccessAndRefreshTokens refreshTokens(HttpServletRequest request) {

        String refreshToken = AuthorizationHeaderUtil.getToken(request);
        RefreshTokenEntity refreshTokenEntity =
                refreshTokenRepository.findByValue(refreshToken)
                        .orElseThrow(RefreshTokenNotFoundException::new);

        refreshTokenRepository.delete(refreshTokenEntity);

        AccessAndRefreshTokens tokens = jwtUtil
                .generateAccessAndRefreshTokens(new UserDetailsImpl(refreshTokenEntity.getAccount()), REFRESH_TOKEN_URL);
        performAuthentication(refreshToken);

        return tokens;
    }

    private void performAuthentication(String refreshToken) {

        RefreshTokenAuthentication refreshTokenAuthentication = new RefreshTokenAuthentication(refreshToken);
        SecurityContextHolder.getContext()
                .setAuthentication(refreshTokenAuthenticationProvider.authenticate(refreshTokenAuthentication));
    }
}
