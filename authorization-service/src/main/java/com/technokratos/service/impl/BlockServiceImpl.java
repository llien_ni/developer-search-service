package com.technokratos.service.impl;

import com.technokratos.client.ChatServiceGrpcClient;
import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.dto.model.MailMessageModel;
import com.technokratos.dto.request.BlockAccountRequest;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.BlockInfoResponse;
import com.technokratos.exception.AccountAlreadyBlockedException;
import com.technokratos.exception.AccountIsNotBlocked;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.mapper.BlockInfoMapper;
import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Role;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.BlockInfoEntity;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.redis.service.RedisAccountService;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.BlockInfoRepository;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.service.AccountService;
import com.technokratos.service.BlockService;
import com.technokratos.util.NotificationUtil;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class BlockServiceImpl implements BlockService {

    private final BlockInfoRepository blockInfoRepository;

    private final AccountRepository accountRepository;

    private final BlockInfoMapper blockInfoMapper;

    private final RequestParamUtil requestParamUtil;

    private final RedisAccountService redisAccountService;

    private final AccountService accountService;

    private final NotificationServiceRabbit notificationServiceRabbit;

    private final DeveloperRepository developerRepository;

    private final ChatServiceGrpcClient chatServiceGrpcClient;

    @Transactional
    @Override
    public AccountResponse unblockAccount(UUID id) {

        AccountEntity account = accountRepository.findById(id).orElseThrow(UserNotFoundException::new);
        checkAccountIsBlocked(account);

        BlockInfoEntity blockInfoEntity = blockInfoRepository.findByAccountAndActiveTrue(account)
                .orElseThrow(() -> new AccountIsNotBlocked(account.getId()));
        blockInfoEntity.setActive(false);
        blockInfoRepository.save(blockInfoEntity);

        if(account.getRole().equals(Role.ROLE_DEVELOPER)){

            DeveloperEntity developer = developerRepository.getReferenceById(account.getId());
            developer.setState(DeveloperState.ACTIVE);
        }

        account.setStatus(Status.CONFIRMED);
        AccountEntity unblockedAccount = accountRepository.save(account);

        chatServiceGrpcClient.setChatStateToActiveByAccount(id);
        return accountService.convertToAccountResponse(unblockedAccount);
    }

    private void checkAccountIsBlocked(AccountEntity account) {

        if(!account.getStatus().equals(Status.BLOCKED)){

            throw new AccountIsNotBlocked(account.getId());
        }
    }

    @Transactional
    @Override
    public BlockInfoResponse blockAccount(BlockAccountRequest request) {

        AccountEntity account = accountRepository.findById(request.getAccountId())
                .orElseThrow(UserNotFoundException::new);
        checkAccountIsNotBlocked(account);
        account.setStatus(Status.BLOCKED);

        if(account.getRole().equals(Role.ROLE_DEVELOPER)){

            DeveloperEntity developer = developerRepository.getReferenceById(account.getId());
            developer.setState(DeveloperState.NOT_ACTIVE);
        }

        BlockInfoEntity blockInfo = BlockInfoEntity.builder().account(account)
                .finishDate(request.getFinishDate())
                .startDate(OffsetDateTime.now())
                .reason(request.getReason())
                .active(true)
                .build();
        BlockInfoEntity saveBlockInfo = blockInfoRepository.save(blockInfo);

        account.getBlockInfoEntities().add(saveBlockInfo);
        accountRepository.save(account);
        redisAccountService.addAllTokensToBlackList(account);

        MailMessageModel mailMessage = NotificationUtil.createMailMessageAboutAccountIsBlocked(account);
        notificationServiceRabbit.sendMailMessage(mailMessage);

        chatServiceGrpcClient.setChatStateToNotActiveByAccount(account.getId());
        BlockInfoResponse blockInfoResponse = blockInfoMapper.fromEntityToResponse(blockInfo);
        blockInfoResponse.setAccount(accountService.convertToAccountResponse(account));
        return blockInfoResponse;
    }

    private void checkAccountIsNotBlocked(AccountEntity account) {

        if(account.getStatus().equals(Status.BLOCKED)){
            throw new AccountAlreadyBlockedException(account.getId());
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<BlockInfoResponse> getByAccount(UUID accountId, Integer size, Integer number) {

        AccountEntity account = accountRepository.findById(accountId)
                .orElseThrow(UserNotFoundException::new);
        Sort sort = Sort.by(Sort.Direction.DESC, "startDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<BlockInfoEntity> page = blockInfoRepository.findByAccount(account, pageRequest.withSort(sort));
        return page.map(blockInfoMapper::fromEntityToResponse);
    }
}
