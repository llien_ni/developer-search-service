package com.technokratos.service.impl;

import com.technokratos.config.MinioConfigurationProperties;
import com.technokratos.dto.request.DocumentRequest;
import com.technokratos.dto.response.DocumentResponse;
import com.technokratos.exception.DeveloperNotFoundException;
import com.technokratos.exception.FileNotFoundException;
import com.technokratos.exception.FileNotLoadedException;
import com.technokratos.helper.MinioHelper;
import com.technokratos.mapper.DocumentMapper;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.DocumentEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.DocumentRepository;
import com.technokratos.service.DocumentService;
import com.technokratos.util.RequestParamUtil;
import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.errors.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import static com.technokratos.util.constants.Constants.AUTHORIZATION_SERVICE_URL;

@Service
@RequiredArgsConstructor
@Slf4j
public class DocumentServiceImpl implements DocumentService {

    private final MinioClient minioClient;

    private final RequestParamUtil requestParamUtil;

    private final DocumentRepository documentRepository;

    private final DeveloperRepository developerRepository;

    private final DocumentMapper documentMapper;

    private final MinioConfigurationProperties properties;

    private final MinioHelper minioHelper;

    private String createDownloadUrl(UUID documentId) {

        return AUTHORIZATION_SERVICE_URL.concat("/document/download/").concat(documentId.toString());
    }

    @Transactional(readOnly = true)
    @Override
    public DocumentResponse getDocumentById(UUID documentId) {

        DocumentEntity documentEntity = documentRepository.findById(documentId)
                .orElseThrow(() -> new FileNotFoundException(documentId));

        DocumentResponse documentResponse = documentMapper.fromEntityToResponse(documentEntity);
        documentResponse.setDownloadUrl(createDownloadUrl(documentEntity.getId()));

        return documentResponse;
    }

    @Transactional
    @Override
    public UUID delete(UUID id, UUID developerId) {

        DocumentEntity document = documentRepository.findByIdAndAndDeveloper_Id(id, developerId)
                .orElseThrow(() -> new FileNotFoundException(id));

        DeveloperEntity developer = document.getDeveloper();
        developer.getDocuments().remove(document);
        documentRepository.delete(document);

        minioHelper.removeFromMinio(id, document.getFileNameInBucket(), properties.getBucketNameDocuments());
        return document.getId();
    }

    @Transactional
    @Override
    public DocumentResponse uploadFile(MultipartFile file, DocumentRequest request, UUID developerId) {

        minioHelper.createBucketIfNotExist(properties.getBucketNameDocuments());

        DeveloperEntity developer = developerRepository.findById(developerId)
                .orElseThrow(DeveloperNotFoundException::new);

        DocumentEntity documentEntity = new DocumentEntity();
        documentEntity.setFileName(file.getOriginalFilename());
        documentEntity.setDeveloper(developer);
        documentEntity.setDescription(request.getDescription());
        documentEntity.setSize(BigDecimal.valueOf(file.getSize()));
        documentEntity.setType(file.getContentType());
        documentEntity.setFileNameInBucket(minioHelper.createFileNameInBucket(file));

        DocumentEntity savedEntity = documentRepository.save(documentEntity);
        developer.getDocuments().add(documentEntity);
        developerRepository.save(developer);

        minioHelper.uploadFileToMinIOServer(file, savedEntity.getFileNameInBucket(),
                savedEntity.getType(), savedEntity.getFileName(), properties.getBucketNameDocuments());

        DocumentResponse documentResponse = documentMapper.fromEntityToResponse(documentEntity);
        documentResponse.setDownloadUrl(createDownloadUrl(savedEntity.getId()));
        return documentResponse;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<DocumentResponse> getDocumentsByDeveloper(UUID developerId, Integer size, Integer number) {

        DeveloperEntity developer = developerRepository.findById(developerId).orElseThrow(DeveloperNotFoundException::new);

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<DocumentEntity> page = documentRepository.findAllByDeveloper(developer, pageRequest);

        return page.map(documentEntity -> {
            DocumentResponse documentResponse = documentMapper.fromEntityToResponse(documentEntity);
            documentResponse.setDownloadUrl(createDownloadUrl(documentResponse.getId()));
            return documentResponse;
        });
    }

    @Transactional
    @Override
    public void downloadDocument(UUID documentId, HttpServletResponse response) {

        DocumentEntity documentEntity = documentRepository.findById(documentId)
                .orElseThrow(() -> new FileNotFoundException(documentId));

        try(InputStream inputStream = minioClient.getObject(GetObjectArgs.builder()
                .bucket(properties.getBucketNameDocuments())
                .object(documentEntity.getFileNameInBucket())
                .length(documentEntity.getSize().longValue())
                .build())) {

            response.setHeader("Content-Disposition", "attachment;filename="
                    + URLEncoder.encode(documentEntity.getFileName(), StandardCharsets.UTF_8));
            response.setCharacterEncoding("UTF-8");
            IOUtils.copy(inputStream, response.getOutputStream());
        } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException |
                XmlParserException e) {
            throw new FileNotLoadedException(documentEntity.getFileName());
        }
    }
}
