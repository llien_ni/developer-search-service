package com.technokratos.service.impl;

import com.technokratos.dto.request.EducationRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.EducationResponse;
import com.technokratos.exception.DeveloperNotFoundException;
import com.technokratos.exception.EducationAlreadyAddedException;
import com.technokratos.exception.EducationNotFoundException;
import com.technokratos.mapper.EducationMapper;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.EducationEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.EducationRepository;
import com.technokratos.service.EducationService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EducationServiceImpl implements EducationService {

    private final EducationMapper educationMapper;

    private final EducationRepository educationRepository;

    private final RequestParamUtil requestParamUtil;

    private final DeveloperRepository developerRepository;

    @Transactional
    @Override
    public EducationResponse add(EducationRequest request, UUID developerId) {

        DeveloperEntity developer = developerRepository.findById(developerId)
                .orElseThrow(DeveloperNotFoundException::new);

        checkDeveloperNotAddedThisEducation(request, developerId);
        EducationEntity educationEntity = educationMapper.fromRequestToEntity(request);
        educationEntity.setDeveloper(developer);

        EducationEntity savedEntity = educationRepository.save(educationEntity);
        developer.getEducations().add(savedEntity);
        developerRepository.save(developer);

        return educationMapper.fromEntityToResponse(savedEntity);
    }

    @Transactional
    @Override
    public UUID delete(RequestID requestId, UUID developerId) {

        EducationEntity education = educationRepository.findByIdAndDeveloper_Id(requestId.getId(), developerId)
                .orElseThrow(EducationNotFoundException::new);
        DeveloperEntity developer = education.getDeveloper();
        developer.getEducations().remove(education);
        educationRepository.delete(education);

        return education.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EducationResponse> getAllByDeveloper(Integer size, Integer number, UUID developerId) {

        Sort sort = Sort.by(Sort.Direction.DESC, "startDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<EducationEntity> entities = educationRepository.findAllByDeveloper_Id(developerId, pageRequest.withSort(sort));
        return entities.map(educationMapper::fromEntityToResponse);
    }

    private void checkDeveloperNotAddedThisEducation(EducationRequest request, UUID developerId) {

        Optional<EducationEntity> entity = educationRepository.findAllByNameAndTypeAndDeveloper_Id(request.getName(), request.getType(), developerId);
        if(entity.isPresent()){
            throw new EducationAlreadyAddedException();
        }
    }
}
