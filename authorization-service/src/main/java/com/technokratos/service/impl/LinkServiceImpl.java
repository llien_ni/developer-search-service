package com.technokratos.service.impl;

import com.technokratos.dto.request.LinkRequest;
import com.technokratos.dto.request.UpdateLinkRequest;
import com.technokratos.dto.response.LinkResponse;
import com.technokratos.exception.DeveloperNotFoundException;
import com.technokratos.exception.LinkNotFoundException;
import com.technokratos.mapper.LinkMapper;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.LinkEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.LinkRepository;
import com.technokratos.service.LinkService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class LinkServiceImpl implements LinkService {

    private final LinkRepository linkRepository;

    private final LinkMapper linkMapper;

    private final RequestParamUtil requestParamUtil;

    private final DeveloperRepository developerRepository;

    @Transactional
    @Override
    public LinkResponse add(LinkRequest request, UUID developerId) {

        DeveloperEntity developer = developerRepository.findById(developerId)
                .orElseThrow(DeveloperNotFoundException::new);

        LinkEntity linkEntity = linkMapper.fromRequestToEntity(request);
        linkEntity.setDeveloper(developer);
        LinkEntity savedLink = linkRepository.save(linkEntity);
        developer.getLinks().add(linkEntity);
        developerRepository.save(developer);

        return linkMapper.fromEntityToResponse(savedLink);
    }

    @Transactional
    @Override
    public LinkResponse update(UpdateLinkRequest request, UUID developerId) {

        LinkEntity linkEntity = linkRepository.findByIdAndDeveloper_Id(request.getId(), developerId)
                .orElseThrow(LinkNotFoundException::new);
        linkMapper.update(request, linkEntity);

        return linkMapper.fromEntityToResponse(linkRepository.save(linkEntity));
    }

    @Transactional
    @Override
    public UUID delete(UUID linkId, UUID developerId) {

        LinkEntity linkEntity = linkRepository.findByIdAndDeveloper_Id(linkId, developerId)
                .orElseThrow(LinkNotFoundException::new);

        DeveloperEntity developer = linkEntity.getDeveloper();
        developer.getLinks().remove(linkEntity);
        developerRepository.save(developer);

        linkRepository.delete(linkEntity);
        return linkEntity.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public LinkResponse getById(UUID id) {

        LinkEntity linkEntity = linkRepository
                .findById(id)
                .orElseThrow(LinkNotFoundException::new);

        return linkMapper.fromEntityToResponse(linkEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LinkResponse> findByDeveloper(UUID developerId, Integer size, Integer number) {

        DeveloperEntity developer = developerRepository.findById(developerId)
                .orElseThrow(DeveloperNotFoundException::new);

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<LinkEntity> links = linkRepository.findAllByDeveloper(developer, pageRequest);
        return links.map(linkMapper::fromEntityToResponse);
    }
}
