package com.technokratos.service.impl;

import com.technokratos.dto.request.CreateDeveloperRequest;
import com.technokratos.dto.request.UpdateDeveloperRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.exception.DeveloperAlreadyExistException;
import com.technokratos.exception.DeveloperNotFoundException;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.mapper.DeveloperMapper;
import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Role;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.service.AccountService;
import com.technokratos.service.DeveloperService;
import com.technokratos.service.PhotoService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class DeveloperServiceImpl implements DeveloperService {

    private final DeveloperRepository developerRepository;

    private final DeveloperMapper developerMapper;

    private final AccountService accountService;

    private final RequestParamUtil requestParamUtil;

    private final PhotoService photoService;

    @Override
    public DeveloperResponse getDeveloperResponse(DeveloperEntity developer) {

        DeveloperResponse developerResponse = developerMapper.fromEntityToResponse(developer);
        if (developer.getPhoto() != null) {
            PhotoResponse photo = photoService.getPhotoResponse(developer.getPhoto());
            developerResponse.setPhoto(photo);
        }
        return developerResponse;
    }

    @Transactional
    @Override
    public DeveloperResponse save(CreateDeveloperRequest request) {

        checkDeveloperWithThisUsernameIsNotExist(request.getUsername());

        DeveloperEntity developerEntity = developerMapper.fromRequestToEntity(request);


        AccountEntity account = accountService.save(developerEntity);
        developerEntity.setId(account.getId());
        developerEntity.setState(DeveloperState.ACTIVE);
        developerEntity.setLastVisitDate(OffsetDateTime.now());
        developerEntity.setRole(Role.ROLE_DEVELOPER);
        developerEntity.setRating(BigDecimal.ZERO);

        DeveloperEntity developer = developerRepository.save(developerEntity);

        return getDeveloperResponse(developer);
    }

    @Transactional
    @Override
    public DeveloperResponse update(UUID id, UpdateDeveloperRequest request) {

        DeveloperEntity developerEntity = developerRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);

        if (Objects.nonNull(request.getUsername())) {
            checkDeveloperWithThisUsernameIsNotExist(request.getUsername());
        }

        developerMapper.updateDeveloperEntity(request, developerEntity);
        DeveloperEntity developer = developerRepository.save(developerEntity);

        return getDeveloperResponse(developer);
    }

    private void checkDeveloperWithThisUsernameIsNotExist(String username) {

        Optional<DeveloperEntity> developerEntity = developerRepository.findByUsername(username);
        if (developerEntity.isPresent()) {
            throw new DeveloperAlreadyExistException();
        }
    }

    @Transactional
    @Override
    public UUID delete(UUID id) {

        DeveloperEntity developer = developerRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);
        developer.setState(DeveloperState.NOT_ACTIVE);
        developer.setStatus(Status.DELETED);
        developerRepository.save(developer);

        return developer.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public DeveloperResponse findById(UUID id) {

        DeveloperEntity developer = developerRepository
                .findById(id)
                .orElseThrow(DeveloperNotFoundException::new);

        return getDeveloperResponse(developer);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<DeveloperResponse> findAllByStateAndStatus(Integer size, Integer number, DeveloperState state, Status status) {

        Sort sort = Sort.by(Sort.Direction.DESC, "rating");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<DeveloperEntity> entities = developerRepository.findAllByStateAndStatus(state, status, pageRequest.withSort(sort));

        return entities.map(this::getDeveloperResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<DeveloperResponse> findByIdsList(List<UUID> list, Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, "rating");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<DeveloperEntity> entities = developerRepository.findAllByIdIsIn(list, pageRequest.withSort(sort));

        return entities.map(this::getDeveloperResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public DeveloperResponse findByUserName(String username) {

        DeveloperEntity developer = developerRepository.findByUsernameAndStatus(username, Status.CONFIRMED)
                .orElseThrow(DeveloperNotFoundException::new);

        return getDeveloperResponse(developer);
    }

    @Transactional
    @Override
    public void updateDeveloperRating(UUID developerId, int value) {

        DeveloperEntity developer = developerRepository.findById(developerId).orElseThrow(DeveloperNotFoundException::new);

        developer.setRating(BigDecimal.valueOf(developer.getRating().longValue() + value));
        developerRepository.save(developer);
    }

    @Transactional(readOnly = true)
    @Override
    public DeveloperResponse findByIdAndStatus(UUID id, Status status) {

        DeveloperEntity developer = developerRepository.findByIdAndStatus(id, status)
                .orElseThrow(DeveloperNotFoundException::new);

        return getDeveloperResponse(developer);
    }

    @Override
    public void updateLastVisitDate(UUID developerId) {

        DeveloperEntity developer = developerRepository.findById(developerId)
                .orElseThrow(DeveloperNotFoundException::new);
        developer.setLastVisitDate(OffsetDateTime.now());
        developerRepository.save(developer);

    }
}
