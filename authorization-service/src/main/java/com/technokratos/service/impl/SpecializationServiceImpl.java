package com.technokratos.service.impl;

import com.technokratos.dto.request.SpecializationRequest;
import com.technokratos.dto.request.SpecializationsListRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.exception.DeveloperNotFoundException;
import com.technokratos.exception.SpecializationAlreadyAddedException;
import com.technokratos.exception.SpecializationNotFoundException;
import com.technokratos.mapper.DeveloperMapper;
import com.technokratos.mapper.SpecializationMapper;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.SpecializationEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.SpecializationRepository;
import com.technokratos.service.DeveloperService;
import com.technokratos.service.SpecializationService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
public class SpecializationServiceImpl implements SpecializationService {

    private final SpecializationRepository specializationRepository;

    private final SpecializationMapper specializationMapper;

    private final RequestParamUtil requestParamUtil;

    private final DeveloperMapper developerMapper;

    private final DeveloperRepository developerRepository;

    private final DeveloperService developerService;

    @Transactional
    @Override
    public SpecializationResponse add(SpecializationRequest request, UUID developerId) {

        request.setName(request.getName().toLowerCase(Locale.ROOT));
        DeveloperEntity developer = developerRepository.findById(developerId).orElseThrow(DeveloperNotFoundException::new);
        checkDeveloperNotAddedThisYet(request.getName(), developer);
        SpecializationEntity specialization = specializationMapper.fromRequestToEntity(request);
        specialization.setDeveloper(developer);

        SpecializationEntity savedEntity = specializationRepository.save(specialization);
        developer.getSpecializations().add(specialization);

        return specializationMapper.fromEntityToResponse(savedEntity);
    }

    @Transactional
    @Override
    public List<SpecializationResponse> addList(SpecializationsListRequest request, UUID developerId) {

        DeveloperEntity developer = developerRepository.findById(developerId).orElseThrow(DeveloperNotFoundException::new);

        List<SpecializationEntity> specializationEntities =
                request.getSpecializations()
                        .stream()
                        .map(specializationRequest -> {

                            specializationRequest.setName(specializationRequest
                                    .getName()
                                    .toLowerCase(Locale.ROOT));

                            checkDeveloperNotAddedThisYet(specializationRequest.getName(), developer);
                            SpecializationEntity specializationEntity = specializationMapper
                                    .fromRequestToEntity(specializationRequest);
                            specializationEntity.setDeveloper(developer);

                            developer.getSpecializations().add(specializationEntity);
                            return specializationEntity;
                        }).toList();

        List<SpecializationEntity> specializations = specializationRepository.saveAll(specializationEntities);
        developerRepository.save(developer);
        return specializationMapper.fromEntitiesToResponse(specializations);
    }

    private void checkDeveloperNotAddedThisYet(String name, DeveloperEntity developer) {

        Optional<SpecializationEntity> specializationEntity = specializationRepository
                .findAllByNameAndDeveloper(name, developer);
        if (specializationEntity.isPresent()) {
            throw new SpecializationAlreadyAddedException(name);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public SpecializationResponse findById(UUID specializationId) {

        return specializationMapper.fromEntityToResponse(specializationRepository.findById(specializationId)
                .orElseThrow(SpecializationNotFoundException::new));
    }

    @Transactional
    @Override
    public UUID delete(UUID specializationId, UUID developerId) {

        DeveloperEntity developer = developerRepository.getReferenceById(developerId);

        SpecializationEntity specialization = specializationRepository.findByIdAndDeveloper_Id(specializationId, developerId)
                .orElseThrow(SpecializationNotFoundException::new);

        developer.getSpecializations().remove(specialization);
        developerRepository.save(developer);
        specializationRepository.delete(specialization);

        return specialization.getId();
    }


    @Override
    public Page<SpecializationResponse> getByDeveloper(Integer size, Integer number, UUID developerId) {

        Sort sort = Sort.by(Sort.Direction.ASC, "name");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        DeveloperEntity developer = developerRepository.findById(developerId).orElseThrow(DeveloperNotFoundException::new);
        Page<SpecializationEntity> entities = specializationRepository.findByDeveloper(pageRequest.withSort(sort), developer);
        return entities.map(specializationMapper::fromEntityToResponse);
    }

    @Override
    public Page<String> getAllDistinctNames(Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        List<String> names = specializationRepository.findAll()
                .stream()
                .map(SpecializationEntity::getName)
                .distinct()
                .toList();
        return new PageImpl<>(names, pageRequest, names.size());
    }

    @Override
    public Page<DeveloperResponse> getDevelopersBySpecializations(Integer size, Integer number, List<String> specializations) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);

        Map<DeveloperEntity, Integer> developersAndSpecializations = new HashMap<>();

        List<DeveloperEntity> developers = new ArrayList<>();
        List<SpecializationEntity> specializationEntities = specializationRepository.findSpecializationEntitiesByNameIn(specializations);
        specializationEntities.forEach(specializationEntity -> {
            developersAndSpecializations.merge(specializationEntity.getDeveloper(), 1, Integer::sum);
            if (developersAndSpecializations.get(specializationEntity.getDeveloper()) == specializations.size()) {
                developers.add(specializationEntity.getDeveloper());
            }
        });
        Page<DeveloperEntity> entities = new PageImpl<>(developers, pageRequest, developers.size());
        return entities.map(developerService::getDeveloperResponse);
    }
}
