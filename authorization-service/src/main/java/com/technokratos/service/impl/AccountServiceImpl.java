package com.technokratos.service.impl;

import com.technokratos.client.NotificationServiceGrpcClient;
import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.converter.HrConverter;
import com.technokratos.dto.model.MailMessageModel;
import com.technokratos.dto.request.*;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.exception.*;
import com.technokratos.mapper.AccountMapper;
import com.technokratos.mapper.DeveloperMapper;
import com.technokratos.model.constant.RegistrationProvider;
import com.technokratos.model.constant.Role;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.ChangeEmailRequestEntity;
import com.technokratos.model.entity.ConfirmationTokenEntity;
import com.technokratos.model.entity.HrEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.HrRepository;
import com.technokratos.service.AccountService;
import com.technokratos.service.ChangeEmailRequestService;
import com.technokratos.service.ConfirmationTokenService;
import com.technokratos.service.PhotoService;
import com.technokratos.util.NotificationUtil;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import notification.service.NotificationServiceOuterClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    private final ConfirmationTokenService confirmationTokenService;

    private final NotificationServiceGrpcClient notificationServiceClient;

    private final AccountMapper accountMapper;

    private final PasswordEncoder passwordEncoder;

    private final ChangeEmailRequestService changeEmailRequestService;

    private final NotificationServiceRabbit notificationServiceRabbit;

    private final RequestParamUtil requestParamUtil;

    private final DeveloperRepository developerRepository;

    private final DeveloperMapper developerMapper;

    private final HrRepository hrRepository;

    private final PhotoService photoService;

    private final HrConverter hrConverter;

    @Override
    public void checkAccountWithSuchEmailIsNotExist(String email) {

        Optional<AccountEntity> accountEntity = accountRepository.findByEmail(email);
        if (accountEntity.isPresent()) {
            throw new UserAlreadyExistException();
        }
    }

    @Transactional
    @Override
    public AccountResponse confirmAccount(String token) {

        ConfirmationTokenEntity confirmationToken = confirmationTokenService.getByToken(token);
        checkConfirmationTokenIsNotExpired(confirmationToken);

        AccountEntity account = confirmationToken.getAccount();

        if (account.getRole().equals(Role.ROLE_HR)) {

            account.setStatus(Status.MODERATED);
        } else {
            account.setStatus(Status.CONFIRMED);
        }
        confirmationTokenService.delete(confirmationToken);

        AccountEntity savedEntity = accountRepository.save(account);
        return convertToAccountResponse(savedEntity);
    }

    @Transactional
    @Override
    public AccountResponse save(CreateAccountRequest request) {

        AccountEntity account = accountMapper.fromRequestToEntity(request);
        checkAccountWithSuchEmailIsNotExist(request.getEmail());
        account.setRole(Role.ROLE_SIMPLE_ACCOUNT);
        AccountEntity savedAccount = save(account);

        return accountMapper.fromEntityToResponse(savedAccount);
    }

    @Transactional
    @Override
    public AccountEntity save(AccountEntity accountEntity) {

        if(accountEntity.getEmail()!=null){
            checkAccountWithSuchEmailIsNotExist(accountEntity.getEmail());
        }
        if(accountEntity.getPhoneNumber()!=null){
            checkAccountWithSuchPhoneNumberIsNotExist(accountEntity.getPhoneNumber());
        }

        accountEntity.setStatus(Status.NOT_CONFIRMED);
        accountEntity.setRegistrationProvider(RegistrationProvider.NATIVE);
        accountEntity.setHashPassword(passwordEncoder.encode(accountEntity.getHashPassword()));
        accountEntity.setConfirmationTokens(new ArrayList<>());
        accountEntity.setAppeals(new ArrayList<>());
        accountEntity.setSentGrievances(new ArrayList<>());
        accountEntity.setReceivedGrievances(new ArrayList<>());
        accountEntity.setRefreshTokens(new ArrayList<>());
        accountEntity.setChangeEmailRequests(new ArrayList<>());
        accountEntity.setBlockInfoEntities(new ArrayList<>());
        AccountEntity account = accountRepository.save(accountEntity);

        if(account.getEmail()!=null){

            ConfirmationTokenEntity tokenForAccount = confirmationTokenService
                    .createTokenForAccount(account);

            NotificationServiceOuterClass.MailMessageRequest mailMessageRequest = NotificationUtil
                    .createMailMessageToConfirmAccount(tokenForAccount, account);

            notificationServiceClient.sendMail(mailMessageRequest);
        }else{

            ConfirmationTokenEntity tokenForAccount = confirmationTokenService
                    .createCodeConfirmationForAccount(account);

            NotificationServiceOuterClass.SmsModel smsModel = NotificationUtil
                    .createSmsConfirmAccount(tokenForAccount, account);

            notificationServiceClient.sendSms(smsModel);
        }

        return account;
    }

    private void checkAccountWithSuchPhoneNumberIsNotExist(String phoneNumber) {

        Optional<AccountEntity> accountEntity = accountRepository.findByPhoneNumber(phoneNumber);
        if (accountEntity.isPresent()) {
            throw new UserAlreadyExistException();
        }
    }

    @Transactional
    @Override
    public UUID delete(UUID id) {

        AccountEntity account = accountRepository.findById(id).orElseThrow(HrNotFoundException::new);
        account.setStatus(Status.DELETED);
        accountRepository.save(account);
        return account.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public AccountResponse getById(UUID id) {

        AccountEntity accountEntity = accountRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);

        return convertToAccountResponse(accountEntity);
    }

    @Transactional
    @Override
    public String resetPassword(PhoneOrEmailRequest request) {

        if(request.getLoginType().equals(PhoneOrEmailRequest.LoginType.EMAIL)){

            AccountEntity account = accountRepository.findByEmail(request.getLogin())
                    .orElseThrow(UserNotFoundException::new);

            checkAccountStatusIsConfirmed(account);
            account.setStatus(Status.NOT_CONFIRMED);
            accountRepository.save(account);

            ConfirmationTokenEntity tokenForAccount = confirmationTokenService
                    .createTokenForAccount(account);
            NotificationServiceOuterClass.MailMessageRequest mailMessageRequest = NotificationUtil
                    .resetPassword(tokenForAccount, request.getLogin());
            notificationServiceClient.sendMail(mailMessageRequest);

            return tokenForAccount.getAccount().getEmail();
        }else{

            AccountEntity account = accountRepository.findByPhoneNumber(request.getLogin())
                    .orElseThrow(UserNotFoundException::new);

            checkAccountStatusIsConfirmed(account);
            account.setStatus(Status.NOT_CONFIRMED);
            accountRepository.save(account);

            ConfirmationTokenEntity tokenForAccount = confirmationTokenService
                    .createCodeConfirmationForAccount(account);
            NotificationServiceOuterClass.SmsModel smsModel = NotificationUtil
                    .resetPasswordByPhoneNumber(tokenForAccount, request.getLogin());
            notificationServiceClient.sendSms(smsModel);

            return tokenForAccount.getAccount().getPhoneNumber();

        }

    }

    private void checkAccountStatusIsConfirmed(AccountEntity account) {

        if(!account.getStatus().equals(Status.CONFIRMED)){

            throw new AccountIsNotConfirmed();
        }
    }

    @Transactional
    @Override
    public AccountResponse resetPasswordConfirm(ResetPasswordRequest request) {

        ConfirmationTokenEntity confirmationToken = confirmationTokenService.getByToken(request.getToken());
        checkConfirmationTokenIsNotExpired(confirmationToken);

        AccountEntity account = confirmationToken.getAccount();
        account.setHashPassword(passwordEncoder.encode(request.getPassword()));
        account.setStatus(Status.CONFIRMED);

        account.getConfirmationTokens().remove(confirmationToken);
        confirmationTokenService.delete(confirmationToken);
        AccountEntity savedEntity = accountRepository.save(account);

        return convertToAccountResponse(savedEntity);
    }

    private void checkConfirmationTokenIsNotExpired(ConfirmationTokenEntity confirmationToken) {

        if (OffsetDateTime.now().isAfter(confirmationToken.getExpiredDate())) {
            throw new TokenExpiredException();
        }
    }

    @Transactional
    @Override
    public void changePassword(ChangePasswordRequest request, UUID accountId) {

        AccountEntity account = accountRepository.findById(accountId)
                .orElseThrow(UserNotFoundException::new);

        if (passwordEncoder.matches(request.getOldPassword(), account.getHashPassword())) {
            account.setHashPassword(passwordEncoder.encode(request.getNewPassword()));
            accountRepository.save(account);
        } else {
            throw new ChangePasswordException();
        }
    }

    @Transactional
    @Override
    public String restoreAccount(PhoneOrEmailRequest request) {

        if(request.getLoginType().equals(PhoneOrEmailRequest.LoginType.EMAIL)){

            AccountEntity account = accountRepository.findByEmail(request.getLogin())
                    .orElseThrow(UserNotFoundException::new);
            checkAccountIsDeleted(account);

            ConfirmationTokenEntity tokenForAccount = confirmationTokenService.createTokenForAccount(account);

            account.setStatus(Status.NOT_CONFIRMED);
            accountRepository.save(account);

            NotificationServiceOuterClass.MailMessageRequest mailMessageRequest = NotificationUtil
                    .restoreAccount(tokenForAccount, request.getLogin());
            notificationServiceClient.sendMail(mailMessageRequest);
            return account.getEmail();
        }else{

            AccountEntity account = accountRepository.findByPhoneNumber(request.getLogin())
                    .orElseThrow(UserNotFoundException::new);
            checkAccountIsDeleted(account);

            ConfirmationTokenEntity tokenForAccount = confirmationTokenService.createCodeConfirmationForAccount(account);

            account.setStatus(Status.NOT_CONFIRMED);
            accountRepository.save(account);

            NotificationServiceOuterClass.SmsModel smsModel = NotificationUtil
                    .restoreAccountByPhoneNumber(tokenForAccount, request.getLogin());
            notificationServiceClient.sendSms(smsModel);
            return account.getPhoneNumber();
        }
    }

    private void checkAccountIsDeleted(AccountEntity account) {

        if (!account.getStatus().equals(Status.DELETED)) {
            throw new RestoreAccountException("Account has not been deleted");
        }
    }

    @Transactional
    @Override
    public AccountResponse update(UUID accountId, UpdateAccountRequest request) {

        AccountEntity account = accountRepository.getReferenceById(accountId);
        accountMapper.updateAccountEntity(request, account);
        AccountEntity savedEntity = accountRepository.save(account);

        return convertToAccountResponse(savedEntity);
    }

    @Transactional
    @Override
    public String changeEmail(ChangeEmailRequest request, UUID accountId) {

        AccountEntity account = accountRepository.findById(accountId)
                .orElseThrow(UserNotFoundException::new);

        checkPassword(request, account);
        checkAccountWithSuchEmailIsNotExist(request.getNewEmail());

        ChangeEmailRequestEntity savedChangeEmailRequest = changeEmailRequestService.createChangeEmailRequest(request, account);

        NotificationServiceOuterClass.MailMessageRequest mailMessageRequest = NotificationUtil
                .createMailMessageToChangeEmail(savedChangeEmailRequest.getToken().toString(), request.getNewEmail());
        notificationServiceClient.sendMail(mailMessageRequest);

        return savedChangeEmailRequest.getNewEmail();
    }

    private void checkPassword(ChangeEmailRequest request, AccountEntity account) {

        if (!passwordEncoder.matches(request.getPassword(), account.getHashPassword())) {
            throw new ChangeEmailException();
        }
    }

    @Transactional
    @Override
    public AccountResponse changeEmailConfirm(String token) {

        ChangeEmailRequestEntity changeEmailRequest = changeEmailRequestService.getByToken(token);
        checkChangeEmailTokenIsNotExpired(changeEmailRequest);

        AccountEntity account = changeEmailRequest.getAccount();
        account.setEmail(changeEmailRequest.getNewEmail());
        AccountEntity savedEntity = accountRepository.save(account);

        return convertToAccountResponse(savedEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<AccountResponse> getAll(String status, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);

        Page<AccountEntity> page;
        if (status != null) {
            checkStatusIsValid(status);
            page = accountRepository.findAllByStatus(Status.valueOf(status), pageRequest);
        }else{
            page = accountRepository.findAll(pageRequest);
        }

        return page.map(this::convertToAccountResponse);
    }

    private void checkStatusIsValid(String status) {

        try {
            Status.valueOf(status);
        } catch (IllegalArgumentException e) {
            throw new StatusInvalidException(status);
        }
    }

    @Transactional
    @Override
    public AccountResponse updateAccountStatus(UUID id, Status status) {

        AccountEntity accountEntity = accountRepository.findById(id).orElseThrow(UserNotFoundException::new);
        accountEntity.setStatus(status);
        accountRepository.save(accountEntity);

        sendMailMessageToAccount(status, accountEntity);

        return convertToAccountResponse(accountEntity);
    }

    private void sendMailMessageToAccount(Status status, AccountEntity account) {

        MailMessageModel mailMessage = null;
        if(status.equals(Status.BLOCKED)){
             mailMessage = NotificationUtil.createMailMessageAboutAccountIsBlocked(account);
        }
        if(status.equals(Status.CONFIRMED)){

             mailMessage = NotificationUtil.createMailMessageAboutAccountIsUnBlocked(account);
        }

        if(mailMessage!=null){
            notificationServiceRabbit.sendMailMessage(mailMessage);
        }
    }

    private void checkChangeEmailTokenIsNotExpired(ChangeEmailRequestEntity changeEmailRequest) {

        if (OffsetDateTime.now().isAfter(changeEmailRequest.getExpiredDate())) {
            throw new TokenExpiredException();
        }
    }

    @Override
    public AccountResponse convertToAccountResponse(AccountEntity account) {

        AccountResponse accountResponse;
        if (account.getRole().equals(Role.ROLE_HR)) {

            HrEntity hrEntity = hrRepository.getReferenceById(account.getId());

            HrResponse hrResponse = hrConverter.convertToHrResponse(hrEntity);
            accountResponse = hrConverter.addCompanyResponseToHrResponse(hrResponse, hrEntity.getCompanyId());
        } else if (account.getRole().equals(Role.ROLE_DEVELOPER)) {

            accountResponse = developerMapper.fromEntityToResponse(developerRepository.findById(account.getId())
                    .orElseThrow(UserNotFoundException::new));
        } else {
            accountResponse = accountMapper.fromEntityToResponse(account);
        }
        return setPhotoInResponse(account, accountResponse);
    }

    public AccountResponse setPhotoInResponse(AccountEntity accountEntity, AccountResponse accountResponse) {

        if (accountEntity.getPhoto() != null) {
            PhotoResponse photo = photoService.getPhotoResponse(accountEntity.getPhoto());
            accountResponse.setPhoto(photo);
        }
        return accountResponse;
    }
}
