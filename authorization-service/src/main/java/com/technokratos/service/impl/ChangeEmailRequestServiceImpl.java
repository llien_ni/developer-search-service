package com.technokratos.service.impl;

import com.technokratos.dto.request.ChangeEmailRequest;
import com.technokratos.exception.TokenNotFoundException;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.ChangeEmailRequestEntity;
import com.technokratos.repository.ChangeEmailRequestRepository;
import com.technokratos.service.ChangeEmailRequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ChangeEmailRequestServiceImpl implements ChangeEmailRequestService {

    private final ChangeEmailRequestRepository changeEmailRequestRepository;

    @Transactional
    @Override
    public ChangeEmailRequestEntity createChangeEmailRequest(ChangeEmailRequest request, AccountEntity account) {

        ChangeEmailRequestEntity changeEmailRequestEntity = ChangeEmailRequestEntity.builder()
                .newEmail(request.getNewEmail())
                .account(account)
                .expiredDate(OffsetDateTime.now().plusMinutes(10))
                .token(UUID.randomUUID())
                .build();

        return changeEmailRequestRepository.save(changeEmailRequestEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public ChangeEmailRequestEntity getByToken(String token) {

        return changeEmailRequestRepository.findByToken(UUID.fromString(token)).orElseThrow(TokenNotFoundException::new);
    }

    @Transactional
    @Override
    public void deleteExpiredRequests() {

        List<ChangeEmailRequestEntity> requests = changeEmailRequestRepository
                .findChangeEmailRequestEntitiesByExpiredDateLessThan(OffsetDateTime.now());
        changeEmailRequestRepository.deleteAll(requests);
    }
}
