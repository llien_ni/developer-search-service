package com.technokratos.service.impl;

import com.technokratos.client.ChatServiceGrpcClient;
import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.dto.model.MailMessageModel;
import com.technokratos.dto.request.GrievanceRequest;
import com.technokratos.dto.response.GrievanceResponse;
import com.technokratos.exception.AccountSendManyGrievances;
import com.technokratos.exception.GrievanceAlreadyExistException;
import com.technokratos.exception.GrievanceNotFoundException;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.mapper.GrievanceMapper;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.BlockInfoEntity;
import com.technokratos.model.entity.GrievanceEntity;
import com.technokratos.redis.service.RedisAccountService;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.GrievanceRepository;
import com.technokratos.service.GrievanceService;
import com.technokratos.util.NotificationUtil;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GrievanceServiceImpl implements GrievanceService {

    private final GrievanceRepository grievanceRepository;

    private final AccountRepository accountRepository;

    private final GrievanceMapper grievanceMapper;

    private final RequestParamUtil requestParamUtil;

    private final NotificationServiceRabbit notificationServiceRabbit;

    private final RedisAccountService redisAccountService;

    private final ChatServiceGrpcClient chatServiceGrpcClient;

    private static final String CREATED_DATE = "createdDate";

    @Value("${grievances.limit}")
    private int grievancesLimit;

    @Transactional
    @Override
    public GrievanceResponse add(GrievanceRequest request, UUID authorId) {

        AccountEntity author = accountRepository.findById(authorId)
                .orElseThrow(UserNotFoundException::new);

        if (blockIfAccountSentManyGrievances(author)) {
            throw new AccountSendManyGrievances(author.getId());
        }

        AccountEntity account = accountRepository.findById(request.getAccountId())
                .orElseThrow(UserNotFoundException::new);

        checkGrievanceIsNotExist(request.getAccountId(), authorId);

        GrievanceEntity grievanceEntity = grievanceMapper.fromRequestToEntity(request);
        grievanceEntity.setAuthor(author);
        grievanceEntity.setAccount(account);

        GrievanceEntity grievance = grievanceRepository.save(grievanceEntity);
        author.getSentGrievances().add(grievance);
        account.getReceivedGrievances().add(grievance);
        blockAccountIfManyGrievance(account);
        return grievanceMapper.fromEntityToResponse(grievance);
    }

    private void checkGrievanceIsNotExist(UUID accountId, UUID authorId) {

        if (grievanceRepository.findByAccount_IdAndAuthor_Id(accountId, authorId).isPresent()) {

            throw new GrievanceAlreadyExistException();
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<GrievanceResponse> getByAuthor(Integer size, Integer number, UUID authorId) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        AccountEntity author = accountRepository.findById(authorId)
                .orElseThrow(UserNotFoundException::new);

        Page<GrievanceEntity> entities = grievanceRepository.findAllByAuthor(author, pageRequest.withSort(sort));
        return entities.map(grievanceMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<GrievanceResponse> getByAccount(Integer size, Integer number, UUID accountId) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);

        AccountEntity account = accountRepository.findById(accountId).orElseThrow(UserNotFoundException::new);
        Page<GrievanceEntity> entities = grievanceRepository.findAllByAccount(account, pageRequest.withSort(sort));
        return entities.map(grievanceMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<GrievanceResponse> getAll(Integer size, Integer number) {

        Sort sort = Sort.by(Sort.Direction.DESC, CREATED_DATE);
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);
        Page<GrievanceEntity> entities = grievanceRepository.findAll(pageRequest.withSort(sort));
        return entities.map(grievanceMapper::fromEntityToResponse);
    }

    @Transactional
    @Override
    public UUID delete(UUID id) {

        GrievanceEntity grievance = grievanceRepository.findById(id)
                .orElseThrow(GrievanceNotFoundException::new);
        AccountEntity account = grievance.getAccount();
        AccountEntity author = grievance.getAuthor();

        account.getReceivedGrievances().remove(grievance);
        author.getSentGrievances().remove(grievance);
        accountRepository.save(account);
        accountRepository.save(author);
        grievanceRepository.delete(grievance);

        return grievance.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public GrievanceResponse getById(UUID id) {

        GrievanceEntity grievanceEntity = grievanceRepository.findById(id).orElseThrow(GrievanceNotFoundException::new);
        return grievanceMapper.fromEntityToResponse(grievanceEntity);
    }

    private boolean blockIfAccountSentManyGrievances(AccountEntity author) {

        List<GrievanceEntity> sentGrievances = grievanceRepository
                .findAllByAuthorAndCreatedDateBetween(author, OffsetDateTime.now().minusYears(1L), OffsetDateTime.now());

        if (sentGrievances.size() >= grievancesLimit) {

            author.setStatus(Status.BLOCKED);
            accountRepository.save(author);

            BlockInfoEntity blockInfo = BlockInfoEntity.builder()
                    .account(author)
                    .reason("Account sent many grievances")
                    .finishDate(OffsetDateTime.now().plusDays(10))
                    .active(true)
                    .build();

            author.getBlockInfoEntities().add(blockInfo);
            redisAccountService.addAllTokensToBlackList(author);

            chatServiceGrpcClient.setChatStateToNotActiveByAccount(author.getId());

            MailMessageModel mailMessage = NotificationUtil.createMailMessageAboutAccountIsBlocked(author);
            notificationServiceRabbit.sendMailMessage(mailMessage);

            return true;
        }
        return false;
    }

    private void blockAccountIfManyGrievance(AccountEntity account) {

        List<GrievanceEntity> entities = grievanceRepository.findAllByAccount(account);

        if (entities.size() >= grievancesLimit) {

            account.setStatus(Status.BLOCKED);
            accountRepository.save(account);

            BlockInfoEntity blockInfo = BlockInfoEntity.builder()
                    .account(account)
                    .reason("Account have many grievances")
                    .active(true)
                    .finishDate(OffsetDateTime.now().plusDays(10))
                    .build();

            account.getBlockInfoEntities().add(blockInfo);
            redisAccountService.addAllTokensToBlackList(account);

            chatServiceGrpcClient.setChatStateToNotActiveByAccount(account.getId());

            MailMessageModel mailMessage = NotificationUtil.createMailMessageAboutAccountIsBlocked(account);
            notificationServiceRabbit.sendMailMessage(mailMessage);
        }
    }
}
