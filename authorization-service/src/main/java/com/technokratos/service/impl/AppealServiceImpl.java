package com.technokratos.service.impl;

import com.technokratos.dto.request.AppealRequest;
import com.technokratos.dto.request.ResponseToAppeal;
import com.technokratos.dto.response.AppealResponse;
import com.technokratos.exception.AppealNotFoundException;
import com.technokratos.exception.StatusInvalidException;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.mapper.AppealMapper;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.AppealEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.AppealRepository;
import com.technokratos.service.AppealService;
import com.technokratos.util.RequestParamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class AppealServiceImpl implements AppealService {

    private final AppealRepository appealRepository;

    private final AppealMapper appealMapper;

    private final RequestParamUtil requestParamUtil;

    private final AccountRepository accountRepository;

    @Transactional
    @Override
    public AppealResponse add(AppealRequest request, UUID accountId) {

        AccountEntity account = accountRepository.findById(accountId)
                .orElseThrow(UserNotFoundException::new);

        AppealEntity appealEntity = appealMapper.fromRequestToEntity(request);
        appealEntity.setCreatedDate(OffsetDateTime.now());
        appealEntity.setAuthor(account);
        appealEntity.setStatus(AppealEntity.Status.OPEN);

        account.getAppeals().add(appealEntity);
        accountRepository.save(account);

        return appealMapper.fromEntityToResponse(appealEntity);
    }

    @Transactional
    @Override
    public UUID delete(UUID id, UUID account) {

        AppealEntity appeal = appealRepository.findByIdAndAuthor_Id(id, account)
                .orElseThrow(AppealNotFoundException::new);
        appeal.setStatus(AppealEntity.Status.DELETED);
        AppealEntity updatedAppeal = appealRepository.save(appeal);

        return updatedAppeal.getId();
    }

    @Transactional(readOnly = true)
    @Override
    public Page<AppealResponse> getByAuthorAndStatus(String status, Integer size, Integer number, UUID authorId) {

        AccountEntity author = accountRepository.findById(authorId)
                .orElseThrow(UserNotFoundException::new);

        Sort sort = Sort.by(Sort.Direction.DESC, "createdDate");
        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);

        Page<AppealEntity> page;
        if (status != null) {

            checkStatusIsValid(status);
            page = appealRepository.findAllByAuthorAndStatus(author,AppealEntity.Status.valueOf(status), pageRequest.withSort(sort));

        }else{
            page = appealRepository.findAllByAuthor(author, pageRequest.withSort(sort));
        }
        return page.map(appealMapper::fromEntityToResponse);
    }

    @Transactional(readOnly = true)
    @Override
    public AppealResponse getById(UUID id, UUID authorId) {

        AppealEntity appealEntity = appealRepository.findByIdAndAuthor_Id(id, authorId)
                .orElseThrow(AppealNotFoundException::new);
        return appealMapper.fromEntityToResponse(appealEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public AppealResponse getById(UUID id) {

        return appealMapper.fromEntityToResponse(appealRepository.findById(id).orElseThrow(AppealNotFoundException::new));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<AppealResponse> getAllByStatus(String status, Integer size, Integer number) {

        PageRequest pageRequest = requestParamUtil.getPageRequest(size, number);

        Page<AppealEntity> page;
        if (status != null) {
            checkStatusIsValid(status);
            page = appealRepository.findAllByStatus(AppealEntity.Status.valueOf(status), pageRequest);
        }else{
            page = appealRepository.findAll(pageRequest);
        }
        return page.map(appealMapper::fromEntityToResponse);
    }

    private void checkStatusIsValid(String status) {
        try {
            AppealEntity.Status.valueOf(status);
        } catch (IllegalArgumentException e){
            throw new StatusInvalidException(status);
        }
    }

    @Transactional
    @Override
    public AppealResponse respondToAppeal(ResponseToAppeal responseToAppeal) {

        AppealEntity appealEntity = appealRepository.findById(responseToAppeal.getId())
                .orElseThrow(AppealNotFoundException::new);
        appealMapper.updateAppealEntity(responseToAppeal, appealEntity);
        appealEntity.setResponseDate(OffsetDateTime.now());
        appealEntity.setStatus(AppealEntity.Status.CLOSED);
        return appealMapper.fromEntityToResponse(appealRepository.save(appealEntity));
    }
}
