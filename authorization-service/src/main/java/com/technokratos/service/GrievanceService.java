package com.technokratos.service;


import com.technokratos.dto.request.GrievanceRequest;
import com.technokratos.dto.response.GrievanceResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface GrievanceService {

    GrievanceResponse add(GrievanceRequest request, UUID authorId);

    Page<GrievanceResponse> getByAuthor(Integer size, Integer number, UUID authorId);

    Page<GrievanceResponse> getByAccount(Integer size, Integer number, UUID id);

    Page<GrievanceResponse> getAll(Integer size, Integer number);

    UUID delete(UUID id);

    GrievanceResponse getById(UUID id);
}
