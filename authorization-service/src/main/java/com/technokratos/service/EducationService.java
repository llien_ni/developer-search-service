package com.technokratos.service;

import com.technokratos.dto.request.EducationRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.EducationResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface EducationService {

    EducationResponse add(EducationRequest request, UUID accountId);

    UUID delete(RequestID requestID, UUID id);

    Page<EducationResponse> getAllByDeveloper(Integer page, Integer size, UUID developerId);
}
