package com.technokratos.service;

import com.technokratos.dto.request.ChangeEmailRequest;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.ChangeEmailRequestEntity;

public interface ChangeEmailRequestService {

    ChangeEmailRequestEntity createChangeEmailRequest(ChangeEmailRequest request, AccountEntity account);

    ChangeEmailRequestEntity getByToken(String token);

    void deleteExpiredRequests();
}
