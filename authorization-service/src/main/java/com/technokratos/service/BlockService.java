package com.technokratos.service;

import com.technokratos.dto.request.BlockAccountRequest;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.BlockInfoResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface BlockService {

    AccountResponse unblockAccount(UUID id);

    BlockInfoResponse blockAccount(BlockAccountRequest request);

    Page<BlockInfoResponse> getByAccount(UUID accountId, Integer size, Integer number);
}
