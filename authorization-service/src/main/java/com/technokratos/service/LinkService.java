package com.technokratos.service;

import com.technokratos.dto.request.LinkRequest;
import com.technokratos.dto.request.UpdateLinkRequest;
import com.technokratos.dto.response.LinkResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface LinkService {

    LinkResponse add(LinkRequest request, UUID developerId);

    LinkResponse update(UpdateLinkRequest request, UUID developerId);

    UUID delete(UUID linkId, UUID developerId);

    LinkResponse getById(UUID id);

    Page<LinkResponse> findByDeveloper(UUID developerId, Integer size, Integer number);
}
