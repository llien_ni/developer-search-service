package com.technokratos.service;

import com.technokratos.dto.request.CreateDeveloperRequest;
import com.technokratos.dto.request.UpdateDeveloperRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.DeveloperEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

public interface DeveloperService {

    DeveloperResponse getDeveloperResponse(DeveloperEntity developer);

    DeveloperResponse save(CreateDeveloperRequest request);

    DeveloperResponse update(UUID id, UpdateDeveloperRequest request);

    UUID delete(UUID id);

    DeveloperResponse findById(UUID id);

    Page<DeveloperResponse> findAllByStateAndStatus(Integer size, Integer number, DeveloperState confirmed, Status status);

    Page<DeveloperResponse> findByIdsList(List<UUID> idList, Integer size, Integer number);

    DeveloperResponse findByUserName(String username);

    void updateDeveloperRating(UUID developerId, int value);

    DeveloperResponse findByIdAndStatus(UUID id, Status confirmed);

    void updateLastVisitDate(UUID developerId);
}

