package com.technokratos.service;

import com.technokratos.dto.request.AppealRequest;
import com.technokratos.dto.request.ResponseToAppeal;
import com.technokratos.dto.response.AppealResponse;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface AppealService {

    AppealResponse add(AppealRequest request, UUID account);

    UUID delete(UUID id, UUID account);

    Page<AppealResponse> getByAuthorAndStatus(String status, Integer size, Integer number, UUID account);

    AppealResponse getById(UUID id, UUID account);

    AppealResponse getById(UUID id);

    Page<AppealResponse> getAllByStatus(String status, Integer size, Integer number);

    AppealResponse respondToAppeal(ResponseToAppeal responseToAppeal);
}
