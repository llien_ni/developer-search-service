package com.technokratos.service;

import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.ConfirmationTokenEntity;

public interface ConfirmationTokenService {

    ConfirmationTokenEntity createTokenForAccount(AccountEntity account);

    ConfirmationTokenEntity getByToken(String token);

    void delete(ConfirmationTokenEntity confirmationToken);

    void deleteExpiredTokens();

    ConfirmationTokenEntity createCodeConfirmationForAccount(AccountEntity account);
}
