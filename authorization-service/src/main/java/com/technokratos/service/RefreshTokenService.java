package com.technokratos.service;

import com.technokratos.security.model.AccessAndRefreshTokens;

import javax.servlet.http.HttpServletRequest;

public interface RefreshTokenService {

    AccessAndRefreshTokens refreshTokens(HttpServletRequest request);
}
