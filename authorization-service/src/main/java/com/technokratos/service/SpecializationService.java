package com.technokratos.service;

import com.technokratos.dto.request.SpecializationRequest;
import com.technokratos.dto.request.SpecializationsListRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.SpecializationResponse;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

public interface SpecializationService {

    List<SpecializationResponse> addList(SpecializationsListRequest request, UUID developerId);

    SpecializationResponse findById(UUID specializationId);

    UUID delete(UUID specializationId, UUID developerId);

    Page<SpecializationResponse> getByDeveloper(Integer size, Integer number, UUID developerId);

    Page<String> getAllDistinctNames(Integer size, Integer number);

    Page<DeveloperResponse> getDevelopersBySpecializations(Integer size, Integer number, List<String> specializations);

    SpecializationResponse add(SpecializationRequest request, UUID id);
}
