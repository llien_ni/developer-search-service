package com.technokratos.service;

import com.technokratos.dto.request.CreateHrRequest;
import com.technokratos.dto.request.UpdateHrRequest;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.model.constant.Status;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface HrService {

    HrResponse save(CreateHrRequest request);

    HrResponse update(UUID id, UpdateHrRequest request);

    HrResponse getByIdAndStatus(UUID id, Status confirmed);

    Page<HrResponse> getAllByStatus(Status status, Integer size, Integer number);

    HrResponse confirmHr(UUID id);

    Page<HrResponse> getAllByStatusAndCompanyName(String companyName, Status confirmed, Integer size, Integer number);
}
