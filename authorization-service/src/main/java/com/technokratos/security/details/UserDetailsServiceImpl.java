package com.technokratos.security.details;


import com.technokratos.model.entity.AccountEntity;
import com.technokratos.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        Optional<AccountEntity> accountEntity = accountRepository
                .findByEmail(login);
        if(accountEntity.isEmpty()){
            accountEntity = accountRepository.findByPhoneNumber(login);
        }
        if(accountEntity.isEmpty()){
            try {
                UUID userId = UUID.fromString(login);
                return new UserDetailsImpl(accountRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User <" + login + "> not found")));
            }catch (IllegalArgumentException e){
                throw new UsernameNotFoundException("User <" + login + "> not found");
            }

        }else{

            return new UserDetailsImpl(accountEntity.get());
        }
    }
}
