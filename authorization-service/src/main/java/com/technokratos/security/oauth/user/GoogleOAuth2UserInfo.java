package com.technokratos.security.oauth.user;

import java.util.Map;

public class GoogleOAuth2UserInfo {

    protected Map<String, Object> attributes;

    public GoogleOAuth2UserInfo(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public String getId() {

        return (String) attributes.get("sub");
    }

    public String getName() {

        return (String) attributes.get("name");
    }

    public String getFirstName() {

        return (String) attributes.get("given_name");
    }

    public String getLastName() {

        return (String) attributes.get("family_name");
    }

    public String getEmail() {

        return (String) attributes.get("email");
    }

}
