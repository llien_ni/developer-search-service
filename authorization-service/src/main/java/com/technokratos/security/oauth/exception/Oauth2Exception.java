package com.technokratos.security.oauth.exception;

import org.springframework.security.core.AuthenticationException;

public class Oauth2Exception extends AuthenticationException {

    public Oauth2Exception(String detail) {
        super(detail);
    }
}
