package com.technokratos.security.oauth.handler;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Role;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.security.model.AccessAndRefreshTokens;
import com.technokratos.security.oauth.dto.InvalidRoleResponse;
import com.technokratos.security.oauth.repository.HttpCookieOAuth2AuthorizationRequestRepository;
import com.technokratos.security.oauth.util.CookieUtils;
import com.technokratos.security.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

import static com.technokratos.security.oauth.repository.HttpCookieOAuth2AuthorizationRequestRepository.OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME;


@Component
@RequiredArgsConstructor
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository;

    private final ObjectMapper objectMapper;

    private final JwtUtil jwtUtil;

    private final AccountRepository accountRepository;

    private final DeveloperRepository developerRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        AccountEntity account = userDetails.getAccount();

        //пользователь уже был зарегистрирован
        if (!account.getStatus().equals(Status.NOT_CONFIRMED)) {
            clearAuthenticationAttributes(request, response);
            writeAccessAndRefreshTokens(request, response, account);
        } else {
            //пользователь не был зарегистрирован
            DecodedJWT decodedJWT = CookieUtils.getCookie(request, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME)
                    .map(httpCookieOAuth2AuthorizationRequestRepository::decodeTokenFromCookie)
                    .orElse(null);

            Object role = null;
            if (decodedJWT != null) {
                role = decodedJWT.getClaim("additionalParameters").asMap().get("role");

                if (role != null && (role.toString().equals(Role.ROLE_DEVELOPER.toString()) ||
                        role.toString().equals(Role.ROLE_SIMPLE_ACCOUNT.toString()))) {

                    account.setRole(Role.valueOf(role.toString()));
                    account.setStatus(Status.CONFIRMED);
                    AccountEntity savedAccount = accountRepository.save(account);

                    if(role.toString().equals(Role.ROLE_DEVELOPER.toString())){
                        DeveloperEntity developer = new DeveloperEntity();
                        developer.setId(savedAccount.getId());
                        developer.setState(DeveloperState.ACTIVE);
                        developer.setLastVisitDate(OffsetDateTime.now());
                        developer.setRating(BigDecimal.ZERO);
                        developerRepository.save(new DeveloperEntity());
                    }

                    clearAuthenticationAttributes(request, response);
                    writeAccessAndRefreshTokens(request, response, account);
                }
            }
            if (decodedJWT == null || role == null){
                accountRepository.delete(account);
                clearAuthenticationAttributes(request, response);
                sendErrorResponse(response);
            }
        }
    }

    private void sendErrorResponse(HttpServletResponse response) throws IOException {

        InvalidRoleResponse invalidRoleResponse = InvalidRoleResponse.builder()
                .error("Enter invalid role")
                .build();
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        objectMapper.writeValue(response.getOutputStream(), invalidRoleResponse);
    }

    private void writeAccessAndRefreshTokens(HttpServletRequest request, HttpServletResponse response, AccountEntity account) throws IOException {

        response.setContentType("application/json");
        AccessAndRefreshTokens tokens = jwtUtil.generateAccessAndRefreshTokens(
                new UserDetailsImpl(account), request.getRequestURL().toString());
        objectMapper.writeValue(response.getOutputStream(), tokens);
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request, HttpServletResponse response) {

        super.clearAuthenticationAttributes(request);
        httpCookieOAuth2AuthorizationRequestRepository.removeAuthorizationRequestCookies(request, response);
    }
}
