package com.technokratos.security.oauth.service;


import com.technokratos.model.constant.RegistrationProvider;
import com.technokratos.model.constant.Role;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.security.oauth.exception.Oauth2Exception;
import com.technokratos.security.oauth.user.GoogleOAuth2UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor
public class OAuth2UserService extends DefaultOAuth2UserService {

    private final AccountRepository accountRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {

        OAuth2User user = super.loadUser(userRequest);
        GoogleOAuth2UserInfo googleOAuth2UserInfo = new GoogleOAuth2UserInfo(user.getAttributes());

        Optional<AccountEntity> userInDataBase = accountRepository.findByEmail(googleOAuth2UserInfo.getEmail());
        AccountEntity account;

        if (userInDataBase.isEmpty()) {

            AccountEntity accountEntity = AccountEntity.builder()
                    .email(googleOAuth2UserInfo.getEmail())
                    .firstName(googleOAuth2UserInfo.getFirstName())
                    .lastName(googleOAuth2UserInfo.getLastName())
                    .status(Status.NOT_CONFIRMED)
                    .role(Role.ROLE_SIMPLE_ACCOUNT)
                    .registrationProvider(RegistrationProvider.GOOGLE)
                    .build();
            account = accountRepository.save(accountEntity);

        } else if (userInDataBase.get().getRegistrationProvider().equals(RegistrationProvider.NATIVE)) {

            throw new Oauth2Exception("user_already_signed_up");
        } else {

            account = userInDataBase.get();
        }

        return new UserDetailsImpl(account, user.getAttributes());
    }
}
