package com.technokratos.security.oauth.repository;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.technokratos.security.oauth.util.CookieUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;


@Component
@RequiredArgsConstructor
public class HttpCookieOAuth2AuthorizationRequestRepository implements AuthorizationRequestRepository<OAuth2AuthorizationRequest> {

    public static final String OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME = "oauth2_auth_request";

    public static final String REDIRECT_URI_PARAM_COOKIE_NAME = "redirect_uri";

    private static final int COOKIE_EXPIRE_SECONDS = 600;

    private static final String SECRET_KEY = "ratsAreCute";

    private static final Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY.getBytes(StandardCharsets.UTF_8));

    @Override
    public OAuth2AuthorizationRequest loadAuthorizationRequest(HttpServletRequest request) {

        return CookieUtils.getCookie(request, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME)
                .map(this::toOAuth2AuthorizationRequest)
                .orElse(null);
    }

    @Override
    public void saveAuthorizationRequest(OAuth2AuthorizationRequest authorizationRequest, HttpServletRequest request, HttpServletResponse response) {

        if (authorizationRequest == null) {
            CookieUtils.deleteCookie(request, response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME);
            CookieUtils.deleteCookie(request, response, REDIRECT_URI_PARAM_COOKIE_NAME);
            return;
        }

        Map<String, Object> additionalParameters = authorizationRequest.getAdditionalParameters()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> String.valueOf(e.getValue())));

        String role = request.getParameter("role");

        if (role != null) {
            additionalParameters.put("role", role);
        }

        String token = createToken(authorizationRequest, additionalParameters);
        CookieUtils.addCookie(response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME, token, COOKIE_EXPIRE_SECONDS);

        String redirectUriAfterLogin = request.getParameter(REDIRECT_URI_PARAM_COOKIE_NAME);
        if (redirectUriAfterLogin != null) {
            CookieUtils.addCookie(response, REDIRECT_URI_PARAM_COOKIE_NAME, redirectUriAfterLogin, COOKIE_EXPIRE_SECONDS);
        }
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(HttpServletRequest request) {

        return this.loadAuthorizationRequest(request);
    }

    public void removeAuthorizationRequestCookies(HttpServletRequest request, HttpServletResponse response) {

        CookieUtils.deleteCookie(request, response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME);
        CookieUtils.deleteCookie(request, response, REDIRECT_URI_PARAM_COOKIE_NAME);
    }

    private String createToken(OAuth2AuthorizationRequest authorizationRequest, Map<String, Object> additionalParameters) {

        return JWT.create()
                .withClaim("attributes", authorizationRequest.getAttributes())
                .withClaim("additionalParameters", additionalParameters)
                .withClaim("client-id", authorizationRequest.getClientId())
                .withClaim("authorization-uri", authorizationRequest.getAuthorizationUri())
                .withClaim("redirect-uri", authorizationRequest.getRedirectUri())
                .withClaim("scopes", new ArrayList<>(authorizationRequest.getScopes()))
                .withClaim("state", authorizationRequest.getState())
                .sign(algorithm);
    }

    public DecodedJWT decodeTokenFromCookie(Cookie cookie){

        JWTVerifier jwtVerifier = JWT.require(algorithm).build();
        return jwtVerifier.verify(cookie.getValue());
    }

    private OAuth2AuthorizationRequest toOAuth2AuthorizationRequest(Cookie cookie) {

        DecodedJWT decodedJWT = decodeTokenFromCookie(cookie);
        return OAuth2AuthorizationRequest.authorizationCode()
                .additionalParameters(decodedJWT.getClaim("additionalParameters").asMap())
                .attributes(decodedJWT.getClaim("attributes").asMap())
                .clientId(decodedJWT.getClaim("client-id").asString())
                .authorizationUri(decodedJWT.getClaim("authorization-uri").asString())
                .redirectUri(decodedJWT.getClaim("redirect-uri").asString())
                .scopes(new HashSet<>(decodedJWT.getClaim("scopes").asList(String.class)))
                .state(decodedJWT.getClaim("state").asString())
                .build();
    }
}
