package com.technokratos.security.util;


import com.auth0.jwt.exceptions.JWTVerificationException;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import com.technokratos.exception.ParseTokenException;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.model.constant.Role;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.RefreshTokenEntity;
import com.technokratos.redis.service.RedisAccountService;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.RefreshTokenRepository;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.security.model.AccessAndRefreshTokens;
import com.technokratos.service.DeveloperService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;


@Slf4j
@Component
@RequiredArgsConstructor
public class JwtUtil {

    @Value("${application.token.access.expires.time}")
    private long accessTokenExpiresTime;

    @Value("${application.token.refresh.expires.time}")
    private long refreshTokenExpiresTime;

    @Value("${jwt.private-key}")
    private String privateKeyFileName;

    private final AccountRepository accountRepository;

    private final RefreshTokenRepository refreshTokenRepository;

    private final RedisAccountService redisAccountService;

    private final DeveloperService developerService;

    private RSAKey getPrivateKey() {

        RSAKey jwk = null;
        try (FileInputStream inputStream = new FileInputStream(privateKeyFileName)){
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(inputStream, "qwerty007".toCharArray());

            String alias = keyStore.aliases().nextElement();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyStore.getKey(alias, "qwerty007".toCharArray());
            X509Certificate certificate = (X509Certificate) keyStore.getCertificate(alias);

            jwk = new RSAKey.Builder(RSAKey.parse(certificate))
                    .privateKey(privateKey)
                    .build();

        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | UnrecoverableKeyException | JOSEException e) {
            e.printStackTrace();
        }
        return jwk;
    }

    public AccessAndRefreshTokens generateAccessAndRefreshTokens(UserDetailsImpl userDetails, String issuer) {

        String accessToken = null;
        String refreshToken = null;

        try {
            RSAKey serverJWK = getPrivateKey();
            JWSHeader jwtHeader = new JWSHeader.Builder(JWSAlgorithm.RS256).base64URLEncodePayload(true).build();
            String jwtID = String.valueOf(new Date(System.currentTimeMillis()).getTime());
            JWTClaimsSet accessTokenClaims = new JWTClaimsSet.Builder()
                    .subject(userDetails.getAccount().getId().toString())
                    .issuer(issuer)
                    .expirationTime(new Date(System.currentTimeMillis() + accessTokenExpiresTime))
                    .claim("role", userDetails.getAccount().getRole())
                    .jwtID(jwtID)
                    .build();

            SignedJWT jwsAccess = new SignedJWT(jwtHeader, accessTokenClaims);
            RSASSASigner signer = new RSASSASigner(serverJWK);
            jwsAccess.sign(signer);

            accessToken = jwsAccess.serialize();

            JWTClaimsSet refreshTokenClaims = new JWTClaimsSet.Builder()
                    .subject(userDetails.getAccount().getId().toString())
                    .issuer(issuer)
                    .expirationTime(new Date(System.currentTimeMillis() + refreshTokenExpiresTime))
                    .claim("role", userDetails.getAccount().getRole())
                    .jwtID(jwtID)
                    .build();

            SignedJWT jwsRefresh = new SignedJWT(jwtHeader, refreshTokenClaims);
            jwsRefresh.sign(signer);
            refreshToken = jwsRefresh.serialize();

        } catch (JOSEException e) {
            e.printStackTrace();
        }
        AccessAndRefreshTokens tokens = AccessAndRefreshTokens.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
        saveRefreshToken(userDetails, tokens);

        redisAccountService.addTokenToAccount(userDetails.getAccount(), accessToken);

        if(userDetails.getAccount().getRole().equals(Role.ROLE_DEVELOPER)){
            developerService.updateLastVisitDate(userDetails.getAccount().getId());
        }
        return tokens;
    }

    public Authentication buildAuthentication(String token) {

        ParsedToken parsedToken = parse(token);
        UserDetailsImpl userDetails = new UserDetailsImpl(AccountEntity.builder()
                .id(UUID.fromString(parsedToken.getId()))
                .role(Role.valueOf(parsedToken.getRole()))
                .build());
        return new UsernamePasswordAuthenticationToken(userDetails,
                null,
                Collections.singleton(new SimpleGrantedAuthority(parsedToken.getRole())));
    }

    private ParsedToken parse(String token) {

        try {
            JWT jwt = JWTParser.parse(token);
            SignedJWT jws = (SignedJWT) jwt;

            RSAKey privateKey = getPrivateKey();
            RSASSAVerifier signVerifier = new RSASSAVerifier(privateKey);

            if (jws.verify(signVerifier)) {
                log.info("Token is valid");

                JWTClaimsSet claims = jws.getJWTClaimsSet();

                String role = claims.getClaim("role").toString();
                String id = claims.getSubject();

                return ParsedToken.builder()
                        .id(id)
                        .role(role)
                        .build();
            }else{
                log.info("Token is invalid");
                throw  new JWTVerificationException("Token is invalid");
            }
        } catch (ParseException | JOSEException e) {
            throw new ParseTokenException();
        }
    }

    public void saveRefreshToken(UserDetailsImpl userDetails, AccessAndRefreshTokens tokens) {

        AccountEntity account = accountRepository
                .findById(userDetails.getAccount().getId())
                .orElseThrow(UserNotFoundException::new);

        RefreshTokenEntity refreshToken = RefreshTokenEntity.builder()
                .value(tokens.getRefreshToken())
                .account(account)
                .build();

        refreshTokenRepository.save(refreshToken);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @lombok.Builder
    private static class ParsedToken {

        private String id;

        private String role;
    }
}
