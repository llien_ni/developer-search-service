package com.technokratos.converter;

import com.technokratos.dto.response.CompanyResponse;
import company.service.CompanyServiceOuterClass;

import java.util.UUID;

public class CompanyResponseConverter {

    public static CompanyServiceOuterClass.CompanyResponse toGrpcResponse(CompanyResponse companyResponse){

        return CompanyServiceOuterClass.CompanyResponse
                .newBuilder()
                .setId(companyResponse.getId().toString())
                .setName(companyResponse.getName())
                .setDescription(companyResponse.getDescription() == null ? "" : companyResponse.getDescription())
                .setEmail(companyResponse.getEmail())
                .setStatus(companyResponse.getStatus())
                .setType(companyResponse.getType())
                .setWebsiteLink(companyResponse.getWebsiteLink())
                .build();

    }

    public static CompanyResponse fromGrpcResponse(CompanyServiceOuterClass.CompanyResponse companyResponse) {

        return CompanyResponse
                .builder()
                .id(UUID.fromString(companyResponse.getId()))
                .name(companyResponse.getName())
                .description(companyResponse.getDescription())
                .email(companyResponse.getEmail())
                .status(companyResponse.getStatus())
                .type(companyResponse.getType())
                .websiteLink(companyResponse.getWebsiteLink())
                .build();
    }
}
