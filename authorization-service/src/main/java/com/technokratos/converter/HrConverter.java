package com.technokratos.converter;

import com.technokratos.client.VacancyServiceGrpcClient;
import com.technokratos.dto.response.CompanyResponse;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.mapper.HrMapper;
import com.technokratos.model.entity.HrEntity;
import com.technokratos.service.PhotoService;
import company.service.CompanyServiceOuterClass;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class HrConverter {

    private final HrMapper hrMapper;

    private final PhotoService photoService;

    private final VacancyServiceGrpcClient vacancyServiceGrpcClient;

    public HrResponse convertToHrResponse(HrEntity hrEntity){

        HrResponse hrResponse = hrMapper.fromEntityToResponse(hrEntity);
        if(hrEntity.getPhoto()!=null){
            PhotoResponse photo = photoService.getPhotoResponse(hrEntity.getPhoto());
            hrResponse.setPhoto(photo);
        }
        return hrResponse;
    }

    public HrResponse addCompanyResponseToHrResponse(HrResponse hrResponse, UUID companyId){

        CompanyResponse companyResponse = getCompanyResponse(companyId);
        hrResponse.setCompanyResponse(companyResponse);
        return hrResponse;
    }

    public CompanyResponse getCompanyResponse(UUID companyId){

        CompanyServiceOuterClass.CompanyResponse companyResponse = vacancyServiceGrpcClient.getCompanyById(CompanyServiceOuterClass.GetCompanyByIdRequest.newBuilder()
                .setCompanyId(companyId.toString())
                .build());

        return CompanyResponseConverter.fromGrpcResponse(companyResponse);
    }
}
