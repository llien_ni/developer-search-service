package com.technokratos.scheduling;


import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.BlockInfoEntity;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.repository.BlockInfoRepository;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.service.ChangeEmailRequestService;
import com.technokratos.service.ConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;


@Slf4j
@Component
@RequiredArgsConstructor
public class ScheduledTasks {

    private final DeveloperRepository developerRepository;

    private final ConfirmationTokenService confirmationTokenService;

    private final ChangeEmailRequestService changeEmailRequestService;

    private final BlockInfoRepository blockInfoRepository;

    @Scheduled(fixedRate = 86400000)
    public void unblockAccount() {

        long now = OffsetDateTime.now().toEpochSecond();
        List<BlockInfoEntity> list = blockInfoRepository.findAllByActiveTrue();
        list.forEach(blockInfo -> {
            if (blockInfo.getFinishDate() != null) {

                long finishDate = blockInfo.getFinishDate().toEpochSecond();
                if (now >= finishDate) {
                    blockInfo.setActive(false);
                    blockInfo.getAccount().setStatus(Status.CONFIRMED);
                    blockInfoRepository.save(blockInfo);
                }
            }
        });
    }

    @Scheduled(fixedRate = 86400000)
    public void setDeveloperState() {

        List<DeveloperEntity> developers = developerRepository.findAllByState(DeveloperState.ACTIVE);
        developers.forEach(developer -> {
            if(developer.getLastVisitDate()!=null){
                long now = OffsetDateTime.now().toEpochSecond();
                long lastVisitDate = developer.getLastVisitDate().toEpochSecond();
                //15768000 == 6 months
                long sixMonths = 15768000;
                if (now - lastVisitDate >= sixMonths) {
                    developer.setState(DeveloperState.NOT_ACTIVE);
                    developerRepository.save(developer);
                }
            }

        });
    }

    //2592000000 = 30 days
    @Scheduled(fixedRate = 2592000000L)
    public void deleteTokens() {

        confirmationTokenService.deleteExpiredTokens();
    }

    //2592000000 = 30 days
    @Scheduled(fixedRate = 2592000000L)
    public void deleteChangeEmailRequests() {

        changeEmailRequestService.deleteExpiredRequests();
    }
}
