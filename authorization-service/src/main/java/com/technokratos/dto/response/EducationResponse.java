package com.technokratos.dto.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EducationResponse {

    private UUID id;

    private String type;

    private String name;

    private LocalDate startDate;

    private LocalDate finishDate;
}
