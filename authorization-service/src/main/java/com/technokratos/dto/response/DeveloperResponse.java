package com.technokratos.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.SuperBuilder;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Setter
@Getter
@ToString
@EqualsAndHashCode(callSuper=false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeveloperResponse extends AccountResponse{

    private String username;

    private LocalDate dateOfBirth;

    private OffsetDateTime lastVisitDate;

    private String state;

    private Integer workExperience;

    private String city;

    private Long rating;

    private List<EducationResponse> educations;

    private List<SpecializationResponse> specializations;

    private List<LinkResponse> links;
}
