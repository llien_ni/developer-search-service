package com.technokratos.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppealResponse {

    private UUID id;

    private AccountResponse author;

    private String title;

    private String text;

    private OffsetDateTime createdDate;

    private String response;

    private OffsetDateTime responseDate;

    private String status;
}
