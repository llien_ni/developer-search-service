package com.technokratos.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class GrievanceResponse {

    private UUID id;

    private String text;

    private OffsetDateTime createdDate;

    private AccountResponse author;

    private AccountResponse account;
}
