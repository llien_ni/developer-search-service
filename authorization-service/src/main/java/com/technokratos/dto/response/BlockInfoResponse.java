package com.technokratos.dto.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BlockInfoResponse {

    private UUID id;

    private OffsetDateTime startDate;

    private OffsetDateTime finishDate;

    private String reason;

    private AccountResponse account;
}
