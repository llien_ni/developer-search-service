package com.technokratos.dto.request;


import com.technokratos.validation.constraint.EmailOrPhoneNumberNotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
@EmailOrPhoneNumberNotEmpty
public class CreateAccountRequest {

    @NotBlank
    @Size(max = 20)
    private String firstName;

    @NotBlank
    @Size(max = 20)
    private String lastName;

    @Pattern(regexp = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", message = "Phone number was entered incorrectly")
    private String phoneNumber;

    @Pattern(regexp = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message = "Email was entered incorrectly")
    private String email;

    @NotBlank
    @Size(min = 8, max = 20)
    private String password;

    private String city;
}
