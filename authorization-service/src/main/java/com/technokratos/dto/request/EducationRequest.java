package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EducationRequest {

    @NotBlank
    private String type;

    @NotBlank
    private String name;

    @NotNull
    private LocalDate startDate;

    private LocalDate finishDate;
}
