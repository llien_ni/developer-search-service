package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Data
public class CreateDeveloperRequest extends CreateAccountRequest {

    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotNull
    private LocalDate dateOfBirth;

    @PositiveOrZero
    private Integer workExperience;
}
