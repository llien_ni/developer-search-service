package com.technokratos.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PhoneOrEmailRequest {

    @Enumerated(value = EnumType.STRING)
    private LoginType loginType;

    private String login;

    public enum LoginType{
        EMAIL, PHONE_NUMBER
    }
}
