package com.technokratos.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

@Configuration
public class RabbitMQConfigListener implements RabbitListenerConfigurer {

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Override
    public void configureRabbitListeners(
            RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(myHandlerMethodFactory());
    }

    @Bean
    public DefaultMessageHandlerMethodFactory myHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(new MappingJackson2MessageConverter());
        return factory;
    }


    @Bean
    public Binding questionServiceBinding(Queue queueSendNotificationFromQuestionService, DirectExchange exchange) {

        return BindingBuilder.bind(queueSendNotificationFromQuestionService).to(exchange).with("questions-service");
    }

    @Bean
    public Queue queueSendNotificationFromQuestionService() {

        return QueueBuilder.durable("q.sending-from-questions-service")
                .withArgument("x-dead-letter-exchange", "x.questions-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaForQuestionsServiceQueue() {

        return new Declarables(
                new DirectExchange("x.questions-failure"),
                new Queue("q.fall-back-questions"),
                new Binding("q.fall-back-questions", Binding.DestinationType.QUEUE, "x.questions-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public Binding eventServiceBinding(Queue queueEventService, DirectExchange exchange) {

        return BindingBuilder.bind(queueEventService).to(exchange).with("event-service");
    }

    @Bean
    public Queue queueEventService() {
        return QueueBuilder.durable("q.from-event-service-to-auth-service")
                .withArgument("x-dead-letter-exchange", "x.from-event-service-to-auth-service-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaFromEventServiceToAuth() {
        return new Declarables(
                new DirectExchange("x.from-event-service-to-auth-service-failure"),
                new Queue("q.fall-back-from-event-service-to-auth-service"),
                new Binding("q.fall-back-from-event-service-to-auth-service", Binding.DestinationType.QUEUE, "x.from-event-service-to-auth-service-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public Binding fromVacancyToAuthServiceBinding(Queue queueFromVacancyServiceToAuthService, DirectExchange exchange) {
        return BindingBuilder.bind(queueFromVacancyServiceToAuthService).to(exchange).with("vacancy-service");
    }

    @Bean
    public Queue queueFromVacancyServiceToAuthService() {
        return QueueBuilder.durable("q.from-vacancy-service-to-auth-service")
                .withArgument("x-dead-letter-exchange", "x.from-vacancy-service-to-auth-service-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaFromVacancyServiceToAuthService() {
        return new Declarables(
                new DirectExchange("x.from-vacancy-service-to-auth-service-failure"),
                new Queue("q.fall-back-from-vacancy-service-to-auth-service"),
                new Binding("q.fall-back-from-vacancy-service-to-auth-service", Binding.DestinationType.QUEUE, "x.from-vacancy-service-to-auth-service-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public Binding fromProjectServiceToAuthServiceBinding(Queue queueFromProjectServiceToAuthService, DirectExchange exchange) {
        return BindingBuilder.bind(queueFromProjectServiceToAuthService).to(exchange).with("project-service");
    }

    @Bean
    public Queue queueFromProjectServiceToAuthService() {
        return QueueBuilder.durable("q.from-project-service-to-auth-service")
                .withArgument("x-dead-letter-exchange", "x.from-project-service-to-auth-service-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaFromProjectServiceToAuthService() {
        return new Declarables(
                new DirectExchange("x.from-project-service-to-auth-service-failure"),
                new Queue("q.fall-back-from-project-service-to-auth-service"),
                new Binding("q.fall-back-from-project-service-to-auth-service", Binding.DestinationType.QUEUE, "x.from-project-service-to-auth-service-failure",
                        "fall-back", null)
        );
    }
}
