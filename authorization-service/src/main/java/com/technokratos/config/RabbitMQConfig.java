package com.technokratos.config;


import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;

@Configuration
@RequiredArgsConstructor
public class RabbitMQConfig {

    private final CachingConnectionFactory cachingConnectionFactory;

    @Bean
    public DirectExchange exchange() {

        return new DirectExchange("direct-exchange");
    }

    @Bean
    public Binding authorizationServiceBinding(Queue queueSendNotification, DirectExchange exchange) {
        return BindingBuilder.bind(queueSendNotification).to(exchange).with("authorization-service");
    }

    @Bean
    public Queue queueSendNotification() {
        return QueueBuilder.durable("q.sending-notification-from-authorization-service")
                .withArgument("x-dead-letter-exchange", "x.authorization-service-notification-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchema() {
        return new Declarables(
                new DirectExchange("x.authorization-service-notification-failure"),
                new Queue("q.fall-back-authorization-service-notification"),
                new Binding("q.fall-back-authorization-service-notification", Binding.DestinationType.QUEUE, "x.authorization-service-notification-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public Binding eventNotificationBinding(Queue queueEventServiceSendNotification, DirectExchange exchange) {
        return BindingBuilder.bind(queueEventServiceSendNotification).to(exchange).with("event-service-notification");
    }

    @Bean
    public Queue queueEventServiceSendNotification() {
        return QueueBuilder.durable("q.notification-about-event")
                .withArgument("x-dead-letter-exchange", "x.notification-about-event-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaEventNotification() {
        return new Declarables(
                new DirectExchange("x.notification-about-event-failure"),
                new Queue("q.fall-back-notification-about-event"),
                new Binding("q.fall-back-notification-about-event", Binding.DestinationType.QUEUE, "x.notification-about-event-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public Binding vacancyServiceBinding(Queue queueSendVacancyNotification, DirectExchange exchange) {
        return BindingBuilder.bind(queueSendVacancyNotification).to(exchange).with("vacancy-service-notification");
    }

    @Bean
    public Queue queueSendVacancyNotification() {
        return QueueBuilder.durable("q.vacancy-service-notification")
                .withArgument("x-dead-letter-exchange", "x.vacancy-notification-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaForVacancyNotification() {
        return new Declarables(
                new DirectExchange("x.vacancy-notification-failure"),
                new Queue("q.fall-back-vacancy-notification"),
                new Binding("q.fall-back-vacancy-notification", Binding.DestinationType.QUEUE, "x.vacancy-notification-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public Binding projectServiceBinding(Queue queueSendProjectNotification, DirectExchange exchange) {
        return BindingBuilder.bind(queueSendProjectNotification).to(exchange).with("project-service-notification");
    }

    @Bean
    public Queue queueSendProjectNotification() {
        return QueueBuilder.durable("q.project-service-notification")
                .withArgument("x-dead-letter-exchange", "x.project-notification-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createDeadLetterSchemaForProjectNotification() {
        return new Declarables(
                new DirectExchange("x.project-notification-failure"),
                new Queue("q.fall-back-project-notification"),
                new Binding("q.fall-back-project-notification", Binding.DestinationType.QUEUE, "x.project-notification-failure",
                        "fall-back", null)
        );
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(SimpleRabbitListenerContainerFactoryConfigurer configurer) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, cachingConnectionFactory);
        factory.setAcknowledgeMode(AcknowledgeMode.AUTO);
        factory.setAdviceChain(retryInterceptor());
        factory.setDefaultRequeueRejected(false);
        return factory;
    }

    @Bean
    public RetryOperationsInterceptor retryInterceptor() {
        return RetryInterceptorBuilder.stateless().maxAttempts(3)
                .backOffOptions(2000, 2.0, 100000)
                .recoverer(new RejectAndDontRequeueRecoverer())
                .build();
    }

    @Bean
    public Jackson2JsonMessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(Jackson2JsonMessageConverter converter) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(cachingConnectionFactory);
        rabbitTemplate.setMessageConverter(converter);
        return rabbitTemplate;
    }

}
