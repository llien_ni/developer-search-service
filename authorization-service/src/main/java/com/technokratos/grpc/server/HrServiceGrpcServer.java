package com.technokratos.grpc.server;


import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.exception.HrNotFoundException;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.HrEntity;
import com.technokratos.repository.HrRepository;
import com.technokratos.service.PhotoService;
import hr.service.HrServiceGrpc;
import hr.service.HrServiceOuterClass;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.UUID;

@Slf4j
@GrpcService
@RequiredArgsConstructor
public class HrServiceGrpcServer extends HrServiceGrpc.HrServiceImplBase {

    private final HrRepository hrRepository;

    private final PhotoService photoService;

    @Override
    public void getHrById(HrServiceOuterClass.GetHrByIdRequest request, StreamObserver<HrServiceOuterClass.HrResponse> responseObserver) {

        HrEntity hr = hrRepository.findByIdAndStatus(UUID.fromString(request.getId()), Status.CONFIRMED).orElseThrow(HrNotFoundException::new);

        HrServiceOuterClass.HrResponse.Builder hrResponseBuilder = HrServiceOuterClass.HrResponse.newBuilder()
                .setId(hr.getId().toString())
                .setEmail(hr.getEmail())
                .setFirstName(hr.getFirstName())
                .setLastname(hr.getLastName())
                .setCity(hr.getCity() != null ? hr.getCity() : "")
                .setCompanyId(hr.getCompanyId().toString());


        if(hr.getPhoto()!=null){
            PhotoResponse photoResponse = photoService.getPhotoResponse(hr.getPhoto());
            hrResponseBuilder.setPhoto(photoResponse.getLink());

        }else{
            hrResponseBuilder.setPhoto("");
        }
        HrServiceOuterClass.HrResponse response = hrResponseBuilder.build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
