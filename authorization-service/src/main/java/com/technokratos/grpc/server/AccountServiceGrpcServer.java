package com.technokratos.grpc.server;


import authorization.service.AccountServiceGrpc;
import authorization.service.AccountServiceOuterClass;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.service.AccountService;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.UUID;


@Slf4j
@GrpcService
@RequiredArgsConstructor
public class AccountServiceGrpcServer extends AccountServiceGrpc.AccountServiceImplBase {

    private final AccountService accountService;

    @Override
    public void getAccountById(AccountServiceOuterClass.GetAccountByIdRequest request, StreamObserver<AccountServiceOuterClass.AccountResponse> responseObserver) {

        AccountResponse accountResponse = accountService.getById(UUID.fromString(request.getId()));
        AccountServiceOuterClass.AccountResponse.Builder accountResponseBuilder = AccountServiceOuterClass.AccountResponse
                .newBuilder()
                .setId(accountResponse.getId().toString())
                .setFirstName(accountResponse.getFirstName())
                .setLastname(accountResponse.getLastName())
                .setEmail(accountResponse.getEmail())
                .setRole(accountResponse.getRole())
                .setStatus(accountResponse.getStatus());

        if (accountResponse.getPhoto() != null) {
            accountResponseBuilder.setPhoto(accountResponse.getPhoto().getLink());
        } else {
            accountResponseBuilder.setPhoto("");
        }
        AccountServiceOuterClass.AccountResponse response = accountResponseBuilder.build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();

    }
}
