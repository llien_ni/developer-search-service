package com.technokratos.grpc.server;


import authorization.service.DeveloperServiceGrpc;
import authorization.service.DeveloperServiceOuterClass;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.service.DeveloperService;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.UUID;

@Slf4j
@GrpcService
@RequiredArgsConstructor
public class DeveloperServiceGrpcServer extends DeveloperServiceGrpc.DeveloperServiceImplBase {

    private final DeveloperService developerService;

    @Override
    public void getDeveloperById(DeveloperServiceOuterClass.GetDeveloperByIdRequest request, StreamObserver<DeveloperServiceOuterClass.DeveloperResponse> responseObserver) {

        DeveloperResponse developerResponse = developerService.findById(UUID.fromString(request.getId()));

        DeveloperServiceOuterClass.DeveloperResponse.Builder developerResponseBuilder = DeveloperServiceOuterClass.DeveloperResponse.newBuilder()
                .setId(developerResponse.getId().toString())
                .setFirstName(developerResponse.getFirstName())
                .setLastname(developerResponse.getLastName())
                .setEmail(developerResponse.getEmail())
                .setUsername(developerResponse.getUsername());

        if(developerResponse.getPhoto()!=null){
            developerResponseBuilder.setPhoto(developerResponse.getPhoto().getLink());
        }else{
            developerResponseBuilder.setPhoto("");
        }
        DeveloperServiceOuterClass.DeveloperResponse response = developerResponseBuilder.build();


        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getDeveloperByUsername(DeveloperServiceOuterClass.GetDeveloperByUsernameRequest request, StreamObserver<DeveloperServiceOuterClass.DeveloperResponse> responseObserver) {

        DeveloperResponse developerResponse = developerService.findByUserName(request.getUsername());

        DeveloperServiceOuterClass.DeveloperResponse response = DeveloperServiceOuterClass.DeveloperResponse.newBuilder()
                .setId(developerResponse.getId().toString())
                .setFirstName(developerResponse.getFirstName())
                .setLastname(developerResponse.getLastName())
                .setEmail(developerResponse.getEmail())
                .setUsername(developerResponse.getUsername())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}

