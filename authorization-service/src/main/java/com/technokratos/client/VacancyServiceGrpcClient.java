package com.technokratos.client;


import company.service.CompanyServiceGrpc;
import company.service.CompanyServiceOuterClass;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

@Service
public class VacancyServiceGrpcClient {

    @GrpcClient("vacancy-service")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    public CompanyServiceOuterClass.CompanyResponse getCompanyByName(CompanyServiceOuterClass.GetCompanyByNameRequest request){
        return companyServiceBlockingStub.getCompanyByName(request);
    }

    public CompanyServiceOuterClass.CompanyResponse getCompanyById(CompanyServiceOuterClass.GetCompanyByIdRequest request){
        return companyServiceBlockingStub.getCompanyById(request);
    }
}
