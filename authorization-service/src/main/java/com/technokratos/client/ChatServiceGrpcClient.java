package com.technokratos.client;


import chat.service.ChatServiceGrpc;
import chat.service.ChatServiceOuterClass;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ChatServiceGrpcClient {

    @GrpcClient("chat-service")
    private ChatServiceGrpc.ChatServiceBlockingStub chatServiceBlockingStub;

    public void setChatStateToNotActiveByAccount(UUID accountId) {

        ChatServiceOuterClass.AccountIdRequest request = ChatServiceOuterClass.AccountIdRequest.newBuilder()
                .setAccountId(accountId.toString())
                .build();

        chatServiceBlockingStub.setChatStateToNotActiveByAccount(request);
    }

    public void setChatStateToActiveByAccount(UUID accountId) {

        ChatServiceOuterClass.AccountIdRequest request = ChatServiceOuterClass.AccountIdRequest.newBuilder()
                .setAccountId(accountId.toString())
                .build();

        chatServiceBlockingStub.setChatStateToActiveByAccount(request);
    }
}
