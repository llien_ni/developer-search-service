package com.technokratos.client;


import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import notification.service.NotificationServiceOuterClass;
import org.springframework.stereotype.Service;


@Service
public class NotificationServiceGrpcClient {

    @GrpcClient("notification-service")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    public void sendMail(NotificationServiceOuterClass.MailMessageRequest request) {

        notificationServiceBlockingStub.sendMail(request);
    }

    public void sendSms(NotificationServiceOuterClass.SmsModel smsModel) {

        notificationServiceBlockingStub.sendSms(smsModel);
    }
}
