package com.technokratos.client;

import com.technokratos.dto.model.MailMessageModel;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationServiceRabbit {

    private final RabbitTemplate rabbitTemplate;

    private static final String DIRECT_EXCHANGE = "direct-exchange";

    public void sendMailMessage(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend(DIRECT_EXCHANGE, "authorization-service", mailMessageModel);
    }

    public void sendMailMessageFromEventService(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend(DIRECT_EXCHANGE, "event-service-notification", mailMessageModel);
    }

    public void sendMailMessageFromVacancyService(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend(DIRECT_EXCHANGE, "vacancy-service-notification", mailMessageModel);
    }

    public void sendMailMessageFromProjectService(MailMessageModel mailMessageModel) {

        rabbitTemplate.convertAndSend(DIRECT_EXCHANGE, "project-service-notification", mailMessageModel);
    }
}
