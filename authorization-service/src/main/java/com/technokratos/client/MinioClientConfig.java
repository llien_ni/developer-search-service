package com.technokratos.client;


import com.technokratos.config.MinioConfigurationProperties;
import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class MinioClientConfig {

    private final MinioConfigurationProperties properties;

    @Bean
    public MinioClient minioClient() {
        return new io.minio.MinioClient.Builder()
                .credentials(properties.getAccessKey(), properties.getSecretKey())
                .endpoint(properties.getEndpoint())
                .build();
    }
}
