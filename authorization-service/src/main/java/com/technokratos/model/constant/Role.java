package com.technokratos.model.constant;

public enum Role {
    ROLE_HR, ROLE_DEVELOPER, ROLE_ADMIN, ROLE_MODERATOR, ROLE_SIMPLE_ACCOUNT
}
