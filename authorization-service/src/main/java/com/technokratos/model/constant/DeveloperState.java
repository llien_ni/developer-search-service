package com.technokratos.model.constant;

public enum DeveloperState {
    ACTIVE, NOT_ACTIVE
}
