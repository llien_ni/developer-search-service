package com.technokratos.model.constant;

public enum RegistrationProvider {

    GOOGLE, NATIVE
}
