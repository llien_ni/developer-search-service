package com.technokratos.model.constant;

public enum Status {
    CONFIRMED, NOT_CONFIRMED, MODERATED, BLOCKED, DELETED
}
