package com.technokratos.model.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;


@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "link")
public class LinkEntity extends AbstractBaseEntity{

    private String url;

    private String description;

    @ManyToOne
    @JoinColumn(name = "developer_id", referencedColumnName = "id")
    private DeveloperEntity developer;
}
