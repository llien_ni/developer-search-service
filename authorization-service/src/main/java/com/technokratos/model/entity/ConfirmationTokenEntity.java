package com.technokratos.model.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.time.OffsetDateTime;



@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "confirmation_token")
public class ConfirmationTokenEntity extends AbstractBaseEntity{

    @Column(name = "token")
    private String token;

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;

    @Column(name = "expired_date", nullable = false)
    private OffsetDateTime expiredDate;

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private AccountEntity account;
}
