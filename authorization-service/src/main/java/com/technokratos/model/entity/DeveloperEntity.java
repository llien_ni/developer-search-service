package com.technokratos.model.entity;


import com.technokratos.model.constant.DeveloperState;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;


@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "developer")
@PrimaryKeyJoinColumn(name = "id")
public class DeveloperEntity extends AccountEntity{

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "last_visit_date")
    private OffsetDateTime lastVisitDate;

    @Column(name = "work_experience")
    private Integer workExperience;

    private String city;

    @Enumerated(value = EnumType.STRING)
    private DeveloperState state;

    private String username;

    private BigDecimal rating;

    @OneToMany(mappedBy = "developer", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<DocumentEntity> documents;

    @OneToMany(mappedBy = "developer", cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<EducationEntity> educations;

    @OneToMany(mappedBy = "developer")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<LinkEntity> links;

    @OneToMany(mappedBy = "developer")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<SpecializationEntity> specializations;
}
