/*
 * This file is generated by jOOQ.
 */
package com.technokratos.model.entity;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;


@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "appeal")
public class AppealEntity extends AbstractBaseEntity{

    public enum Status{
        OPEN, CLOSED, DELETED
    }

    private String title;

    private String text;

    @Column(name = "created_date", nullable = false)
    @CreationTimestamp
    private OffsetDateTime createdDate;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private AccountEntity author;

    @Enumerated(value = EnumType.STRING)
    private Status status;

    private String response;

    @Column(name = "response_date", nullable = false)
    private OffsetDateTime responseDate;
}
