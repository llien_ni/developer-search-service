package com.technokratos.model.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.UUID;


@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "hr")
@PrimaryKeyJoinColumn(name = "id")
public class HrEntity extends AccountEntity{

    @Column(name = "company_id")
    private UUID companyId;

    private String city;
}
