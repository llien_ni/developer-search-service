package com.technokratos.model.entity;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@Getter
@Setter
@Entity
@Table(name = "block_info")
public class BlockInfoEntity extends AbstractBaseEntity{

    @Column(name = "start_date")
    @CreationTimestamp
    private OffsetDateTime startDate;

    @Column(name = "finish_date")
    private OffsetDateTime finishDate;

    private String reason;

    @ManyToOne()
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private AccountEntity account;

    private boolean active;
}
