package com.technokratos.api;

import com.technokratos.dto.request.ResponseToAppeal;
import com.technokratos.dto.response.AppealResponse;
import com.technokratos.dto.response.ExceptionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
@RequestMapping("/appeal")
public interface AdminAppealApi {

    @Operation(summary = "get appeals sent by a certain user and certain appeal status")
    @ApiResponse(responseCode = "200", description = "appeals received successfully ",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "Account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/author/{account-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<AppealResponse> getByAuthor(@RequestParam(value = "status", required = false) String status,
                                     @RequestParam(value = "size", required = false) Integer size,
                                     @RequestParam(value = "number", required = false) Integer number,
                                     @PathVariable("account-id") UUID authorId);

    @Operation(summary = "get all appeals or appeals with certain status")
    @ApiResponse(responseCode = "200", description = "appeals received successfully ",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<AppealResponse> getAll(@RequestParam(value = "status", required = false) String status,
                                @RequestParam(value = "size", required = false) Integer size,
                                @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get appeal by id")
    @ApiResponse(responseCode = "200", description = "appeal received successfully ",
            content = @Content(schema = @Schema(implementation = AppealResponse.class)))
    @ApiResponse(responseCode = "404", description = "Appeal not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    AppealResponse getById(@PathVariable("id") UUID id);

    @Operation(summary = "respond to a user's appeal")
    @ApiResponse(responseCode = "202", description = "respond successfully added",
            content = @Content(schema = @Schema(implementation = AppealResponse.class)))
    @ApiResponse(responseCode = "404", description = "Appeal not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PatchMapping("/respond")
    @ResponseStatus(HttpStatus.ACCEPTED)
    AppealResponse respondToAppeal(@RequestBody @Valid ResponseToAppeal responseToAppeal);
}
