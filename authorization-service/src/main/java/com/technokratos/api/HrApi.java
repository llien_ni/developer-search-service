package com.technokratos.api;

import com.technokratos.dto.request.CreateHrRequest;
import com.technokratos.dto.request.UpdateHrRequest;
import com.technokratos.dto.response.*;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/hr")
public interface HrApi {

    @Operation(summary = "hr registration")
    @ApiResponse(responseCode = "201", description = "the hr has been successfully registered " +
            "and a email to confirm registration has been sent",
            content = @Content(schema = @Schema(implementation = HrResponse.class)))
    @ApiResponse(responseCode = "400", description = "account with such email already exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping("/sign-up")
    @ResponseStatus(HttpStatus.CREATED)
    HrResponse register(@RequestBody @Valid CreateHrRequest request);

    @Operation(summary = "update hr")
    @ApiResponse(responseCode = "202", description = "the hr has been successfully updated",
            content = @Content(schema = @Schema(implementation = HrResponse.class)))
    @ApiResponse(responseCode = "404", description = "hr not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_HR')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    HrResponse update(@RequestBody @Valid UpdateHrRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "get hr by id and status CONFIRMED")
    @ApiResponse(responseCode = "200", description = "the hr has been successfully received",
            content = @Content(schema = @Schema(implementation = HrResponse.class)))
    @ApiResponse(responseCode = "404", description = "hr not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    HrResponse getById(@PathVariable UUID id);

    @Operation(summary = "get all hrs with status CONFIRMED")
    @ApiResponse(responseCode = "200", description = "hrs successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasAnyRole('ROLE_HR', 'ROLE_MODERATOR', 'ROLE_ADMIN')")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<HrResponse> getAll(@RequestParam(value = "size", required = false) Integer size,
                            @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get all hrs by company name and status CONFIRMED")
    @ApiResponse(responseCode = "200", description = "hrs successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "company not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_HR', 'ROLE_MODERATOR', 'ROLE_ADMIN')")
    @GetMapping("/by")
    Page<HrResponse> getAllByCompany(@RequestParam(value = "company", required = false) String companyName,
                                     @RequestParam(value = "size", required = false) Integer size,
                                     @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "delete hr")
    @ApiResponse(responseCode = "202", description = "hr successfully deleted",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "hr not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping
    @PreAuthorize("hasRole('ROLE_HR')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@AuthenticationPrincipal UserDetailsImpl userDetails);
}
