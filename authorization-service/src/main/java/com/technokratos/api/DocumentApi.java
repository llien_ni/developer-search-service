package com.technokratos.api;

import com.technokratos.dto.request.DocumentRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.*;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/document")
public interface DocumentApi {

    @Operation(summary = "get documents by developer id and download urls for this documents")
    @ApiResponse(responseCode = "200", description = "documents successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/developer/{developer-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<DocumentResponse> getDocumentsByDeveloperAndDownloadUrls(@PathVariable("developer-id") UUID developerId,
                                                                  @RequestParam(value = "size", required = false) Integer size,
                                                                  @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get document by id")
    @ApiResponse(responseCode = "200", description = "documents successfully received" ,
            content = @Content(schema = @Schema(implementation = DocumentResponse.class)))
    @ApiResponse(responseCode = "404", description = "document not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{document-id}")
    @ResponseStatus(HttpStatus.OK)
    DocumentResponse getDocumentById(@PathVariable("document-id") UUID documentId);

    @Operation(summary = "upload document")
    @ApiResponse(responseCode = "201", description = "document successfully uploaded" ,
            content = @Content(schema = @Schema(implementation = DocumentResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping("/upload")
    @ResponseStatus(HttpStatus.CREATED)
    DocumentResponse uploadDocument(@ModelAttribute("file") MultipartFile file,
                                    @RequestBody @Valid DocumentRequest request,
                                    @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "download document")
    @ApiResponse(responseCode = "200", description = "document successfully download")
    @ApiResponse(responseCode = "404", description = "document not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/download/{document-id}")
    @ResponseStatus(HttpStatus.OK)
    void downloadDocument(@PathVariable("document-id") UUID documentId, HttpServletResponse response);

    @Operation(summary = "delete document")
    @ApiResponse(responseCode = "202", description = "document successfully deleted" ,
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "document not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestID request, @AuthenticationPrincipal UserDetailsImpl userDetails);
}
