package com.technokratos.api;


import com.technokratos.dto.request.CreateDeveloperRequest;
import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.UpdateDeveloperRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/developer")
public interface DeveloperApi {

    @Operation(summary = "developer registration")
    @ApiResponse(responseCode = "201", description = "the developer has been successfully registered " +
            "and a email to confirm registration has been sent" ,
            content = @Content(schema = @Schema(implementation = DeveloperResponse.class)))
    @ApiResponse(responseCode = "400", description = "account with such email already exist or developer " +
            "with this username already exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping("/sign-up")
    @ResponseStatus(HttpStatus.CREATED)
    DeveloperResponse register(@RequestBody @Valid CreateDeveloperRequest request);

    @Operation(summary = "update developer")
    @ApiResponse(responseCode = "202", description = "the developer has been successfully updated" ,
            content = @Content(schema = @Schema(implementation = DeveloperResponse.class)))
    @ApiResponse(responseCode = "400", description = "developer with this username already exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    DeveloperResponse update(@RequestBody @Valid UpdateDeveloperRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "delete developer")
    @ApiResponse(responseCode = "202", description = "the developer has been successfully deleted" ,
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "get developer by id and status CONFIRMED")
    @ApiResponse(responseCode = "200", description = "developer successfully received" ,
            content = @Content(schema = @Schema(implementation = DeveloperResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    DeveloperResponse getById(@PathVariable UUID id);

    @Operation(summary = "get all developers with status CONFIRMED and state ACTIVE sort by rating")
    @ApiResponse(responseCode = "200", description = "developers successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasAnyRole('ROLE_HR')")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<DeveloperResponse> getAllDevelopersByStateActive(@RequestParam(value = "size", required = false) Integer size,
                                                          @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get all developers by ids in list sort by rating")
    @ApiResponse(responseCode = "200", description = "developers successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PostMapping("/list/by/ids")
    @ResponseStatus(HttpStatus.OK)
    Page<DeveloperResponse> getDevelopersByIdsList(@RequestBody ListIdsRequest request,
                                        @RequestParam(value = "size", required = false) Integer size,
                                        @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get developer by username")
    @ApiResponse(responseCode = "200", description = "developer successfully received" ,
            content = @Content(schema = @Schema(implementation = DeveloperResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping( "/username/{username}")
    @ResponseStatus(HttpStatus.OK)
    DeveloperResponse getDeveloperByUserName(@PathVariable String username);
}
