package com.technokratos.api;


import com.technokratos.dto.request.PhotoRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@PreAuthorize("isAuthenticated()")
@RequestMapping("/account/photo")
public interface AccountPhotoApi {

    @Operation(summary = "user add one photo to account")
    @ApiResponse(responseCode = "201", description = "photo successfully added" ,
            content = @Content(schema = @Schema(implementation = PhotoResponse.class)))
    @ApiResponse(responseCode = "404", description = "Account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    PhotoResponse add(@ModelAttribute PhotoRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "delete photo")
    @ApiResponse(responseCode = "202", description = "photo successfully deleted" ,
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "Photo not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@AuthenticationPrincipal UserDetailsImpl userDetails);
}
