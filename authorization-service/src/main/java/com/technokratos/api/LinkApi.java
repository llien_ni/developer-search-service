package com.technokratos.api;


import com.technokratos.dto.request.LinkRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.request.UpdateLinkRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.LinkResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/developer/link")
public interface LinkApi {

    @Operation(summary = "add link")
    @ApiResponse(responseCode = "201", description = "link successfully added",
            content = @Content(schema = @Schema(implementation = LinkResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    LinkResponse add(@RequestBody @Valid LinkRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "update link")
    @ApiResponse(responseCode = "202", description = "link successfully updated",
            content = @Content(schema = @Schema(implementation = LinkResponse.class)))
    @ApiResponse(responseCode = "404", description = "link not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    LinkResponse update(@RequestBody @Valid UpdateLinkRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "delete link")
    @ApiResponse(responseCode = "202", description = "link successfully deleted",
            content = @Content(schema = @Schema(implementation = LinkResponse.class)))
    @ApiResponse(responseCode = "404", description = "link not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestID requestId, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "get link by id")
    @ApiResponse(responseCode = "200", description = "link successfully received",
            content = @Content(schema = @Schema(implementation = LinkResponse.class)))
    @ApiResponse(responseCode = "404", description = "link not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    LinkResponse getById(@PathVariable("id") UUID id);

    @Operation(summary = "get all links by developer id")
    @ApiResponse(responseCode = "200", description = "links successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/developer/{developer-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<LinkResponse> getByDeveloper(@PathVariable("developer-id") UUID developerId,
                                      @RequestParam(value = "size", required = false) Integer size,
                                      @RequestParam(value = "number", required = false) Integer number);

}
