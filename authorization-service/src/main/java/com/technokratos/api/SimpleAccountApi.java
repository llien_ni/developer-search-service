package com.technokratos.api;

import com.technokratos.dto.request.CreateAccountRequest;
import com.technokratos.dto.request.UpdateAccountRequest;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/simple_account")
public interface SimpleAccountApi {

    @Operation(summary = "simple account registration")
    @ApiResponse(responseCode = "201", description = "the simple account has been successfully registered " +
            "and an email to confirm registration has been sent" ,
            content = @Content(schema = @Schema(implementation = AccountResponse.class)))
    @ApiResponse(responseCode = "400", description = "account with such email already exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping("/sign-up")
    @ResponseStatus(HttpStatus.CREATED)
    AccountResponse register(@RequestBody @Valid CreateAccountRequest request);

    @Operation(summary = "update simple account")
    @ApiResponse(responseCode = "202", description = "the simple account updated" ,
            content = @Content(schema = @Schema(implementation = AccountResponse.class)))
    @ApiResponse(responseCode = "404", description = "account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_SIMPLE_ACCOUNT')")
    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    AccountResponse update(@RequestBody @Valid UpdateAccountRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "delete simple account")
    @ApiResponse(responseCode = "202", description = "the simple account deleted" ,
            content = @Content(schema = @Schema(implementation = AccountResponse.class)))
    @ApiResponse(responseCode = "404", description = "account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_SIMPLE_ACCOUNT')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@AuthenticationPrincipal UserDetailsImpl userDetails);
}
