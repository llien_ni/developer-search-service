package com.technokratos.api;

import com.technokratos.dto.request.ListStringsRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.request.SpecializationRequest;
import com.technokratos.dto.request.SpecializationsListRequest;
import com.technokratos.dto.response.*;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping("/developer/spec")
public interface SpecializationApi {

    @Operation(summary = "adding specializations as a list")
    @ApiResponse(responseCode = "201", description = "specializations successfully added" ,
            content = @Content(schema = @Schema(implementation = List.class)))
    @ApiResponse(responseCode = "400", description = "specialization already added",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping("/list")
    @ResponseStatus(HttpStatus.CREATED)
    List<SpecializationResponse> addList(@RequestBody @Valid SpecializationsListRequest request,
                                                 @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "add specialization")
    @ApiResponse(responseCode = "201", description = "specialization successfully added" ,
            content = @Content(schema = @Schema(implementation = SpecializationResponse.class)))
    @ApiResponse(responseCode = "400", description = "specialization already added",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    SpecializationResponse add(@RequestBody @Valid SpecializationRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "delete specialization")
    @ApiResponse(responseCode = "202", description = "specialization successfully deleted" ,
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "specialization not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestID requestID, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "get specialization by id")
    @ApiResponse(responseCode = "200", description = "specialization successfully received" ,
            content = @Content(schema = @Schema(implementation = SpecializationResponse.class)))
    @ApiResponse(responseCode = "404", description = "specialization not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{specialization-id}")
    @ResponseStatus(HttpStatus.OK)
    SpecializationResponse getById(@PathVariable("specialization-id") UUID id);

    @Operation(summary = "get specialization by developer id and sort by specialization name")
    @ApiResponse(responseCode = "200", description = "specializations successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<SpecializationResponse> getByDeveloper(@RequestParam(value = "size", required = false) Integer size,
                                                @RequestParam(value = "number", required = false) Integer number,
                                                @RequestParam(value = "developer_id") UUID developerId);

    @Operation(summary = "get unique names of specializations")
    @ApiResponse(responseCode = "200", description = "specializations successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    Page<String> getAllDistinctNames(@RequestParam(value = "size", required = false) Integer size,
                                     @RequestParam(value = "number", required = false) Integer number);


    @Operation(summary = "get developers by specializations")
    @ApiResponse(responseCode = "200", description = "developers successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    Page<DeveloperResponse> getDevelopersBySpecialization(@RequestParam(value = "size", required = false) Integer size,
                                                          @RequestParam(value = "number", required = false) Integer number,
                                                          @RequestBody ListStringsRequest specializations);
}
