package com.technokratos.api;

import com.technokratos.dto.request.BlockAccountRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.BlockInfoResponse;
import com.technokratos.dto.response.ExceptionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping("/account")
public interface AdminApi {

    @Operation(summary = "admin unblock account")
    @ApiResponse(responseCode = "202", description = "account has been successfully unblocked" ,
            content = @Content(schema = @Schema(implementation = AccountResponse.class)))
    @ApiResponse(responseCode = "404", description = "Account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "400", description = "Account is not blocked",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PatchMapping("/unblock")
    @ResponseStatus(HttpStatus.ACCEPTED)
    AccountResponse unblockAccount(@RequestBody @Valid RequestID requestID);

    @Operation(summary = "admin block account")
    @ApiResponse(responseCode = "202", description = "account has been successfully blocked" ,
            content = @Content(schema = @Schema(implementation = BlockInfoResponse.class)))
    @ApiResponse(responseCode = "404", description = "Account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "400", description = "Account has already been blocked",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PatchMapping("/block")
    @ResponseStatus(HttpStatus.ACCEPTED)
    BlockInfoResponse blockAccount(@RequestBody @Valid BlockAccountRequest request);

    @Operation(summary = "get information about all account locks by account id")
    @ApiResponse(responseCode = "200", description = "account has been successfully blocked" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "Account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/block/{id}")
    @ResponseStatus(HttpStatus.OK)
    Page<BlockInfoResponse> getByAccount(@PathVariable("id")UUID accountId,
                                         @RequestParam(value = "size", required = false) Integer size,
                                         @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "get all accounts or get accounts with a certain status")
    @ApiResponse(responseCode = "200", description = "account has been successfully blocked" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "Account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<AccountResponse> getAll(@RequestParam(value = "status", required = false) String status,
                                 @RequestParam(value = "size", required = false) Integer size,
                                 @RequestParam(value = "number", required = false) Integer number);
}
