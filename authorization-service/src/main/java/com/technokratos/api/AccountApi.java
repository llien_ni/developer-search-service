package com.technokratos.api;

import com.technokratos.dto.request.*;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/account")
public interface AccountApi {

    @Operation(summary = "get account by id")
    @ApiResponse(responseCode = "201", description = "account successfully received",
            content = @Content(schema = @Schema(implementation = AccountResponse.class)))
    @ApiResponse(responseCode = "404", description = "account with such id is not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    AccountResponse getById(@PathVariable("id") UUID id);

    @Operation(summary = "confirm account")
    @ApiResponse(responseCode = "200", description = "account successfully confirmed, if account role is DEVELOPER or SIMPLE_ACCOUNT status changed to CONFIRMED, " +
            " if account role is HR status changed to MODERATED",
            content = @Content(schema = @Schema(implementation = AccountResponse.class)))
    @ApiResponse(responseCode = "400", description = "token expired",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/confirm")
    @ResponseStatus(HttpStatus.OK)
    AccountResponse confirm(@RequestParam("token") String token);

    @Operation(summary = "confirmation of account password reset")
    @ApiResponse(responseCode = "200", description = "reset password successfully confirmed",
            content = @Content(schema = @Schema(implementation = AccountResponse.class)))
    @ApiResponse(responseCode = "400", description = "token expired",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping("/reset-password/confirm")
    @ResponseStatus(HttpStatus.OK)
    AccountResponse resetPasswordConfirm(@RequestParam("token") String token, @RequestBody ResetPasswordRequest request);

    @Operation(summary = "change password")
    @ApiResponse(responseCode = "202", description = "change password successfully",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "Failed to change password, the " +
            "entered password did not match the password stored in the database",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("isAuthenticated()")
    @PatchMapping("/change-password")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse changePassword(@RequestBody @Valid ChangePasswordRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "change email")
    @ApiResponse(responseCode = "202", description = "the user sends a request for a change of mail, " +
            "he receives an email confirming the password change",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "Invalid password or user with such email already exist",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("isAuthenticated()")
    @PatchMapping("/change-email")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse changeEmail(@RequestBody @Valid ChangeEmailRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "confirmation of the email change")
    @ApiResponse(responseCode = "202", description = "change email successfully confirmed",
            content = @Content(schema = @Schema(implementation = AccountResponse.class)))
    @ApiResponse(responseCode = "400", description = "Token expired",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/change-email/confirm")
    @ResponseStatus(HttpStatus.ACCEPTED)
    AccountResponse changeEmailConfirm(@RequestParam("token") String token);

    @Operation(summary = "the user sends a request to reset the password")
    @ApiResponse(responseCode = "202", description = "the request to reset the password is saved to the database " +
            "and an email or sms has been sent to confirm password reset",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "Account is not confirmed",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping("/forgot-password")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse resetPassword(@RequestBody @Valid PhoneOrEmailRequest request);

    @Operation(summary = "restoring a deleted account")
    @ApiResponse(responseCode = "202", description = "an email or sms code has been sent to restore the user's account",
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "400", description = "Account has not been deleted",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "Account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping("/restore")
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse restoreAccount(@RequestBody @Valid PhoneOrEmailRequest request);
}
