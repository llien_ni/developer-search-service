package com.technokratos.api;

import com.technokratos.dto.request.BlockAccountRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@PreAuthorize("hasRole('ROLE_MODERATOR')")
@RequestMapping("/moderator/hr")
public interface ModeratorApi {

    @Operation(summary = "get hrs by status")
    @ApiResponse(responseCode = "200", description = "hrs successfully received",
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<HrResponse> getHrsByStatus(@RequestParam(value = "status") String status,
                                    @RequestParam(value = "size", required = false) Integer size,
                                    @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "confirm hr by hr id")
    @ApiResponse(responseCode = "202", description = "hr successfully confirmed",
            content = @Content(schema = @Schema(implementation = HrResponse.class)))
    @ApiResponse(responseCode = "404", description = "hr not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PatchMapping("/confirm")
    @ResponseStatus(HttpStatus.ACCEPTED)
    HrResponse confirmHr(@RequestBody @Valid RequestID request);

    @Operation(summary = "block hr")
    @ApiResponse(responseCode = "202", description = "hr successfully blocked",
            content = @Content(schema = @Schema(implementation = BlockInfoResponse.class)))
    @ApiResponse(responseCode = "400", description = "hr already blocked",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "hr not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PatchMapping("/block")
    @ResponseStatus(HttpStatus.ACCEPTED)
    BlockInfoResponse blockHr(@RequestBody @Valid BlockAccountRequest request);
}
