package com.technokratos.api;

import com.technokratos.dto.request.AppealRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.AppealResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@PreAuthorize("isAuthenticated()")
@RequestMapping("/appeal/my")
public interface AppealApi {

    @Operation(summary = "add appeal")
    @ApiResponse(responseCode = "201", description = "appeal successfully added" ,
            content = @Content(schema = @Schema(implementation = AppealResponse.class)))
    @ApiResponse(responseCode = "404", description = "account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    AppealResponse add(@RequestBody @Valid AppealRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "delete appeal")
    @ApiResponse(responseCode = "202", description = "appeal successfully deleted" ,
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "appeal not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestID requestID, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "get all appeals sent a certain user or appeals with certain status")
    @ApiResponse(responseCode = "200", description = "appeals successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<AppealResponse> getByAuthor(@RequestParam(value = "status", required = false) String status,
                                     @RequestParam(value = "size", required = false) Integer size,
                                     @RequestParam(value = "number", required = false) Integer number,
                                     @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "get appeal by id")
    @ApiResponse(responseCode = "200", description = "appeal successfully received" ,
            content = @Content(schema = @Schema(implementation = AppealResponse.class)))
    @ApiResponse(responseCode = "404", description = "appeal not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    AppealResponse getById(@PathVariable("id") UUID id, @AuthenticationPrincipal UserDetailsImpl userDetails);
}
