package com.technokratos.api;

import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.security.model.AccessAndRefreshTokens;
import com.technokratos.security.util.constants.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

public interface RefreshTokenApi {

    @Operation(summary = "refresh tokens")
    @ApiResponse(responseCode = "200", description = "refreshed tokens received successfully" ,
            content = @Content(schema = @Schema(implementation = AccessAndRefreshTokens.class)))
    @ApiResponse(responseCode = "404", description = "refresh token not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @GetMapping(value = Constants.REFRESH_TOKEN_URL)
    @ResponseStatus(HttpStatus.OK)
    AccessAndRefreshTokens refreshTokensAndAuthentication(HttpServletRequest request);
}
