package com.technokratos.api;


import com.technokratos.dto.request.EducationRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.EducationResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;


@RequestMapping("/developer/education")
public interface EducationApi {

    @Operation(summary = "add information about education")
    @ApiResponse(responseCode = "201", description = "information about education successfully added" ,
            content = @Content(schema = @Schema(implementation = EducationResponse.class)))
    @ApiResponse(responseCode = "404", description = "developer not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    EducationResponse add(@RequestBody @Valid EducationRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "delete information about education by id")
    @ApiResponse(responseCode = "202", description = "information about education successfully deleted" ,
            content = @Content(schema = @Schema(implementation = EducationResponse.class)))
    @ApiResponse(responseCode = "404", description = "information about education not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasRole('ROLE_DEVELOPER')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestID requestID, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "get information about education by developer id")
    @ApiResponse(responseCode = "200", description = "information about education successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @GetMapping("/{developer-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<EducationResponse> getAllByDeveloper(@RequestParam(value = "size", required = false) Integer size,
                                              @RequestParam(value = "number", required = false) Integer number,
                                              @PathVariable("developer-id")UUID developerId);
}
