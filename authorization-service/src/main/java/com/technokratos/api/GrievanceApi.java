package com.technokratos.api;

import com.technokratos.dto.request.GrievanceRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.GrievanceResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;


@RequestMapping("/grievance")
public interface GrievanceApi {

    @Operation(summary = "add grievance")
    @ApiResponse(responseCode = "201", description = "grievance successfully added" ,
            content = @Content(schema = @Schema(implementation = GrievanceResponse.class)))
    @ApiResponse(responseCode = "400", description = "account sent many grievances",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @ApiResponse(responseCode = "404", description = "account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("isAuthenticated()")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    GrievanceResponse add(@RequestBody @Valid GrievanceRequest request, @AuthenticationPrincipal UserDetailsImpl userDetails);

    @Operation(summary = "admin delete grievance")
    @ApiResponse(responseCode = "202", description = "grievance successfully deleted" ,
            content = @Content(schema = @Schema(implementation = MessageResponse.class)))
    @ApiResponse(responseCode = "404", description = "grievance not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @DeleteMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    MessageResponse delete(@RequestBody @Valid RequestID request);

    @Operation(summary = "admin get all grievances sent by the user and sort by grievance created date")
    @ApiResponse(responseCode = "200", description = "grievances successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/author/{author-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<GrievanceResponse> getByAuthor(@RequestParam(value = "size", required = false) Integer size,
                                        @RequestParam(value = "number", required = false) Integer number,
                                        @PathVariable("author-id") UUID authorId);

    @Operation(summary = "admin get all grievances sent to the user and sort by grievance created date")
    @ApiResponse(responseCode = "200", description = "grievances successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "404", description = "account not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/account/{account-id}")
    @ResponseStatus(HttpStatus.OK)
    Page<GrievanceResponse> getByAccount(@RequestParam(value = "size", required = false) Integer size,
                                         @RequestParam(value = "number", required = false) Integer number,
                                         @PathVariable("account-id") UUID id);

    @Operation(summary = "admin get all grievances and sort by grievance created date")
    @ApiResponse(responseCode = "200", description = "grievances successfully received" ,
            content = @Content(schema = @Schema(implementation = Page.class)))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<GrievanceResponse> getAll(@RequestParam(value = "size", required = false) Integer size,
                                   @RequestParam(value = "number", required = false) Integer number);

    @Operation(summary = "admin get grievance by id")
    @ApiResponse(responseCode = "200", description = "grievance successfully received" ,
            content = @Content(schema = @Schema(implementation = GrievanceResponse.class)))
    @ApiResponse(responseCode = "404", description = "grievance not found",
            content = @Content(schema = @Schema(implementation = ExceptionResponse.class)))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    GrievanceResponse getById(@PathVariable("id") UUID id);
}
