package com.technokratos.mapper;

import com.technokratos.dto.response.BlockInfoResponse;
import com.technokratos.model.entity.BlockInfoEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = {AccountMapper.class})
public interface BlockInfoMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    BlockInfoResponse fromEntityToResponse(BlockInfoEntity save);
}
