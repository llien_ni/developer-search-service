package com.technokratos.mapper;


import com.technokratos.dto.request.AppealRequest;
import com.technokratos.dto.request.ResponseToAppeal;
import com.technokratos.dto.response.AppealResponse;
import com.technokratos.model.entity.AppealEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface AppealMapper{

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    AppealEntity fromRequestToEntity(AppealRequest request);

    AppealResponse fromEntityToResponse(AppealEntity appealEntity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateAppealEntity(ResponseToAppeal responseToAppeal, @MappingTarget AppealEntity appealEntity);
}
