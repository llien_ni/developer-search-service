package com.technokratos.mapper;


import com.technokratos.dto.request.GrievanceRequest;
import com.technokratos.dto.response.GrievanceResponse;
import com.technokratos.model.entity.GrievanceEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = {AccountMapper.class})
public interface GrievanceMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    GrievanceEntity fromRequestToEntity(GrievanceRequest request);

    GrievanceResponse fromEntityToResponse(GrievanceEntity grievanceEntity);
}
