package com.technokratos.mapper;

import com.technokratos.dto.request.LinkRequest;
import com.technokratos.dto.request.UpdateLinkRequest;
import com.technokratos.dto.response.LinkResponse;
import com.technokratos.model.entity.LinkEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LinkMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    LinkEntity fromRequestToEntity(LinkRequest request);

    @Mapping(source = "developer.id", target = "developerId")
    LinkResponse fromEntityToResponse(LinkEntity linkEntity);

    List<LinkResponse> fromEntitiesToResponses(List<LinkEntity> entities);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(UpdateLinkRequest request, @MappingTarget LinkEntity linkEntity);
}
