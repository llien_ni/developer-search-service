package com.technokratos.mapper;


import com.technokratos.dto.request.CreateAccountRequest;
import com.technokratos.dto.request.UpdateAccountRequest;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.model.entity.AccountEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface AccountMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    AccountResponse fromEntityToResponse(AccountEntity accountEntity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateAccountEntity(UpdateAccountRequest request, @MappingTarget AccountEntity accountEntity);

    @Mapping(source = "password", target = "hashPassword")
    AccountEntity fromRequestToEntity(CreateAccountRequest request);
}
