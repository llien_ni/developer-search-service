package com.technokratos.mapper;


import com.technokratos.dto.request.CreateDeveloperRequest;
import com.technokratos.dto.request.UpdateDeveloperRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.model.entity.DeveloperEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AccountMapper.class, LinkMapper.class, DocumentMapper.class, EducationMapper.class, SpecializationMapper.class})
public interface DeveloperMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "password", target = "hashPassword")
    DeveloperEntity fromRequestToEntity(CreateDeveloperRequest request);

    DeveloperResponse fromEntityToResponse(DeveloperEntity developer);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateDeveloperEntity(UpdateDeveloperRequest request, @MappingTarget DeveloperEntity developerEntity);

    List<DeveloperResponse> fromEntitiesToResponses(List<DeveloperEntity> entities);
}
