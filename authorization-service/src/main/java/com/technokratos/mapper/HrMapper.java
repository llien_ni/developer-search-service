package com.technokratos.mapper;


import com.technokratos.dto.request.CreateHrRequest;
import com.technokratos.dto.request.UpdateHrRequest;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.model.entity.HrEntity;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface HrMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "password", target = "hashPassword")
    HrEntity fromRequestToEntity(CreateHrRequest request);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    HrResponse fromEntityToResponse(HrEntity hrEntity1);


    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateHrEntity(UpdateHrRequest request, @MappingTarget HrEntity hrEntity);
}
