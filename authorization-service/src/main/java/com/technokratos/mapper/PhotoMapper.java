package com.technokratos.mapper;


import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.model.entity.PhotoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PhotoMapper {

    @Mapping(source = "account.id", target = "accountId")
    PhotoResponse fromEntityToResponse(PhotoEntity savedPhoto);
}
