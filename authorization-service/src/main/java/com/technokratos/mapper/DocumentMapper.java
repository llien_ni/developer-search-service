package com.technokratos.mapper;

import com.technokratos.dto.request.DocumentRequest;
import com.technokratos.dto.response.DocumentResponse;
import com.technokratos.dto.response.DownloadedDocumentResponse;
import com.technokratos.model.entity.DocumentEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DocumentMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    DocumentEntity fromRequestToEntity(DocumentRequest request);

    @Mapping(source ="developer.id", target = "developerId")
    DocumentResponse fromEntityToResponse(DocumentEntity savedEntity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    DownloadedDocumentResponse fromEntityToDownloadedDocumentResponse(DocumentEntity documentEntity);

    List<DocumentResponse> fromEntitiesToResponses(List<DocumentEntity> byDeveloperId);
}
