package com.technokratos.mapper;


import com.technokratos.dto.request.EducationRequest;
import com.technokratos.dto.response.EducationResponse;
import com.technokratos.model.entity.EducationEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface EducationMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    EducationEntity fromRequestToEntity(EducationRequest request);

    EducationResponse fromEntityToResponse(EducationEntity educationEntity);
}
