package com.technokratos.mapper;


import com.technokratos.dto.request.SpecializationRequest;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.model.entity.SpecializationEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SpecializationMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    SpecializationEntity fromRequestToEntity(SpecializationRequest request);

    @Mapping(source = "developer.id", target = "developerId")
    SpecializationResponse fromEntityToResponse(SpecializationEntity specializationEntity);

    List<SpecializationResponse> fromEntitiesToResponse(List<SpecializationEntity> entities);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    List<SpecializationEntity> fromRequestToEntities(List<SpecializationRequest> specializations);
}
