package com.technokratos.validation.constraint;


import com.technokratos.validation.validator.EmailAndPhoneNumberValidator;
import org.springframework.messaging.handler.annotation.Payload;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {EmailAndPhoneNumberValidator.class})
@Target({TYPE})
@Retention(RUNTIME)
public @interface EmailOrPhoneNumberNotEmpty {

    String message() default "{UserConstraint error}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

