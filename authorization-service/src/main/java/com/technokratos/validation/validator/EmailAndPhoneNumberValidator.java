package com.technokratos.validation.validator;

import com.technokratos.dto.request.CreateAccountRequest;
import com.technokratos.validation.constraint.EmailOrPhoneNumberNotEmpty;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class EmailAndPhoneNumberValidator implements ConstraintValidator<EmailOrPhoneNumberNotEmpty, CreateAccountRequest> {

    private static final String MESSAGE = "One field must be filled in. Phone number or mail";

    @Override
    public boolean isValid(CreateAccountRequest request, ConstraintValidatorContext context) {
        boolean valid = true;
        if (Objects.nonNull(request.getEmail()) & Objects.nonNull(request.getPhoneNumber())) {
            valid = false;
            buildConstraintViolationWithTemplate(context);
        }

        if (Objects.isNull(request.getEmail()) & Objects.isNull(request.getPhoneNumber())) {
            valid = false;
            buildConstraintViolationWithTemplate(context);
        }

        return valid;
    }

    private void buildConstraintViolationWithTemplate(ConstraintValidatorContext context) {

        context.buildConstraintViolationWithTemplate(MESSAGE)
                .addPropertyNode("phoneNumber")
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
    }

}
