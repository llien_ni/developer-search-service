package com.technokratos.controller;

import com.technokratos.api.SpecializationApi;
import com.technokratos.dto.request.ListStringsRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.request.SpecializationRequest;
import com.technokratos.dto.request.SpecializationsListRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.SpecializationResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.SpecializationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class SpecializationController implements SpecializationApi {

    private final SpecializationService specializationService;

    @Override
    public SpecializationResponse add(SpecializationRequest request, UserDetailsImpl userDetails) {

        return specializationService.add(request, userDetails.getAccount().getId());
    }

    @Override
    public List<SpecializationResponse> addList(SpecializationsListRequest request, UserDetailsImpl userDetails) {

        return specializationService.addList(request, userDetails.getAccount().getId());
    }

    @Override
    public SpecializationResponse getById(UUID id) {

        return specializationService.findById(id);
    }

    @Override
    public MessageResponse delete(RequestID requestID, UserDetailsImpl userDetails) {

        UUID id = specializationService.delete(requestID.getId(), userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Specialization %s successfully deleted", id))
                .build();
    }

    @Override
    public Page<SpecializationResponse> getByDeveloper(Integer size, Integer number, UUID accountId) {

        return specializationService.getByDeveloper(size, number, accountId);
    }

    @Override
    public Page<String> getAllDistinctNames(Integer size, Integer number) {

        return specializationService.getAllDistinctNames(size, number);
    }

    @Override
    public Page<DeveloperResponse> getDevelopersBySpecialization(Integer size, Integer number, ListStringsRequest listRequest) {

        return specializationService.getDevelopersBySpecializations(size, number, listRequest.getList());
    }
}
