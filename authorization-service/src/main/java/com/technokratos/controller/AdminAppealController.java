package com.technokratos.controller;

import com.technokratos.api.AdminAppealApi;
import com.technokratos.dto.request.ResponseToAppeal;
import com.technokratos.dto.response.AppealResponse;
import com.technokratos.service.AppealService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AdminAppealController implements AdminAppealApi {

    private final AppealService appealService;

    @Override
    public Page<AppealResponse> getByAuthor(String status, Integer size, Integer number, UUID authorId) {

        return appealService.getByAuthorAndStatus(status, size, number, authorId);
    }

    @Override
    public Page<AppealResponse> getAll(String status, Integer size, Integer number) {

        return appealService.getAllByStatus(status, size, number);
    }

    @Override
    public AppealResponse getById(UUID id) {

        return appealService.getById(id);
    }

    @Override
    public AppealResponse respondToAppeal(ResponseToAppeal responseToAppeal) {

        return appealService.respondToAppeal(responseToAppeal);
    }
}
