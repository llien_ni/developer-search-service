package com.technokratos.controller;

import com.technokratos.api.RefreshTokenApi;
import com.technokratos.security.model.AccessAndRefreshTokens;
import com.technokratos.service.RefreshTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequiredArgsConstructor
public class RefreshTokenController implements RefreshTokenApi {

    private final RefreshTokenService refreshTokenService;

    @Override
    public AccessAndRefreshTokens refreshTokensAndAuthentication(HttpServletRequest request) {

        return refreshTokenService.refreshTokens(request);
    }
}
