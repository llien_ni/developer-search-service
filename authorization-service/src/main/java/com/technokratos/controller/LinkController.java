package com.technokratos.controller;

import com.technokratos.api.LinkApi;
import com.technokratos.dto.request.LinkRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.request.UpdateLinkRequest;
import com.technokratos.dto.response.LinkResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.LinkService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
public class LinkController implements LinkApi {

    private final LinkService linkService;

    @Override
    public LinkResponse add(LinkRequest request, UserDetailsImpl userDetails) {

        return linkService.add(request, userDetails.getAccount().getId());
    }

    @Override
    public LinkResponse update(UpdateLinkRequest request, UserDetailsImpl userDetails) {

        return linkService.update(request, userDetails.getAccount().getId());
    }

    @Override
    public MessageResponse delete(RequestID requestId, UserDetailsImpl userDetails) {

        UUID linkId = linkService.delete(requestId.getId(), userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Link %s successfully deleted", linkId))
                .build();
    }

    @Override
    public LinkResponse getById(UUID id) {

        return linkService.getById(id);
    }

    @Override
    public Page<LinkResponse> getByDeveloper(UUID developerId, Integer size, Integer number) {

        return linkService.findByDeveloper(developerId, size, number);
    }
}
