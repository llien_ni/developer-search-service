package com.technokratos.controller;

import com.technokratos.api.AdminApi;
import com.technokratos.dto.request.BlockAccountRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.BlockInfoResponse;
import com.technokratos.service.AccountService;
import com.technokratos.service.BlockService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
public class AdminController implements AdminApi {

    private final AccountService accountService;

    private final BlockService blockService;

    @Override
    public AccountResponse unblockAccount(RequestID requestID) {

        return blockService.unblockAccount(requestID.getId());
    }

    @Override
    public BlockInfoResponse blockAccount(BlockAccountRequest request) {

        return blockService.blockAccount(request);
    }

    @Override
    public Page<BlockInfoResponse> getByAccount(UUID accountId, Integer size, Integer number) {

        return blockService.getByAccount(accountId, size, number);
    }

    @Override
    public Page<AccountResponse> getAll(String status, Integer size, Integer number) {

        return accountService.getAll(status, size, number);
    }
}
