package com.technokratos.controller;

import com.technokratos.api.ModeratorApi;
import com.technokratos.dto.request.BlockAccountRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.BlockInfoResponse;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.model.constant.Status;
import com.technokratos.service.BlockService;
import com.technokratos.service.HrService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ModeratorController implements ModeratorApi {

    private final HrService hrService;

    private final BlockService blockService;

    @Override
    public Page<HrResponse> getHrsByStatus(String status, Integer size, Integer number) {

        return hrService.getAllByStatus(Status.valueOf(status), size, number);
    }

    @Override
    public HrResponse confirmHr(RequestID request) {

        return hrService.confirmHr(request.getId());
    }

    @Override
    public BlockInfoResponse blockHr(BlockAccountRequest request) {

        return blockService.blockAccount(request);
    }
}
