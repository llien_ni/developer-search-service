package com.technokratos.controller;

import com.technokratos.api.AccountApi;
import com.technokratos.dto.request.*;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AccountController implements AccountApi {

    private final AccountService accountService;

    @Override
    public AccountResponse getById(UUID id) {

        return accountService.getById(id);
    }

    @Override
    public AccountResponse confirm(String token) {

        return accountService.confirmAccount(token);
    }

    @Override
    public MessageResponse resetPassword(PhoneOrEmailRequest request) {

        String login = accountService.resetPassword(request);
        return MessageResponse.builder()
                .message(String.format("A message to reset the password has been sent to the %s %s", request.getLoginType(), login))
                .build();
    }

    @Override
    public AccountResponse resetPasswordConfirm(String token, ResetPasswordRequest request) {

        request.setToken(token);
        return accountService.resetPasswordConfirm(request);
    }

    @Override
    public MessageResponse changePassword(ChangePasswordRequest request, UserDetailsImpl userDetails) {

        accountService.changePassword(request, userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message("Password successfully changed")
                .build();
    }

    @Override
    public MessageResponse changeEmail(ChangeEmailRequest request, UserDetailsImpl userDetails) {

        String newEmail = accountService.changeEmail(request, userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Sent a link to change email to %s", newEmail))
                .build();
    }

    @Override
    public AccountResponse changeEmailConfirm(String token) {

        return accountService.changeEmailConfirm(token);
    }

    @Override
    public MessageResponse restoreAccount(PhoneOrEmailRequest request) {

        String login = accountService.restoreAccount(request);
        return MessageResponse.builder()
                .message(String.format("A message to restore your account to sent %s %s", request.getLoginType(), login))
                .build();
    }
}
