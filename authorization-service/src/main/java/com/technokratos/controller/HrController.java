package com.technokratos.controller;

import com.technokratos.api.HrApi;
import com.technokratos.dto.request.CreateHrRequest;
import com.technokratos.dto.request.UpdateHrRequest;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.model.constant.Status;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.AccountService;
import com.technokratos.service.HrService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
public class HrController implements HrApi {

    private final HrService hrService;

    private final AccountService accountService;

    @Override
    public HrResponse register(CreateHrRequest request) {

        return hrService.save(request);
    }

    @Override
    public HrResponse update(UpdateHrRequest request, UserDetailsImpl userDetails) {

        return hrService.update(userDetails.getAccount().getId(), request);
    }

    @Override
    public HrResponse getById(UUID id) {

        return hrService.getByIdAndStatus(id, Status.CONFIRMED);
    }

    @Override
    public Page<HrResponse> getAll(Integer size, Integer number) {

        return hrService.getAllByStatus(Status.CONFIRMED, size, number);
    }

    @Override
    public Page<HrResponse> getAllByCompany(String companyName, Integer size, Integer number) {

        return hrService.getAllByStatusAndCompanyName(companyName, Status.CONFIRMED, size, number);
    }

    @Override
    public MessageResponse delete(UserDetailsImpl userDetails) {

        UUID id = accountService.delete(userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Hr %s successfully deleted", id))
                .build();
    }
}
