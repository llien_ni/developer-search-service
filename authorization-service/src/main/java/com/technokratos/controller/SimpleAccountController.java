package com.technokratos.controller;

import com.technokratos.api.SimpleAccountApi;
import com.technokratos.dto.request.CreateAccountRequest;
import com.technokratos.dto.request.UpdateAccountRequest;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class SimpleAccountController implements SimpleAccountApi {

    private final AccountService accountService;

    @Override
    public AccountResponse register(CreateAccountRequest request) {

        return accountService.save(request);
    }

    @Override
    public AccountResponse update(UpdateAccountRequest request, UserDetailsImpl userDetails) {

        return accountService.update(userDetails.getAccount().getId(), request);
    }

    @Override
    public MessageResponse delete(UserDetailsImpl userDetails) {

        UUID accountId = accountService.delete(userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Account this id %s successfully deleted", accountId))
                .build();
    }
}
