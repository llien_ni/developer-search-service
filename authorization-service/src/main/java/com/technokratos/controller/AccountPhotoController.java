package com.technokratos.controller;

import com.technokratos.api.AccountPhotoApi;
import com.technokratos.dto.request.PhotoRequest;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.PhotoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
public class AccountPhotoController implements AccountPhotoApi {

    private final PhotoService photoService;

    @Override
    public PhotoResponse add(PhotoRequest request, UserDetailsImpl userDetails) {

        return photoService.uploadPhoto(request, userDetails.getAccount().getId());
    }

    @Override
    public MessageResponse delete(UserDetailsImpl userDetails) {

        UUID id = photoService.delete(userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Photo %s successfully deleted", id))
                .build();
    }
}
