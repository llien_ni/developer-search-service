package com.technokratos.controller;

import com.technokratos.api.EducationApi;
import com.technokratos.dto.request.EducationRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.EducationResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.EducationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class EducationController implements EducationApi {

    private final EducationService educationService;

    @Override
    public EducationResponse add(EducationRequest request, UserDetailsImpl userDetails) {

        return educationService.add(request, userDetails.getAccount().getId());
    }

    @Override
    public MessageResponse delete(RequestID requestID, UserDetailsImpl userDetails) {

        UUID id = educationService.delete(requestID, userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Education %s successfully deleted", id))
                .build();
    }

    @Override
    public Page<EducationResponse> getAllByDeveloper(Integer size, Integer number, UUID developerId) {

        return educationService.getAllByDeveloper(size, number, developerId);
    }
}
