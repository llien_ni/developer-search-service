package com.technokratos.controller;

import com.technokratos.api.GrievanceApi;
import com.technokratos.dto.request.GrievanceRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.GrievanceResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.GrievanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class GrievanceController implements GrievanceApi {

    private final GrievanceService grievanceService;

    @Override
    public GrievanceResponse add(GrievanceRequest request, UserDetailsImpl userDetails) {

        return grievanceService.add(request, userDetails.getAccount().getId());
    }

    @Override
    public Page<GrievanceResponse> getByAuthor(Integer size, Integer number, UUID authorId) {

        return grievanceService.getByAuthor(size, number, authorId);
    }

    @Override
    public Page<GrievanceResponse> getByAccount(Integer size, Integer number, UUID id) {

        return grievanceService.getByAccount(size, number, id);
    }

    @Override
    public Page<GrievanceResponse> getAll(Integer size, Integer number) {

        return grievanceService.getAll(size, number);
    }

    @Override
    public MessageResponse delete(RequestID request) {

        String grievanceId = grievanceService.delete(request.getId()).toString();
        return MessageResponse.builder()
                .message(String.format("Grievance %s deleted", grievanceId))
                .build();
    }

    @Override
    public GrievanceResponse getById(UUID id) {

        return grievanceService.getById(id);
    }
}
