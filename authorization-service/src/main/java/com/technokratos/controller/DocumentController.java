package com.technokratos.controller;

import com.technokratos.api.DocumentApi;
import com.technokratos.dto.request.DocumentRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.DocumentResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.DocumentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class DocumentController implements DocumentApi {

    private final DocumentService documentService;

    @Override
    public DocumentResponse uploadDocument(MultipartFile file, DocumentRequest request, UserDetailsImpl userDetails) {

        return documentService.uploadFile(file, request, userDetails.getAccount().getId());
    }

    @Override
    public void downloadDocument(UUID documentId, HttpServletResponse response) {

        documentService.downloadDocument(documentId, response);
    }

    @Override
    public MessageResponse delete(RequestID request, UserDetailsImpl userDetails) {

        UUID id = documentService.delete(request.getId(), userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Document %s successfully deleted", id))
                .build();
    }

    @Override
    public Page<DocumentResponse> getDocumentsByDeveloperAndDownloadUrls(UUID developerId, Integer size, Integer number) {

        return documentService.getDocumentsByDeveloper(developerId, size, number);
    }

    @Override
    public DocumentResponse getDocumentById(UUID documentId) {

        return documentService.getDocumentById(documentId);
    }
}
