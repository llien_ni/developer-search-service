package com.technokratos.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.api.DeveloperApi;
import com.technokratos.dto.request.CreateDeveloperRequest;
import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.UpdateDeveloperRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Status;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.DeveloperService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class DeveloperController implements DeveloperApi {

    private final DeveloperService developerService;

    private final ObjectMapper objectMapper;

    @Override
    public DeveloperResponse register(CreateDeveloperRequest request) {

        return developerService.save(request);
    }

    @Override
    public DeveloperResponse update(UpdateDeveloperRequest request, UserDetailsImpl userDetails) {

        return developerService.update(userDetails.getAccount().getId(), request);
    }

    @Override
    public MessageResponse delete(UserDetailsImpl userDetails) {

        UUID id = developerService.delete(userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Developer %s successfully deleted", id))
                .build();

    }

    @Override
    public DeveloperResponse getById(UUID id) {

        return developerService.findByIdAndStatus(id, Status.CONFIRMED);
    }

    @Override
    public Page<DeveloperResponse> getAllDevelopersByStateActive(Integer size, Integer number) {

        return developerService.findAllByStateAndStatus(size, number, DeveloperState.ACTIVE, Status.CONFIRMED);
    }

    @Override
    public Page<DeveloperResponse> getDevelopersByIdsList(ListIdsRequest request, Integer size, Integer number) {

        return developerService.findByIdsList(request.getIds(), size, number);
    }

    @Override
    public DeveloperResponse getDeveloperByUserName(String username) {

        return developerService.findByUserName(username);
    }
}
