package com.technokratos.controller;

import com.technokratos.api.AppealApi;
import com.technokratos.dto.request.AppealRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.AppealResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.AppealService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AppealController implements AppealApi {

    private final AppealService appealService;

    @Override
    public AppealResponse add(AppealRequest request, UserDetailsImpl userDetails) {

        return appealService.add(request, userDetails.getAccount().getId());
    }

    @Override
    public MessageResponse delete(RequestID requestID, UserDetailsImpl userDetails) {

        UUID appealId = appealService.delete(requestID.getId(), userDetails.getAccount().getId());
        return MessageResponse.builder()
                .message(String.format("Appeal %s is deleted", appealId))
                .build();
    }

    @Override
    public Page<AppealResponse> getByAuthor(String status, Integer size, Integer number, UserDetailsImpl userDetails) {

        return appealService.getByAuthorAndStatus(status, size, number, userDetails.getAccount().getId());
    }

    @Override
    public AppealResponse getById(UUID id, UserDetailsImpl userDetails) {

        return appealService.getById(id, userDetails.getAccount().getId());
    }
}
