package com.technokratos.exception;

public class GrievanceNotFoundException extends NotFoundException {

    private static final String MESSAGE = "Grievance not found";

    public GrievanceNotFoundException() {
        super(MESSAGE);
    }
}
