package com.technokratos.exception;

public class AccountIsNotConfirmed extends BadRequestException {

    private static final String MESSAGE = "Account is not confirmed";

    public AccountIsNotConfirmed() {

        super(MESSAGE);
    }
}
