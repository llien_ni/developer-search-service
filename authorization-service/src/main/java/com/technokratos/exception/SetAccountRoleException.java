package com.technokratos.exception;


public class SetAccountRoleException extends BadRequestException {

    private static final String MESSAGE = "Can't change role to %s";

    public SetAccountRoleException(String role) {
        super(String.format(MESSAGE, role));
    }
}
