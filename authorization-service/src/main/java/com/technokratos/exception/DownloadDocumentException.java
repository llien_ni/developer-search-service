package com.technokratos.exception;

public class DownloadDocumentException extends BadRequestException {

    private static final String MESSAGE = "Download document exception";

    public DownloadDocumentException() {

        super(MESSAGE);
    }
}
