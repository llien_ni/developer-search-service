package com.technokratos.exception;

public class ChangeEmailException extends BadRequestException {

    private static final String MESSAGE = "Invalid password entered";

    public ChangeEmailException() {
        super(MESSAGE);
    }
}
