package com.technokratos.exception;

public class SpecializationNotFoundException extends NotFoundException {

    private static final String MESSAGE = "Specialization not found";

    public SpecializationNotFoundException() {
        super(MESSAGE);
    }
}
