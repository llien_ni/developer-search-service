package com.technokratos.exception;

import java.util.UUID;

public class UserDeleteException extends BadRequestException {

    private static final String MESSAGE = "User %s cannot be deleted";

    public UserDeleteException(UUID id) {
        super(String.format(MESSAGE, id));
    }
}
