package com.technokratos.exception;

public class EducationNotFoundException extends NotFoundException {

    private static final String MESSAGE = "Education not found exception";

    public EducationNotFoundException() {
        super(MESSAGE);
    }
}
