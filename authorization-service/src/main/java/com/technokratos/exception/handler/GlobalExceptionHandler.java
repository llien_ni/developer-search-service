package com.technokratos.exception.handler;


import com.google.protobuf.InvalidProtocolBufferException;
import com.google.rpc.Status;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.ResponseErrorMessage;
import com.technokratos.exception.BadRequestException;
import com.technokratos.exception.NotFoundException;
import error.service.Error;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.StatusProto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(StatusRuntimeException.class)
    public ResponseEntity<ExceptionResponse> handleStatusRuntimeException(StatusRuntimeException exception) {

        log.info("handle StatusRuntimeException");

        Status status = StatusProto.fromThrowable(exception);
        Error.ErrorInfo errorInfo = status.getDetailsList()
                .stream()
                .filter(any -> any.is(Error.ErrorInfo.class))
                .map(e -> {
                    try {
                        return e.unpack(Error.ErrorInfo.class);
                    } catch (InvalidProtocolBufferException ex) {
                        throw new IllegalArgumentException("can't unpack error");
                    }
                }).findFirst().orElseThrow(() -> new IllegalArgumentException("can't extract errorInfo"));

        return ResponseEntity.status(errorInfo.getCode()).body(ExceptionResponse.builder()
                .message(errorInfo.getMsg())
                .build());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public <T extends NotFoundException>  ExceptionResponse handleNotFoundException(T exception){
        return ExceptionResponse.builder()
                .message(exception.getMessage())
                .build();
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public <T extends BadRequestException>  ExceptionResponse handleAlreadyExistException(T exception){
        return ExceptionResponse.builder()
                .message(exception.getMessage())
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ResponseErrorMessage handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return ResponseErrorMessage.builder()
                .status(HttpStatus.BAD_REQUEST)
                .errors(ex.getBindingResult()
                        .getFieldErrors()
                        .stream()
                        .map(fieldError ->
                                new ResponseErrorMessage.Error(fieldError.getField(),
                                        fieldError.getCode(), fieldError.getDefaultMessage()))
                        .collect(Collectors.toList()))
                .build();
    }

}
