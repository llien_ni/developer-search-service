package com.technokratos.exception;

public class ChangePasswordException extends BadRequestException {

    private static final String MESSAGE = "Failed to change password";

    public ChangePasswordException() {
        super(MESSAGE);
    }
}
