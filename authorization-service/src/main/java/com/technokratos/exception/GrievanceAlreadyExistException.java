package com.technokratos.exception;

public class GrievanceAlreadyExistException extends BadRequestException {

    private static final String MESSAGE = "Grievance already exist";

    public GrievanceAlreadyExistException() {

        super(MESSAGE);
    }
}
