package com.technokratos.exception;

public class RestoreAccountException extends BadRequestException {

    private static final String MESSAGE = "Failed to recover account. ";

    public RestoreAccountException(String message) {
        super(MESSAGE.concat(message));
    }
}
