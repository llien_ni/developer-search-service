package com.technokratos.exception;

import java.util.UUID;

public class AccountSendManyGrievances extends BadRequestException{

    private static final String MESSAGE = "Account %s Sent many grievances";

    public AccountSendManyGrievances(UUID id) {

        super(String.format(MESSAGE, id));
    }
}
