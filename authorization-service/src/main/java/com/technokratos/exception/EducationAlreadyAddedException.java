package com.technokratos.exception;

public class EducationAlreadyAddedException extends BadRequestException {

    private static final String MESSAGE = "User already added this education";

    public EducationAlreadyAddedException() {
        super(MESSAGE);
    }
}
