package com.technokratos.exception;

public class AppealNotFoundException extends NotFoundException{

    private static final String MESSAGE = "Appeal not found";

    public AppealNotFoundException() {
        super(MESSAGE);
    }
}
