package com.technokratos.exception;

public class LinkNotFoundException extends NotFoundException {

    private static final String MESSAGE = "link not found";

    public LinkNotFoundException() {
        super(MESSAGE);
    }
}
