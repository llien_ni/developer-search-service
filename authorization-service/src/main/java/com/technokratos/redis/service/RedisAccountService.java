package com.technokratos.redis.service;

import com.technokratos.model.entity.AccountEntity;

public interface RedisAccountService {

    void addTokenToAccount(AccountEntity account, String accessToken);

    void addAllTokensToBlackList(AccountEntity account);
}
