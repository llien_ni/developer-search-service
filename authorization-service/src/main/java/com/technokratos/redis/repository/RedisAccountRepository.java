package com.technokratos.redis.repository;

import com.technokratos.redis.model.RedisAccount;
import org.springframework.data.keyvalue.repository.KeyValueRepository;

public interface RedisAccountRepository extends KeyValueRepository<RedisAccount, String> {
}
