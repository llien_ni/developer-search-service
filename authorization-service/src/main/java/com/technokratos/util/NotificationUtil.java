package com.technokratos.util;


import com.technokratos.dto.model.MailMessageModel;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.ConfirmationTokenEntity;
import lombok.experimental.UtilityClass;
import notification.service.NotificationServiceOuterClass;

import static com.technokratos.util.constants.Constants.AUTHORIZATION_SERVICE_URL;

@UtilityClass
public class NotificationUtil {

    public static NotificationServiceOuterClass.MailMessageRequest createMailMessageToConfirmAccount(ConfirmationTokenEntity tokenForAccount, AccountEntity account) {
        return NotificationServiceOuterClass.MailMessageRequest.newBuilder()
                        .setTo(account.getEmail())
                        .setSubject("PLEASE CONFIRM YOUR ACCOUNT")
                        .setMessage(AUTHORIZATION_SERVICE_URL+"/account/confirm?token=" + tokenForAccount.getToken())
                        .build();
    }

    public static NotificationServiceOuterClass.MailMessageRequest createMailMessageToChangeEmail(String token, String newEmail) {
        return NotificationServiceOuterClass.MailMessageRequest.newBuilder()
                .setTo(newEmail)
                .setSubject("PLEASE CONFIRM CHANGE EMAIL")
                .setMessage(AUTHORIZATION_SERVICE_URL+"/account/change-email/confirm?token=" + token)
                .build();
    }

    public static NotificationServiceOuterClass.MailMessageRequest resetPassword(ConfirmationTokenEntity tokenForAccount, String email) {
        return NotificationServiceOuterClass.MailMessageRequest.newBuilder()
                .setTo(email)
                .setSubject("CHANGE PASSWORD REQUEST")
                .setMessage(AUTHORIZATION_SERVICE_URL+"/account/reset-password/confirm?token=" + tokenForAccount.getToken())
                .build();
    }

    public static NotificationServiceOuterClass.MailMessageRequest restoreAccount(ConfirmationTokenEntity tokenForAccount, String email) {
        return NotificationServiceOuterClass.MailMessageRequest.newBuilder()
                .setTo(email)
                .setSubject("YOU HAVE SENT A REQUEST TO RESTORE YOUR ACCOUNT")
                .setMessage(AUTHORIZATION_SERVICE_URL+"/account/confirm?token=" + tokenForAccount.getToken())
                .build();
    }

    public static MailMessageModel createMailMessageAboutAccountIsBlocked(AccountEntity account) {

        return MailMessageModel.builder()
                .message("your account has been blocked")
                .to(account.getEmail())
                .subject("YOUR ACCOUNT HAS BEEN BLOCKED")
                .build();
    }
    public static MailMessageModel createMailMessageAboutAccountIsUnBlocked(AccountEntity account) {

        return MailMessageModel.builder()
                .message("your account has been unblocked")
                .to(account.getEmail())
                .subject("YOUR ACCOUNT HAS BEEN UNBLOCKED")
                .build();
    }

    public static MailMessageModel createMailMessageThatHrConfirmed(AccountEntity account) {

        return MailMessageModel.builder()
                .message("your account has been confirmed")
                .to(account.getEmail())
                .subject("YOUR ACCOUNT HAS BEEN CONFIRMED")
                .build();
    }

    public static NotificationServiceOuterClass.SmsModel createSmsConfirmAccount(ConfirmationTokenEntity tokenForAccount, AccountEntity account) {

        return NotificationServiceOuterClass.SmsModel.newBuilder()
                .setTo(account.getPhoneNumber())
                .setMessage("Сode "+tokenForAccount.getToken())
                .build();
    }

    public static NotificationServiceOuterClass.SmsModel resetPasswordByPhoneNumber(ConfirmationTokenEntity tokenForAccount, String phoneNumber) {

        return NotificationServiceOuterClass.SmsModel.newBuilder()
                .setTo(phoneNumber)
                .setMessage("Reset password code "+tokenForAccount.getToken())
                .build();
    }

    public static NotificationServiceOuterClass.SmsModel restoreAccountByPhoneNumber(ConfirmationTokenEntity tokenForAccount, String phoneNumber) {

        return NotificationServiceOuterClass.SmsModel.newBuilder()
                .setTo(phoneNumber)
                .setMessage("Restore account code "+tokenForAccount.getToken())
                .build();
    }
}
