package com.technokratos.repository;

import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.GrievanceEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface GrievanceRepository extends JpaRepository<GrievanceEntity, UUID> {

    List<GrievanceEntity> findAllByAccount(AccountEntity account);

    List<GrievanceEntity> findAllByAuthorAndCreatedDateBetween(AccountEntity account, OffsetDateTime beginTime, OffsetDateTime endTime);

    Page<GrievanceEntity> findAllByAuthor_Id(UUID id, Pageable pageable);

    Page<GrievanceEntity> findAllByAccount_Id(UUID accountId, Pageable pageable);

    Optional<GrievanceEntity> findByAccount_IdAndAuthor_Id(UUID accountId, UUID authorId);

    Page<GrievanceEntity> findAllByAuthor(AccountEntity author, PageRequest withSort);

    Page<GrievanceEntity> findAllByAccount(AccountEntity account, PageRequest withSort);
}
