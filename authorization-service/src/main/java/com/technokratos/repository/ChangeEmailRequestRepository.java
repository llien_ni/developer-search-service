package com.technokratos.repository;

import com.technokratos.model.entity.ChangeEmailRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ChangeEmailRequestRepository extends JpaRepository<ChangeEmailRequestEntity, UUID> {

    Optional<ChangeEmailRequestEntity> findByToken(UUID token);

    List<ChangeEmailRequestEntity> findChangeEmailRequestEntitiesByExpiredDateLessThan(OffsetDateTime dateTime);
}
