package com.technokratos.repository;


import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.HrEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface HrRepository extends JpaRepository<HrEntity, UUID> {

    Page<HrEntity> findAllByStatus(Status status, Pageable pageable);

    Page<HrEntity> findAllByStatusAndCompanyId(Status status, UUID companyId, Pageable pageable);

    Optional<HrEntity> findByIdAndStatus(UUID id, Status status);
}
