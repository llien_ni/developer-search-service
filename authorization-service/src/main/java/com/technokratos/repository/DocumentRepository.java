package com.technokratos.repository;

import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.DocumentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface DocumentRepository extends JpaRepository<DocumentEntity, UUID> {

    Page<DocumentEntity> findAllByDeveloper(DeveloperEntity developer, Pageable pageable);

    Optional<DocumentEntity> findByIdAndAndDeveloper_Id(UUID id, UUID developerId);
}
