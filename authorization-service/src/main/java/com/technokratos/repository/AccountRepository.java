package com.technokratos.repository;

import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, UUID> {

    Optional<AccountEntity> findByEmail(String email);

    Optional<AccountEntity> findByPhoneNumber(String phoneNumber);

    List<AccountEntity> findAllByStatus(Status status);

    Page<AccountEntity> findAllByStatus(Status status, Pageable pageable);
}
