package com.technokratos.repository;

import com.technokratos.model.entity.EducationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface EducationRepository extends JpaRepository<EducationEntity, UUID> {

    Optional<EducationEntity> findAllByNameAndTypeAndDeveloper_Id(String name, String type, UUID developerId);

    Optional<EducationEntity>  findAllByIdAndDeveloper_Id(UUID id, UUID developerId);

    Page<EducationEntity> findAllByDeveloper_Id(UUID developerId, Pageable pageable);

    Optional<EducationEntity> findByIdAndDeveloper_Id(UUID id, UUID developerId);
}
