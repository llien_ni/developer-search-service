package com.technokratos.repository;

import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.AppealEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AppealRepository extends JpaRepository<AppealEntity, UUID> {

    Page<AppealEntity> findAllByAuthorAndStatus(AccountEntity author, AppealEntity.Status status, Pageable pageable);

    Page<AppealEntity> findAllByAuthor(AccountEntity author, Pageable pageable);

    Page<AppealEntity> findAllByStatus(AppealEntity.Status status, Pageable pageable);

    Optional<AppealEntity> findByIdAndAuthor_Id(UUID id, UUID authorId);
}
