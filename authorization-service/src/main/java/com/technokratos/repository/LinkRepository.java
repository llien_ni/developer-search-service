package com.technokratos.repository;

import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.LinkEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface LinkRepository extends JpaRepository<LinkEntity, UUID> {

    Page<LinkEntity> findAllByDeveloper_Id(UUID developerId, Pageable pageable);

    Optional<LinkEntity> findByIdAndDeveloper_Id(UUID id, UUID developerId);

    Page<LinkEntity> findAllByDeveloper(DeveloperEntity developer, PageRequest pageRequest);
}
