package com.technokratos.repository;

import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.SpecializationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DeveloperRepository extends JpaRepository<DeveloperEntity, UUID> {

    Optional<DeveloperEntity> findByIdAndStatus(UUID id, Status status);

    Optional<DeveloperEntity> findByUsernameAndStatus(String username, Status status);

    List<DeveloperEntity> findAllByState(DeveloperState state);

    Page<DeveloperEntity> findAllByIdIsIn(List<UUID> list, Pageable pageable);

    Page<DeveloperEntity> findAllByStateAndStatus(DeveloperState state, Status status, Pageable pageable);

    Page<DeveloperEntity> findDeveloperEntityBySpecializationsIn(List<SpecializationEntity> specializations, Pageable pageable);

    Optional<DeveloperEntity> findByUsername(String username);
}
