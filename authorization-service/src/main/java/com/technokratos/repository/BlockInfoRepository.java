package com.technokratos.repository;

import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.BlockInfoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BlockInfoRepository extends JpaRepository<BlockInfoEntity, UUID> {

    Optional<BlockInfoEntity> findByAccountAndActiveTrue(AccountEntity account);

    Page<BlockInfoEntity> findByAccount(AccountEntity account, Pageable pageable);

    List<BlockInfoEntity> findAllByActiveTrue();
}
