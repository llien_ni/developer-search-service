package com.technokratos.repository;

import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.SpecializationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface SpecializationRepository extends JpaRepository<SpecializationEntity, UUID> {

    Page<SpecializationEntity> findByDeveloper(Pageable pageable, DeveloperEntity developer);

    List<SpecializationEntity> findSpecializationEntitiesByNameIn(List<String> list);

    Optional<SpecializationEntity> findAllByNameAndDeveloper(String name, DeveloperEntity developer);

    Optional<SpecializationEntity> findByIdAndDeveloper_Id(UUID id, UUID developerId);
}
