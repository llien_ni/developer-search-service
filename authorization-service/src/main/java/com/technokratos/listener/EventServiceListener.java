package com.technokratos.listener;


import com.technokratos.client.NotificationServiceRabbit;
import com.technokratos.dto.model.MailMessageModel;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class EventServiceListener {

    private final AccountService accountService;

    private final NotificationServiceRabbit notificationServiceRabbit;

    @RabbitListener(queues = {"q.from-event-service-to-auth-service"})
    public void handleMessage(@Payload MailMessageModel mailMessageModel) {

        AccountResponse accountResponse = accountService.getById(UUID.fromString(mailMessageModel.getTo()));
        mailMessageModel.setTo(accountResponse.getEmail());
        notificationServiceRabbit.sendMailMessageFromEventService(mailMessageModel);
    }
}
