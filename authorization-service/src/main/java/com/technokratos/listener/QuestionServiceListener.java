package com.technokratos.listener;


import com.technokratos.dto.request.RatingChangeRequest;
import com.technokratos.service.DeveloperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class QuestionServiceListener {

    private final DeveloperService developerService;

    @RabbitListener(queues = {"q.sending-from-questions-service"})
    public void handleMessage(@Payload RatingChangeRequest ratingChangeRequest) {
        log.info("Get ratingChangeRequest from questions-service");
        developerService.updateDeveloperRating(ratingChangeRequest.getDeveloperId(), ratingChangeRequest.getValue());
        log.info("Changed");
    }
}
