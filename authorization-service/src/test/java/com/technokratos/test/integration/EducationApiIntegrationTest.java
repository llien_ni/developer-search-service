package com.technokratos.test.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.EducationRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.EducationResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.EducationEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.EducationRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/education-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class EducationApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String DEVELOPER_EDUCATION_PREFIX = "/developer/education";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private EducationRepository educationRepository;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Test
    void addEducationSuccessfully()throws Exception{

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(0, developer.getEducations().size());

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        EducationRequest educationRequest = EducationRequest.builder()
                .name("KFU")
                .type("Univer")
                .startDate(LocalDate.now())
                .build();

        EducationResponse educationResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_EDUCATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(educationRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(developer.getId());
        Assertions.assertEquals(1, updatedDeveloper.getEducations().size());

        EducationEntity educationEntity = educationRepository.getReferenceById(educationResponse.getId());
        Assertions.assertEquals(educationEntity.getName(), educationResponse.getName());
        Assertions.assertEquals(educationEntity.getType(), educationResponse.getType());
        Assertions.assertNotNull(educationEntity.getDeveloper());
        Assertions.assertNotNull(educationEntity.getDeveloper().getId());
        Assertions.assertNotNull(educationEntity.getId());
    }

    @Test
    void addEducationFailedDeveloperAlreadyAddThisEducation()throws Exception{

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(1, developer.getEducations().size());

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        EducationRequest educationRequest = EducationRequest.builder()
                .name("KFU")
                .type("Univer")
                .startDate(LocalDate.now())
                .build();

        ExceptionResponse exceptionResponse =  objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_EDUCATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(educationRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteEducationSuccessfully()throws Exception{

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(1, developer.getEducations().size());

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        RequestID requestID = new RequestID(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        MessageResponse messageResponse =  objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + DEVELOPER_EDUCATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestID))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(developer.getId());
        Assertions.assertEquals(0, updatedDeveloper.getEducations().size());
        Assertions.assertTrue(educationRepository.findById(requestID.getId()).isEmpty());
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void deleteEducationFailedEducationNotFound()throws Exception{

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(1, developer.getEducations().size());

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        RequestID requestID = new RequestID(UUID.fromString("16dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        ExceptionResponse exceptionResponse =  objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + DEVELOPER_EDUCATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestID))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllSuccessfully()throws Exception{

        String developerId = "14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ResponsePage<EducationResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_EDUCATION_PREFIX+"/"+ developerId)
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertEquals("14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b", page.getContent().get(0).getId().toString());
    }
}
