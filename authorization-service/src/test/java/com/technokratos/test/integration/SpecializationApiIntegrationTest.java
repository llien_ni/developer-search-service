package com.technokratos.test.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.*;
import com.technokratos.dto.response.*;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.RemoveBucketArgs;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Sql(scripts = "/sql/specialization-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class SpecializationApiIntegrationTest extends AbstractIntegrationTest{

    @BeforeEach
    void setUp() throws Exception {

        minioClient.makeBucket(MakeBucketArgs.builder().bucket("account-photo").build());
        minioClient.makeBucket(MakeBucketArgs.builder().bucket("developer-document").build());
    }

    @AfterEach
    void afterEach() throws Exception {

        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("account-photo").build());
        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("developer-document").build());
    }

    private static final String LOCALHOST = "http://localhost:";

    private static final String DEVELOPER_SPECIALIZATION_PREFIX = "/developer/spec";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private MinioClient minioClient;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Test
    void addSpecializationSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(0, developer.getSpecializations().size());

        SpecializationRequest specializationRequest = new SpecializationRequest("Backend");

        SpecializationResponse specializationResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(specializationRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertEquals(specializationRequest.getName().toLowerCase(Locale.ROOT), specializationResponse.getName());
        Assertions.assertNotNull(specializationResponse.getId());
        Assertions.assertEquals(specializationResponse.getDeveloperId(), developer.getId().toString());

        Assertions.assertEquals(1, updatedDeveloper.getSpecializations().size());
        Assertions.assertNotNull(updatedDeveloper.getSpecializations().get(0).getName());
        Assertions.assertEquals(updatedDeveloper.getSpecializations().get(0).getId(), specializationResponse.getId());
    }

    @Test
    void addSpecializationFailedDeveloperAlreadyAddThisSpecialization() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        SpecializationRequest specializationRequest = new SpecializationRequest("Frontend");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(specializationRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteSpecializationSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(2, developer.getSpecializations().size());

        RequestID requestId = new RequestID(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(developer.getId());
        Assertions.assertEquals(1, updatedDeveloper.getSpecializations().size());
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void deleteSpecializationFailedSpecializationNotFound() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        SpecializationRequest specializationRequest = new SpecializationRequest("Frontend");

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(specializationRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByIdSpecializationSuccessfully() throws Exception {

        String specializationId = "12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        SpecializationResponse specializationResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX + "/" + specializationId)

                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertEquals("frontend", specializationResponse.getName());
    }

    @Test
    void getByIdSpecializationFailedSpecializationNotFound() throws Exception {

        String specializationId = "99dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX + "/" + specializationId)

                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByDeveloperSuccessfully() throws Exception {

        String developerId = "12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ResponsePage<SpecializationResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX)
                                .param("developer_id", developerId)
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertEquals("backend", page.getContent().get(0).getName());
        Assertions.assertEquals("frontend", page.getContent().get(1).getName());
    }

    @Test
    void getByDeveloperFailedDeveloperNotFound() throws Exception {

        String developerId = "97dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX + "/" + developerId)

                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllDistinctNamesSuccessfully() throws Exception {

        ResponsePage<String> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX + "/all")

                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        System.out.println(page.getContent());
        Assertions.assertEquals(3, page.getTotalElements());
    }

    @Test
    void getDevelopersBySpecializations() throws Exception {

        ListStringsRequest listStringsRequest = ListStringsRequest.builder()
                .list(List.of("backend", "frontend")).build();
        ResponsePage<DeveloperResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX +"/search")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listStringsRequest))
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
    }

    @Test
    void addSpecializationsListSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev10@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("19dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(0, developer.getSpecializations().size());

        List<SpecializationRequest> specializationRequests = new ArrayList<>();
        specializationRequests.add(new SpecializationRequest("backend"));
        specializationRequests.add(new SpecializationRequest("frontend"));
        specializationRequests.add(new SpecializationRequest("devops"));
        SpecializationsListRequest listStringsRequest = SpecializationsListRequest.builder()
                .specializations(specializationRequests)
                .build();

        mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_SPECIALIZATION_PREFIX +"/list")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listStringsRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isCreated());
        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(developer.getId());
        Assertions.assertEquals(3, updatedDeveloper.getSpecializations().size());
    }
}

