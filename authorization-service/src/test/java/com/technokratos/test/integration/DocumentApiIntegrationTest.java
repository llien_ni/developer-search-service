package com.technokratos.test.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.DocumentRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.DocumentResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.DocumentEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.DocumentRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import io.minio.*;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/document-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class DocumentApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String DOCUMENT_PREFIX = "/document";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private MinioClient minioClient;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @BeforeEach
    void setUp() throws Exception {

        minioClient.makeBucket(MakeBucketArgs.builder().bucket("account-photo").build());
        minioClient.makeBucket(MakeBucketArgs.builder().bucket("developer-document").build());
    }

    @AfterEach
    void afterEach() throws Exception {

        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("account-photo").build());
        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("developer-document").build());
    }

    @Test
    void uploadDocumentSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        MockMultipartFile document = new MockMultipartFile("file",
                "myDocument.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );
        DocumentRequest documentRequest = DocumentRequest.builder()
                .description("My file")
                .build();

        DocumentResponse documentResponse = objectMapper.readValue(mockMvc.perform(
                        multipart(LOCALHOST + port + DOCUMENT_PREFIX + "/upload")
                                .file(document)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(documentRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        DocumentEntity documentEntity = documentRepository.getReferenceById(documentResponse.getId());

        Assertions.assertNotNull(developer.getDocuments());
        Assertions.assertNotNull(developer.getDocuments().get(0));
        Assertions.assertEquals(document.getOriginalFilename(), developer.getDocuments().get(0).getFileName());
        Assertions.assertEquals(documentResponse.getDeveloperId(), developer.getId());
        Assertions.assertNotNull(documentEntity.getDeveloper());
        Assertions.assertNotNull(documentEntity.getFileNameInBucket());
        Assertions.assertEquals(documentRequest.getDescription(), documentEntity.getDescription());

        minioClient.removeObject(RemoveObjectArgs.builder()
                .bucket("developer-document")
                .object(documentEntity.getFileNameInBucket())
                .build());
    }

    @Test
    void downloadDocumentSuccessfully() throws Exception {

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        DocumentEntity documentEntity = developer.getDocuments().get(0);

        MockMultipartFile document = new MockMultipartFile("file",
                documentEntity.getFileName(),
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );
        minioClient.putObject(PutObjectArgs.builder().bucket("developer-document")
                .contentType(documentEntity.getType())
                .stream(document.getInputStream(), document.getSize(), -1)
                .object(documentEntity.getFileNameInBucket())
                .build());

        mockMvc.perform(
                get(LOCALHOST + port + DOCUMENT_PREFIX + "/download" + "/" + documentEntity.getId())
        ).andExpect(status().isOk());


        minioClient.removeObject(RemoveObjectArgs.builder()
                .bucket("developer-document")
                .object(documentEntity.getFileNameInBucket())
                .build());
    }

    @Test
    void downloadDocumentFailedFileNotInBucket() throws Exception {

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        DocumentEntity documentEntity = developer.getDocuments().get(0);

        mockMvc.perform(
                get(LOCALHOST + port + DOCUMENT_PREFIX + "/download" + "/" + documentEntity.getId())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void downloadDocumentFailedFileNotFound() throws Exception {

        mockMvc.perform(
                get(LOCALHOST + port + DOCUMENT_PREFIX + "/download" + "/" + UUID.randomUUID())
        ).andExpect(status().isNotFound());
    }

    @Test
    void deleteDocumentSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);


        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        DocumentEntity documentFirst = developer.getDocuments().get(0);
        DocumentEntity documentSecond = developer.getDocuments().get(1);

        MockMultipartFile fileFirst = new MockMultipartFile("file",
                documentFirst.getFileName(),
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );
        minioClient.putObject(PutObjectArgs.builder().bucket("developer-document")
                .contentType(documentFirst.getType())
                .stream(fileFirst.getInputStream(), fileFirst.getSize(), -1)
                .object(documentFirst.getFileNameInBucket())
                .build());

        MockMultipartFile fileSecond = new MockMultipartFile("file",
                documentSecond.getFileName(),
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );
        minioClient.putObject(PutObjectArgs.builder().bucket("developer-document")
                .contentType(documentSecond.getType())
                .stream(fileSecond.getInputStream(), fileSecond.getSize(), -1)
                .object(documentSecond.getFileNameInBucket())
                .build());

        Assertions.assertEquals(2, developer.getDocuments().size());

        RequestID requestId = new RequestID(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + DOCUMENT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(developer.getId());
        Assertions.assertEquals(1, updatedDeveloper.getDocuments().size());
        Assertions.assertEquals(updatedDeveloper.getDocuments().get(0).getId(), UUID.fromString("14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        minioClient.removeObject(RemoveObjectArgs.builder()
                .bucket("developer-document")
                .object(documentFirst.getFileNameInBucket())
                .build());
        minioClient.removeObject(RemoveObjectArgs.builder()
                .bucket("developer-document")
                .object(documentSecond.getFileNameInBucket())
                .build());
    }

    @Test
    void deleteDocumentFailedDocumentNotFound() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        RequestID requestId = new RequestID(UUID.fromString("15dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + DOCUMENT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getDocumentByIdSuccessfully() throws Exception {

        String documentId = "13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        DocumentEntity document = documentRepository.getReferenceById(UUID.fromString(documentId));

        MockMultipartFile file = new MockMultipartFile("file",
                document.getFileName(),
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );
        minioClient.putObject(PutObjectArgs.builder().bucket("developer-document")
                .contentType(document.getType())
                .stream(file.getInputStream(), file.getSize(), -1)
                .object(document.getFileNameInBucket())
                .build());

        DocumentResponse documentResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DOCUMENT_PREFIX + "/" + documentId)
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertEquals(document.getId(), documentResponse.getId());
        Assertions.assertEquals(document.getFileName(), documentResponse.getFileName());
        Assertions.assertEquals(document.getSize().longValue(), documentResponse.getSize());
        Assertions.assertEquals(document.getType(), documentResponse.getType());
        Assertions.assertEquals(document.getDescription(), documentResponse.getDescription());
        Assertions.assertNotNull(documentResponse.getDownloadUrl());
        Assertions.assertEquals(document.getDeveloper().getId(), documentResponse.getId());

        minioClient.removeObject(RemoveObjectArgs.builder()
                .bucket("developer-document")
                .object(document.getFileNameInBucket())
                .build());

    }

    @Test
    void getDocumentByIdFailedDocumentNotFound() throws Exception {

        String documentId = "17dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DOCUMENT_PREFIX + "/" + documentId)
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getDocumentsByDeveloperAndDownloadUrlsSuccessfully() throws Exception {

        String developerId = "13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        ResponsePage<DocumentResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DOCUMENT_PREFIX + "/developer" + "/" + developerId)
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertEquals(2, page.getTotalElements());
        DocumentResponse documentResponse = page.getContent().get(0);
        Assertions.assertNotNull(documentResponse.getId());
        Assertions.assertNotNull(documentResponse.getFileName());
        Assertions.assertNotNull(documentResponse.getDownloadUrl());
        Assertions.assertNotNull(documentResponse.getSize());
        Assertions.assertNotNull(documentResponse.getType());
        Assertions.assertNotNull(documentResponse.getDeveloperId());
        Assertions.assertNotNull(documentResponse.getDescription());
    }

    @Test
    void getDocumentsByDeveloperAndDownloadUrlsFailedDeveloperNotFound() throws Exception {

        String developerId = "19dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DOCUMENT_PREFIX + "/developer" + "/" + developerId)
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
