package com.technokratos.test.integration;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.CreateHrRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.UpdateHrRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.HrEntity;
import com.technokratos.repository.HrRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.RemoveBucketArgs;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/hr-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class HrApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String HR_PREFIX = "/hr";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HrRepository hrRepository;

    @Autowired
    private MinioClient minioClient;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @BeforeEach
    void setUp()throws Exception{

        minioClient.makeBucket(MakeBucketArgs.builder().bucket("account-photo").build());
        minioClient.makeBucket(MakeBucketArgs.builder().bucket("developer-document").build());

    }
    @AfterEach
    void afterEach()throws Exception{

        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("account-photo").build());
        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("developer-document").build());
    }

    @Test
    void registerSuccessfully() throws Exception {

        CreateHrRequest createHrRequest = CreateHrRequest.builder()
                .firstName("hrFirstName")
                .lastName("hrLastName")
                .email("hrik@gmail.com")
                .password("qwerty007")
                .city("Kazan")
                .companyName("Technokratia")
                .build();

        HrResponse hrResponse = objectMapper.readValue(
                mockMvc.perform(post(LOCALHOST + port + HR_PREFIX + "/sign-up")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createHrRequest)))
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });

        Assertions.assertEquals(createHrRequest.getFirstName(), hrResponse.getFirstName());
        Assertions.assertEquals(createHrRequest.getLastName(), hrResponse.getLastName());
        Assertions.assertEquals(createHrRequest.getEmail(), hrResponse.getEmail());
        Assertions.assertEquals(createHrRequest.getCity(), hrResponse.getCity());
        Assertions.assertNotNull(hrResponse.getId());
        Assertions.assertNotNull(hrResponse.getCompanyResponse());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
        Assertions.assertEquals(hrResponse.getStatus(), Status.NOT_CONFIRMED.toString());
    }

    @Test
    void registerFailedRequestIsInvalid() throws Exception {

        CreateHrRequest createHrRequest = CreateHrRequest.builder()
                .firstName("hrFirstName")
                .lastName("hrLastName")
                .email("hrik@gmail.com")
                .password("qwerty007")
                .city("Kazan")
                .build();

        mockMvc.perform(post(LOCALHOST + port + HR_PREFIX + "/sign-up")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createHrRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void registerFailedCompanyIsNotExist() throws Exception {

        CreateHrRequest createHrRequest = CreateHrRequest.builder()
                .firstName("hrFirstName")
                .lastName("hrLastName")
                .email("hrik@gmail.com")
                .password("qwerty007")
                .city("Kazan")
                .companyName("Pokemon")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(
                mockMvc.perform(post(LOCALHOST + port + HR_PREFIX + "/sign-up")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createHrRequest)))
                        .andExpect(status().isNotFound())
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void registerFailedAccountWithSuchEmailExist() throws Exception {

        CreateHrRequest createHrRequest = CreateHrRequest.builder()
                .firstName("hrFirstName")
                .lastName("hrLastName")
                .email("hr@gmail.com")
                .password("qwerty007")
                .city("Kazan")
                .companyName("Technokratia")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(
                mockMvc.perform(post(LOCALHOST + port + HR_PREFIX + "/sign-up")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createHrRequest)))
                        .andExpect(status().isBadRequest())
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void updateHrSuccessfully() throws Exception {

        UpdateHrRequest updateHrRequest = UpdateHrRequest.builder()
                .firstName("newName")
                .city("Ulyanovsk")
                .build();

        HrEntity hrEntity = hrRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login(hrEntity.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        HrResponse hrResponse = objectMapper.readValue(
                mockMvc.perform(patch(LOCALHOST + port + HR_PREFIX)
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                                .content(objectMapper.writeValueAsString(updateHrRequest)))
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });

        HrEntity updatedEntity = hrRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertEquals(updateHrRequest.getFirstName(), hrResponse.getFirstName());
        Assertions.assertEquals(updatedEntity.getLastName(), hrResponse.getLastName());
        Assertions.assertEquals(updatedEntity.getEmail(), hrResponse.getEmail());
        Assertions.assertEquals(updateHrRequest.getCity(), hrResponse.getCity());
        Assertions.assertNotNull(hrResponse.getId());
        Assertions.assertNotNull(hrResponse.getCompanyResponse());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
        Assertions.assertEquals(hrResponse.getStatus(), Status.CONFIRMED.toString());
    }

    @Test
    void updateHrFailedRequestIsInvalid() throws Exception {

        UpdateHrRequest updateHrRequest = UpdateHrRequest.builder()
                .firstName("DorogoyDnevnikMneNePeredatTuBol")
                .city("Ulyanovsk")
                .build();

        HrEntity hrEntity = hrRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login(hrEntity.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        mockMvc.perform(patch(LOCALHOST + port + HR_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(updateHrRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getByIdSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        HrEntity hrEntity = hrRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        HrResponse hrResponse = objectMapper.readValue(
                mockMvc.perform(get(LOCALHOST + port + HR_PREFIX + "/11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });

        Assertions.assertEquals(hrEntity.getFirstName(), hrResponse.getFirstName());
        Assertions.assertEquals(hrEntity.getLastName(), hrResponse.getLastName());
        Assertions.assertEquals(hrEntity.getEmail(), hrResponse.getEmail());
        Assertions.assertEquals(hrEntity.getCity(), hrResponse.getCity());
        Assertions.assertNotNull(hrResponse.getPhoto());
        Assertions.assertNotNull(hrResponse.getId());
        Assertions.assertNotNull(hrResponse.getCompanyResponse());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
        Assertions.assertEquals(hrResponse.getStatus(), Status.CONFIRMED.toString());
    }

    @Test
    void getByIdFailedHrNotFound() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(
                mockMvc.perform(get(LOCALHOST + port + HR_PREFIX + "/99dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                        .andExpect(status().isNotFound())
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ResponsePage<HrResponse> page = objectMapper.readValue(
                mockMvc.perform(get(LOCALHOST + port + HR_PREFIX)
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });
        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getCompanyResponse().getName());
        Assertions.assertNotNull(page.getContent().get(0).getId());
        Assertions.assertNotNull(page.getContent().get(0).getCity());
    }

    @Test
    void getAllByCompanySuccessfully() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ResponsePage<HrResponse> page = objectMapper.readValue(
                mockMvc.perform(get(LOCALHOST + port + HR_PREFIX + "/by")
                                .param("company", "Technokratia")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });
        Assertions.assertEquals(1, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getCompanyResponse().getName());
        Assertions.assertNotNull(page.getContent().get(0).getId());
        Assertions.assertNotNull(page.getContent().get(0).getCity());
    }

    @Test
    void getAllByCompanyFailedCompanyNotFound() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(
                mockMvc.perform(get(LOCALHOST + port + HR_PREFIX + "/by")
                                .param("company", "Pokemon")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                        .andExpect(status().isNotFound())
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteSuccessfully() throws Exception {

        HrEntity hrEntity = hrRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login(hrEntity.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        MessageResponse messageResponse = objectMapper.readValue(
                mockMvc.perform(delete(LOCALHOST + port + HR_PREFIX)
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                        .andExpect(status().isAccepted())
                        .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
                });

        HrEntity deletedEntity = hrRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(Status.DELETED, deletedEntity.getStatus());
        Assertions.assertNotNull(messageResponse.getMessage());
    }
}
