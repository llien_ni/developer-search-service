package com.technokratos.test.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.ResponseToAppeal;
import com.technokratos.dto.response.AppealResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.AppealEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.AppealRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Sql(scripts = "/sql/admin-appeal-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class AdminAppealApiIntegrationTest extends AbstractIntegrationTest{

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private AppealRepository appealRepository;

    @Autowired
    private MockMvc mockMvc;

    private static final String LOCALHOST = "http://localhost:";

    private static final String APPEAL_PREFIX = "/appeal";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AccountRepository accountRepository;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @BeforeEach
    void setUp() throws Exception {

        AccountEntity admin = accountRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login(admin.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

    }

    @Test
    void getByAuthorSuccessfully() throws Exception {

        String accountId = "12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ResponsePage<AppealResponse> appeals = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX+"/author"+"/"+accountId)
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        AppealResponse appealResponse = appeals.getContent().get(0);

        Assertions.assertEquals(2, appeals.getTotalElements());
        Assertions.assertEquals(appeals.getContent().get(0).getAuthor().getId().toString(), accountId);
        Assertions.assertNotNull(appealResponse.getAuthor().getFirstName());
        Assertions.assertNotNull(appealResponse.getAuthor().getLastName());
        Assertions.assertNotNull(appealResponse.getAuthor().getEmail());

        Assertions.assertNotNull(appealResponse.getCreatedDate());
        Assertions.assertNotNull(appealResponse.getId());
        Assertions.assertNotNull(appealResponse.getText());
        Assertions.assertNotNull(appealResponse.getTitle());
        Assertions.assertNotNull(appealResponse.getTitle());
    }

    @Test
    void getByAuthorAndStatusSuccessfully() throws Exception {

        String accountId = "12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ResponsePage<AppealResponse> appeals = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX+"/author"+"/"+accountId)
                                .param("status", AppealEntity.Status.OPEN.toString())
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        AppealResponse appealResponse = appeals.getContent().get(0);

        Assertions.assertEquals(1, appeals.getTotalElements());
        Assertions.assertEquals(appeals.getContent().get(0).getAuthor().getId().toString(), accountId);
        Assertions.assertNotNull(appealResponse.getAuthor().getFirstName());
        Assertions.assertNotNull(appealResponse.getAuthor().getLastName());
        Assertions.assertNotNull(appealResponse.getAuthor().getEmail());

        Assertions.assertNotNull(appealResponse.getCreatedDate());
        Assertions.assertNotNull(appealResponse.getId());
        Assertions.assertNotNull(appealResponse.getText());
        Assertions.assertNotNull(appealResponse.getTitle());
        Assertions.assertNotNull(appealResponse.getTitle());
    }

    @Test
    void getByAuthorFailedAuthorIsNotExist() throws Exception {

        String accountId = "99dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX+"/author"+"/"+accountId)
                                .param("status", AppealEntity.Status.OPEN.toString())
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByAuthorFailedStatusIsNotExist() throws Exception{

        String accountId = "12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX+"/author"+"/"+accountId)
                                .param("status", "Invalid status")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllSuccessfully() throws Exception{

        ResponsePage<AppealResponse> appeals = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX)
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertEquals(4, appeals.getTotalElements());
    }

    @Test
    void getAllByStatusSuccessfully() throws Exception{

        ResponsePage<AppealResponse> appeals = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX)
                                .param("status", AppealEntity.Status.CLOSED.toString())
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertEquals(1, appeals.getTotalElements());
    }

    @Test
    void getAllByStatusFailedStatusIsNotExist() throws Exception{

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX)
                                .param("status", "Invalid status")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByIdSuccessfully() throws Exception{

        String appealId = "12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        AppealEntity appealEntity = appealRepository.getReferenceById(UUID.fromString(appealId));

        AppealResponse appealResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX+"/"+appealId)

                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertEquals(appealEntity.getText(), appealResponse.getText());
        Assertions.assertEquals(appealEntity.getAuthor().getId(), appealResponse.getAuthor().getId());
    }

    @Test
    void getByIdFailedAppealIsNotExist() throws Exception{

        String appealId = "65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + APPEAL_PREFIX+"/"+appealId)

                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void respondToAppealSuccessfully() throws Exception{

        ResponseToAppeal responseToAppeal = ResponseToAppeal.builder()
                .response("I see you appeal")
                .id(UUID.fromString("21dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        AppealResponse appealResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + APPEAL_PREFIX+"/respond")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(responseToAppeal))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        AppealEntity appealEntity = appealRepository.getReferenceById(UUID.fromString("21dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(AppealEntity.Status.CLOSED, appealEntity.getStatus());
        Assertions.assertNotNull(appealEntity.getResponse());
        Assertions.assertNotNull(appealEntity.getResponseDate());
        Assertions.assertNotNull(appealResponse.getResponse());
        Assertions.assertNotNull(appealResponse.getResponseDate());

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("25dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(AppealEntity.Status.CLOSED, account.getAppeals().get(0).getStatus());
    }

    @Test
    void respondToAppealFailedAppealNotFound() throws Exception{

        ResponseToAppeal responseToAppeal = ResponseToAppeal.builder()
                .response("I see you appeal")
                .id(UUID.fromString("50dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + APPEAL_PREFIX+"/respond")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(responseToAppeal))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
