package com.technokratos.test.integration;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.AppealRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.AppealResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.AppealEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.AppealRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/appeal-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class AppealApiIntegrationTest extends AbstractIntegrationTest{

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    private static final String LOCALHOST = "http://localhost:";

    private static final String APPEAL_PREFIX = "/appeal/my";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AccountRepository accountRepository;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Autowired
    private AppealRepository appealRepository;

    @Test
    void addAppealSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("developer@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AppealRequest appealRequest = AppealRequest.builder()
                .text("some text")
                .title("appeal")
                .build();

        AppealResponse appealResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + APPEAL_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(appealRequest)))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AppealEntity savedAppeal = appealRepository.getReferenceById(account.getAppeals().get(0).getId());

        Assertions.assertNotNull(account.getAppeals().get(0).getId());
        Assertions.assertNotNull(savedAppeal);
        Assertions.assertEquals(savedAppeal.getText(), appealRequest.getText());
        Assertions.assertNotNull(appealResponse.getAuthor().getEmail());
        Assertions.assertEquals(appealResponse.getStatus(), AppealEntity.Status.OPEN.toString());
        Assertions.assertEquals(account.getId().toString(), appealResponse.getAuthor().getId().toString());
    }

    @Test
    void addAppealFailedInvalidRequest() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("developer@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AppealRequest appealRequest = AppealRequest.builder()
                .text("some text")
                .build();

        mockMvc.perform(post(LOCALHOST + port + APPEAL_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(appealRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteAppealSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("55dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(AppealEntity.Status.OPEN, account.getAppeals().get(0).getStatus());
        RequestID requestID = new RequestID(UUID.fromString("55dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(delete(LOCALHOST + port + APPEAL_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(requestID)))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        AppealEntity deletedAppeal = appealRepository.getReferenceById(UUID.fromString("55dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AccountEntity accountEntity = accountRepository.getReferenceById(UUID.fromString("55dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(AppealEntity.Status.DELETED, accountEntity.getAppeals().get(0).getStatus());
        Assertions.assertEquals(AppealEntity.Status.DELETED, deletedAppeal.getStatus());
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void deleteAppealFailedAppealNotFound() throws Exception {

        RequestID requestID = new RequestID(UUID.fromString("99dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(delete(LOCALHOST + port + APPEAL_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(requestID)))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByAuthorSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);
        ResponsePage<AppealResponse> page = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + APPEAL_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(3, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor());
        Assertions.assertNotNull(page.getContent().get(0).getCreatedDate());
    }

    @Test
    void getByAuthorAndStatusSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ResponsePage<AppealResponse> page = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + APPEAL_PREFIX)
                        .contentType("application/json")
                        .param("status", "OPEN")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor());
        Assertions.assertNotNull(page.getContent().get(0).getCreatedDate());
    }

    @Test
    void getByAuthorAndStatusFailedStatusIsInvalid() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        mockMvc.perform(get(LOCALHOST + port + APPEAL_PREFIX)
                        .contentType("application/json")
                        .param("status", "INVALID")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getByIdSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .param("status", "OPEN")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AppealResponse appealResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + APPEAL_PREFIX + "/12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertEquals(appealResponse.getAuthor().getId(), account.getId());
        Assertions.assertEquals(appealResponse.getAuthor().getFirstName(), account.getFirstName());
        Assertions.assertNotNull(appealResponse.getResponse());
    }

    @Test
    void getByIdFailedAppealIsNotExist() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .param("status", "OPEN")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + APPEAL_PREFIX + "/10dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
