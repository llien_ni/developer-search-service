package com.technokratos.test.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.BlockAccountRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.BlockInfoResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.HrResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.HrEntity;
import com.technokratos.repository.HrRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Sql(scripts = "/sql/moderator-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class ModeratorApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String MODERATOR_HR_PREFIX = "/moderator/hr";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HrRepository hrRepository;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @BeforeEach
    void setUp() throws Exception{
        LoginRequest loginRequest = LoginRequest.builder()
                .login("moderator@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);
    }

    @Test
    void confirmHrSuccessfully() throws Exception{

        RequestID requestId = new RequestID(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        HrResponse hrResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + MODERATOR_HR_PREFIX+"/confirm")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        HrEntity hrEntity = hrRepository.getReferenceById(requestId.getId());
        Assertions.assertEquals(Status.CONFIRMED, hrEntity.getStatus());
        Assertions.assertEquals(Status.CONFIRMED.toString(), hrResponse.getStatus());
        Assertions.assertNotNull(hrResponse.getId());
        Assertions.assertNotNull(hrResponse.getCity());
        Assertions.assertNotNull(hrResponse.getFirstName());
        Assertions.assertNotNull(hrResponse.getCompanyResponse());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
    }

    @Test
    void confirmHrFailedHrNotFound() throws Exception{

        RequestID requestId = new RequestID(UUID.fromString("96dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        ExceptionResponse exceptionResponse= objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + MODERATOR_HR_PREFIX+"/confirm")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void confirmHrThatWasBlockedSuccessfully() throws Exception{

        HrEntity hrEntity = hrRepository.getReferenceById(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(1, hrEntity.getBlockInfoEntities().size());
        Assertions.assertTrue(hrEntity.getBlockInfoEntities().get(0).isActive());

        RequestID requestId = new RequestID(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        HrResponse hrResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + MODERATOR_HR_PREFIX+"/confirm")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestId))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        HrEntity updatedHrEntity = hrRepository.getReferenceById(requestId.getId());
        Assertions.assertFalse(updatedHrEntity.getBlockInfoEntities().get(0).isActive());
        Assertions.assertEquals(1, updatedHrEntity.getBlockInfoEntities().size());
        Assertions.assertEquals(Status.CONFIRMED, updatedHrEntity.getStatus());
        Assertions.assertEquals(Status.CONFIRMED.toString(), hrResponse.getStatus());
        Assertions.assertNotNull(hrResponse.getId());
        Assertions.assertNotNull(hrResponse.getCity());
        Assertions.assertNotNull(hrResponse.getFirstName());
        Assertions.assertNotNull(hrResponse.getCompanyResponse());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
    }

    @Test
    void blockHrSuccessfully()throws Exception{

        BlockAccountRequest blockAccountRequest = BlockAccountRequest.builder()
                .accountId(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .reason("block")
                .build();

        BlockInfoResponse blockInfoResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + MODERATOR_HR_PREFIX+"/block")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(blockAccountRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        HrEntity hrEntity = hrRepository.getReferenceById(blockAccountRequest.getAccountId());
        Assertions.assertEquals(1, hrEntity.getBlockInfoEntities().size());
        Assertions.assertEquals(hrEntity.getBlockInfoEntities().get(0).getId(),blockInfoResponse.getId());
        Assertions.assertNotNull(hrEntity.getBlockInfoEntities().get(0).getId());
        Assertions.assertEquals(Status.BLOCKED, hrEntity.getStatus());
        Assertions.assertNotNull(blockInfoResponse.getAccount().getFirstName());
    }

    @Test
    void getHrsByStatus() throws Exception{

       ResponsePage<HrResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + MODERATOR_HR_PREFIX)
                                .param("status", "MODERATED")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

       Assertions.assertEquals(1, page.getTotalElements());
    }
}
