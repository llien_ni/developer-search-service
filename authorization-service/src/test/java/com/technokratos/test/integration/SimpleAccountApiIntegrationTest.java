package com.technokratos.test.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.CreateAccountRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.UpdateAccountRequest;
import com.technokratos.dto.response.AccountResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Sql(scripts = "/sql/simple-account-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class SimpleAccountApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String SIMPLE_ACCOUNT_PREFIX = "/simple_account";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Test
    void registerSuccessfully() throws Exception {

        CreateAccountRequest createAccountRequest = CreateAccountRequest
                .builder()
                .firstName("simFirstName")
                .lastName("simLastName")
                .email("sim@gmail.com")
                .password("qwerty007")
                .build();

        AccountResponse accountResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + SIMPLE_ACCOUNT_PREFIX+"/sign-up")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createAccountRequest))
                ).andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertEquals(createAccountRequest.getFirstName(), accountResponse.getFirstName());
        Assertions.assertEquals(createAccountRequest.getLastName(), accountResponse.getLastName());
        Assertions.assertEquals(createAccountRequest.getEmail(), accountResponse.getEmail());
        Assertions.assertNotNull(accountResponse.getId());
    }

    @Test
    void registerFailedDeveloperWithSuchEmailAlreadyExist() throws Exception {

        CreateAccountRequest createAccountRequest = CreateAccountRequest
                .builder()
                .firstName("simFirstName")
                .lastName("simLastName")
                .email("sim2@gmail.com")
                .password("qwerty007")
                .build();
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + SIMPLE_ACCOUNT_PREFIX+"/sign-up")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createAccountRequest))
                ).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void updateAccountSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("sim2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        UpdateAccountRequest updateAccountRequest = UpdateAccountRequest.builder()
                .firstName("New name")
                .build();

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AccountResponse accountResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + SIMPLE_ACCOUNT_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateAccountRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertEquals(updateAccountRequest.getFirstName(), accountResponse.getFirstName());
        Assertions.assertEquals(account.getLastName(), accountResponse.getLastName());
        Assertions.assertEquals(account.getId(), accountResponse.getId());
    }

    @Test
    void deleteAccountSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("sim2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + SIMPLE_ACCOUNT_PREFIX)
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(Status.DELETED, account.getStatus());
        Assertions.assertNotNull(messageResponse.getMessage());
    }
}
