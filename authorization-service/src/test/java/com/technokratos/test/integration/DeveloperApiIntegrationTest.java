package com.technokratos.test.integration;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.CreateDeveloperRequest;
import com.technokratos.dto.request.ListIdsRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.UpdateDeveloperRequest;
import com.technokratos.dto.response.DeveloperResponse;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.repository.BlacklistRepository;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@Sql(scripts = "/sql/developer-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class DeveloperApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String DEVELOPER_PREFIX = "/developer";

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private BlacklistRepository blacklistRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Test
    void registerDeveloperSuccessfully() throws Exception {

        CreateDeveloperRequest createDeveloperRequest = CreateDeveloperRequest.builder()
                .firstName("name")
                .lastName("lastName")
                .username("pokemon88")
                .email("dev88@gmail.com")
                .password("qwerty007")
                .dateOfBirth(LocalDate.of(2003, 10, 7))
                .workExperience(5)
                .build();

        DeveloperResponse developerResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_PREFIX + "/sign-up")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createDeveloperRequest)))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(developerResponse.getFirstName(), createDeveloperRequest.getFirstName());
        Assertions.assertNotNull(developerResponse.getId());
        Assertions.assertEquals(0, developerResponse.getRating());
        Assertions.assertEquals(developerResponse.getState(), DeveloperState.ACTIVE.toString());
        Assertions.assertEquals(developerResponse.getStatus(), Status.NOT_CONFIRMED.toString());
        Assertions.assertNotNull(developerResponse.getId());
        Assertions.assertEquals(createDeveloperRequest.getWorkExperience(), developerResponse.getWorkExperience());
    }

    @Test
    void registerDeveloperFailedInvalidRequest() throws Exception {

        CreateDeveloperRequest createDeveloperRequest = CreateDeveloperRequest.builder()
                .firstName("name")
                .lastName("lastname")
                .username("pokemon99")
                .email("p")
                .password("qwerty007")
                .dateOfBirth(LocalDate.of(2003, 10, 7))
                .workExperience(5)
                .build();

        mockMvc.perform(post(LOCALHOST + port + DEVELOPER_PREFIX + "/sign-up")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(createDeveloperRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void registerDeveloperFailedUsernameAlreadyExist() throws Exception {

        CreateDeveloperRequest createDeveloperRequest = CreateDeveloperRequest.builder()
                .firstName("name")
                .lastName("lastname")
                .username("pokemon")
                .email("mozart@gmail.com")
                .password("qwerty008")
                .dateOfBirth(LocalDate.of(2003, 10, 7))
                .workExperience(5)
                .build();

        mockMvc.perform(post(LOCALHOST + port + DEVELOPER_PREFIX + "/sign-up")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createDeveloperRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateDeveloperSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("developer@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperEntity oldDeveloper = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        UpdateDeveloperRequest updateDeveloperRequest = UpdateDeveloperRequest.builder()
                .firstName("New firstName")
                .username("New Username")
                .build();

        DeveloperResponse developerResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + DEVELOPER_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateDeveloperRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<DeveloperResponse>() {
        });

        Assertions.assertEquals(updateDeveloperRequest.getFirstName(), developerResponse.getFirstName());

        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertEquals(updateDeveloperRequest.getFirstName(), updatedDeveloper.getFirstName());
        Assertions.assertEquals(updateDeveloperRequest.getUsername(), updatedDeveloper.getUsername());
        Assertions.assertEquals(2, developerResponse.getEducations().size());
        Assertions.assertEquals(2, developerResponse.getSpecializations().size());
        Assertions.assertEquals(1, developerResponse.getLinks().size());
        Assertions.assertEquals(oldDeveloper.getLastName(), updatedDeveloper.getLastName());
        Assertions.assertNotNull(developerResponse.getId());
    }

    @Test
    void deleteDeveloperSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("developer@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + DEVELOPER_PREFIX)

                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(Status.DELETED, developer.getStatus());
        Assertions.assertEquals(DeveloperState.NOT_ACTIVE, developer.getState());
        Assertions.assertNotNull(messageResponse.getMessage());
    }


    @Test
    void updateDeveloperFailed() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("developer@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        UpdateDeveloperRequest updateDeveloperRequest = UpdateDeveloperRequest.builder()
                .firstName("New firstv very very very long name very very")
                .build();
        mockMvc.perform(patch(LOCALHOST + port + DEVELOPER_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateDeveloperRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteDeveloperFailedTokenInBlackList() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("developer@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        blacklistRepository.save(tokens.getAccessToken());

        mockMvc.perform(delete(LOCALHOST + DEVELOPER_PREFIX)
                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())).andExpect(status().isForbidden());
    }

    @Test
    void getDeveloperByUserNameFailedDeveloperNotfound() throws Exception {

        mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_PREFIX + "/username/" + "notusername"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getDeveloperByUserNameSuccessfully() throws Exception {

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        DeveloperResponse developerResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_PREFIX + "/username" + "/pokemon")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(developerResponse.getFirstName(), developer.getFirstName());
        Assertions.assertEquals(developerResponse.getUsername(), developer.getUsername());
        Assertions.assertEquals(2, developerResponse.getEducations().size());
        Assertions.assertEquals(2, developerResponse.getSpecializations().size());
        Assertions.assertEquals(1, developerResponse.getLinks().size());
        Assertions.assertEquals(developerResponse.getLastName(), developer.getLastName());
        Assertions.assertNotNull(developerResponse.getId());

    }

    @Test
    void getDeveloperByIdSuccessfully() throws Exception {

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        DeveloperResponse developerResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_PREFIX + "/65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")

                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(developerResponse.getFirstName(), developer.getFirstName());
        Assertions.assertEquals(developerResponse.getUsername(), developer.getUsername());
        Assertions.assertEquals(2, developerResponse.getEducations().size());
        Assertions.assertEquals(2, developerResponse.getSpecializations().size());
        Assertions.assertEquals(1, developerResponse.getLinks().size());
        Assertions.assertEquals(developerResponse.getLastName(), developer.getLastName());
        Assertions.assertNotNull(developerResponse.getId());
    }

    @Test
    void getByIdFailedDeveloperBlocked() throws Exception {

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_PREFIX + "/68dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllDeveloperByStateActiveSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("hr@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);


        ResponsePage<DeveloperResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_PREFIX )
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(2, page.getTotalElements());
    }

    @Test
    void getDevelopersByIdsListSuccessfully() throws Exception{

        ListIdsRequest listIdsRequest = new ListIdsRequest(List.of(UUID.fromString("66dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"),UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")));

        ResponsePage<DeveloperResponse> page = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_PREFIX + "/list/by/ids")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(listIdsRequest)))

                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(1).getId());
        Assertions.assertNotNull(page.getContent().get(1).getFirstName());
        Assertions.assertNotNull(page.getContent().get(1).getLastName());
        Assertions.assertNotNull(page.getContent().get(1).getUsername());
        Assertions.assertNotNull(page.getContent().get(1).getSpecializations());
        Assertions.assertNotNull(page.getContent().get(1).getLinks());
    }
}
