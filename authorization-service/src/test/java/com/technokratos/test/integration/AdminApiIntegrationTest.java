package com.technokratos.test.integration;

import chat.service.ChatServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.BlockAccountRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.*;
import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.HrEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.repository.BlockInfoRepository;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.HrRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.RemoveBucketArgs;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/admin-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class AdminApiIntegrationTest extends AbstractIntegrationTest{

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private ChatServiceGrpc.ChatServiceBlockingStub chatServiceBlockingStub;

    @Autowired
    private BlockInfoRepository blockInfoRepository;

    @Autowired
    private MockMvc mockMvc;

    private static final String LOCALHOST = "http://localhost:";

    private static final String ACCOUNT_PREFIX = "/account";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private HrRepository hrRepository;

    @Autowired
    private AccountRepository accountRepository;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Autowired
    private MinioClient minioClient;

    @BeforeEach
    void setUpBeforeEach() throws Exception {

        minioClient.makeBucket(MakeBucketArgs.builder().bucket("account-photo").build());
        minioClient.makeBucket(MakeBucketArgs.builder().bucket("developer-document").build());

        AccountEntity admin = accountRepository.getReferenceById(UUID.fromString("95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login(admin.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

    }

    @AfterEach
    void afterEach() throws Exception {

        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("account-photo").build());
        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("developer-document").build());
    }

    @Test
    void blockAccountSuccessfully() throws Exception {

        BlockAccountRequest blockAccountRequest = BlockAccountRequest.builder()
                .accountId(UUID.fromString("96dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .reason("Он нам не нравится")
                .finishDate(OffsetDateTime.of(LocalDate.parse("2023-04-28"), LocalTime.of(13, 35, 44, 487697), ZoneOffset.ofHours(3)))
                .build();

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("96dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        BlockInfoResponse blockInfoResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/block")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(blockAccountRequest))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        AccountEntity blockedAccount = accountRepository.getReferenceById(UUID.fromString("96dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertNotNull(blockInfoRepository.getReferenceById(blockedAccount.getBlockInfoEntities().get(0).getId()).getAccount());

        Assertions.assertNotNull(blockInfoResponse.getAccount());
        Assertions.assertEquals(blockInfoResponse.getAccount().getFirstName(), account.getFirstName());
        Assertions.assertNotNull(blockedAccount.getBlockInfoEntities().get(0));
        Assertions.assertEquals(blockedAccount.getBlockInfoEntities().get(0).getReason(), blockAccountRequest.getReason());
        Assertions.assertNotNull(blockInfoResponse.getStartDate());
    }

    @Test
    void blockAccountFailedAccountIsNotExist() throws Exception {

        BlockAccountRequest blockAccountRequest = BlockAccountRequest.builder()
                .accountId(UUID.fromString("17dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .reason("Он нам не нравится")
                .finishDate(OffsetDateTime.of(LocalDate.parse("2023-04-28"), LocalTime.of(13, 35, 44, 487697), ZoneOffset.ofHours(3)))
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/block")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(blockAccountRequest))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void blockAccountFailedAccountAlreadyBlocked() throws Exception {

        BlockAccountRequest blockAccountRequest = BlockAccountRequest.builder()
                .accountId(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .reason("Он нам не нравится")
                .finishDate(OffsetDateTime.of(LocalDate.parse("2023-04-28"), LocalTime.of(13, 35, 44, 487697), ZoneOffset.ofHours(3)))
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/block")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(blockAccountRequest))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }


    @Test
    void unblockAccountSuccessfully() throws Exception {

        RequestID requestId = RequestID.builder()
                .id(UUID.fromString("15dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        AccountEntity blockedAccount = accountRepository.getReferenceById(UUID.fromString("15dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertTrue(blockInfoRepository.getReferenceById(blockedAccount.getBlockInfoEntities().get(0).getId()).isActive());
        Assertions.assertEquals(Status.BLOCKED, blockedAccount.getStatus());
        AccountResponse accountResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/unblock")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(requestId))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<DeveloperResponse>() {
        });
        AccountEntity unblockedAccount = accountRepository.getReferenceById(UUID.fromString("15dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertFalse(blockInfoRepository.getReferenceById(unblockedAccount.getBlockInfoEntities().get(0).getId()).isActive());
        Assertions.assertEquals(Status.CONFIRMED, unblockedAccount.getStatus());
        Assertions.assertEquals(accountResponse.getStatus(), Status.CONFIRMED.toString());
        Assertions.assertEquals(accountResponse.getFirstName(), blockedAccount.getFirstName());
        Assertions.assertNotNull(accountResponse.getPhoto().getLink());
    }

    @Test
    void unblockDeveloperSuccessfully() throws Exception {

        RequestID requestId = RequestID.builder()
                .id(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        DeveloperEntity blockedDeveloper = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(DeveloperState.NOT_ACTIVE, blockedDeveloper.getState());

        Assertions.assertEquals(Status.BLOCKED, blockedDeveloper.getStatus());

        DeveloperResponse developerResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/unblock")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(requestId))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<DeveloperResponse>() {
        });

        DeveloperEntity unblockedDeveloper = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(DeveloperState.ACTIVE, unblockedDeveloper.getState());

        Assertions.assertFalse(blockInfoRepository.getReferenceById(unblockedDeveloper.getBlockInfoEntities().get(0).getId()).isActive());
        Assertions.assertEquals(Status.CONFIRMED, unblockedDeveloper.getStatus());
        Assertions.assertEquals(developerResponse.getStatus(), Status.CONFIRMED.toString());
        Assertions.assertEquals(developerResponse.getFirstName(), blockedDeveloper.getFirstName());
        Assertions.assertEquals(0, developerResponse.getRating());
        Assertions.assertNotNull(developerResponse.getPhoto().getLink());
    }

    @Test
    void unblockHrSuccessfully() throws Exception {

        RequestID requestId = RequestID.builder()
                .id(UUID.fromString("75dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        HrEntity blockedHr = hrRepository.getReferenceById(UUID.fromString("75dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        HrResponse hrResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/unblock")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(requestId))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        HrEntity unblockedHr = hrRepository.getReferenceById(UUID.fromString("75dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertFalse(blockInfoRepository.getReferenceById(unblockedHr.getBlockInfoEntities().get(0).getId()).isActive());
        Assertions.assertEquals(Status.CONFIRMED, unblockedHr.getStatus());
        Assertions.assertEquals(hrResponse.getStatus(), Status.CONFIRMED.toString());
        Assertions.assertEquals(hrResponse.getFirstName(), blockedHr.getFirstName());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
    }

    @Test
    void unblockAccountFailedAccountIsNotBlocked() throws Exception {

        RequestID requestId = RequestID.builder().id(UUID.fromString("25dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")).build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/unblock")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(requestId))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void blockAndUnblockAccountSuccessfully() throws Exception {

        BlockAccountRequest blockAccountRequest = BlockAccountRequest.builder()
                .accountId(UUID.fromString("96dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .reason("Он нам не нравится")
                .finishDate(OffsetDateTime.of(LocalDate.parse("2023-04-28"), LocalTime.of(13, 35, 44, 487697), ZoneOffset.ofHours(3)))
                .build();

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("96dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        BlockInfoResponse blockInfoResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/block")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(blockAccountRequest))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        AccountEntity blockedAccount = accountRepository.getReferenceById(UUID.fromString("96dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertNotNull(blockInfoRepository.getReferenceById(blockedAccount.getBlockInfoEntities().get(0).getId()).getAccount());

        Assertions.assertNotNull(blockInfoResponse.getAccount());
        Assertions.assertEquals(blockInfoResponse.getAccount().getFirstName(), account.getFirstName());
        Assertions.assertNotNull(blockedAccount.getBlockInfoEntities().get(0));
        Assertions.assertEquals(blockedAccount.getBlockInfoEntities().get(0).getReason(), blockAccountRequest.getReason());
        Assertions.assertNotNull(blockInfoResponse.getStartDate());

        RequestID requestId = RequestID.builder()
                .id(UUID.fromString("15dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        Assertions.assertTrue(blockInfoRepository.getReferenceById(blockedAccount.getBlockInfoEntities().get(0).getId()).isActive());
        Assertions.assertEquals(Status.BLOCKED, blockedAccount.getStatus());
        AccountResponse accountResponse = objectMapper.readValue(mockMvc.perform(patch(LOCALHOST + port + ACCOUNT_PREFIX + "/unblock")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(requestId))
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<DeveloperResponse>() {
        });
        AccountEntity unblockedAccount = accountRepository.getReferenceById(UUID.fromString("15dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertFalse(blockInfoRepository.getReferenceById(unblockedAccount.getBlockInfoEntities().get(0).getId()).isActive());
        Assertions.assertEquals(Status.CONFIRMED, unblockedAccount.getStatus());
        Assertions.assertEquals(accountResponse.getStatus(), Status.CONFIRMED.toString());
        Assertions.assertEquals(accountResponse.getFirstName(), blockedAccount.getFirstName());
        Assertions.assertNotNull(accountResponse.getPhoto().getLink());
    }

    @Test
    void getByAccountSuccessfully() throws Exception {

        String accountId = "56dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ResponsePage<BlockInfoResponse> page = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/block" + "/" + accountId)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<ResponsePage<BlockInfoResponse>>() {
        });

        Assertions.assertNotNull(page);
        Assertions.assertNotNull(page.getContent());
        Assertions.assertEquals(page.getContent().get(0).getId(), UUID.fromString("ab8a5d70-11ee-4106-8df0-98574c53264a"));
    }

    @Test
    void getByAccountFailedAccountIsNotFound() throws Exception {

        String accountId = "37dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/block" + "/" + accountId)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllSuccessfully() throws Exception {

        ResponsePage page = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(16, page.getTotalElements());
    }

    @Test
    void getAllByStatusSuccessfully() throws Exception {

        ResponsePage page = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX)
                        .contentType("application/json")
                        .param("status", "BLOCKED")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(12, page.getTotalElements());
    }
}
