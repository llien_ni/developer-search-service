package com.technokratos.test.integration;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.GrievanceRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.GrievanceResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.repository.AccountRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/grievance-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class GrievanceApiIntegrationTest extends AbstractIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String GRIEVANCE_PREFIX = "/grievance";

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    void setUp() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev1@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);
    }

    @Test
    void addGrievanceSuccessfully() throws Exception{

        GrievanceRequest grievanceRequest = GrievanceRequest.builder()
                .text("Some text")
                .accountId(UUID.fromString("95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        GrievanceResponse grievanceResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST+port+GRIEVANCE_PREFIX)
                        .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                                .content(objectMapper.writeValueAsString(grievanceRequest)))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        AccountEntity author = accountRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AccountEntity account  = accountRepository.getReferenceById(UUID.fromString("95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertEquals(grievanceResponse.getAuthor().getFirstName(),author.getFirstName());
        Assertions.assertEquals(grievanceResponse.getAuthor().getId(), author.getId());
        Assertions.assertEquals(1, author.getSentGrievances().size());
        Assertions.assertEquals(0, author.getReceivedGrievances().size());
        Assertions.assertEquals(1, account.getReceivedGrievances().size());
        Assertions.assertEquals(0, account.getSentGrievances().size());
        Assertions.assertNotNull(grievanceResponse.getId());
    }

    @Test
    void addGrievanceSuccessfullyAndAccountBlocked() throws Exception{

        AccountEntity accountEntity  = accountRepository.getReferenceById(UUID.fromString("46dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(Status.CONFIRMED, accountEntity.getStatus());

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        GrievanceRequest grievanceRequest = GrievanceRequest.builder()
                .text("Some some text")
                .accountId(UUID.fromString("46dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        GrievanceResponse grievanceResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST+port+GRIEVANCE_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(grievanceRequest)))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        AccountEntity author = accountRepository.getReferenceById(UUID.fromString("35dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AccountEntity account  = accountRepository.getReferenceById(UUID.fromString("46dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertEquals(grievanceResponse.getAuthor().getFirstName(),author.getFirstName());
        Assertions.assertEquals(grievanceResponse.getAuthor().getId(), author.getId());
        Assertions.assertEquals(grievanceResponse.getAccount().getId(), account.getId());
        Assertions.assertEquals(grievanceResponse.getAccount().getFirstName(), account.getFirstName());
        Assertions.assertNotNull(grievanceResponse.getId());

        Assertions.assertEquals(Status.BLOCKED, account.getStatus());
        Assertions.assertEquals("Some some text", account.getReceivedGrievances().get(4).getText());
    }

    @Test
    void addGrievanceFailedAccountNotFound() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        GrievanceRequest grievanceRequest = GrievanceRequest.builder()
                .text("Some some text")
                .accountId(UUID.fromString("96dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST+port+GRIEVANCE_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(grievanceRequest)))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void addGrievanceFailedGrievanceAlreadyExist() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("sim1@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        GrievanceRequest grievanceRequest = GrievanceRequest.builder()
                .text("Some some text")
                .accountId(UUID.fromString("82dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST+port+GRIEVANCE_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(grievanceRequest)))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void addGrievanceFailedAccountSentManyGrievances() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("sim3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        GrievanceRequest grievanceRequest = GrievanceRequest.builder()
                .text("Some some text")
                .accountId(UUID.fromString("72dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST+port+GRIEVANCE_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(grievanceRequest)))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("72dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AccountEntity author = accountRepository.getReferenceById(UUID.fromString("71dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(0, account.getReceivedGrievances().size());
        Assertions.assertEquals(Status.BLOCKED, author.getStatus());
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteSuccessfully() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AccountEntity author = accountRepository.getReferenceById(UUID.fromString("51dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AccountEntity account= accountRepository.getReferenceById(UUID.fromString("22dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertEquals(1, author.getSentGrievances().size());
        Assertions.assertEquals(1, account.getReceivedGrievances().size());

        RequestID requestID = new RequestID(UUID.fromString("51dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(delete(LOCALHOST+port+GRIEVANCE_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(requestID)))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        AccountEntity updatedAuthor = accountRepository.getReferenceById(UUID.fromString("51dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AccountEntity updatedAccount= accountRepository.getReferenceById(UUID.fromString("22dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertNotNull(messageResponse.getMessage());
        Assertions.assertEquals(0, updatedAuthor.getSentGrievances().size());
        Assertions.assertEquals(0, updatedAccount.getReceivedGrievances().size());
    }

    @Test
    void deleteFailedGrievanceNotFound() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        RequestID requestID = new RequestID(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(delete(LOCALHOST+port+GRIEVANCE_PREFIX)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                        .content(objectMapper.writeValueAsString(requestID)))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse);
    }

    @Test
    void getByAuthorSuccessfully() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);


        UUID id = UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        ResponsePage<GrievanceResponse> page =  objectMapper.readValue(mockMvc.perform(
                get(LOCALHOST+port+GRIEVANCE_PREFIX+"/author"+"/"+id)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertEquals(UUID.fromString("80dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"), page
                .getContent().get(0).getId());
        Assertions.assertEquals("sim", page.getContent().get(0).getAccount().getFirstName());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor().getFirstName());
    }

    @Test
    void getByAuthorFailedAuthorNotFound() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);


        UUID id = UUID.fromString("57dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        ExceptionResponse exceptionResponse =  objectMapper.readValue(
                mockMvc.perform(get(LOCALHOST+port+GRIEVANCE_PREFIX+"/author"+"/"+id)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                )
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByAccountSuccessfully() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        String accountId = "92bab38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        ResponsePage<GrievanceResponse> page =  objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST+port+GRIEVANCE_PREFIX+"/account"+"/"+accountId)
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(3, page.getTotalElements());
        Assertions.assertEquals("72bab38e-3a15-4ab1-aee2-cf2f13ee9c5b", page.getContent().get(0).getId().toString());
        Assertions.assertEquals("73bab38e-3a15-4ab1-aee2-cf2f13ee9c5b", page.getContent().get(1).getId().toString());
        Assertions.assertEquals("71bab38e-3a15-4ab1-aee2-cf2f13ee9c5b", page.getContent().get(2).getId().toString());
        Assertions.assertNotNull(page.getContent().get(0).getAccount().getFirstName());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor().getFirstName());


    }
    @Test
    void getByAccountFailedAccountNotFound() throws Exception{
        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        String accountId = "13bab38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        ExceptionResponse exceptionResponse =  objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST+port+GRIEVANCE_PREFIX+"/account"+"/"+accountId)
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                        .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAllSuccessfully() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);


        ResponsePage<GrievanceResponse> page =  objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST+port+GRIEVANCE_PREFIX)
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(16, page.getTotalElements());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor().getFirstName());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor().getLastName());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor().getId());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor().getRole());
        Assertions.assertNotNull(page.getContent().get(0).getAuthor().getStatus());
    }

    @Test
    void getByIdSuccessfully() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AccountEntity author = accountRepository.getReferenceById(UUID.fromString("93bab38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("92bab38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        GrievanceResponse grievanceResponse =  objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST+port+GRIEVANCE_PREFIX+"/71bab38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(account.getId(), grievanceResponse.getAccount().getId());
        Assertions.assertEquals(account.getFirstName(), grievanceResponse.getAccount().getFirstName());
        Assertions.assertEquals(account.getLastName(), grievanceResponse.getAccount().getLastName());
        Assertions.assertEquals(account.getRole().toString(), grievanceResponse.getAccount().getRole());
        Assertions.assertEquals(author.getId(), grievanceResponse.getAuthor().getId());
        Assertions.assertEquals(author.getFirstName(), grievanceResponse.getAuthor().getFirstName());
        Assertions.assertEquals(author.getRole().toString(), grievanceResponse.getAuthor().getRole());
        Assertions.assertNotNull(grievanceResponse.getId());
        Assertions.assertNotNull(grievanceResponse.getCreatedDate());
        Assertions.assertNotNull(grievanceResponse.getText());
    }

    @Test
    void getByIdFailedGrievanceNotFound() throws Exception{

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse =  objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST+port+GRIEVANCE_PREFIX+"/71cab38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                                .contentType("application/json")
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
