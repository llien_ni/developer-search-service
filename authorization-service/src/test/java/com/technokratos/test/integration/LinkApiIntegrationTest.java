package com.technokratos.test.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.LinkRequest;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.request.RequestID;
import com.technokratos.dto.request.UpdateLinkRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.LinkResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.ResponsePage;
import com.technokratos.model.entity.DeveloperEntity;
import com.technokratos.model.entity.LinkEntity;
import com.technokratos.repository.DeveloperRepository;
import com.technokratos.repository.LinkRepository;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Sql(scripts = "/sql/link-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class LinkApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String DEVELOPER_LINK_PREFIX = "/developer/link";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DeveloperRepository developerRepository;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Autowired
    private LinkRepository linkRepository;

    @Test
    void addLinkSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LinkRequest linkRequest = LinkRequest.builder()
                .url("http://my-link")
                .description("This is my link")
                .build();

        LinkResponse linkResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + DEVELOPER_LINK_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(linkRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(developer.getId());

        LinkEntity linkEntity = linkRepository.getReferenceById(linkResponse.getId());
        Assertions.assertEquals(developer.getId(), linkEntity.getDeveloper().getId());
        Assertions.assertNotNull(linkResponse.getId());
        Assertions.assertEquals(1, updatedDeveloper.getLinks().size());
        Assertions.assertEquals(updatedDeveloper.getLinks().get(0).getId(), linkResponse.getId());

        Assertions.assertEquals(linkResponse.getDescription(), linkEntity.getDescription());
        Assertions.assertEquals(linkResponse.getUrl(), linkEntity.getUrl());
    }

    @Test
    void deleteLinkSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        RequestID requestID = new RequestID(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + DEVELOPER_LINK_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestID))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        DeveloperEntity updatedDeveloper = developerRepository.getReferenceById(developer.getId());
        Assertions.assertEquals(0, updatedDeveloper.getLinks().size());
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void updateLinkSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        UpdateLinkRequest updateLinkRequest = UpdateLinkRequest
                .builder()
                .id(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .description("New description")
                .build();

        LinkResponse linkResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + DEVELOPER_LINK_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateLinkRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        LinkEntity updatedLink = linkRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(updateLinkRequest.getDescription(), updatedLink.getDescription());
        Assertions.assertNotNull(updatedLink.getUrl());
        Assertions.assertNotNull(updatedLink.getId());
        Assertions.assertNotNull(updatedLink.getDeveloper().getId());
        Assertions.assertNotNull(linkResponse.getUrl());
        Assertions.assertNotNull(linkResponse.getId());
        Assertions.assertNotNull(linkResponse.getDescription());

        DeveloperEntity developer = developerRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(developer.getLinks().get(0).getDescription(), updateLinkRequest.getDescription());
        System.out.println(developer.getLinks().get(0).getDescription());
    }

    @Test
    void updateLinkFailedLinkNotFound() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        UpdateLinkRequest updateLinkRequest = UpdateLinkRequest
                .builder()
                .id(UUID.fromString("92dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"))
                .description("New description")
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + DEVELOPER_LINK_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(updateLinkRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void deleteLinkFailedLinkNotFound() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        RequestID requestID = new RequestID(UUID.randomUUID());

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + DEVELOPER_LINK_PREFIX)
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(requestID))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByIdSuccessfully() throws Exception {

        String linkId = "12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        LinkResponse linkResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_LINK_PREFIX + "/" + linkId)
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(linkResponse.getDeveloperId());
        Assertions.assertNotNull(linkResponse.getId());
        Assertions.assertNotNull(linkResponse.getDescription());
        Assertions.assertNotNull(linkResponse.getUrl());
    }

    @Test
    void getByIdFailedLinkNotFound() throws Exception {

        String linkId = UUID.randomUUID().toString();
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_LINK_PREFIX + "/" + linkId)
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getByDeveloperSuccessfully() throws Exception {

        String developerId = "12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        ResponsePage<LinkResponse> page = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_LINK_PREFIX + "/developer/" + developerId)
                ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        LinkEntity linkEntity = linkRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        Assertions.assertEquals(1, page.getTotalElements());
        LinkResponse linkResponse = page.getContent().get(0);
        Assertions.assertEquals(linkEntity.getDeveloper().getId(), linkResponse.getDeveloperId());
        Assertions.assertEquals(linkEntity.getUrl(), linkResponse.getUrl());
        Assertions.assertEquals(linkEntity.getDescription(), linkResponse.getDescription());
    }

    @Test
    void getByDeveloperFailedDeveloperNotFound() throws Exception {

        String accountId = "89dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + DEVELOPER_LINK_PREFIX + "/developer/" + accountId)
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
