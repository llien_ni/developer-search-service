package com.technokratos.test.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.response.ExceptionResponse;
import com.technokratos.dto.response.MessageResponse;
import com.technokratos.dto.response.PhotoResponse;
import com.technokratos.model.entity.AccountEntity;
import com.technokratos.model.entity.PhotoEntity;
import com.technokratos.repository.*;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.RemoveBucketArgs;
import io.minio.RemoveObjectArgs;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/account-photo-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class AccountPhotoApiIntegrationTest extends AbstractIntegrationTest{

    private static final String LOCALHOST = "http://localhost:";

    private static final String ACCOUNT_PHOTO_PREFIX = "/account/photo";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private HrRepository hrRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ChangeEmailRequestRepository changeEmailRequestRepository;

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private MinioClient minioClient;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @BeforeEach
    void setUp() throws Exception {

        minioClient.makeBucket(MakeBucketArgs.builder().bucket("account-photo").build());
        minioClient.makeBucket(MakeBucketArgs.builder().bucket("developer-document").build());
    }

    @AfterEach
    void afterEach() throws Exception {

        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("account-photo").build());
        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("developer-document").build());
    }

    @Test
    void addPhotoSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("dev@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertNull(account.getPhoto());

        MockMultipartFile photoFile = new MockMultipartFile("file",
                "myPhoto.jpg",
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );

        PhotoResponse photoResponse = objectMapper.readValue(mockMvc.perform(
                        multipart(LOCALHOST + port + ACCOUNT_PHOTO_PREFIX)
                                .file(photoFile)
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        AccountEntity updatedAccount = accountRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertNotNull(updatedAccount.getPhoto());
        Assertions.assertNotNull(photoResponse.getId());
        Assertions.assertEquals(account.getId(), photoResponse.getAccountId());
        Assertions.assertNotNull(photoResponse.getLink());
        Assertions.assertNotNull(photoResponse.getType());
        Assertions.assertNotNull(photoResponse.getFileName());

        PhotoEntity photo = photoRepository.getReferenceById(photoResponse.getId());

        minioClient.removeObject(RemoveObjectArgs.builder().bucket("account-photo")
                .object(photo.getFileNameInBucket())
                .build());
    }

    @Test
    void addPhotoSuccessfullyWhenAccountAlreadyHavePhoto() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("sim@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        PhotoEntity oldPhoto = account.getPhoto();
        Assertions.assertNotNull(oldPhoto);

        MockMultipartFile photoFile = new MockMultipartFile("file",
                "myPhoto.jpg",
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );

        PhotoResponse photoResponse = objectMapper.readValue(mockMvc.perform(
                        multipart(LOCALHOST + port + ACCOUNT_PHOTO_PREFIX)
                                .file(photoFile)
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });
        AccountEntity updatedAccount = accountRepository.getReferenceById(account.getId());

        Assertions.assertNotNull(updatedAccount.getPhoto());
        Assertions.assertNotEquals(photoResponse.getId(), oldPhoto.getId());
        Assertions.assertNotEquals(photoResponse.getFileName(), oldPhoto.getFileName());
        Assertions.assertEquals(updatedAccount.getPhoto().getId(), photoResponse.getId());
        Assertions.assertNotEquals(updatedAccount.getPhoto().getId(), oldPhoto.getId());
        Assertions.assertEquals("myPhoto.jpg", photoResponse.getFileName());
        Assertions.assertEquals("myPhoto.jpg", updatedAccount.getPhoto().getFileName());
        Assertions.assertNotNull(account.getPhoto().getFileNameInBucket());
        Assertions.assertNotNull(photoResponse.getId());
        Assertions.assertEquals(account.getId(), photoResponse.getAccountId());
        Assertions.assertNotNull(photoResponse.getLink());
        Assertions.assertNotNull(photoResponse.getType());
        Assertions.assertNotNull(photoResponse.getFileName());

        PhotoEntity photo = photoRepository.getReferenceById(photoResponse.getId());

        minioClient.removeObject(RemoveObjectArgs.builder().bucket("account-photo")
                .object(photo.getFileNameInBucket())
                .build());
    }

    @Test
    void deletePhotoSuccessfully() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("sim2@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertNotNull(account.getPhoto());

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + ACCOUNT_PHOTO_PREFIX)
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        AccountEntity updatedAccount = accountRepository.getReferenceById(UUID.fromString("14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertNull(updatedAccount.getPhoto());
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void deletePhotoFailedPhotoNotFound() throws Exception {

        LoginRequest loginRequest = LoginRequest.builder()
                .login("sim3@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("15dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertNull(account.getPhoto());

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        delete(LOCALHOST + port + ACCOUNT_PHOTO_PREFIX)
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken())
                ).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new com.fasterxml.jackson.core.type.TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }
}
