package com.technokratos.test.integration;

import chat.service.ChatServiceGrpc;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technokratos.dto.request.*;
import com.technokratos.dto.response.*;
import com.technokratos.model.constant.DeveloperState;
import com.technokratos.model.constant.Role;
import com.technokratos.model.constant.Status;
import com.technokratos.model.entity.*;
import com.technokratos.repository.*;
import com.technokratos.security.model.AccessAndRefreshTokens;
import company.service.CompanyServiceGrpc;
import io.minio.*;
import net.devh.boot.grpc.client.inject.GrpcClient;
import notification.service.NotificationServiceGrpc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(scripts = "/sql/account-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class AccountApiIntegrationTest extends AbstractIntegrationTest {

    private static final String LOCALHOST = "http://localhost:";

    private static final String ACCOUNT_PREFIX = "/account";

    private static final String LOGIN_REQUEST = "/login";

    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private ObjectMapper objectMapper;

    @GrpcClient("inProcess")
    private CompanyServiceGrpc.CompanyServiceBlockingStub companyServiceBlockingStub;

    @GrpcClient("inProcess")
    private NotificationServiceGrpc.NotificationServiceBlockingStub notificationServiceBlockingStub;

    @GrpcClient("inProcess")
    private ChatServiceGrpc.ChatServiceBlockingStub chatServiceBlockingStub;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private HrRepository hrRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private MinioClient minioClient;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @LocalServerPort
    private int port;

    private AccessAndRefreshTokens tokens;

    @Autowired
    private PhotoRepository photoRepository;

    @BeforeEach
    void setUp() throws Exception {

        minioClient.makeBucket(MakeBucketArgs.builder().bucket("account-photo").build());
        minioClient.makeBucket(MakeBucketArgs.builder().bucket("developer-document").build());

        PhotoEntity photo = photoRepository.getReferenceById(UUID.fromString("90f91dd0-a9bb-4da5-a71d-70c3a48329e1"));

        MockMultipartFile document = new MockMultipartFile("file",
                photo.getFileName(),
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );
        minioClient.putObject(PutObjectArgs.builder().bucket("account-photo")
                .contentType(photo.getType())
                .stream(document.getInputStream(), document.getSize(), -1)
                .object(photo.getFileNameInBucket())
                .build());
    }

    @AfterEach
    void afterEach() throws Exception {

        PhotoEntity photo = photoRepository.getReferenceById(UUID.fromString("90f91dd0-a9bb-4da5-a71d-70c3a48329e1"));

        minioClient.removeObject(RemoveObjectArgs.builder().bucket("account-photo")
                .object(photo.getFileNameInBucket())
                .build());

        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("account-photo").build());
        minioClient.removeBucket(RemoveBucketArgs.builder().bucket("developer-document").build());
    }

    @Test
    void confirmAccountDeveloperSuccessfully() throws Exception {

        DeveloperEntity developerEntity = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        UUID confirmToken = UUID.fromString("45dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        DeveloperResponse developerResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + ACCOUNT_PREFIX + "/confirm")
                                .param("token", confirmToken.toString()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(developerResponse.getFirstName(), developerEntity.getFirstName());
        Assertions.assertEquals(developerResponse.getLastName(), developerEntity.getLastName());
        Assertions.assertEquals(developerResponse.getEmail(), developerEntity.getEmail());
        Assertions.assertEquals(developerResponse.getRole(), Role.ROLE_DEVELOPER.toString());
        Assertions.assertEquals(developerResponse.getState(), DeveloperState.ACTIVE.toString());
        Assertions.assertEquals(0, developerResponse.getRating());
        Assertions.assertEquals(developerResponse.getStatus(), Status.CONFIRMED.toString());
    }

    @Test
    void confirmAccountHrSuccessfully() throws Exception {

        UUID confirmToken = UUID.fromString("75dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        HrResponse hrResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + ACCOUNT_PREFIX + "/confirm")
                                .param("token", confirmToken.toString()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        HrEntity hrEntity = hrRepository.getReferenceById(UUID.fromString("75dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));
        Assertions.assertEquals(hrResponse.getFirstName(), hrEntity.getFirstName());
        Assertions.assertEquals(hrResponse.getLastName(), hrEntity.getLastName());
        Assertions.assertEquals(hrResponse.getEmail(), hrEntity.getEmail());
        Assertions.assertEquals(hrResponse.getRole(), Role.ROLE_HR.toString());
        Assertions.assertEquals(hrResponse.getCity(), hrEntity.getCity());
        Assertions.assertNotNull(hrResponse.getCompanyResponse());
        Assertions.assertNotNull(hrResponse.getPhoto());
        Assertions.assertNotNull(hrResponse.getPhoto().getLink());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
        Assertions.assertEquals(hrResponse.getStatus(), Status.MODERATED.toString());
    }

    @Test
    void confirmAccountSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        UUID confirmToken = UUID.fromString("46dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        AccountResponse accountResponse = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + ACCOUNT_PREFIX + "/confirm")
                                .param("token", confirmToken.toString()))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(confirmationTokenRepository.findByToken("46dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"), Optional.empty());
        Assertions.assertEquals(accountResponse.getFirstName(), account.getFirstName());
        Assertions.assertEquals(accountResponse.getLastName(), account.getLastName());
        Assertions.assertEquals(accountResponse.getEmail(), account.getEmail());
        Assertions.assertEquals(accountResponse.getStatus(), Status.CONFIRMED.toString());
    }

    @Test
    void confirmAccountFailedTokenNotFound() throws Exception {

        UUID confirmToken = UUID.fromString("49dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        ExceptionResponse response = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + ACCOUNT_PREFIX + "/confirm")
                                .param("token", confirmToken.toString())).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(response.getMessage());
    }

    @Test
    void confirmAccountFailedTokenIsExpired() throws Exception {

        UUID confirmToken = UUID.fromString("16dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        ExceptionResponse response = objectMapper.readValue(mockMvc.perform(
                        get(LOCALHOST + port + ACCOUNT_PREFIX + "/confirm")
                                .param("token", confirmToken.toString())).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(response.getMessage());
    }

    @Test
    void resetPasswordConfirmSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        UUID confirmToken = UUID.fromString("46dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        ResetPasswordRequest resetPasswordRequest = ResetPasswordRequest.builder()
                .password("qwerty008")
                .build();

        AccountResponse accountResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + ACCOUNT_PREFIX + "/reset-password/confirm")
                                .param("token", confirmToken.toString())
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(resetPasswordRequest)))
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(accountResponse.getFirstName(), account.getFirstName());
        Assertions.assertEquals(accountResponse.getLastName(), account.getLastName());
        Assertions.assertEquals(accountResponse.getEmail(), account.getEmail());
        Assertions.assertEquals(accountResponse.getStatus(), Status.CONFIRMED.toString());
    }

    @Test
    void resetPasswordConfirmFailedTokenNotFound() throws Exception {

        UUID confirmToken = UUID.fromString("49dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        ResetPasswordRequest resetPasswordRequest = ResetPasswordRequest.builder()
                .password("qwerty008")
                .build();

        ExceptionResponse response = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + ACCOUNT_PREFIX + "/reset-password/confirm")
                                .param("token", confirmToken.toString())
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(resetPasswordRequest))).andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(response.getMessage());
    }

    @Test
    void resetPasswordConfirmFailedTokenIsExpired() throws Exception {

        UUID confirmToken = UUID.fromString("16dad38e-3a15-4ab1-aee2-cf2f13ee9c5b");

        ResetPasswordRequest resetPasswordRequest = ResetPasswordRequest.builder()
                .password("qwerty008")
                .build();

        ExceptionResponse response = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + ACCOUNT_PREFIX + "/reset-password/confirm")
                                .param("token", confirmToken.toString())
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(resetPasswordRequest))).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(response.getMessage());
    }

    @Test
    void changePasswordSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        ChangePasswordRequest changePasswordRequest = ChangePasswordRequest.builder()
                .newPassword("qwerty008")
                .oldPassword("qwerty007")
                .build();

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ACCOUNT_PREFIX + "/change-password")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(changePasswordRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void changePasswordFailedInvalidOldPassword() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        ChangePasswordRequest changePasswordRequest = ChangePasswordRequest.builder()
                .newPassword("qwerty008")
                .oldPassword("qwerty009")
                .build();

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ACCOUNT_PREFIX + "/change-password")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(changePasswordRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void changeEmailSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        ChangeEmailRequest changeEmailRequest = ChangeEmailRequest.builder()
                .newEmail("newAcc@gmail.com").password("qwerty007").build();

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);


        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ACCOUNT_PREFIX + "/change-email")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(changeEmailRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(accountRepository.getReferenceById(account.getId()).getChangeEmailRequests());
        Assertions.assertNotNull(messageResponse);
        Assertions.assertEquals(messageResponse.getMessage(), "Sent a link to change email to " + changeEmailRequest.getNewEmail());
    }

    @Test
    void changeEmailFailedInvalidPassword() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        ChangeEmailRequest changeEmailRequest = ChangeEmailRequest.builder()
                .newEmail("newAcc@gmail.com").password("qwerty008").build();

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ACCOUNT_PREFIX + "/change-email")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(changeEmailRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void changeEmailFailedAccountThisSuchEmailExist() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        ChangeEmailRequest changeEmailRequest = ChangeEmailRequest.builder()
                .newEmail("admin@gmail.com").password("qwerty007").build();

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        patch(LOCALHOST + port + ACCOUNT_PREFIX + "/change-email")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(changeEmailRequest))
                                .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void changeEmailConfirmForDeveloperSuccessfully() throws Exception {

        DeveloperEntity developerEntity = developerRepository.getReferenceById(UUID.fromString("65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        String confirmToken = "65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        LoginRequest loginRequest = LoginRequest.builder()
                .login(developerEntity.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        DeveloperResponse developerResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/change-email/confirm")
                        .param("token", confirmToken)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(developerResponse.getFirstName(), developerEntity.getFirstName());
        Assertions.assertEquals(developerResponse.getLastName(), developerEntity.getLastName());
        Assertions.assertEquals(developerResponse.getEmail(), developerEntity.getEmail());
        Assertions.assertEquals(developerResponse.getRole(), Role.ROLE_DEVELOPER.toString());
        Assertions.assertEquals(developerResponse.getState(), DeveloperState.ACTIVE.toString());
        Assertions.assertEquals(0, developerResponse.getRating());
        Assertions.assertEquals(developerResponse.getStatus(), Status.CONFIRMED.toString());
        Assertions.assertNotNull(developerResponse.getLinks());
        Assertions.assertEquals("KFU", developerResponse.getEducations().get(0).getName());
        Assertions.assertNotNull(developerResponse.getEducations());
        Assertions.assertNotNull(developerResponse.getSpecializations());
    }

    @Test
    void changeEmailConfirmForHrSuccessfully() throws Exception {

        HrEntity hrEntity = hrRepository.getReferenceById(UUID.fromString("75dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        String confirmToken = "75dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        LoginRequest loginRequest = LoginRequest.builder()
                .login(hrEntity.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        HrResponse hrResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/change-email/confirm")
                        .param("token", confirmToken)
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(hrResponse.getFirstName());
        Assertions.assertEquals(hrResponse.getFirstName(), hrEntity.getFirstName());
        Assertions.assertEquals(hrResponse.getLastName(), hrEntity.getLastName());
        Assertions.assertEquals(hrResponse.getEmail(), hrEntity.getEmail());
        Assertions.assertEquals(hrResponse.getRole(), Role.ROLE_HR.toString());
        Assertions.assertEquals(hrResponse.getCity(), hrEntity.getCity());
        Assertions.assertNotNull(hrResponse.getCompanyResponse());
        Assertions.assertNotNull(hrResponse.getPhoto());
        Assertions.assertNotNull(hrResponse.getPhoto().getLink());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
        Assertions.assertEquals(hrResponse.getStatus(), Status.CONFIRMED.toString());
    }

    @Test
    void changeEmailConfirmForAccountConfirmSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        String confirmToken = "85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        AccountResponse accountResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/change-email/confirm")
                        .param("token", confirmToken).contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(accountResponse.getFirstName(), account.getFirstName());
        Assertions.assertEquals(accountResponse.getLastName(), account.getLastName());
        Assertions.assertEquals(accountResponse.getEmail(), account.getEmail());
    }

    @Test
    void changeConfirmEmailForAccountFailedTokenIsExpired() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        String confirmToken = "64dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/change-email/confirm")
                        .param("token", confirmToken).contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void changeConfirmEmailForAccountFailedTokenNotFound() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        String confirmToken = "34dad38e-3a15-4ab1-aee2-cf2f13ee9c5b";

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/change-email/confirm")
                        .param("token", confirmToken)
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void resetPasswordSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        PhoneOrEmailRequest request = PhoneOrEmailRequest.builder()
                .loginType(PhoneOrEmailRequest.LoginType.EMAIL)
                .login("simpleaccount@gmail.com")
                .build();

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + ACCOUNT_PREFIX + "/forgot-password")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(request))).andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(Status.NOT_CONFIRMED, accountRepository.getReferenceById(account.getId()).getStatus());
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void resetPasswordByPhoneNumberSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        PhoneOrEmailRequest request = PhoneOrEmailRequest.builder()
                .loginType(PhoneOrEmailRequest.LoginType.PHONE_NUMBER)
                .login("81234567890")
                .build();

        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + ACCOUNT_PREFIX + "/forgot-password")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertEquals(Status.NOT_CONFIRMED, accountRepository.getReferenceById(account.getId()).getStatus());
        Assertions.assertNotNull(messageResponse.getMessage());
    }

    @Test
    void resetPasswordFailedAccountIsNotConfirmed() throws Exception {

        PhoneOrEmailRequest request = PhoneOrEmailRequest.builder()
                .login("simpleaccount2@gmail.com")
                .loginType(PhoneOrEmailRequest.LoginType.EMAIL)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + ACCOUNT_PREFIX + "/forgot-password")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void resetPasswordFailedAccountNotExist() throws Exception {

        PhoneOrEmailRequest request = PhoneOrEmailRequest.builder()
                .login("simpleaccount3@gmail.com")
                .loginType(PhoneOrEmailRequest.LoginType.EMAIL)
                .build();

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + ACCOUNT_PREFIX + "/forgot-password")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void restoreAccountSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("77dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        PhoneOrEmailRequest request = PhoneOrEmailRequest.builder()
                .loginType(PhoneOrEmailRequest.LoginType.EMAIL)
                .login("simpleaccount4@gmail.com")
                .build();
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + ACCOUNT_PREFIX + "/restore")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        ConfirmationTokenEntity confirmationTokenEntity = accountRepository.getReferenceById(account.getId()).getConfirmationTokens().get(0);
        Assertions.assertNotNull(confirmationTokenRepository.findByToken(confirmationTokenEntity.getToken()));

        Assertions.assertNotNull(confirmationTokenEntity);
        Assertions.assertEquals(Status.NOT_CONFIRMED, accountRepository.getReferenceById(UUID.fromString("77dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")).getStatus());
        Assertions.assertNotNull(accountRepository.getReferenceById(UUID.fromString("77dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")).getConfirmationTokens().get(0));
        Assertions.assertNotNull(messageResponse);
    }

    @Test
    void restoreAccountByPhoneNumberSuccessfully() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        PhoneOrEmailRequest request = PhoneOrEmailRequest.builder()
                .loginType(PhoneOrEmailRequest.LoginType.PHONE_NUMBER)
                .login("81234567891")
                .build();
        MessageResponse messageResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + ACCOUNT_PREFIX + "/restore")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isAccepted())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        ConfirmationTokenEntity confirmationTokenEntity = accountRepository.getReferenceById(account.getId()).getConfirmationTokens().get(0);
        Assertions.assertNotNull(confirmationTokenRepository.findByToken(confirmationTokenEntity.getToken()));

        Assertions.assertNotNull(confirmationTokenEntity);
        Assertions.assertEquals(Status.NOT_CONFIRMED, accountRepository.getReferenceById(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")).getStatus());
        Assertions.assertNotNull(accountRepository.getReferenceById(UUID.fromString("13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")).getConfirmationTokens().get(0));
        Assertions.assertNotNull(messageResponse);
    }

    @Test
    void restoreAccountFailedAccountIsNotDeleted() throws Exception {

        PhoneOrEmailRequest request = PhoneOrEmailRequest.builder()
                .login("simpleaccount5@gmail.com")
                .loginType(PhoneOrEmailRequest.LoginType.EMAIL)
                .build();
        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(
                        post(LOCALHOST + port + ACCOUNT_PREFIX + "/restore")
                                .contentType("application/json")
                                .content(objectMapper.writeValueAsString(request))).andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAccountByIdSuccessfully() throws Exception {

        HrEntity hrEntity = hrRepository.getReferenceById(UUID.fromString("75dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login("admin@gmail.com")
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        HrResponse hrResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/" + hrEntity.getId().toString())
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });

        Assertions.assertNotNull(hrResponse.getFirstName());
        Assertions.assertEquals(hrResponse.getFirstName(), hrEntity.getFirstName());
        Assertions.assertEquals(hrResponse.getLastName(), hrEntity.getLastName());
        Assertions.assertEquals(hrResponse.getEmail(), hrEntity.getEmail());
        Assertions.assertEquals(hrResponse.getRole(), Role.ROLE_HR.toString());
        Assertions.assertEquals(hrResponse.getCity(), hrEntity.getCity());
        Assertions.assertNotNull(hrResponse.getCompanyResponse());
        Assertions.assertNotNull(hrResponse.getPhoto());
        Assertions.assertNotNull(hrResponse.getPhoto().getLink());
        Assertions.assertEquals("Technokratia", hrResponse.getCompanyResponse().getName());
        Assertions.assertEquals(hrResponse.getStatus(), Status.CONFIRMED.toString());
    }

    @Test
    void getAccountByIdFailedAccountIsNotExist() throws Exception {

        AccountEntity admin = accountRepository.getReferenceById(UUID.fromString("95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login(admin.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loginRequest))).andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        ExceptionResponse exceptionResponse = objectMapper.readValue(mockMvc.perform(get(LOCALHOST + port + ACCOUNT_PREFIX + "/88dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertNotNull(exceptionResponse.getMessage());
    }

    @Test
    void getAccountByIdFailedEndpointIsForbidden() throws Exception {

        AccountEntity account = accountRepository.getReferenceById(UUID.fromString("85dad38e-3a15-4ab1-aee2-cf2f13ee9c5b"));

        LoginRequest loginRequest = LoginRequest.builder()
                .login(account.getEmail())
                .password("qwerty007")
                .build();

        tokens = objectMapper.readValue(mockMvc.perform(post(LOCALHOST + port + LOGIN_REQUEST)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andReturn().getResponse().getContentAsString(), AccessAndRefreshTokens.class);

        mockMvc.perform(get(
                        LOCALHOST + port + ACCOUNT_PREFIX + "/88dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                        .contentType("application/json")
                        .header("Authorization", BEARER_PREFIX + tokens.getAccessToken()))
                .andExpect(status().isForbidden());
    }
}

