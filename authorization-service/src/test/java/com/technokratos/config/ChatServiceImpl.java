package com.technokratos.config;


import chat.service.ChatServiceGrpc;
import chat.service.ChatServiceOuterClass;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class ChatServiceImpl extends ChatServiceGrpc.ChatServiceImplBase {

    @Override
    public void setChatStateToNotActiveByAccount(ChatServiceOuterClass.AccountIdRequest request, StreamObserver<ChatServiceOuterClass.Empty> responseObserver) {

        responseObserver.onNext(ChatServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }

    @Override
    public void setChatStateToActiveByAccount(ChatServiceOuterClass.AccountIdRequest request, StreamObserver<ChatServiceOuterClass.Empty> responseObserver) {

        responseObserver.onNext(ChatServiceOuterClass.Empty.getDefaultInstance());
        responseObserver.onCompleted();
    }
}
