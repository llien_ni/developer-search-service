package com.technokratos.config;

import com.technokratos.exception.NotFoundException;
import company.service.CompanyServiceGrpc;
import company.service.CompanyServiceOuterClass;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class CompanyServiceImpl extends CompanyServiceGrpc.CompanyServiceImplBase {

    @Override
    public void getCompanyById(CompanyServiceOuterClass.GetCompanyByIdRequest request, StreamObserver<CompanyServiceOuterClass.CompanyResponse> responseObserver){

        CompanyServiceOuterClass.CompanyResponse companyResponse = CompanyServiceOuterClass.CompanyResponse.newBuilder()
                .setId(request.getCompanyId())
                .setName("Technokratia")
                .setDescription("Работаем товарищи")
                .build();

        responseObserver.onNext(companyResponse);
        responseObserver.onCompleted();
    }
    @Override
    public void getCompanyByName(CompanyServiceOuterClass.GetCompanyByNameRequest request, StreamObserver<CompanyServiceOuterClass.CompanyResponse> responseObserver){

        if(request.getCompanyName().equals("Pokemon")){
            throw new NotFoundException("Company not found");
        }
        CompanyServiceOuterClass.CompanyResponse companyResponse = CompanyServiceOuterClass.CompanyResponse.newBuilder()
                .setId("25dad38e-3a15-4ab1-aee2-cf2f13ee9c5b")
                .setName("Technokratia")
                .setDescription("Работаем товарищи")
                .build();

        responseObserver.onNext(companyResponse);
        responseObserver.onCompleted();
    }
}
