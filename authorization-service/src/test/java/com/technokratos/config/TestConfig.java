package com.technokratos.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;
import io.minio.MinioClient;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.utility.DockerImageName;

@TestConfiguration
public class TestConfig {

    @Bean
    public TestRestTemplate testRestTemplate() {
        return new TestRestTemplate();
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        return objectMapper;
    }


    private static final String ADMIN_ACCESS_KEY = "EySLoMKpAFT8sbu7";
    private static final String ADMIN_SECRET_KEY = "Bfphf5WVQCJb82pDpSGaEJSF4m7hxBTi";
    private static final Integer exposedPort = 9000;

    @Container
    private static final GenericContainer<?> minioContainer =
            new GenericContainer<>(DockerImageName.parse("quay.io/minio/minio"))
                    .withExposedPorts(exposedPort)
                    .withEnv("MINIO_ROOT_USER", ADMIN_ACCESS_KEY)
                    .withEnv("MINIO_ROOT_PASSWORD", ADMIN_SECRET_KEY)
                    .withCommand("server", "/data")
                    .withCreateContainerCmdModifier(cmd -> cmd.withHostConfig(
                            new HostConfig().withPortBindings(new PortBinding(Ports.Binding.bindPort(exposedPort), new ExposedPort(exposedPort)))));

    @Bean
    public MinioClient minioClient(){
        minioContainer.start();
        return new MinioClient.Builder()
                .credentials(ADMIN_ACCESS_KEY, ADMIN_SECRET_KEY)
                .endpoint(minioContainer.getHost(), minioContainer.getMappedPort(exposedPort), false)
                .build();
    }

    @Bean
    public Queue queueSendNotificationFromQuestionService(){
        return new Queue("q.sending-from-questions-service");
    }

    @Bean
    public Queue queueEventService(){
        return new Queue("q.from-event-service-to-auth-service");
    }

    @Bean
    public Queue queueFromVacancyServiceToAuthService(){
        return new Queue("q.from-vacancy-service-to-auth-service");
    }

    @Bean
    public Queue queueFromProjectServiceToAuthService(){
        return new Queue("q.from-project-service-to-auth-service");
    }

}
