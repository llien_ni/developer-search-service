INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'developer@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lena', 'LLLL', 'CONFIRMED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemon', 0);

INSERT INTO public.refresh_token (id, created_date, value, account_id) VALUES ('7ffb5725-f991-465c-951c-cabefbd5b452', '2023-04-27 20:14:08.563947 +00:00', 'eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvbG9naW4iLCJzdWIiOiI2M2I2YWNjNC0wNDhmLTQyMGEtODlkYS0yOWFiYmY3Njk5MmYiLCJyb2xlIjoiUk9MRV9ERVZFTE9QRVIiLCJleHAiOjE2ODI3MTI4NDgsImp0aSI6IjE2ODI2MjY0NDg1NDEifQ.DdZlAE4O2QLfZ3S9CT_V1rwRRsjNr9hHrQDmUOyso_fBSS72MpSj_DOh97thZLn3yAz4X8EX_N0kALLQwXv1V3Mms1F7B_vPGDjbD6re9JcRPpDqSAY-g6IuvbKbWAdmpWufFyE0Bs8RVksgj7nLMRirTboWSL8Ea25KvWbCe19jLHiEf-dqWPbP4G1Waa6xehx6LaEyOtwuYIrae4PTsi6__MLTyfIaf6qvnMyV-vOi3Vb2q3H3DQEuh20wU11hd9JHujrguyI1kDPaESM0evQORwUx5LeYOIpMdXXSZOJn5LppWRUeveboRwOFvJKMhWkUnUi60TXP5t3RdW8DLA', '65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b');


INSERT INTO public.specialization (id, developer_id, name) VALUES ('65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'frontend');
INSERT INTO public.specialization (id, developer_id, name) VALUES ('66dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'backend');

INSERT INTO public.link (id, developer_id, url, description) VALUES ('12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'http://my', 'My my link');

INSERT INTO public.education (id, type, name, developer_id, start_date, finish_date) VALUES ('13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'Univer', 'KFU', '65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-05-01', '2023-05-04');
INSERT INTO public.education (id, type, name, developer_id, start_date, finish_date) VALUES ('14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'Yandex', 'Course', '65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-06-01', '2023-06-04');

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('66dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'dev1@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lena', 'LLLL', 'CONFIRMED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('66dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemon1', 0);

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('67dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'dev2@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lena', 'LLLL', 'DELETED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('67dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemon2', 0);

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('68dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'dev3@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lena', 'LLLL', 'BLOCKED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('68dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemon3', 0);

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-29 20:41:09.349000 +00:00', '2023-04-29 20:41:11.624000 +00:00', 'hr@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'HrfirstName', 'HrLastName', 'CONFIRMED', 'ROLE_HR', 'NATIVE', null);
INSERT INTO public.hr (id, company_id, city) VALUES ('11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '25dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'Kazan');
