INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'developer@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lena', 'LLLL', 'CONFIRMED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('65dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemon', 0);

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('55dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'dev2@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lenaa', 'LLLL', 'CONFIRMED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('55dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemonn', 0);

INSERT INTO public.appeal (id, author_id, title, text, created_date, response, response_date, status) VALUES ('55dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '55dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'op', 'opa', '2023-04-30 18:33:36.394000 +00:00', null, null, 'OPEN');

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'dev3@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lenaa', 'LLLL', 'CONFIRMED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemonn', 0);

INSERT INTO public.appeal (id, author_id, title, text, created_date, response, response_date, status) VALUES ('11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'op', 'opa', '2023-04-30 18:33:36.394000 +00:00', null, null, 'OPEN');

INSERT INTO public.appeal (id, author_id, title, text, created_date, response, response_date, status) VALUES ('12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'op', 'opa', '2023-04-30 18:33:36.394000 +00:00', 'Ничем не можем помочь, извините', '2023-05-30 18:33:36.394000 +00:00', 'CLOSED');

INSERT INTO public.appeal (id, author_id, title, text, created_date, response, response_date, status) VALUES ('13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '95dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'op', 'opa', '2023-04-30 18:33:36.394000 +00:00', null, null, 'OPEN');
