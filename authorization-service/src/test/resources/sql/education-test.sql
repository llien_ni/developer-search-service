INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'dev@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lena', 'LLLL', 'CONFIRMED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemon', 0);


INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'dev2@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lena', 'LLLL', 'CONFIRMED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemon', 0);

INSERT INTO public.education (id, type, name, developer_id, start_date, finish_date) VALUES ('12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'Univer', 'KFU', '12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-05-01', '2023-05-04');

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-28 13:35:44.487697 +00:00', '2023-04-28 13:35:44.487697 +00:00', 'dev4@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'Lena', 'LLLL', 'CONFIRMED', 'ROLE_DEVELOPER', 'NATIVE', null);
INSERT INTO public.developer (id, date_of_birth, last_visit_date, work_experience, city, state, username, rating) VALUES ('14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-07', '2023-04-28 16:58:14.445000 +00:00', 8, 'Moscow', 'ACTIVE', 'pokemon4', 0);

INSERT INTO public.education (id, type, name, developer_id, start_date, finish_date) VALUES ('13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'Univer', 'KFU', '14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-05-01', '2023-05-04');
INSERT INTO public.education (id, type, name, developer_id, start_date, finish_date) VALUES ('14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'Yandex', 'Course', '14dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-06-01', '2023-06-04');
