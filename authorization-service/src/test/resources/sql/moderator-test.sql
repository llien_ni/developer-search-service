INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('11dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-29 20:46:29.922000 +00:00', '2023-04-29 20:46:31.643000 +00:00', 'moderator@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'moderatorFirstName', 'moderatorLastName', 'CONFIRMED', 'ROLE_MODERATOR', 'NATIVE', null);

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-29 20:41:09.349000 +00:00', '2023-04-29 20:41:11.624000 +00:00', 'hr@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'HrfirstName', 'HrLastName', 'MODERATED', 'ROLE_HR', 'NATIVE', null);
INSERT INTO public.hr (id, company_id, city) VALUES ('12dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '25dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'Kazan');

INSERT INTO public.account (id, created_date, updated_date, email, hash_password, first_name, last_name, status, role, registration_provider, redis_id) VALUES ('13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-04-29 20:41:09.349000 +00:00', '2023-04-29 20:41:11.624000 +00:00', 'hr2@gmail.com', '$2a$10$kY2yS5wrfyBkmSy0K0hcYeHtRBCmSekTBNPsZ2FeUHgR6x3szSoKe', 'HrfirstName', 'HrLastName', 'BLOCKED', 'ROLE_HR', 'NATIVE', null);
INSERT INTO public.hr (id, company_id, city) VALUES ('13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '25dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', 'Kazan');


INSERT INTO public.block_info (id, start_date, finish_date, reason, account_id, active) VALUES ('13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', '2023-05-01 15:42:53.151000 +00:00', null, 'Плохой человек', '13dad38e-3a15-4ab1-aee2-cf2f13ee9c5b', true);
